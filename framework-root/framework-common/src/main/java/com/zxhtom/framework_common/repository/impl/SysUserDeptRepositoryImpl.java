package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import com.zxhtom.framework_common.dto.SysUserDeptDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysUserDeptMapper;
import com.zxhtom.framework_common.model.SysUserDept;
import com.zxhtom.framework_common.repository.SysUserDeptRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysUserDeptRepositoryImpl implements SysUserDeptRepository{

	@Autowired
	private SysUserDeptMapper sysUserDeptMapper;
	@Override
	public PagedResult<SysUserDept> selectSysUserDeptsByPK(Long userDeptId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysUserDept.class,false,false);
		example.createCriteria().andEqualTo("userDeptId", userDeptId);
		return new PagedResult<>(sysUserDeptMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysUserDeptsByPK(Long userDeptId) {
		Example example = new Example(SysUserDept.class,false,false);
		example.createCriteria().andEqualTo("userDeptId", userDeptId);
		return sysUserDeptMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysUserDept(SysUserDept sysUserDept) {
		return sysUserDeptMapper.updateByPrimaryKeySelective(sysUserDept);
	}
	@Override
	public Integer insertSysUserDept(SysUserDept sysUserDept) {
		return sysUserDeptMapper.insert(sysUserDept);
	}
	
	@Override
	public Integer updateSysUserDeptBatch(List<SysUserDept> sysUserDepts) {
		return sysUserDeptMapper.updateSysUserDeptBatch(sysUserDepts);
	}
	@Override
	public Integer insertSysUserDeptBatch(List<SysUserDept> sysUserDepts) {
		return sysUserDeptMapper.insertSysUserDeptBatch(sysUserDepts);
	}

    @Override
    public List<SysUserDeptDto> selectSysUserDeptDtosByPK(Long userDeptId) {

		return sysUserDeptMapper.selectSysUserDeptDtosByPK(userDeptId);
    }
}
