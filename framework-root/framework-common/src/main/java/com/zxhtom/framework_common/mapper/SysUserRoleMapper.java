package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.dto.SysUserRoleDto;
import com.zxhtom.framework_common.model.SysUserRole;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
public interface SysUserRoleMapper extends Mapper<SysUserRole>{

	/**
	 * 批量更新
	 * @param sysUserRoles
	 * @return
	 */
	Integer updateSysUserRoleBatch(List<SysUserRole> sysUserRoles);

	/**
	 * 批量新增
	 * @param sysUserRoles
	 * @return
	 */
	Integer insertSysUserRoleBatch(List<SysUserRole> sysUserRoles);

	/**
	 * 根据主键获取所有用户角色关联信息列表
	 * @param userRoleId
	 * @return
	 */
    List<SysUserRoleDto> selectSysUserRolesDtoByPK(@Param("userRoleId") Long userRoleId);

	/**
	 * 批量删除用户角色
	 * @param userIds
	 * @return
	 */
    Integer deleteSysUserRolesByUserId(@Param("userIds") List<Long> userIds);
}
