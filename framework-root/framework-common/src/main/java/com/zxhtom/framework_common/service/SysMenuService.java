package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysMenu;

public interface SysMenuService {

	/**
	 * 根据主键查询菜单信息
	 * @param menuId 菜单主键
	 * @return
	 */
	PagedResult<SysMenu> selectSysMenusByPK(Long menuId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除菜单信息
	 * @param menuId 菜单主键
	 * @return
	 */
	Integer deleteSysMenuPK(Long menuId);

	/**
	 * 单条更新数据
	 * @param SysMenu
	 * @return
	 */
	Integer updateSysMenu(SysMenu sysMenu);

	/**
	 * 单条新增数据
	 * @param SysMenu
	 * @return
	 */
	Integer insertSysMenu(SysMenu sysMenu);
	
	/**
	 * 批量根据主键删除菜单信息
	 * @param menuId 菜单主键
	 * @return
	 */
	Integer deleteSysMenuPKBatch(List<Long> menuIds);
	
	/**
	 * 批量更新数据
	 * @param SysMenus
	 * @return
	 */
	Integer updateSysMenuBatch(List<SysMenu> sysMenus);

	/**
	 * 批量新增数据
	 * @param SysMenus
	 * @return
	 */
	Integer insertSysMenuBatch(List<SysMenu> sysMenus);
}
