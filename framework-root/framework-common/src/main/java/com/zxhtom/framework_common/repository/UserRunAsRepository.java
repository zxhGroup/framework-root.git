package com.zxhtom.framework_common.repository;

import com.zxhtom.framework_common.model.UserRunAs;

/**
 * @package com.zxhtom.framework_common.repository
 * @Class UserRunAsRepository
 * @Description 托管管理数据层
 * @Author zhangxinhua
 * @Date 19-6-3 下午5:11
 */
public interface UserRunAsRepository {
    /**
     * 查询数据
     * @param fromUserId
     * @param toUserId
     * @return
     */
    UserRunAs selectDataByFromAndTo(Long fromUserId, Long toUserId);
}
