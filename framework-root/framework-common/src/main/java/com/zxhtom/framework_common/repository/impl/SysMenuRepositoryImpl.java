package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysMenuMapper;
import com.zxhtom.framework_common.model.SysMenu;
import com.zxhtom.framework_common.repository.SysMenuRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysMenuRepositoryImpl implements SysMenuRepository{

	@Autowired
	private SysMenuMapper sysMenuMapper;
	@Override
	public PagedResult<SysMenu> selectSysMenusByPK(Long menuId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysMenu.class,false,false);
		example.createCriteria().andEqualTo("menuId", menuId);
		return new PagedResult<>(sysMenuMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysMenusByPK(Long menuId) {
		Example example = new Example(SysMenu.class,false,false);
		example.createCriteria().andEqualTo("menuId", menuId);
		return sysMenuMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysMenu(SysMenu sysMenu) {
		return sysMenuMapper.updateByPrimaryKeySelective(sysMenu);
	}
	@Override
	public Integer insertSysMenu(SysMenu sysMenu) {
		return sysMenuMapper.insert(sysMenu);
	}
	
	@Override
	public Integer updateSysMenuBatch(List<SysMenu> sysMenus) {
		return sysMenuMapper.updateSysMenuBatch(sysMenus);
	}
	@Override
	public Integer insertSysMenuBatch(List<SysMenu> sysMenus) {
		return sysMenuMapper.insertSysMenuBatch(sysMenus);
	}
}
