package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysRoleMenuMapper;
import com.zxhtom.framework_common.model.SysRoleMenu;
import com.zxhtom.framework_common.repository.SysRoleMenuRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysRoleMenuRepositoryImpl implements SysRoleMenuRepository{

	@Autowired
	private SysRoleMenuMapper sysRoleMenuMapper;
	@Override
	public PagedResult<SysRoleMenu> selectSysRoleMenusByPK(Long id,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysRoleMenu.class,false,false);
		example.createCriteria().andEqualTo("id", id);
		return new PagedResult<>(sysRoleMenuMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysRoleMenusByPK(Long id) {
		Example example = new Example(SysRoleMenu.class,false,false);
		example.createCriteria().andEqualTo("id", id);
		return sysRoleMenuMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysRoleMenu(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuMapper.updateByPrimaryKeySelective(sysRoleMenu);
	}
	@Override
	public Integer insertSysRoleMenu(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuMapper.insert(sysRoleMenu);
	}
	
	@Override
	public Integer updateSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus) {
		return sysRoleMenuMapper.updateSysRoleMenuBatch(sysRoleMenus);
	}
	@Override
	public Integer insertSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus) {
		return sysRoleMenuMapper.insertSysRoleMenuBatch(sysRoleMenus);
	}
}
