package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRoleModule;
import com.zxhtom.framework_common.repository.SysRoleModuleRepository;
import com.zxhtom.framework_common.service.SysRoleModuleService;

@Service
public class SysRoleModuleServiceImpl implements SysRoleModuleService {

	@Autowired
	private SysRoleModuleRepository sysRoleModuleRepository;

	@Override
	public PagedResult<SysRoleModule> selectSysRoleModulesByPK(Long id,Integer pageNumber, Integer pageSize) {
		PagedResult<SysRoleModule> result = new PagedResult<>();
		if(0==id){
			//查询所有
			id=null;
		}
		result = sysRoleModuleRepository.selectSysRoleModulesByPK(id, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysRoleModulePK(Long id) {
		if(0==id){
			//删除所有
			id=null;
		}
		return sysRoleModuleRepository.deleteSysRoleModulesByPK(id);
	}

	@Override
	public Integer updateSysRoleModule(SysRoleModule sysRoleModule) {
		return sysRoleModuleRepository.updateSysRoleModule(sysRoleModule);
	}

	@Override
	public Integer insertSysRoleModule(SysRoleModule sysRoleModule) {
		sysRoleModule.setId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysRoleModuleRepository.insertSysRoleModule(sysRoleModule);
	}
	
	@Override
	public Integer deleteSysRoleModulePKBatch(List<Long> ids){
		for(Long id : ids){
			sysRoleModuleRepository.deleteSysRoleModulesByPK(id);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysRoleModuleBatch(List<SysRoleModule> sysRoleModules) {
		return sysRoleModuleRepository.updateSysRoleModuleBatch(sysRoleModules);
	}

	@Override
	public Integer insertSysRoleModuleBatch(List<SysRoleModule> sysRoleModules) {
		for(SysRoleModule sysRoleModule : sysRoleModules){
		sysRoleModule.setId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysRoleModuleRepository.insertSysRoleModuleBatch(sysRoleModules);
	}

}
