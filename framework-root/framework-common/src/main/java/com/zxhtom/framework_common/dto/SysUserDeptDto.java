package com.zxhtom.framework_common.dto;

import com.zxhtom.framework_common.model.SysDept;
import com.zxhtom.framework_common.model.SysUser;

import java.util.List;

/**
 * @author John
 */
public class SysUserDeptDto extends SysDeptDto {
    /**
     * 部门下的用户
     */
    private List<SysUser> userList;

    public List<SysUser> getUserList() {
        return userList;
    }

    public void setUserList(List<SysUser> userList) {
        this.userList = userList;
    }
}
