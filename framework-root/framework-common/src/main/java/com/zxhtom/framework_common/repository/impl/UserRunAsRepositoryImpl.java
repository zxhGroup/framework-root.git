package com.zxhtom.framework_common.repository.impl;

import com.zxhtom.framework_common.mapper.UserRunAsMapper;
import com.zxhtom.framework_common.model.UserRunAs;
import com.zxhtom.framework_common.repository.UserRunAsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.zxhtom.utils.CollectionUtil;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @package com.zxhtom.framework_common.repository.impl
 * @Class UserRunAsRepositoryImpl
 * @Description 托管管理
 * @Author zhangxinhua
 * @Date 19-6-3 下午5:11
 */
@Repository
public class UserRunAsRepositoryImpl implements UserRunAsRepository {
    @Autowired
    private UserRunAsMapper userRunAsMapper;
    @Override
    public UserRunAs selectDataByFromAndTo(Long fromUserId, Long toUserId) {
        Example example = new Example(UserRunAs.class, false, false);
        example.createCriteria().andEqualTo("fromUserId", fromUserId).andEqualTo("toUserId", toUserId);
        List<UserRunAs> userRunAs = userRunAsMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(userRunAs)) {
            return userRunAs.get(0);
        }
        return null;
    }
}
