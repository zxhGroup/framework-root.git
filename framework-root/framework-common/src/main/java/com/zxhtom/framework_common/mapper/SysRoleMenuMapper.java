package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysRoleMenu;

import tk.mybatis.mapper.common.Mapper;
public interface SysRoleMenuMapper extends Mapper<SysRoleMenu>{

	/**
	 * 批量更新
	 * @param sysRoleMenus
	 * @return
	 */
	Integer updateSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus);

	/**
	 * 批量新增
	 * @param sysRoleMenus
	 * @return
	 */
	Integer insertSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus);
}
