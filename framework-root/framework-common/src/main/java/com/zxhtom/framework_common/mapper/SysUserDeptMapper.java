package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.dto.SysUserDeptDto;
import com.zxhtom.framework_common.model.SysUserDept;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
public interface SysUserDeptMapper extends Mapper<SysUserDept>{

	/**
	 * 批量更新
	 * @param sysUserDepts
	 * @return
	 */
	Integer updateSysUserDeptBatch(List<SysUserDept> sysUserDepts);

	/**
	 * 批量新增
	 * @param sysUserDepts
	 * @return
	 */
	Integer insertSysUserDeptBatch(List<SysUserDept> sysUserDepts);

	/**
	 * 查询部门人员树形结构
	 * @param userDeptId
	 * @return
	 */
    List<SysUserDeptDto> selectSysUserDeptDtosByPK(@Param("userDeptId") Long userDeptId);

}
