package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysRoleDto;
import com.zxhtom.framework_common.model.SysRole;

public interface SysRoleRepository {

	/**
	 * 根据主键查询用户信息
	 * 
	 * @param roleId
	 *            用户主键
	 * @return
	 */
	PagedResult<SysRole> selectSysRolesByPK(Long roleId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户信息
	 * @param roleId
	 * @return
	 */
	Integer deleteSysRolesByPK(Long roleId);
	
	/**
	 * 单条更新用户信息
	 * @param sysRoles
	 * @return
	 */
	Integer updateSysRole(SysRole sysRole);

	/**
	 * 单条新增信息
	 * @param sysRoles
	 * @return
	 */
	Integer insertSysRole(SysRole sysRole);
	
	/**
	 * 批量更新用户信息
	 * @param sysRoles
	 * @return
	 */
	Integer updateSysRoleBatch(List<SysRole> sysRoles);

	/**
	 * 批量新增信息
	 * @param sysRoles
	 * @return
	 */
	Integer insertSysRoleBatch(List<SysRole> sysRoles);

	/**
	 * 根据用户获取角色树
	 * @param userId
	 * @return
	 */
    List<SysRoleDto> selectTreeRoleByUserId(Long userId);
}
