package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysDept;

import tk.mybatis.mapper.common.Mapper;
public interface SysDeptMapper extends Mapper<SysDept>{

	/**
	 * 批量更新
	 * @param sysDepts
	 * @return
	 */
	Integer updateSysDeptBatch(List<SysDept> sysDepts);

	/**
	 * 批量新增
	 * @param sysDepts
	 * @return
	 */
	Integer insertSysDeptBatch(List<SysDept> sysDepts);
}
