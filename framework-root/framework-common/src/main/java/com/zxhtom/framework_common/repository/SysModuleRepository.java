package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysModule;

public interface SysModuleRepository {

	/**
	 * 根据主键查询模块信息
	 * 
	 * @param moduleId
	 *            模块主键
	 * @return
	 */
	PagedResult<SysModule> selectSysModulesByPK(Long moduleId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除模块信息
	 * @param moduleId
	 * @return
	 */
	Integer deleteSysModulesByPK(Long moduleId);
	
	/**
	 * 单条更新模块信息
	 * @param sysModules
	 * @return
	 */
	Integer updateSysModule(SysModule sysModule);

	/**
	 * 单条新增信息
	 * @param sysModules
	 * @return
	 */
	Integer insertSysModule(SysModule sysModule);
	
	/**
	 * 批量更新模块信息
	 * @param sysModules
	 * @return
	 */
	Integer updateSysModuleBatch(List<SysModule> sysModules);

	/**
	 * 批量新增信息
	 * @param sysModules
	 * @return
	 */
	Integer insertSysModuleBatch(List<SysModule> sysModules);

	/**
	 * 通过用户id获取模块
	 * @param userId
	 * @return
	 */
    List<SysModule> selectModuleByUserId(Long userId);
}
