package com.zxhtom.framework_common.repository.impl;

import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysUserPassWord;
import com.zxhtom.framework_common.mapper.SysUserMapper;
import com.zxhtom.framework_common.model.SysUser;
import com.zxhtom.framework_common.repository.SysUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Repository
public class SysUserRepositoryImpl implements SysUserRepository{

	@Autowired
	private SysUserMapper sysUserMapper;
	@Override
	public PagedResult<SysUser> selectSysUsersByPK(Long userId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysUser.class,false,false);
		example.createCriteria().andEqualTo("userId", userId);
		return new PagedResult<>(sysUserMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysUsersByPK(Long userId) {
		Example example = new Example(SysUser.class,false,false);
		example.createCriteria().andEqualTo("userId", userId);
		return sysUserMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysUser(SysUser sysUser) {
		return sysUserMapper.updateSysUser(sysUser);
	}
	@Override
	public Integer updateSysUserPassword(SysUserPassWord sysUserPassWord) {
		return sysUserMapper.updateUserByUserIdAndOldPassword(sysUserPassWord);
	}
	@Override
	public Integer insertSysUser(SysUser sysUser) {
		return sysUserMapper.insert(sysUser);
	}
	
	@Override
	public Integer updateSysUserBatch(List<SysUser> sysUsers) {
		return sysUserMapper.updateSysUserBatch(sysUsers);
	}
	@Override
	public Integer insertSysUserBatch(List<SysUser> sysUsers) {
		return sysUserMapper.insertSysUserBatch(sysUsers);
	}
}
