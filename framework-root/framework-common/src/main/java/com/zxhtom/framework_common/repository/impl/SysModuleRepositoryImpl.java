package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysModuleMapper;
import com.zxhtom.framework_common.model.SysModule;
import com.zxhtom.framework_common.repository.SysModuleRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysModuleRepositoryImpl implements SysModuleRepository{

	@Autowired
	private SysModuleMapper sysModuleMapper;
	@Override
	public PagedResult<SysModule> selectSysModulesByPK(Long moduleId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysModule.class,false,false);
		example.createCriteria().andEqualTo("moduleId", moduleId);
		return new PagedResult<>(sysModuleMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysModulesByPK(Long moduleId) {
		Example example = new Example(SysModule.class,false,false);
		example.createCriteria().andEqualTo("moduleId", moduleId);
		return sysModuleMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysModule(SysModule sysModule) {
		return sysModuleMapper.updateByPrimaryKeySelective(sysModule);
	}
	@Override
	public Integer insertSysModule(SysModule sysModule) {
		return sysModuleMapper.insert(sysModule);
	}
	
	@Override
	public Integer updateSysModuleBatch(List<SysModule> sysModules) {
		return sysModuleMapper.updateSysModuleBatch(sysModules);
	}
	@Override
	public Integer insertSysModuleBatch(List<SysModule> sysModules) {
		return sysModuleMapper.insertSysModuleBatch(sysModules);
	}

    @Override
    public List<SysModule> selectModuleByUserId(Long userId) {
		return sysModuleMapper.selectModuleByUserId(userId);
    }
}
