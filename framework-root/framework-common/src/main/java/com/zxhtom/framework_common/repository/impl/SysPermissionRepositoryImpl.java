package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysPermissionMapper;
import com.zxhtom.framework_common.model.SysPermission;
import com.zxhtom.framework_common.repository.SysPermissionRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysPermissionRepositoryImpl implements SysPermissionRepository{

	@Autowired
	private SysPermissionMapper sysPermissionMapper;
	@Override
	public PagedResult<SysPermission> selectSysPermissionsByPK(Long permissionId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysPermission.class,false,false);
		example.createCriteria().andEqualTo("permissionId", permissionId);
		return new PagedResult<>(sysPermissionMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysPermissionsByPK(Long permissionId) {
		Example example = new Example(SysPermission.class,false,false);
		example.createCriteria().andEqualTo("permissionId", permissionId);
		return sysPermissionMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysPermission(SysPermission sysPermission) {
		return sysPermissionMapper.updateByPrimaryKeySelective(sysPermission);
	}
	@Override
	public Integer insertSysPermission(SysPermission sysPermission) {
		return sysPermissionMapper.insert(sysPermission);
	}
	
	@Override
	public Integer updateSysPermissionBatch(List<SysPermission> sysPermissions) {
		return sysPermissionMapper.updateSysPermissionBatch(sysPermissions);
	}
	@Override
	public Integer insertSysPermissionBatch(List<SysPermission> sysPermissions) {
		return sysPermissionMapper.insertSysPermissionBatch(sysPermissions);
	}
}
