package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRoleModule;

public interface SysRoleModuleService {

	/**
	 * 根据主键查询角色模块关联信息
	 * @param id 角色模块关联主键
	 * @return
	 */
	PagedResult<SysRoleModule> selectSysRoleModulesByPK(Long id,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除角色模块关联信息
	 * @param id 角色模块关联主键
	 * @return
	 */
	Integer deleteSysRoleModulePK(Long id);

	/**
	 * 单条更新数据
	 * @param SysRoleModule
	 * @return
	 */
	Integer updateSysRoleModule(SysRoleModule sysRoleModule);

	/**
	 * 单条新增数据
	 * @param SysRoleModule
	 * @return
	 */
	Integer insertSysRoleModule(SysRoleModule sysRoleModule);
	
	/**
	 * 批量根据主键删除角色模块关联信息
	 * @param id 角色模块关联主键
	 * @return
	 */
	Integer deleteSysRoleModulePKBatch(List<Long> ids);
	
	/**
	 * 批量更新数据
	 * @param SysRoleModules
	 * @return
	 */
	Integer updateSysRoleModuleBatch(List<SysRoleModule> sysRoleModules);

	/**
	 * 批量新增数据
	 * @param SysRoleModules
	 * @return
	 */
	Integer insertSysRoleModuleBatch(List<SysRoleModule> sysRoleModules);
}
