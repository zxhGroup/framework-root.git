package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysModule;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
public interface SysModuleMapper extends Mapper<SysModule>{

	/**
	 * 批量更新
	 * @param sysModules
	 * @return
	 */
	Integer updateSysModuleBatch(List<SysModule> sysModules);

	/**
	 * 批量新增
	 * @param sysModules
	 * @return
	 */
	Integer insertSysModuleBatch(List<SysModule> sysModules);

	/**
	 * 通过用户id获取模块
	 * @param userId
	 * @return
	 */
    List<SysModule> selectModuleByUserId(@Param("userId") Long userId);
}
