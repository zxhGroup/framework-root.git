package com.zxhtom.framework_common.repository;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysUserPassWord;
import com.zxhtom.framework_common.model.SysUser;

import java.util.List;

public interface SysUserRepository {

	/**
	 * 根据主键查询用户信息
	 * 
	 * @param userId
	 *            用户主键
	 * @return
	 */
	PagedResult<SysUser> selectSysUsersByPK(Long userId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户信息
	 * @param userId
	 * @return
	 */
	Integer deleteSysUsersByPK(Long userId);

	/**
	 * 单条更新用户信息
	 * @param sysUser
	 * @return
	 */
	Integer updateSysUser(SysUser sysUser);

	/**
	 * 单条新增信息
	 * @param sysUser
	 * @return
	 */
	Integer insertSysUser(SysUser sysUser);
	
	/**
	 * 批量更新用户信息
	 * @param sysUsers
	 * @return
	 */
	Integer updateSysUserBatch(List<SysUser> sysUsers);

	/**
	 * 批量新增信息
	 * @param sysUsers
	 * @return
	 */
	Integer insertSysUserBatch(List<SysUser> sysUsers);

	/**
	 * 更新密码
	 * @param sysUserPassWord
	 * @return
	 */
    Integer updateSysUserPassword(SysUserPassWord sysUserPassWord);
}
