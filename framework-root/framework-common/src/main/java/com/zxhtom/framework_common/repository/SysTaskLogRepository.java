package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysTaskLog;

public interface SysTaskLogRepository {

	/**
	 * 根据主键查询任务日志信息
	 * 
	 * @param taskLogId
	 *            任务日志主键
	 * @return
	 */
	PagedResult<SysTaskLog> selectSysTaskLogsByPK(Long taskLogId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除任务日志信息
	 * @param taskLogId
	 * @return
	 */
	Integer deleteSysTaskLogsByPK(Long taskLogId);
	
	/**
	 * 单条更新任务日志信息
	 * @param sysTaskLogs
	 * @return
	 */
	Integer updateSysTaskLog(SysTaskLog sysTaskLog);

	/**
	 * 单条新增信息
	 * @param sysTaskLogs
	 * @return
	 */
	Integer insertSysTaskLog(SysTaskLog sysTaskLog);
	
	/**
	 * 批量更新任务日志信息
	 * @param sysTaskLogs
	 * @return
	 */
	Integer updateSysTaskLogBatch(List<SysTaskLog> sysTaskLogs);

	/**
	 * 批量新增信息
	 * @param sysTaskLogs
	 * @return
	 */
	Integer insertSysTaskLogBatch(List<SysTaskLog> sysTaskLogs);
}
