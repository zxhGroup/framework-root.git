package com.zxhtom.framework_common.service.impl;

import com.zxhtom.framework_common.model.UserRunAs;
import com.zxhtom.framework_common.repository.UserRunAsRepository;
import com.zxhtom.framework_common.service.UserRunAsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @package com.zxhtom.framework_common.service.impl
 * @Class UserRunAsServiceImpl
 * @Description 托管管理
 * @Author zhangxinhua
 * @Date 19-6-3 下午5:09
 */
@Service
public class UserRunAsServiceImpl implements UserRunAsService {

    @Autowired
    private UserRunAsRepository userRunAsRepository;

    @Override
    public void grantRunAs(Long fromUserId, Long toUserId) {

    }

    @Override
    public void revokeRunAs(Long fromUserId, Long toUserId) {

    }

    @Override
    public Boolean exists(Long fromUserId, Long toUserId) {
        UserRunAs userRunAs = userRunAsRepository.selectDataByFromAndTo(fromUserId,toUserId);
        if (null != userRunAs) {
            return true;
        }
        return false;
    }

    @Override
    public List<Long> findFromUserIds(Long toUserId) {
        return null;
    }

    @Override
    public List<Long> findToUserIds(Long fromUserId) {
        return null;
    }
}
