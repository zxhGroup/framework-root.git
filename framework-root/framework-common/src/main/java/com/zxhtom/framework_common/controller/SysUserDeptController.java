package com.zxhtom.framework_common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.zxhtom.framework_common.dto.SysUserDeptDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.framework_common.model.SysUserDept;
import com.zxhtom.framework_common.service.SysUserDeptService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/framework_common/sysUserDept")
@Api(tags = "framework_common sysUserDept", description = "用户部门关联信息表管理")
public class SysUserDeptController {

	private Logger logger = LogManager.getLogger(SysUserDeptController.class);
	@Autowired
	private SysUserDeptService sysUserDeptService;

	@RequestMapping(value = "/{userDeptId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有用户部门关联信息列表 传0表示查询所有")
	public PagedResult<SysUserDept> selectSysUserDeptsByPK(@ApiParam(value = "用户部门关联主键", required = true) @PathVariable @NotNull Long userDeptId,
	@ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
	@ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return sysUserDeptService.selectSysUserDeptsByPK(userDeptId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/dto/{userDeptId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有用户部门关联信息列表 传0表示查询所有")
	public PagedResult<SysUserDeptDto> selectSysUserDeptDtosByPK(@ApiParam(value = "用户部门关联主键", required = true) @PathVariable @NotNull Long userDeptId,
																 @ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
																 @ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return sysUserDeptService.selectSysUserDeptDtosByPK(userDeptId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysUserDeptByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long userDeptId) {
		return sysUserDeptService.deleteSysUserDeptPK(userDeptId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) SysUserDept sysUserDept) {
		return sysUserDeptService.updateSysUserDept(sysUserDept);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysUserDept sysUserDept) {
		return sysUserDeptService.insertSysUserDept(sysUserDept);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysUserDeptByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> userDeptIds) {
		return sysUserDeptService.deleteSysUserDeptPKBatch(userDeptIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysUserDept> sysUserDepts) {
		return sysUserDeptService.updateSysUserDeptBatch(sysUserDepts);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysUserDept> sysUserDepts) {
		return sysUserDeptService.insertSysUserDeptBatch(sysUserDepts);
	}
}
