package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysModule;

public interface SysModuleService {

	/**
	 * 根据主键查询模块信息
	 * @param moduleId 模块主键
	 * @return
	 */
	PagedResult<SysModule> selectSysModulesByPK(Long moduleId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除模块信息
	 * @param moduleId 模块主键
	 * @return
	 */
	Integer deleteSysModulePK(Long moduleId);

	/**
	 * 单条更新数据
	 * @param SysModule
	 * @return
	 */
	Integer updateSysModule(SysModule sysModule);

	/**
	 * 单条新增数据
	 * @param SysModule
	 * @return
	 */
	Integer insertSysModule(SysModule sysModule);
	
	/**
	 * 批量根据主键删除模块信息
	 * @param moduleId 模块主键
	 * @return
	 */
	Integer deleteSysModulePKBatch(List<Long> moduleIds);
	
	/**
	 * 批量更新数据
	 * @param SysModules
	 * @return
	 */
	Integer updateSysModuleBatch(List<SysModule> sysModules);

	/**
	 * 批量新增数据
	 * @param SysModules
	 * @return
	 */
	Integer insertSysModuleBatch(List<SysModule> sysModules);

	/**
	 * 根据用户获取模块
	 * @return
	 */
    List<SysModule> selectModuleByCurrentUser();
}
