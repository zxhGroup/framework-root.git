package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;

@ApiModel(description = "用户部门关联表")
@Table(name = "SYS_USER_DEPT")
public class SysUserDept {

	/**
	 * USER_DEPT_ID
	 */
	@ApiModelProperty(value = "USER_DEPT_ID")
	@Id
	private BigDecimal userDeptId;
	/**
	 * 用户ID
	 */
	@ApiModelProperty(value = "用户ID")
	private BigDecimal userId;
	/**
	 * 部门ID
	 */
	@ApiModelProperty(value = "部门ID")
	private BigDecimal deptId;
	
	/**
	 * 设置：USER_DEPT_ID
	 */
	public void setUserDeptId(BigDecimal userDeptId) {
		this.userDeptId = userDeptId;
	}
	/**
	 * 获取：USER_DEPT_ID
	 */
	public BigDecimal getUserDeptId() {
		return userDeptId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public BigDecimal getUserId() {
		return userId;
	}
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(BigDecimal deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public BigDecimal getDeptId() {
		return deptId;
	}

	@Override
	public String toString() {
		return "SysUserDept [userDeptId=" + userDeptId+",userId=" + userId + ",deptId=" + deptId + "]";
	}

}
