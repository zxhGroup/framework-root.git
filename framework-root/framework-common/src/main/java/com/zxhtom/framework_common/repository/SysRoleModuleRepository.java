package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRoleModule;

public interface SysRoleModuleRepository {

	/**
	 * 根据主键查询角色模块关联信息
	 * 
	 * @param id
	 *            角色模块关联主键
	 * @return
	 */
	PagedResult<SysRoleModule> selectSysRoleModulesByPK(Long id,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除角色模块关联信息
	 * @param id
	 * @return
	 */
	Integer deleteSysRoleModulesByPK(Long id);
	
	/**
	 * 单条更新角色模块关联信息
	 * @param sysRoleModules
	 * @return
	 */
	Integer updateSysRoleModule(SysRoleModule sysRoleModule);

	/**
	 * 单条新增信息
	 * @param sysRoleModules
	 * @return
	 */
	Integer insertSysRoleModule(SysRoleModule sysRoleModule);
	
	/**
	 * 批量更新角色模块关联信息
	 * @param sysRoleModules
	 * @return
	 */
	Integer updateSysRoleModuleBatch(List<SysRoleModule> sysRoleModules);

	/**
	 * 批量新增信息
	 * @param sysRoleModules
	 * @return
	 */
	Integer insertSysRoleModuleBatch(List<SysRoleModule> sysRoleModules);
}
