package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysMenu;

import tk.mybatis.mapper.common.Mapper;
public interface SysMenuMapper extends Mapper<SysMenu>{

	/**
	 * 批量更新
	 * @param sysMenus
	 * @return
	 */
	Integer updateSysMenuBatch(List<SysMenu> sysMenus);

	/**
	 * 批量新增
	 * @param sysMenus
	 * @return
	 */
	Integer insertSysMenuBatch(List<SysMenu> sysMenus);
}
