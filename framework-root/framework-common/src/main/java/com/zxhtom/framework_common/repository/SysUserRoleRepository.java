package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysUserRoleDto;
import com.zxhtom.framework_common.model.SysUserRole;

public interface SysUserRoleRepository {

	/**
	 * 根据主键查询用户角色关联信息
	 * 
	 * @param userRoleId
	 *            用户角色关联主键
	 * @return
	 */
	PagedResult<SysUserRole> selectSysUserRolesByPK(Long userRoleId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户角色关联信息
	 * @param userRoleId
	 * @return
	 */
	Integer deleteSysUserRolesByPK(Long userRoleId);
	
	/**
	 * 单条更新用户角色关联信息
	 * @param sysUserRoles
	 * @return
	 */
	Integer updateSysUserRole(SysUserRole sysUserRole);

	/**
	 * 单条新增信息
	 * @param sysUserRoles
	 * @return
	 */
	Integer insertSysUserRole(SysUserRole sysUserRole);
	
	/**
	 * 批量更新用户角色关联信息
	 * @param sysUserRoles
	 * @return
	 */
	Integer updateSysUserRoleBatch(List<SysUserRole> sysUserRoles);

	/**
	 * 批量新增信息
	 * @param sysUserRoles
	 * @return
	 */
	Integer insertSysUserRoleBatch(List<SysUserRole> sysUserRoles);

	/**
	 * 根据主键获取所有用户角色关联信息列表
	 * @param userRoleId
	 * @return
	 */
    List<SysUserRoleDto> selectSysUserRolesDtoByPK(Long userRoleId);

	/**
	 * 通过用户ID删除用户角色信息
	 * @param userId
	 */
    Integer deleteSysUserRolesByUserId(Long userId);

	/**
	 * 批量删除用户角色
	 * @param userIds
	 * @return
	 */
    Integer deleteSysUserRolesByUserIds(List<Long> userIds);
}
