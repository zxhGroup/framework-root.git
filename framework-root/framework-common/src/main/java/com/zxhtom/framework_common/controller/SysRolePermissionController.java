package com.zxhtom.framework_common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.framework_common.model.SysRolePermission;
import com.zxhtom.framework_common.service.SysRolePermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/framework_common/sysRolePermission")
@Api(tags = "framework_common sysRolePermission", description = "角色权限关联信息表管理")
public class SysRolePermissionController {

	private Logger logger = LogManager.getLogger(SysRolePermissionController.class);
	@Autowired
	private SysRolePermissionService sysRolePermissionService;

	@RequestMapping(value = "/{rolePermissionId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有角色权限关联信息列表 传0表示查询所有")
	public PagedResult<SysRolePermission> selectSysRolePermissionsByPK(@ApiParam(value = "角色权限关联主键", required = true) @PathVariable @NotNull Long rolePermissionId,
	@ApiParam(value = "页码", required = false) @RequestParam Integer pageNumber, 
	@ApiParam(value = "页量", required = false) @RequestParam Integer pageSize) {
		return sysRolePermissionService.selectSysRolePermissionsByPK(rolePermissionId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysRolePermissionByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long rolePermissionId) {
		return sysRolePermissionService.deleteSysRolePermissionPK(rolePermissionId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) SysRolePermission sysRolePermission) {
		return sysRolePermissionService.updateSysRolePermission(sysRolePermission);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysRolePermission sysRolePermission) {
		return sysRolePermissionService.insertSysRolePermission(sysRolePermission);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysRolePermissionByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> rolePermissionIds) {
		return sysRolePermissionService.deleteSysRolePermissionPKBatch(rolePermissionIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysRolePermission> sysRolePermissions) {
		return sysRolePermissionService.updateSysRolePermissionBatch(sysRolePermissions);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysRolePermission> sysRolePermissions) {
		return sysRolePermissionService.insertSysRolePermissionBatch(sysRolePermissions);
	}
}
