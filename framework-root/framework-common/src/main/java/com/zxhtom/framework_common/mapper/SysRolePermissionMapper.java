package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysRolePermission;

import tk.mybatis.mapper.common.Mapper;
public interface SysRolePermissionMapper extends Mapper<SysRolePermission>{

	/**
	 * 批量更新
	 * @param sysRolePermissions
	 * @return
	 */
	Integer updateSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions);

	/**
	 * 批量新增
	 * @param sysRolePermissions
	 * @return
	 */
	Integer insertSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions);
}
