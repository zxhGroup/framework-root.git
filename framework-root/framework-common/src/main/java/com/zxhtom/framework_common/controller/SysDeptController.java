package com.zxhtom.framework_common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.framework_common.model.SysDept;
import com.zxhtom.framework_common.service.SysDeptService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/framework_common/sysDept")
@Api(tags = "framework_common sysDept", description = "部门信息表管理")
public class SysDeptController {

	private Logger logger = LogManager.getLogger(SysDeptController.class);
	@Autowired
	private SysDeptService sysDeptService;

	@RequestMapping(value = "/{deptId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有部门信息列表 传0表示查询所有")
	public PagedResult<SysDept> selectSysDeptsByPK(@ApiParam(value = "部门主键", required = true) @PathVariable @NotNull Long deptId,
	@ApiParam(value = "页码", required = false) @RequestParam Integer pageNumber, 
	@ApiParam(value = "页量", required = false) @RequestParam Integer pageSize) {
		return sysDeptService.selectSysDeptsByPK(deptId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysDeptByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long deptId) {
		return sysDeptService.deleteSysDeptPK(deptId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) SysDept sysDept) {
		return sysDeptService.updateSysDept(sysDept);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysDept sysDept) {
		return sysDeptService.insertSysDept(sysDept);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysDeptByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> deptIds) {
		return sysDeptService.deleteSysDeptPKBatch(deptIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysDept> sysDepts) {
		return sysDeptService.updateSysDeptBatch(sysDepts);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysDept> sysDepts) {
		return sysDeptService.insertSysDeptBatch(sysDepts);
	}
}
