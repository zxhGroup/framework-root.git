package com.zxhtom.framework_common.dto;

import com.zxhtom.framework_common.model.SysDept;

import java.util.List;

/**
 * @author John
 */
public class SysDeptDto extends SysDept {
    private List<SysDept> children;

    public List<SysDept> getChildren() {
        return children;
    }

    public void setChildren(List<SysDept> children) {
        this.children = children;
    }
}
