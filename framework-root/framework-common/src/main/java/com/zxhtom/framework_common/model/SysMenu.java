package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "菜单表")
@Table(name = "SYS_MENU")
public class SysMenu {

    /**
     * 菜单ID
     */
    @Id
    private Long menuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 菜单编码
     */
    private String menuCode;
    /**
     * 菜单链接
     */
    private String menuUrl;
    /**
     * 菜单类型 ， 0真实菜单；1是父级虚拟菜单
     */
    private Integer menuType;
    /**
     * 模块ID
     */
    private Long moduleId;
    /**
     * 父级菜单ID，根菜单的父菜单是自己
     */
    private Long parentMenuId;

    /**
     * 设置：菜单ID
     */
    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取：菜单ID
     */
    public Long getMenuId() {
        return menuId;
    }

    /**
     * 设置：菜单名称
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     * 获取：菜单名称
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * 设置：菜单编码
     */
    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    /**
     * 获取：菜单编码
     */
    public String getMenuCode() {
        return menuCode;
    }

    /**
     * 设置：菜单链接
     */
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    /**
     * 获取：菜单链接
     */
    public String getMenuUrl() {
        return menuUrl;
    }

    /**
     * 设置：菜单类型 ， 0真实菜单；1是父级虚拟菜单
     */
    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    /**
     * 获取：菜单类型 ， 0真实菜单；1是父级虚拟菜单
     */
    public Integer getMenuType() {
        return menuType;
    }

    /**
     * 设置：模块ID
     */
    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    /**
     * 获取：模块ID
     */
    public Long getModuleId() {
        return moduleId;
    }

    /**
     * 设置：父级菜单ID，根菜单的父菜单是自己
     */
    public void setParentMenuId(Long parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    /**
     * 获取：父级菜单ID，根菜单的父菜单是自己
     */
    public Long getParentMenuId() {
        return parentMenuId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "SysMenu [menuId=" + menuId + ",icon=" + icon + ",menuName=" + menuName + ",menuCode=" + menuCode + ",menuUrl=" + menuUrl + ",menuType=" + menuType + ",moduleId=" + moduleId + ",parentMenuId=" + parentMenuId + "]";
    }

}
