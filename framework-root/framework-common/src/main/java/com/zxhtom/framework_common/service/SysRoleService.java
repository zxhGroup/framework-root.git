package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysRoleDto;
import com.zxhtom.framework_common.model.SysRole;

public interface SysRoleService {

	/**
	 * 根据主键查询用户信息
	 * @param roleId 用户主键
	 * @return
	 */
	PagedResult<SysRole> selectSysRolesByPK(Long roleId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户信息
	 * @param roleId 用户主键
	 * @return
	 */
	Integer deleteSysRolePK(Long roleId);

	/**
	 * 单条更新数据
	 * @param SysRole
	 * @return
	 */
	Integer updateSysRole(SysRole sysRole);

	/**
	 * 单条新增数据
	 * @param SysRole
	 * @return
	 */
	Integer insertSysRole(SysRole sysRole);
	
	/**
	 * 批量根据主键删除用户信息
	 * @param roleId 用户主键
	 * @return
	 */
	Integer deleteSysRolePKBatch(List<Long> roleIds);
	
	/**
	 * 批量更新数据
	 * @param SysRoles
	 * @return
	 */
	Integer updateSysRoleBatch(List<SysRole> sysRoles);

	/**
	 * 批量新增数据
	 * @param SysRoles
	 * @return
	 */
	Integer insertSysRoleBatch(List<SysRole> sysRoles);

	/**
	 * 获取当前用户的角色树
	 * @return
	 */
    List<SysRoleDto> selectCurrentTreeRole();
}
