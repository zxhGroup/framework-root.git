package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.CoreConstants;
import com.zxhtom.framework_common.dto.SysUserRoleDto;
import com.zxhtom.framework_common.mapper.SysOracleUserRoleMapper;
import com.zxhtom.framework_common.mapper.SysUserMapper;
import com.zxhtom.framework_common.model.SysUser;
import com.zxhtom.vconstant.RedisLocalKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysUserRoleMapper;
import com.zxhtom.framework_common.model.SysUserRole;
import com.zxhtom.framework_common.repository.SysUserRoleRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysUserRoleRepositoryImpl implements SysUserRoleRepository{

	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	@Autowired
	private SysOracleUserRoleMapper sysOracleUserRoleMapper;
	@Autowired
	private DruidDataSource dataSource;

	@Override
	public PagedResult<SysUserRole> selectSysUserRolesByPK(Long userRoleId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysUserRole.class,false,false);
		example.createCriteria().andEqualTo("userRoleId", userRoleId);
		return new PagedResult<>(sysUserRoleMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysUserRolesByPK(Long userRoleId) {
		Example example = new Example(SysUserRole.class,false,false);
		example.createCriteria().andEqualTo("userRoleId", userRoleId);
		return sysUserRoleMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysUserRole(SysUserRole sysUserRole) {
		return sysUserRoleMapper.updateByPrimaryKeySelective(sysUserRole);
	}
	@Override
	public Integer insertSysUserRole(SysUserRole sysUserRole) {
		return sysUserRoleMapper.insert(sysUserRole);
	}
	
	@Override
	public Integer updateSysUserRoleBatch(List<SysUserRole> sysUserRoles) {
		return sysUserRoleMapper.updateSysUserRoleBatch(sysUserRoles);
	}
	@Override
	public Integer insertSysUserRoleBatch(List<SysUserRole> sysUserRoles) {
		if (CoreConstants.DATA_SOURCE.equals(dataSource.getDriverClassName())) {
			return sysOracleUserRoleMapper.insertSysUserRoleBatch(sysUserRoles);
		}
		return sysUserRoleMapper.insertSysUserRoleBatch(sysUserRoles);
	}

	@Override
	public List<SysUserRoleDto> selectSysUserRolesDtoByPK(Long userRoleId) {
		if (CoreConstants.DATA_SOURCE.equals(dataSource.getDriverClassName())) {
			return sysOracleUserRoleMapper.selectSysUserRoleDtoByPK(userRoleId);
		}
		return sysUserRoleMapper.selectSysUserRolesDtoByPK(userRoleId);
	}

    @Override
    public Integer deleteSysUserRolesByUserId(Long userId) {
		//获取用户名
		redisTemplate.opsForHash().delete(userId, RedisLocalKey.USERROLELIST);
		Example example = new Example(SysUserRole.class, false, false);
		example.createCriteria().andEqualTo("userId", userId);
		return sysUserRoleMapper.deleteByExample(example);
    }

	@Override
	public Integer deleteSysUserRolesByUserIds(List<Long> userIds) {
		return sysUserRoleMapper.deleteSysUserRolesByUserId(userIds);
	}
}
