package com.zxhtom.framework_common.service.impl;

import com.zxhtom.PagedResult;
import com.zxhtom.core.service.CustomService;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.framework_common.dto.SysUserPassWord;
import com.zxhtom.framework_common.model.SysUser;
import com.zxhtom.framework_common.repository.SysUserRepository;
import com.zxhtom.framework_common.service.SysUserService;
import com.zxhtom.model.CustomUser;
import com.zxhtom.utils.IdUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService {

	@Autowired
	private SysUserRepository sysUserRepository;
	@Autowired
	private CustomService customService;

	@Override
	public PagedResult<SysUser> selectSysUsersByPK(Long userId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysUser> result = new PagedResult<>();
		if(0==userId){
			//查询所有
			userId=null;
		}
		result = sysUserRepository.selectSysUsersByPK(userId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysUserPK(Long userId) {
		if(0==userId){
			//删除所有
			userId=null;
		}
		return sysUserRepository.deleteSysUsersByPK(userId);
	}

	@Override
	public Integer updateSysUser(SysUser sysUser) {
		sysUserRepository.updateSysUser(sysUser);
		return 1;
	}

	@Override
	public Integer insertSysUser(SysUser sysUser) {
		sysUser.setUserId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysUserRepository.insertSysUser(sysUser);
	}
	
	@Override
	public Integer deleteSysUserPKBatch(List<Long> userIds){
		for(Long userId : userIds){
			sysUserRepository.deleteSysUsersByPK(userId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysUserBatch(List<SysUser> sysUsers) {
		return sysUserRepository.updateSysUserBatch(sysUsers);
	}

	@Override
	public Integer insertSysUserBatch(List<SysUser> sysUsers) {
		for(SysUser sysUser : sysUsers){
		sysUser.setUserId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysUserRepository.insertSysUserBatch(sysUsers);
	}

	@Override
	public Integer updateSysUserPassword(SysUserPassWord sysUserPassWord) {
		CustomUser currentUser = (CustomUser) SecurityUtils.getSubject().getPrincipal();
        sysUserPassWord.setUserId(currentUser.getUserId());
		//加密新密码，原密码会被拦截自动加密
		sysUserPassWord.setNewPassword(String.valueOf(new Md5Hash(sysUserPassWord.getNewPassword(),"zxh")));
		SysUser sysUser = new SysUser();
		if (sysUserRepository.updateSysUserPassword(sysUserPassWord) < 1) {
			throw new BusinessException("原密码错误");
		}
		//更新成功之后退出登录
		SecurityUtils.getSubject().logout();
		return 1;
	}

}
