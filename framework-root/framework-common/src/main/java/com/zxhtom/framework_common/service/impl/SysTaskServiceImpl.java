package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysTask;
import com.zxhtom.framework_common.repository.SysTaskRepository;
import com.zxhtom.framework_common.service.SysTaskService;

@Service
public class SysTaskServiceImpl implements SysTaskService {

	@Autowired
	private SysTaskRepository sysTaskRepository;

	@Override
	public PagedResult<SysTask> selectSysTasksByPK(Long taskId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysTask> result = new PagedResult<>();
		if(0==taskId){
			//查询所有
			taskId=null;
		}
		result = sysTaskRepository.selectSysTasksByPK(taskId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysTaskPK(Long taskId) {
		if(0==taskId){
			//删除所有
			taskId=null;
		}
		return sysTaskRepository.deleteSysTasksByPK(taskId);
	}

	@Override
	public Integer updateSysTask(SysTask sysTask) {
		return sysTaskRepository.updateSysTask(sysTask);
	}

	@Override
	public Integer insertSysTask(SysTask sysTask) {
		sysTask.setTaskId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysTaskRepository.insertSysTask(sysTask);
	}
	
	@Override
	public Integer deleteSysTaskPKBatch(List<Long> taskIds){
		for(Long taskId : taskIds){
			sysTaskRepository.deleteSysTasksByPK(taskId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysTaskBatch(List<SysTask> sysTasks) {
		return sysTaskRepository.updateSysTaskBatch(sysTasks);
	}

	@Override
	public Integer insertSysTaskBatch(List<SysTask> sysTasks) {
		for(SysTask sysTask : sysTasks){
		sysTask.setTaskId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysTaskRepository.insertSysTaskBatch(sysTasks);
	}

}
