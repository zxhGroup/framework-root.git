package com.zxhtom.framework_common.mapper;

import com.zxhtom.model.Menu;
import tk.mybatis.mapper.common.Mapper;

public interface MenuMapper extends Mapper<Menu> {

}
