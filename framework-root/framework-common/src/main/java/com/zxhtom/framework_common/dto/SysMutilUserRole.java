package com.zxhtom.framework_common.dto;


import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class SysMutilUserRole {

    @ApiModelProperty(value = "用户id集合")
    private List<Long> userIds;
    @ApiModelProperty(value = "角色id集合")
    private List<Long> roleIds;

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }
}
