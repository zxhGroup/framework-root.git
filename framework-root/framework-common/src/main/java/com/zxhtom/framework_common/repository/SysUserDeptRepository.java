package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysUserDeptDto;
import com.zxhtom.framework_common.model.SysUserDept;

public interface SysUserDeptRepository {

	/**
	 * 根据主键查询用户部门关联信息
	 * 
	 * @param userDeptId
	 *            用户部门关联主键
	 * @return
	 */
	PagedResult<SysUserDept> selectSysUserDeptsByPK(Long userDeptId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户部门关联信息
	 * @param userDeptId
	 * @return
	 */
	Integer deleteSysUserDeptsByPK(Long userDeptId);
	
	/**
	 * 单条更新用户部门关联信息
	 * @param sysUserDepts
	 * @return
	 */
	Integer updateSysUserDept(SysUserDept sysUserDept);

	/**
	 * 单条新增信息
	 * @param sysUserDepts
	 * @return
	 */
	Integer insertSysUserDept(SysUserDept sysUserDept);
	
	/**
	 * 批量更新用户部门关联信息
	 * @param sysUserDepts
	 * @return
	 */
	Integer updateSysUserDeptBatch(List<SysUserDept> sysUserDepts);

	/**
	 * 批量新增信息
	 * @param sysUserDepts
	 * @return
	 */
	Integer insertSysUserDeptBatch(List<SysUserDept> sysUserDepts);

	/**
	 * 通过用户部门ID查询部门下人员的树形结构
	 * @param userDeptId
	 * @return
	 */
    List<SysUserDeptDto> selectSysUserDeptDtosByPK(Long userDeptId);
}
