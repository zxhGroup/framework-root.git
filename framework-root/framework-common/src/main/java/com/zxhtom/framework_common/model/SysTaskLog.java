package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "任务日志表")
@Table(name = "SYS_TASK_LOG")
public class SysTaskLog {

	/**
	 * 流水号
	 */
	@Id
	private Long taskLogId;
	/**
	 * 写日志时间
	 */
	private Date logTime;
	/**
	 * 任务名称
	 */
	private String taskName;
	/**
	 * 任务类名
	 */
	private String jobClass;
	/**
	 * 调度表达式
	 */
	private String cronExpression;
	/**
	 * 任务参数
	 */
	private String param;
	/**
	 * 任务处理状况
	 */
	private String state;
	/**
	 * 任务处理IP
	 */
	private String ip;
	/**
	 * 任务处理开始时间
	 */
	private Date startTime;
	/**
	 * 任务处理结束时间
	 */
	private Date endTime;
	/**
	 * 处理结果
	 */
	private String result;
	
	/**
	 * 设置：流水号
	 */
	public void setTaskLogId(Long taskLogId) {
		this.taskLogId = taskLogId;
	}
	/**
	 * 获取：流水号
	 */
	public Long getTaskLogId() {
		return taskLogId;
	}
	/**
	 * 设置：写日志时间
	 */
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}
	/**
	 * 获取：写日志时间
	 */
	public Date getLogTime() {
		return logTime;
	}
	/**
	 * 设置：任务名称
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	/**
	 * 获取：任务名称
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * 设置：任务类名
	 */
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}
	/**
	 * 获取：任务类名
	 */
	public String getJobClass() {
		return jobClass;
	}
	/**
	 * 设置：调度表达式
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	/**
	 * 获取：调度表达式
	 */
	public String getCronExpression() {
		return cronExpression;
	}
	/**
	 * 设置：任务参数
	 */
	public void setParam(String param) {
		this.param = param;
	}
	/**
	 * 获取：任务参数
	 */
	public String getParam() {
		return param;
	}
	/**
	 * 设置：任务处理状况
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * 获取：任务处理状况
	 */
	public String getState() {
		return state;
	}
	/**
	 * 设置：任务处理IP
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * 获取：任务处理IP
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * 设置：任务处理开始时间
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	/**
	 * 获取：任务处理开始时间
	 */
	public Date getStartTime() {
		return startTime;
	}
	/**
	 * 设置：任务处理结束时间
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/**
	 * 获取：任务处理结束时间
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * 设置：处理结果
	 */
	public void setResult(String result) {
		this.result = result;
	}
	/**
	 * 获取：处理结果
	 */
	public String getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "SysTaskLog [taskLogId=" + taskLogId+",logTime=" + logTime + ",taskName=" + taskName + ",jobClass=" + jobClass + ",cronExpression=" + cronExpression + ",param=" + param + ",state=" + state + ",ip=" + ip + ",startTime=" + startTime + ",endTime=" + endTime + ",result=" + result + "]";
	}

}
