package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysTaskMapper;
import com.zxhtom.framework_common.model.SysTask;
import com.zxhtom.framework_common.repository.SysTaskRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysTaskRepositoryImpl implements SysTaskRepository{

	@Autowired
	private SysTaskMapper sysTaskMapper;
	@Override
	public PagedResult<SysTask> selectSysTasksByPK(Long taskId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysTask.class,false,false);
		example.createCriteria().andEqualTo("taskId", taskId);
		return new PagedResult<>(sysTaskMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysTasksByPK(Long taskId) {
		Example example = new Example(SysTask.class,false,false);
		example.createCriteria().andEqualTo("taskId", taskId);
		return sysTaskMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysTask(SysTask sysTask) {
		return sysTaskMapper.updateByPrimaryKeySelective(sysTask);
	}
	@Override
	public Integer insertSysTask(SysTask sysTask) {
		return sysTaskMapper.insert(sysTask);
	}
	
	@Override
	public Integer updateSysTaskBatch(List<SysTask> sysTasks) {
		return sysTaskMapper.updateSysTaskBatch(sysTasks);
	}
	@Override
	public Integer insertSysTaskBatch(List<SysTask> sysTasks) {
		return sysTaskMapper.insertSysTaskBatch(sysTasks);
	}
}
