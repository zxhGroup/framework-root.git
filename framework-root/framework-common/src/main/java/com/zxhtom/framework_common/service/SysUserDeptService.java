package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysUserDeptDto;
import com.zxhtom.framework_common.model.SysUserDept;

public interface SysUserDeptService {

	/**
	 * 根据主键查询用户部门关联信息
	 * @param userDeptId 用户部门关联主键
	 * @return
	 */
	PagedResult<SysUserDept> selectSysUserDeptsByPK(Long userDeptId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户部门关联信息
	 * @param userDeptId 用户部门关联主键
	 * @return
	 */
	Integer deleteSysUserDeptPK(Long userDeptId);

	/**
	 * 单条更新数据
	 * @param SysUserDept
	 * @return
	 */
	Integer updateSysUserDept(SysUserDept sysUserDept);

	/**
	 * 单条新增数据
	 * @param sysUserDept
	 * @return
	 */
	Integer insertSysUserDept(SysUserDept sysUserDept);
	
	/**
	 * 批量根据主键删除用户部门关联信息
	 * @param userDeptIds 用户部门关联主键
	 * @return
	 */
	Integer deleteSysUserDeptPKBatch(List<Long> userDeptIds);
	
	/**
	 * 批量更新数据
	 * @param sysUserDepts
	 * @return
	 */
	Integer updateSysUserDeptBatch(List<SysUserDept> sysUserDepts);

	/**
	 * 批量新增数据
	 * @param SysUserDepts
	 * @return
	 */
	Integer insertSysUserDeptBatch(List<SysUserDept> sysUserDepts);

	/**
	 * 根据主键获取所有用户部门详细信息列表 传0表示查询所有
	 * @param userDeptId
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
    PagedResult<SysUserDeptDto> selectSysUserDeptDtosByPK(Long userDeptId, Integer pageNumber, Integer pageSize);
}
