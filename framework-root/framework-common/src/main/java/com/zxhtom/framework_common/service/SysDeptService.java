package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysDept;

public interface SysDeptService {

	/**
	 * 根据主键查询部门信息
	 * @param deptId 部门主键
	 * @return
	 */
	PagedResult<SysDept> selectSysDeptsByPK(Long deptId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除部门信息
	 * @param deptId 部门主键
	 * @return
	 */
	Integer deleteSysDeptPK(Long deptId);

	/**
	 * 单条更新数据
	 * @param SysDept
	 * @return
	 */
	Integer updateSysDept(SysDept sysDept);

	/**
	 * 单条新增数据
	 * @param SysDept
	 * @return
	 */
	Integer insertSysDept(SysDept sysDept);
	
	/**
	 * 批量根据主键删除部门信息
	 * @param deptId 部门主键
	 * @return
	 */
	Integer deleteSysDeptPKBatch(List<Long> deptIds);
	
	/**
	 * 批量更新数据
	 * @param SysDepts
	 * @return
	 */
	Integer updateSysDeptBatch(List<SysDept> sysDepts);

	/**
	 * 批量新增数据
	 * @param SysDepts
	 * @return
	 */
	Integer insertSysDeptBatch(List<SysDept> sysDepts);
}
