package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysTaskLog;
import com.zxhtom.framework_common.repository.SysTaskLogRepository;
import com.zxhtom.framework_common.service.SysTaskLogService;

@Service
public class SysTaskLogServiceImpl implements SysTaskLogService {

	@Autowired
	private SysTaskLogRepository sysTaskLogRepository;

	@Override
	public PagedResult<SysTaskLog> selectSysTaskLogsByPK(Long taskLogId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysTaskLog> result = new PagedResult<>();
		if(0==taskLogId){
			//查询所有
			taskLogId=null;
		}
		result = sysTaskLogRepository.selectSysTaskLogsByPK(taskLogId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysTaskLogPK(Long taskLogId) {
		if(0==taskLogId){
			//删除所有
			taskLogId=null;
		}
		return sysTaskLogRepository.deleteSysTaskLogsByPK(taskLogId);
	}

	@Override
	public Integer updateSysTaskLog(SysTaskLog sysTaskLog) {
		return sysTaskLogRepository.updateSysTaskLog(sysTaskLog);
	}

	@Override
	public Integer insertSysTaskLog(SysTaskLog sysTaskLog) {
		sysTaskLog.setTaskLogId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysTaskLogRepository.insertSysTaskLog(sysTaskLog);
	}
	
	@Override
	public Integer deleteSysTaskLogPKBatch(List<Long> taskLogIds){
		for(Long taskLogId : taskLogIds){
			sysTaskLogRepository.deleteSysTaskLogsByPK(taskLogId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysTaskLogBatch(List<SysTaskLog> sysTaskLogs) {
		return sysTaskLogRepository.updateSysTaskLogBatch(sysTaskLogs);
	}

	@Override
	public Integer insertSysTaskLogBatch(List<SysTaskLog> sysTaskLogs) {
		for(SysTaskLog sysTaskLog : sysTaskLogs){
		sysTaskLog.setTaskLogId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysTaskLogRepository.insertSysTaskLogBatch(sysTaskLogs);
	}

}
