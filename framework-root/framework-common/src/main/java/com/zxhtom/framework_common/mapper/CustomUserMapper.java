package com.zxhtom.framework_common.mapper;

import com.zxhtom.model.CustomUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface CustomUserMapper extends Mapper<CustomUser>{

    /**
     * 通过用户名查询用户数据
     * @param userName
     * @return
     */
    List<CustomUser> selectUserInfoByUserName(@Param("userName") String userName);
}
