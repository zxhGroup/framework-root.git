package com.zxhtom.framework_common.aspect;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.zxhtom.CoreConstants;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysUser;

@Component
@Aspect
public class EncryptAspect {
	private Logger logger = LogManager.getLogger(EncryptAspect.class);

	@Pointcut("execution(* com.zxhtom..service.impl.SysUser*Impl.*(..))")
	public void insert() {
	}

	@Before("insert()")
	public void before(JoinPoint joinPoint) {
		doEncrypt(joinPoint, null);
	}

	@AfterReturning(value = "insert()", returning = "object")
	public void after(JoinPoint joinPoint, Object object) {
		extration(object, "******");
	}

	private void extration(Object object , String password){
		if(null!=object){
			if(object instanceof SysUser){
				SysUser user = (SysUser) object;
				if(CoreConstants.NULL.equals(password)){
					user.setPassword(new Md5Hash(user.getPassword(), CoreConstants.SLAT).toString());
				}else{
					user.setPassword(password);
				}
			}else if(object instanceof PagedResult){
				PagedResult<Object> list = (PagedResult<Object>) object;
				for (Object obj : list.getDatas()) {
					if(obj instanceof SysUser){
						SysUser user = (SysUser) obj;
						if(CoreConstants.NULL.equals(password)){
							user.setPassword(new Md5Hash(user.getPassword(), CoreConstants.SLAT).toString());
						}else{
							user.setPassword(password);
						}
					}
				}
			}else if(object instanceof Map){
				Map<Object, Object> map = new HashMap<Object, Object>();
				Collection<Object> values = map.values();
				for (Object obj : values) {
					extration(obj,password);
				}
			}
		}
	} 
	private void doEncrypt(JoinPoint joinPoint, Object obj) {
		Object[] args = joinPoint.getArgs();
		String methodName = joinPoint.getSignature().getName();
		if (Pattern.matches("(insert|update).*", methodName)) {
			// 新增或者更新需要 密码加盐
			for (Object object : args) {
				extration(object, CoreConstants.NULL);
			}
		} else if (Pattern.matches("(?!delete).*", methodName)) {
			// 不以delete 开头的方法 认为是查询
		}
	}

}
