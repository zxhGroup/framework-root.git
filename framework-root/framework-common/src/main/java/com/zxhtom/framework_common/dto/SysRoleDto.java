package com.zxhtom.framework_common.dto;

import com.zxhtom.framework_common.model.SysRole;

import java.util.List;
import java.util.Set;

/**
 * @author John
 */
public class SysRoleDto extends SysRole {

    private List<SysRoleDto> children;

    public List<SysRoleDto> getChildren() {
        return children;
    }

    public void setChildren(List<SysRoleDto> children) {
        this.children = children;
    }
}
