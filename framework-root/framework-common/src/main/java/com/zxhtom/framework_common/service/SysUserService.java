package com.zxhtom.framework_common.service;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysUserPassWord;
import com.zxhtom.framework_common.model.SysUser;

import java.util.List;

public interface SysUserService {

	/**
	 * 根据主键查询用户信息
	 * @param userId 用户主键
	 * @return
	 */
	PagedResult<SysUser> selectSysUsersByPK(Long userId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户信息
	 * @param userId 用户主键
	 * @return
	 */
	Integer deleteSysUserPK(Long userId);

	/**
	 * 单条更新数据
	 * @param sysUser
	 * @return
	 */
	Integer updateSysUser(SysUser sysUser);

	/**
	 * 单条新增数据
	 * @param sysUser
	 * @return
	 */
	Integer insertSysUser(SysUser sysUser);
	
	/**
	 * 批量根据主键删除用户信息
	 * @param userIds 用户主键
	 * @return
	 */
	Integer deleteSysUserPKBatch(List<Long> userIds);
	
	/**
	 * 批量更新数据
	 * @param SysUsers
	 * @return
	 */
	Integer updateSysUserBatch(List<SysUser> sysUsers);

	/**
	 * 批量新增数据
	 * @param SysUsers
	 * @return
	 */
	Integer insertSysUserBatch(List<SysUser> sysUsers);

	/**
	 * 修改密码
	 * @param sysUserPassWord
	 * @return
	 */
    Integer updateSysUserPassword(SysUserPassWord sysUserPassWord);
}
