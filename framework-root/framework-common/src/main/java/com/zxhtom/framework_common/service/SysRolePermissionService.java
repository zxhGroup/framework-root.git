package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRolePermission;

public interface SysRolePermissionService {

	/**
	 * 根据主键查询角色权限关联信息
	 * @param rolePermissionId 角色权限关联主键
	 * @return
	 */
	PagedResult<SysRolePermission> selectSysRolePermissionsByPK(Long rolePermissionId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除角色权限关联信息
	 * @param rolePermissionId 角色权限关联主键
	 * @return
	 */
	Integer deleteSysRolePermissionPK(Long rolePermissionId);

	/**
	 * 单条更新数据
	 * @param SysRolePermission
	 * @return
	 */
	Integer updateSysRolePermission(SysRolePermission sysRolePermission);

	/**
	 * 单条新增数据
	 * @param SysRolePermission
	 * @return
	 */
	Integer insertSysRolePermission(SysRolePermission sysRolePermission);
	
	/**
	 * 批量根据主键删除角色权限关联信息
	 * @param rolePermissionId 角色权限关联主键
	 * @return
	 */
	Integer deleteSysRolePermissionPKBatch(List<Long> rolePermissionIds);
	
	/**
	 * 批量更新数据
	 * @param SysRolePermissions
	 * @return
	 */
	Integer updateSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions);

	/**
	 * 批量新增数据
	 * @param SysRolePermissions
	 * @return
	 */
	Integer insertSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions);
}
