package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRoleMenu;
import com.zxhtom.framework_common.repository.SysRoleMenuRepository;
import com.zxhtom.framework_common.service.SysRoleMenuService;

@Service
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

	@Autowired
	private SysRoleMenuRepository sysRoleMenuRepository;

	@Override
	public PagedResult<SysRoleMenu> selectSysRoleMenusByPK(Long id,Integer pageNumber, Integer pageSize) {
		PagedResult<SysRoleMenu> result = new PagedResult<>();
		if(0==id){
			//查询所有
			id=null;
		}
		result = sysRoleMenuRepository.selectSysRoleMenusByPK(id, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysRoleMenuPK(Long id) {
		if(0==id){
			//删除所有
			id=null;
		}
		return sysRoleMenuRepository.deleteSysRoleMenusByPK(id);
	}

	@Override
	public Integer updateSysRoleMenu(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuRepository.updateSysRoleMenu(sysRoleMenu);
	}

	@Override
	public Integer insertSysRoleMenu(SysRoleMenu sysRoleMenu) {
		sysRoleMenu.setId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysRoleMenuRepository.insertSysRoleMenu(sysRoleMenu);
	}
	
	@Override
	public Integer deleteSysRoleMenuPKBatch(List<Long> ids){
		for(Long id : ids){
			sysRoleMenuRepository.deleteSysRoleMenusByPK(id);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus) {
		return sysRoleMenuRepository.updateSysRoleMenuBatch(sysRoleMenus);
	}

	@Override
	public Integer insertSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus) {
		for(SysRoleMenu sysRoleMenu : sysRoleMenus){
		sysRoleMenu.setId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysRoleMenuRepository.insertSysRoleMenuBatch(sysRoleMenus);
	}

}
