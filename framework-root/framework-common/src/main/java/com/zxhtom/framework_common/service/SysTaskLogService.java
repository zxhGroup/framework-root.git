package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysTaskLog;

public interface SysTaskLogService {

	/**
	 * 根据主键查询任务日志信息
	 * @param taskLogId 任务日志主键
	 * @return
	 */
	PagedResult<SysTaskLog> selectSysTaskLogsByPK(Long taskLogId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除任务日志信息
	 * @param taskLogId 任务日志主键
	 * @return
	 */
	Integer deleteSysTaskLogPK(Long taskLogId);

	/**
	 * 单条更新数据
	 * @param SysTaskLog
	 * @return
	 */
	Integer updateSysTaskLog(SysTaskLog sysTaskLog);

	/**
	 * 单条新增数据
	 * @param SysTaskLog
	 * @return
	 */
	Integer insertSysTaskLog(SysTaskLog sysTaskLog);
	
	/**
	 * 批量根据主键删除任务日志信息
	 * @param taskLogId 任务日志主键
	 * @return
	 */
	Integer deleteSysTaskLogPKBatch(List<Long> taskLogIds);
	
	/**
	 * 批量更新数据
	 * @param SysTaskLogs
	 * @return
	 */
	Integer updateSysTaskLogBatch(List<SysTaskLog> sysTaskLogs);

	/**
	 * 批量新增数据
	 * @param SysTaskLogs
	 * @return
	 */
	Integer insertSysTaskLogBatch(List<SysTaskLog> sysTaskLogs);
}
