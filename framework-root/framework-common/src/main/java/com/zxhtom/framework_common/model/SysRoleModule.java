package com.zxhtom.framework_common.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Id;
import javax.persistence.Table;

@ApiModel(description = "角色模块关联表")
@Table(name = "SYS_ROLE_MODULE")
public class SysRoleModule {

	/**
	 * 流水号
	 */
	@Id
	private Long id;
	/**
	 * 角色ID
	 */
	private Long roleId;
	/**
	 * 模块ID
	 */
	private Long moduleId;

	/**
	 * 产品id
	 */
	private Long oauthClientId;

	public Long getOauthClientId() {
		return oauthClientId;
	}

	public void setOauthClientId(Long oauthClientId) {
		this.oauthClientId = oauthClientId;
	}

	/**
	 * 设置：流水号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：流水号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：角色ID
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID
	 */
	public Long getRoleId() {
		return roleId;
	}
	/**
	 * 设置：模块ID
	 */
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * 获取：模块ID
	 */
	public Long getModuleId() {
		return moduleId;
	}

	@Override
	public String toString() {
		return "SysRoleModule [id=" + id+",roleId=" + roleId + ",moduleId=" + moduleId + "]";
	}

}
