package com.zxhtom.framework_common.service.impl;

import java.util.*;

import com.zxhtom.core.service.CustomService;
import com.zxhtom.framework_common.dto.SysRoleDto;
import com.zxhtom.model.CustomUser;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRole;
import com.zxhtom.framework_common.repository.SysRoleRepository;
import com.zxhtom.framework_common.service.SysRoleService;

@Service
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleRepository sysRoleRepository;

	@Autowired
	private CustomService customService;

	@Override
	public PagedResult<SysRole> selectSysRolesByPK(Long roleId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysRole> result = new PagedResult<>();
		if(0==roleId){
			//查询所有
			roleId=null;
		}
		result = sysRoleRepository.selectSysRolesByPK(roleId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysRolePK(Long roleId) {
		if(0==roleId){
			//删除所有
			roleId=null;
		}
		return sysRoleRepository.deleteSysRolesByPK(roleId);
	}

	@Override
	public Integer updateSysRole(SysRole sysRole) {
		return sysRoleRepository.updateSysRole(sysRole);
	}

	@Override
	public Integer insertSysRole(SysRole sysRole) {
		sysRole.setRoleId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysRoleRepository.insertSysRole(sysRole);
	}
	
	@Override
	public Integer deleteSysRolePKBatch(List<Long> roleIds){
		for(Long roleId : roleIds){
			sysRoleRepository.deleteSysRolesByPK(roleId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysRoleBatch(List<SysRole> sysRoles) {
		return sysRoleRepository.updateSysRoleBatch(sysRoles);
	}

	@Override
	public Integer insertSysRoleBatch(List<SysRole> sysRoles) {
		for(SysRole sysRole : sysRoles){
		sysRole.setRoleId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysRoleRepository.insertSysRoleBatch(sysRoles);
	}

    @Override
    public List<SysRoleDto> selectCurrentTreeRole() {
		CustomUser customUser = (CustomUser) SecurityUtils.getSubject().getPrincipal();

		return sysRoleRepository.selectTreeRoleByUserId(customUser.getUserId());
    }

}
