package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysDept;
import com.zxhtom.framework_common.repository.SysDeptRepository;
import com.zxhtom.framework_common.service.SysDeptService;

@Service
public class SysDeptServiceImpl implements SysDeptService {

	@Autowired
	private SysDeptRepository sysDeptRepository;

	@Override
	public PagedResult<SysDept> selectSysDeptsByPK(Long deptId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysDept> result = new PagedResult<>();
		if(0==deptId){
			//查询所有
			deptId=null;
		}
		result = sysDeptRepository.selectSysDeptsByPK(deptId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysDeptPK(Long deptId) {
		if(0==deptId){
			//删除所有
			deptId=null;
		}
		return sysDeptRepository.deleteSysDeptsByPK(deptId);
	}

	@Override
	public Integer updateSysDept(SysDept sysDept) {
		return sysDeptRepository.updateSysDept(sysDept);
	}

	@Override
	public Integer insertSysDept(SysDept sysDept) {
		sysDept.setDeptId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysDeptRepository.insertSysDept(sysDept);
	}
	
	@Override
	public Integer deleteSysDeptPKBatch(List<Long> deptIds){
		for(Long deptId : deptIds){
			sysDeptRepository.deleteSysDeptsByPK(deptId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysDeptBatch(List<SysDept> sysDepts) {
		return sysDeptRepository.updateSysDeptBatch(sysDepts);
	}

	@Override
	public Integer insertSysDeptBatch(List<SysDept> sysDepts) {
		for(SysDept sysDept : sysDepts){
		sysDept.setDeptId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysDeptRepository.insertSysDeptBatch(sysDepts);
	}

}
