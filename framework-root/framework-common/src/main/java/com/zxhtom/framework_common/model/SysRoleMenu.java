package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "角色菜单表")
@Table(name = "SYS_ROLE_MENU")
public class SysRoleMenu {

	/**
	 * 流水号
	 */
	@Id
	private Long id;
	/**
	 * 角色ID
	 */
	private Long roleId;
	/**
	 * 菜单ID
	 */
	private Long menuId;
	
	/**
	 * 设置：流水号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：流水号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：角色ID
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID
	 */
	public Long getRoleId() {
		return roleId;
	}
	/**
	 * 设置：菜单ID
	 */
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
	/**
	 * 获取：菜单ID
	 */
	public Long getMenuId() {
		return menuId;
	}

	@Override
	public String toString() {
		return "SysRoleMenu [id=" + id+",roleId=" + roleId + ",menuId=" + menuId + "]";
	}

}
