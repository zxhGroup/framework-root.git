package com.zxhtom.framework_common.controller;

import com.zxhtom.PagedResult;
import com.zxhtom.annotation.OnlyLogin;
import com.zxhtom.framework_common.dto.SysUserPassWord;
import com.zxhtom.framework_common.model.SysUser;
import com.zxhtom.framework_common.service.SysUserService;
import com.zxhtom.vconstant.RoleList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/framework_common/sysUser")
@Api(tags = "framework_common sysUser", description = "用户信息表管理")
public class SysUserController {

	private Logger logger = LogManager.getLogger(SysUserController.class);
	@Autowired
	private SysUserService sysUserService;

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有用户信息列表 传0表示查询所有")
	public PagedResult<SysUser> selectSysUsersByPK(@ApiParam(value = "用户主键", required = true) @PathVariable @NotNull Long userId,
	@ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
	@ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return sysUserService.selectSysUsersByPK(userId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysUserByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long userId) {
		return sysUserService.deleteSysUserPK(userId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@OnlyLogin
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) @Validated SysUser sysUser) {
		return sysUserService.updateSysUser(sysUser);
	}

	@RequestMapping(value = "/updatePassword", method = RequestMethod.PUT)
	@ApiOperation(value = "更新密码")
	@OnlyLogin
	public Integer updatePassword(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) @Validated  SysUserPassWord sysUserPassWord) {
		return sysUserService.updateSysUserPassword(sysUserPassWord);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysUser sysUser) {
		return sysUserService.insertSysUser(sysUser);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysUserByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> userIds) {
		return sysUserService.deleteSysUserPKBatch(userIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysUser> sysUsers) {
		return sysUserService.updateSysUserBatch(sysUsers);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysUser> sysUsers) {
		return sysUserService.insertSysUserBatch(sysUsers);
	}
}
