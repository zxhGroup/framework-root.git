package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysTask;

public interface SysTaskRepository {

	/**
	 * 根据主键查询任务信息
	 * 
	 * @param taskId
	 *            任务主键
	 * @return
	 */
	PagedResult<SysTask> selectSysTasksByPK(Long taskId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除任务信息
	 * @param taskId
	 * @return
	 */
	Integer deleteSysTasksByPK(Long taskId);
	
	/**
	 * 单条更新任务信息
	 * @param sysTasks
	 * @return
	 */
	Integer updateSysTask(SysTask sysTask);

	/**
	 * 单条新增信息
	 * @param sysTasks
	 * @return
	 */
	Integer insertSysTask(SysTask sysTask);
	
	/**
	 * 批量更新任务信息
	 * @param sysTasks
	 * @return
	 */
	Integer updateSysTaskBatch(List<SysTask> sysTasks);

	/**
	 * 批量新增信息
	 * @param sysTasks
	 * @return
	 */
	Integer insertSysTaskBatch(List<SysTask> sysTasks);
}
