package com.zxhtom.framework_common.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @package com.zxhtom.model
 * @Class UserRunAs
 * @Description 用户托管
 * @Author zhangxinhua
 * @Date 19-6-3 下午4:28
 */
@ApiModel(description = "用户托管")
@Table(name = "SYS_RUN_AS")
public class UserRunAs {
    /**
     * id
     */
    @Id
    private Long runAsId;
    /**
     * 授予身份帐号
     */
    private Long fromUserId;

    /**
     * 被授予身份帐号
     */
    private Long toUserId;

    public Long getRunAsId() {
        return runAsId;
    }

    public void setRunAsId(Long runAsId) {
        this.runAsId = runAsId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }
}
