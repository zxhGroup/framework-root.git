package com.zxhtom.framework_common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.framework_common.model.SysTask;
import com.zxhtom.framework_common.service.SysTaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/framework_common/sysTask")
@Api(tags = "framework_common sysTask", description = "任务信息表管理")
public class SysTaskController {

	private Logger logger = LogManager.getLogger(SysTaskController.class);
	@Autowired
	private SysTaskService sysTaskService;

	@RequestMapping(value = "/{taskId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有任务信息列表 传0表示查询所有")
	public PagedResult<SysTask> selectSysTasksByPK(@ApiParam(value = "任务主键", required = true) @PathVariable @NotNull Long taskId,
	@ApiParam(value = "页码", required = false) @RequestParam Integer pageNumber, 
	@ApiParam(value = "页量", required = false) @RequestParam Integer pageSize) {
		return sysTaskService.selectSysTasksByPK(taskId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysTaskByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long taskId) {
		return sysTaskService.deleteSysTaskPK(taskId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) SysTask sysTask) {
		return sysTaskService.updateSysTask(sysTask);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysTask sysTask) {
		return sysTaskService.insertSysTask(sysTask);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysTaskByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> taskIds) {
		return sysTaskService.deleteSysTaskPKBatch(taskIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysTask> sysTasks) {
		return sysTaskService.updateSysTaskBatch(sysTasks);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysTask> sysTasks) {
		return sysTaskService.insertSysTaskBatch(sysTasks);
	}
}
