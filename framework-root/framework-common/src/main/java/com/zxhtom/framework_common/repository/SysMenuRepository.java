package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysMenu;

public interface SysMenuRepository {

	/**
	 * 根据主键查询菜单信息
	 * 
	 * @param menuId
	 *            菜单主键
	 * @return
	 */
	PagedResult<SysMenu> selectSysMenusByPK(Long menuId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除菜单信息
	 * @param menuId
	 * @return
	 */
	Integer deleteSysMenusByPK(Long menuId);
	
	/**
	 * 单条更新菜单信息
	 * @param sysMenus
	 * @return
	 */
	Integer updateSysMenu(SysMenu sysMenu);

	/**
	 * 单条新增信息
	 * @param sysMenus
	 * @return
	 */
	Integer insertSysMenu(SysMenu sysMenu);
	
	/**
	 * 批量更新菜单信息
	 * @param sysMenus
	 * @return
	 */
	Integer updateSysMenuBatch(List<SysMenu> sysMenus);

	/**
	 * 批量新增信息
	 * @param sysMenus
	 * @return
	 */
	Integer insertSysMenuBatch(List<SysMenu> sysMenus);
}
