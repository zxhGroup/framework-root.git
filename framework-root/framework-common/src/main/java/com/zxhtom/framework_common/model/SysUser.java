package com.zxhtom.framework_common.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Id;
import javax.persistence.Table;

@ApiModel(description = "用户表")
@Table(name = "SYS_USER")
public class SysUser {

	/**
	 * 用户ID
	 */
	@Id
	private Long userId;
	/**
	 * 用户编号
	 */
	private String userCode;
	/**
	 * 用户名称
	 */
	private String userName;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 性别
	 */
	private Integer sex;

	/**
	 * 头像
	 */
	private String photo;

	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * 头像
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * 设置：用户头像
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	/**
	 * 获取：用户编号
	 */
	public String getUserCode() {
		return userCode;
	}
	/**
	 * 设置：用户名称
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：用户名称
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 获取：密码
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 设置：性别
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别
	 */
	public Integer getSex() {
		return sex;
	}

	@Override
	public String toString() {
		return "SysUser [userId=" + userId+",userCode=" + userCode + ",userName=" + userName + ",password=" + password + ",sex=" + sex + "]";
	}

}
