package com.zxhtom.framework_common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.zxhtom.config.CoreConfig;
import com.zxhtom.config.SpringfoxConfig;
import com.zxhtom.config.WebMvcConfig;
import com.zxhtom.framework_task.conf.TaskConfig;

/**
 * 入口类, 扫描并注入其他配置类和服务
 */
@SpringBootApplication
@ComponentScan(basePackages={"com.zxhtom"})
@Import({CoreConfig.class,TaskConfig.class,WebMvcConfig.class,SpringfoxConfig.class})
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
