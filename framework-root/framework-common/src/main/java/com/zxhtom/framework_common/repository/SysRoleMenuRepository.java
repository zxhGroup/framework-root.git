package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRoleMenu;

public interface SysRoleMenuRepository {

	/**
	 * 根据主键查询角色菜单信息
	 * 
	 * @param id
	 *            角色菜单主键
	 * @return
	 */
	PagedResult<SysRoleMenu> selectSysRoleMenusByPK(Long id,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除角色菜单信息
	 * @param id
	 * @return
	 */
	Integer deleteSysRoleMenusByPK(Long id);
	
	/**
	 * 单条更新角色菜单信息
	 * @param sysRoleMenus
	 * @return
	 */
	Integer updateSysRoleMenu(SysRoleMenu sysRoleMenu);

	/**
	 * 单条新增信息
	 * @param sysRoleMenus
	 * @return
	 */
	Integer insertSysRoleMenu(SysRoleMenu sysRoleMenu);
	
	/**
	 * 批量更新角色菜单信息
	 * @param sysRoleMenus
	 * @return
	 */
	Integer updateSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus);

	/**
	 * 批量新增信息
	 * @param sysRoleMenus
	 * @return
	 */
	Integer insertSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus);
}
