package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "用户角色关联表")
@Table(name = "SYS_USER_ROLE")
public class SysUserRole {

	/**
	 * 用户角色关联ID
	 */
	@Id
	private Long userRoleId;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 角色ID
	 */
	private Long roleId;
	
	/**
	 * 设置：用户角色关联ID
	 */
	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}
	/**
	 * 获取：用户角色关联ID
	 */
	public Long getUserRoleId() {
		return userRoleId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：角色ID
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID
	 */
	public Long getRoleId() {
		return roleId;
	}

	@Override
	public String toString() {
		return "SysUserRole [userRoleId=" + userRoleId+",userId=" + userId + ",roleId=" + roleId + "]";
	}

}
