package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import com.zxhtom.framework_common.dto.SysRoleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysRoleMapper;
import com.zxhtom.framework_common.model.SysRole;
import com.zxhtom.framework_common.repository.SysRoleRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysRoleRepositoryImpl implements SysRoleRepository{

	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Override
	public PagedResult<SysRole> selectSysRolesByPK(Long roleId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysRole.class,false,false);
		example.createCriteria().andEqualTo("roleId", roleId);
		return new PagedResult<>(sysRoleMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysRolesByPK(Long roleId) {
		Example example = new Example(SysRole.class,false,false);
		example.createCriteria().andEqualTo("roleId", roleId);
		return sysRoleMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysRole(SysRole sysRole) {
		return sysRoleMapper.updateByPrimaryKeySelective(sysRole);
	}
	@Override
	public Integer insertSysRole(SysRole sysRole) {
		return sysRoleMapper.insert(sysRole);
	}
	
	@Override
	public Integer updateSysRoleBatch(List<SysRole> sysRoles) {
		return sysRoleMapper.updateSysRoleBatch(sysRoles);
	}
	@Override
	public Integer insertSysRoleBatch(List<SysRole> sysRoles) {
		return sysRoleMapper.insertSysRoleBatch(sysRoles);
	}

    @Override
    public List<SysRoleDto> selectTreeRoleByUserId(Long userId) {
		if (userId == -1) {
			return sysRoleMapper.selectRootTreeRole();
		}
		return sysRoleMapper.selectTreeRoleByUserId(userId);
    }
}
