package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysDept;

public interface SysDeptRepository {

	/**
	 * 根据主键查询部门信息
	 * 
	 * @param deptId
	 *            部门主键
	 * @return
	 */
	PagedResult<SysDept> selectSysDeptsByPK(Long deptId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除部门信息
	 * @param deptId
	 * @return
	 */
	Integer deleteSysDeptsByPK(Long deptId);
	
	/**
	 * 单条更新部门信息
	 * @param sysDepts
	 * @return
	 */
	Integer updateSysDept(SysDept sysDept);

	/**
	 * 单条新增信息
	 * @param sysDepts
	 * @return
	 */
	Integer insertSysDept(SysDept sysDept);
	
	/**
	 * 批量更新部门信息
	 * @param sysDepts
	 * @return
	 */
	Integer updateSysDeptBatch(List<SysDept> sysDepts);

	/**
	 * 批量新增信息
	 * @param sysDepts
	 * @return
	 */
	Integer insertSysDeptBatch(List<SysDept> sysDepts);
}
