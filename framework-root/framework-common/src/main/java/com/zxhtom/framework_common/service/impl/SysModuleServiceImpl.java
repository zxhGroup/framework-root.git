package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.zxhtom.model.CustomUser;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysModule;
import com.zxhtom.framework_common.repository.SysModuleRepository;
import com.zxhtom.framework_common.service.SysModuleService;

@Service
public class SysModuleServiceImpl implements SysModuleService {

	@Autowired
	private SysModuleRepository sysModuleRepository;

	@Override
	public PagedResult<SysModule> selectSysModulesByPK(Long moduleId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysModule> result = new PagedResult<>();
		if(0==moduleId){
			//查询所有
			moduleId=null;
		}
		result = sysModuleRepository.selectSysModulesByPK(moduleId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysModulePK(Long moduleId) {
		if(0==moduleId){
			//删除所有
			moduleId=null;
		}
		return sysModuleRepository.deleteSysModulesByPK(moduleId);
	}

	@Override
	public Integer updateSysModule(SysModule sysModule) {
		return sysModuleRepository.updateSysModule(sysModule);
	}

	@Override
	public Integer insertSysModule(SysModule sysModule) {
		sysModule.setModuleId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysModuleRepository.insertSysModule(sysModule);
	}
	
	@Override
	public Integer deleteSysModulePKBatch(List<Long> moduleIds){
		for(Long moduleId : moduleIds){
			sysModuleRepository.deleteSysModulesByPK(moduleId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysModuleBatch(List<SysModule> sysModules) {
		return sysModuleRepository.updateSysModuleBatch(sysModules);
	}

	@Override
	public Integer insertSysModuleBatch(List<SysModule> sysModules) {
		for(SysModule sysModule : sysModules){
		sysModule.setModuleId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysModuleRepository.insertSysModuleBatch(sysModules);
	}

    @Override
    public List<SysModule> selectModuleByCurrentUser() {
		CustomUser customUser = (CustomUser) SecurityUtils.getSubject().getPrincipal();
		return sysModuleRepository.selectModuleByUserId(customUser.getUserId());
    }

}
