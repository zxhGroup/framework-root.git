package com.zxhtom.framework_common.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Id;
import javax.persistence.Table;

@ApiModel(description = "部门表")
@Table(name = "SYS_DEPT")
public class SysDept {

	/**
	 * 部门ID
	 */
	@Id
	private Long deptId;
	/**
	 * 部门编码
	 */
	private String deptCode;
	/**
	 * 部门类型
	 */
	private Integer deptType;
	/**
	 * 部门名称
	 */
	private String deptName;
	/**
	 * 父部门，根部门父部门是自己
	 */
	private Long parentDeptId;
	/**
	 * 部门负责人
	 */
	private Long leaderId;
	/**
	 * 部门描述
	 */
	private String remark;
	
	/**
	 * 设置：部门ID
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	/**
	 * 获取：部门ID
	 */
	public Long getDeptId() {
		return deptId;
	}
	/**
	 * 设置：部门编码
	 */
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}
	/**
	 * 获取：部门编码
	 */
	public Integer getDeptType() {
		return deptType;
	}
	/**
	 * 设置：部门类型
	 */
	public void setDeptType(Integer deptType) {
		this.deptType = deptType;
	}
	/**
	 * 获取：部门类型
	 */
	public String getDeptCode() {
		return deptCode;
	}
	/**
	 * 设置：部门名称
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	/**
	 * 获取：部门名称
	 */
	public String getDeptName() {
		return deptName;
	}
	/**
	 * 设置：父部门，根部门父部门是自己
	 */
	public void setParentDeptId(Long parentDeptId) {
		this.parentDeptId = parentDeptId;
	}
	/**
	 * 获取：父部门，根部门父部门是自己
	 */
	public Long getParentDeptId() {
		return parentDeptId;
	}
	/**
	 * 设置：部门负责人
	 */
	public void setLeaderId(Long leaderId) {
		this.leaderId = leaderId;
	}
	/**
	 * 获取：部门负责人
	 */
	public Long getLeaderId() {
		return leaderId;
	}
	/**
	 * 设置：部门描述
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：部门描述
	 */
	public String getRemark() {
		return remark;
	}

	@Override
	public String toString() {
		return "SysDept [deptId=" + deptId+",deptCode=" + deptCode + ",deptName=" + deptName + ",parentDeptId=" + parentDeptId + ",leaderId=" + leaderId + ",remark=" + remark + "]";
	}

}
