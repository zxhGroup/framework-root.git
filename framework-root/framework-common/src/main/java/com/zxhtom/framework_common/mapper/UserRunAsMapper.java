package com.zxhtom.framework_common.mapper;

import com.zxhtom.framework_common.model.UserRunAs;
import tk.mybatis.mapper.common.Mapper;

/**
 * @package com.zxhtom.framework_common.mapper
 * @Class UserRunAsMapper
 * @Description 托管用户
 * @Author zhangxinhua
 * @Date 19-6-4 上午9:27
 */
public interface UserRunAsMapper extends Mapper<UserRunAs> {
}
