package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRolePermission;

public interface SysRolePermissionRepository {

	/**
	 * 根据主键查询角色权限关联信息
	 * 
	 * @param rolePermissionId
	 *            角色权限关联主键
	 * @return
	 */
	PagedResult<SysRolePermission> selectSysRolePermissionsByPK(Long rolePermissionId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除角色权限关联信息
	 * @param rolePermissionId
	 * @return
	 */
	Integer deleteSysRolePermissionsByPK(Long rolePermissionId);
	
	/**
	 * 单条更新角色权限关联信息
	 * @param sysRolePermissions
	 * @return
	 */
	Integer updateSysRolePermission(SysRolePermission sysRolePermission);

	/**
	 * 单条新增信息
	 * @param sysRolePermissions
	 * @return
	 */
	Integer insertSysRolePermission(SysRolePermission sysRolePermission);
	
	/**
	 * 批量更新角色权限关联信息
	 * @param sysRolePermissions
	 * @return
	 */
	Integer updateSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions);

	/**
	 * 批量新增信息
	 * @param sysRolePermissions
	 * @return
	 */
	Integer insertSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions);
}
