package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysDeptMapper;
import com.zxhtom.framework_common.model.SysDept;
import com.zxhtom.framework_common.repository.SysDeptRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysDeptRepositoryImpl implements SysDeptRepository{

	@Autowired
	private SysDeptMapper sysDeptMapper;
	@Override
	public PagedResult<SysDept> selectSysDeptsByPK(Long deptId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysDept.class,false,false);
		example.createCriteria().andEqualTo("deptId", deptId);
		return new PagedResult<>(sysDeptMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysDeptsByPK(Long deptId) {
		Example example = new Example(SysDept.class,false,false);
		example.createCriteria().andEqualTo("deptId", deptId);
		return sysDeptMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysDept(SysDept sysDept) {
		return sysDeptMapper.updateByPrimaryKeySelective(sysDept);
	}
	@Override
	public Integer insertSysDept(SysDept sysDept) {
		return sysDeptMapper.insert(sysDept);
	}
	
	@Override
	public Integer updateSysDeptBatch(List<SysDept> sysDepts) {
		return sysDeptMapper.updateSysDeptBatch(sysDepts);
	}
	@Override
	public Integer insertSysDeptBatch(List<SysDept> sysDepts) {
		return sysDeptMapper.insertSysDeptBatch(sysDepts);
	}
}
