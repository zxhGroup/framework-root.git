package com.zxhtom.framework_common.service;

import java.util.List;

/**
 * @package com.zxhtom.framework_common.service
 * @Class UserRunAsService
 * @Description 托管管理
 * @Author zhangxinhua
 * @Date 19-6-3 下午5:05
 */
public interface UserRunAsService {
    /**
     * fromUserId权限暂时授予给toUserId
     * @param fromUserId 授权者
     * @param toUserId 拥有者
     */
    void grantRunAs(Long fromUserId, Long toUserId);

    /**
     * 讲toUserId的fromUserId的权限回收
     * @param fromUserId
     * @param toUserId
     */
    void revokeRunAs(Long fromUserId, Long toUserId);

    /**
     * 判断toUserId 是否已经托管了fromUserId
     * @param fromUserId
     * @param toUserId
     * @return
     */
    Boolean exists(Long fromUserId, Long toUserId);

    /**
     * 查找toUserId的托管用户列表
     * @param toUserId
     * @return
     */
    List<Long> findFromUserIds(Long toUserId);

    /**
     * 查找fromUserId将权限托管给那些人
     * @param fromUserId
     * @return
     */
    List<Long> findToUserIds(Long fromUserId);
}
