package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysRolePermissionMapper;
import com.zxhtom.framework_common.model.SysRolePermission;
import com.zxhtom.framework_common.repository.SysRolePermissionRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysRolePermissionRepositoryImpl implements SysRolePermissionRepository{

	@Autowired
	private SysRolePermissionMapper sysRolePermissionMapper;
	@Override
	public PagedResult<SysRolePermission> selectSysRolePermissionsByPK(Long rolePermissionId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysRolePermission.class,false,false);
		example.createCriteria().andEqualTo("rolePermissionId", rolePermissionId);
		return new PagedResult<>(sysRolePermissionMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysRolePermissionsByPK(Long rolePermissionId) {
		Example example = new Example(SysRolePermission.class,false,false);
		example.createCriteria().andEqualTo("rolePermissionId", rolePermissionId);
		return sysRolePermissionMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysRolePermission(SysRolePermission sysRolePermission) {
		return sysRolePermissionMapper.updateByPrimaryKeySelective(sysRolePermission);
	}
	@Override
	public Integer insertSysRolePermission(SysRolePermission sysRolePermission) {
		return sysRolePermissionMapper.insert(sysRolePermission);
	}
	
	@Override
	public Integer updateSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions) {
		return sysRolePermissionMapper.updateSysRolePermissionBatch(sysRolePermissions);
	}
	@Override
	public Integer insertSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions) {
		return sysRolePermissionMapper.insertSysRolePermissionBatch(sysRolePermissions);
	}
}
