package com.zxhtom.framework_common.mapper;


import com.zxhtom.framework_common.dto.SysUserPassWord;
import com.zxhtom.framework_common.model.SysUser;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
public interface SysUserMapper extends Mapper<SysUser>{

	/**
	 * 批量更新
	 * @param sysUsers
	 * @return
	 */
	Integer updateSysUserBatch(List<SysUser> sysUsers);

	/**
	 * 批量新增
	 * @param sysUsers
	 * @return
	 */
	Integer insertSysUserBatch(List<SysUser> sysUsers);

	/**
	 * 修改用户密码
	 * @param sysUserPassWord
	 * @return
	 */
    Integer updateUserByUserIdAndOldPassword(SysUserPassWord sysUserPassWord);

	/**
	 * 更新用户
	 * @param sysUser
	 * @return
	 */
	Integer updateSysUser(SysUser sysUser);
}
