package com.zxhtom.framework_common.mapper;

import com.zxhtom.model.CustomRole;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface CustomRoleMapper extends Mapper<CustomRole>{

	List<CustomRole> selectRolesByUserId(Long userId);

	List<CustomRole> selectRolesByModuleCode(@Param("moduleCode") String moduleCode, @Param("clientId") String clientId , @Param("clientSecret") String clientSecret);

}
