package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "权限表")
@Table(name = "SYS_PERMISSION")
public class SysPermission {

	/**
	 * 权限ID
	 */
	@Id
	private Long permissionId;
	/**
	 * 权限名称
	 */
	private String permissionName;
	/**
	 * 权限简要描述
	 */
	private String permissionRemark;
	
	/**
	 * 设置：权限ID
	 */
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}
	/**
	 * 获取：权限ID
	 */
	public Long getPermissionId() {
		return permissionId;
	}
	/**
	 * 设置：权限名称
	 */
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	/**
	 * 获取：权限名称
	 */
	public String getPermissionName() {
		return permissionName;
	}
	/**
	 * 设置：权限简要描述
	 */
	public void setPermissionRemark(String permissionRemark) {
		this.permissionRemark = permissionRemark;
	}
	/**
	 * 获取：权限简要描述
	 */
	public String getPermissionRemark() {
		return permissionRemark;
	}

	@Override
	public String toString() {
		return "SysPermission [permissionId=" + permissionId+",permissionName=" + permissionName + ",permissionRemark=" + permissionRemark + "]";
	}

}
