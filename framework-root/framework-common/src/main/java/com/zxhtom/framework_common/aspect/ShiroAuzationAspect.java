package com.zxhtom.framework_common.aspect;

import com.zxhtom.CoreConstants;
import com.zxhtom.PagedResult;
import com.zxhtom.config.shiro.ShiroUtils;
import com.zxhtom.framework_common.model.SysUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Component
@Aspect
public class ShiroAuzationAspect {
	private Logger logger = LogManager.getLogger(ShiroAuzationAspect.class);

	@Pointcut("execution(* com.zxhtom..service.impl.SysUserRole*Impl.*(..))")
	public void insert() {
	}

	@AfterReturning(value = "insert()", returning = "object")
	public void after(JoinPoint joinPoint, Object object) {
		ShiroUtils.clearAuth();
	}
}
