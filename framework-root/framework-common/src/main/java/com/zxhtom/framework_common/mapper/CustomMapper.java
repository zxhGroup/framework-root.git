package com.zxhtom.framework_common.mapper;

import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 自定义的一些查询接口
 * 
 * @ClassName: CustomMapper
 * @author zxhtom
 * @date 2018年7月4日
 * @since 1.0
 */
public interface CustomMapper extends Mapper<CustomUser> {

	/**
	 * 通过菜单获取所需的角色信息
	 * @param menuId
	 * @return   
	 * date 2018年7月11日
	 */
	List<CustomRole> selectRolesByMenuId(Long menuId);

	/**
	 * 根据角色获取对应模块下的菜单根列表
	 * @param roleList
	 * @param moduleCode
	 * @return   
	 * date 2018年7月11日
	 */
	List<MenuDto> selectRootMenusByRoleIdList(@Param(value = "roleList") List<CustomRole> roleList, @Param("oauthClientId") Long oauthClientId, @Param(value = "moduleCode") String moduleCode, @Param(value = "moduleCodeList") List<String> moduleCodeList);

	/**
	 * 获取菜单下的子菜单
	 * @param roleList
	 * @return   
	 * date 2018年7月11日
	 */
	List<MenuDto> selectChildMenuListMenuId(@Param(value = "roleList") List<CustomRole> roleList, @Param(value = "parentMenuId") Long parentMenuId, @Param("oauthClientId") Long oauthClientId);
	
	/**
	 * 获取用户所管理的模块集合
	 * @param userId
	 * @return
	 */
	List<Module> selectModulesByUserId(Long userId);
	
	/**
	 * 获取所有模块信息
	 * @return
	 */
	List<Module> selectAllModules();

	/**
	 * 获取用户的模块
	 * @param userId
	 * @return
	 */
    List<Module> selectModuleByUserId(@Param("userId") Long userId, @Param("oauthClientId") Long oauthClientId);
}
