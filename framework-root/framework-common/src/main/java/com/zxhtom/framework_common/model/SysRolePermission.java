package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "角色权限关联表")
@Table(name = "SYS_ROLE_PERMISSION")
public class SysRolePermission {

	/**
	 * 角色权限关联ID
	 */
	@Id
	private Long rolePermissionId;
	/**
	 * 权限ID
	 */
	private Long permissionId;
	/**
	 * 角色ID
	 */
	private Long roleId;
	
	/**
	 * 设置：角色权限关联ID
	 */
	public void setRolePermissionId(Long rolePermissionId) {
		this.rolePermissionId = rolePermissionId;
	}
	/**
	 * 获取：角色权限关联ID
	 */
	public Long getRolePermissionId() {
		return rolePermissionId;
	}
	/**
	 * 设置：权限ID
	 */
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}
	/**
	 * 获取：权限ID
	 */
	public Long getPermissionId() {
		return permissionId;
	}
	/**
	 * 设置：角色ID
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID
	 */
	public Long getRoleId() {
		return roleId;
	}

	@Override
	public String toString() {
		return "SysRolePermission [rolePermissionId=" + rolePermissionId+",permissionId=" + permissionId + ",roleId=" + roleId + "]";
	}

}
