package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysPermission;

import tk.mybatis.mapper.common.Mapper;
public interface SysPermissionMapper extends Mapper<SysPermission>{

	/**
	 * 批量更新
	 * @param sysPermissions
	 * @return
	 */
	Integer updateSysPermissionBatch(List<SysPermission> sysPermissions);

	/**
	 * 批量新增
	 * @param sysPermissions
	 * @return
	 */
	Integer insertSysPermissionBatch(List<SysPermission> sysPermissions);
}
