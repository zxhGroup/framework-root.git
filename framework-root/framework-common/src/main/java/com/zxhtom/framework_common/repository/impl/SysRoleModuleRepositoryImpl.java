package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysRoleModuleMapper;
import com.zxhtom.framework_common.model.SysRoleModule;
import com.zxhtom.framework_common.repository.SysRoleModuleRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysRoleModuleRepositoryImpl implements SysRoleModuleRepository{

	@Autowired
	private SysRoleModuleMapper sysRoleModuleMapper;
	@Override
	public PagedResult<SysRoleModule> selectSysRoleModulesByPK(Long id,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysRoleModule.class,false,false);
		example.createCriteria().andEqualTo("id", id);
		return new PagedResult<>(sysRoleModuleMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysRoleModulesByPK(Long id) {
		Example example = new Example(SysRoleModule.class,false,false);
		example.createCriteria().andEqualTo("id", id);
		return sysRoleModuleMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysRoleModule(SysRoleModule sysRoleModule) {
		return sysRoleModuleMapper.updateByPrimaryKeySelective(sysRoleModule);
	}
	@Override
	public Integer insertSysRoleModule(SysRoleModule sysRoleModule) {
		return sysRoleModuleMapper.insert(sysRoleModule);
	}
	
	@Override
	public Integer updateSysRoleModuleBatch(List<SysRoleModule> sysRoleModules) {
		return sysRoleModuleMapper.updateSysRoleModuleBatch(sysRoleModules);
	}
	@Override
	public Integer insertSysRoleModuleBatch(List<SysRoleModule> sysRoleModules) {
		return sysRoleModuleMapper.insertSysRoleModuleBatch(sysRoleModules);
	}
}
