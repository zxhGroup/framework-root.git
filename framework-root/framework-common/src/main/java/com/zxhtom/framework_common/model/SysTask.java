package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "任务表")
@Table(name = "SYS_TASK")
public class SysTask {

	/**
	 * 任务流水号
	 */
	@Id
	private Long taskId;
	/**
	 * 任务名称
	 */
	private String taskName;
	/**
	 * 任务类名
	 */
	private String jobClass;
	/**
	 * 调度表达式
	 */
	private String cronExpression;
	/**
	 * 任务参数
	 */
	private String param;
	/**
	 * 定时任务处理服务器编号
	 */
	private Short serverId;
	/**
	 * 启用否
	 */
	private Integer enable;
	/**
	 * 加载任务后立即执行
	 */
	private Integer immediately;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 任务状态
	 */
	private String taskStatus;
	/**
	 * 定时任务运行服务器IP
	 */
	private String runIp;
	/**
	 * 最后一次处理状况
	 */
	private String lastState;
	/**
	 * 最后一次处理IP
	 */
	private String lastIp;
	/**
	 * 最后一次处理开始时间
	 */
	private Date lastStartTime;
	/**
	 * 最后一次处理结束时间
	 */
	private Date lastEndTime;
	/**
	 * 最后一次处理结果
	 */
	private String lastResult;
	
	/**
	 * 设置：任务流水号
	 */
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	/**
	 * 获取：任务流水号
	 */
	public Long getTaskId() {
		return taskId;
	}
	/**
	 * 设置：任务名称
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	/**
	 * 获取：任务名称
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * 设置：任务类名
	 */
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}
	/**
	 * 获取：任务类名
	 */
	public String getJobClass() {
		return jobClass;
	}
	/**
	 * 设置：调度表达式
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	/**
	 * 获取：调度表达式
	 */
	public String getCronExpression() {
		return cronExpression;
	}
	/**
	 * 设置：任务参数
	 */
	public void setParam(String param) {
		this.param = param;
	}
	/**
	 * 获取：任务参数
	 */
	public String getParam() {
		return param;
	}
	/**
	 * 设置：定时任务处理服务器编号
	 */
	public void setServerId(Short serverId) {
		this.serverId = serverId;
	}
	/**
	 * 获取：定时任务处理服务器编号
	 */
	public Short getServerId() {
		return serverId;
	}
	/**
	 * 设置：启用否
	 */
	public void setEnable(Integer enable) {
		this.enable = enable;
	}
	/**
	 * 获取：启用否
	 */
	public Integer getEnable() {
		return enable;
	}
	/**
	 * 设置：加载任务后立即执行
	 */
	public void setImmediately(Integer immediately) {
		this.immediately = immediately;
	}
	/**
	 * 获取：加载任务后立即执行
	 */
	public Integer getImmediately() {
		return immediately;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：任务状态
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	/**
	 * 获取：任务状态
	 */
	public String getTaskStatus() {
		return taskStatus;
	}
	/**
	 * 设置：定时任务运行服务器IP
	 */
	public void setRunIp(String runIp) {
		this.runIp = runIp;
	}
	/**
	 * 获取：定时任务运行服务器IP
	 */
	public String getRunIp() {
		return runIp;
	}
	/**
	 * 设置：最后一次处理状况
	 */
	public void setLastState(String lastState) {
		this.lastState = lastState;
	}
	/**
	 * 获取：最后一次处理状况
	 */
	public String getLastState() {
		return lastState;
	}
	/**
	 * 设置：最后一次处理IP
	 */
	public void setLastIp(String lastIp) {
		this.lastIp = lastIp;
	}
	/**
	 * 获取：最后一次处理IP
	 */
	public String getLastIp() {
		return lastIp;
	}
	/**
	 * 设置：最后一次处理开始时间
	 */
	public void setLastStartTime(Date lastStartTime) {
		this.lastStartTime = lastStartTime;
	}
	/**
	 * 获取：最后一次处理开始时间
	 */
	public Date getLastStartTime() {
		return lastStartTime;
	}
	/**
	 * 设置：最后一次处理结束时间
	 */
	public void setLastEndTime(Date lastEndTime) {
		this.lastEndTime = lastEndTime;
	}
	/**
	 * 获取：最后一次处理结束时间
	 */
	public Date getLastEndTime() {
		return lastEndTime;
	}
	/**
	 * 设置：最后一次处理结果
	 */
	public void setLastResult(String lastResult) {
		this.lastResult = lastResult;
	}
	/**
	 * 获取：最后一次处理结果
	 */
	public String getLastResult() {
		return lastResult;
	}

	@Override
	public String toString() {
		return "SysTask [taskId=" + taskId+",taskName=" + taskName + ",jobClass=" + jobClass + ",cronExpression=" + cronExpression + ",param=" + param + ",serverId=" + serverId + ",enable=" + enable + ",immediately=" + immediately + ",remark=" + remark + ",taskStatus=" + taskStatus + ",runIp=" + runIp + ",lastState=" + lastState + ",lastIp=" + lastIp + ",lastStartTime=" + lastStartTime + ",lastEndTime=" + lastEndTime + ",lastResult=" + lastResult + "]";
	}

}
