package com.zxhtom.framework_common.controller;

import java.util.*;

import javax.validation.constraints.NotNull;

import com.zxhtom.framework_common.dto.SysRoleDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.framework_common.model.SysRole;
import com.zxhtom.framework_common.service.SysRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/framework_common/sysRole")
@Api(tags = "framework_common sysRole", description = "用户信息表管理")
public class SysRoleController {

	private Logger logger = LogManager.getLogger(SysRoleController.class);
	@Autowired
	private SysRoleService sysRoleService;

	@RequestMapping(value = "/selectCurrentTreeRole", method = RequestMethod.GET)
	@ApiOperation(value ="获取当前用户的角色树")
	public List<SysRoleDto> selectCurrentTreeRole() {
		return sysRoleService.selectCurrentTreeRole();
	}

	@RequestMapping(value = "/{roleId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有用户信息列表 传0表示查询所有")
	public PagedResult<SysRole> selectSysRolesByPK(@ApiParam(value = "用户主键", required = true) @PathVariable @NotNull Long roleId,
	@ApiParam(value = "页码", required = false) @RequestParam Integer pageNumber, 
	@ApiParam(value = "页量", required = false) @RequestParam Integer pageSize) {
		return sysRoleService.selectSysRolesByPK(roleId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysRoleByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long roleId) {
		return sysRoleService.deleteSysRolePK(roleId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) SysRole sysRole) {
		return sysRoleService.updateSysRole(sysRole);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysRole sysRole) {
		return sysRoleService.insertSysRole(sysRole);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysRoleByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> roleIds) {
		return sysRoleService.deleteSysRolePKBatch(roleIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysRole> sysRoles) {
		return sysRoleService.updateSysRoleBatch(sysRoles);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysRole> sysRoles) {
		return sysRoleService.insertSysRoleBatch(sysRoles);
	}
}
