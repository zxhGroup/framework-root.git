package com.zxhtom.framework_common.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.zxhtom.framework_common.dto.SysUserDeptDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysUserDept;
import com.zxhtom.framework_common.repository.SysUserDeptRepository;
import com.zxhtom.framework_common.service.SysUserDeptService;

@Service
public class SysUserDeptServiceImpl implements SysUserDeptService {

	@Autowired
	private SysUserDeptRepository sysUserDeptRepository;

	@Override
	public PagedResult<SysUserDept> selectSysUserDeptsByPK(Long userDeptId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysUserDept> result = new PagedResult<>();
		if(0==userDeptId){
			//查询所有
			userDeptId=null;
		}
		result = sysUserDeptRepository.selectSysUserDeptsByPK(userDeptId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysUserDeptPK(Long userDeptId) {
		if(0==userDeptId){
			//删除所有
			userDeptId=null;
		}
		return sysUserDeptRepository.deleteSysUserDeptsByPK(userDeptId);
	}

	@Override
	public Integer updateSysUserDept(SysUserDept sysUserDept) {
		return sysUserDeptRepository.updateSysUserDept(sysUserDept);
	}

	@Override
	public Integer insertSysUserDept(SysUserDept sysUserDept) {
		sysUserDept.setUserDeptId(BigDecimal.valueOf(IdUtil.getInstance().getId()));
		return sysUserDeptRepository.insertSysUserDept(sysUserDept);
	}
	
	@Override
	public Integer deleteSysUserDeptPKBatch(List<Long> userDeptIds){
		for(Long userDeptId : userDeptIds){
			sysUserDeptRepository.deleteSysUserDeptsByPK(userDeptId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysUserDeptBatch(List<SysUserDept> sysUserDepts) {
		return sysUserDeptRepository.updateSysUserDeptBatch(sysUserDepts);
	}

	@Override
	public Integer insertSysUserDeptBatch(List<SysUserDept> sysUserDepts) {
		for(SysUserDept sysUserDept : sysUserDepts){
		sysUserDept.setUserDeptId(BigDecimal.valueOf(IdUtil.getInstance().getId()));
		}
		return sysUserDeptRepository.insertSysUserDeptBatch(sysUserDepts);
	}

	@Override
	public PagedResult<SysUserDeptDto> selectSysUserDeptDtosByPK(Long userDeptId, Integer pageNumber, Integer pageSize) {
		if (pageNumber != null && pageSize != null) {
			PageHelper.startPage(pageNumber, pageSize);
		}
		return new PagedResult<>(sysUserDeptRepository.selectSysUserDeptDtosByPK(userDeptId));
	}

}
