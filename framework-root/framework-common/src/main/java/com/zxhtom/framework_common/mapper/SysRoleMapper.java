package com.zxhtom.framework_common.mapper;


import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.zxhtom.framework_common.dto.SysRoleDto;
import com.zxhtom.framework_common.model.SysRole;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
public interface SysRoleMapper extends Mapper<SysRole>{

	/**
	 * 批量更新
	 * @param sysRoles
	 * @return
	 */
	Integer updateSysRoleBatch(List<SysRole> sysRoles);

	/**
	 * 批量新增
	 * @param sysRoles
	 * @return
	 */
	Integer insertSysRoleBatch(List<SysRole> sysRoles);

	/**
	 * 获取角色树
	 * @param userId
	 * @return
	 */
    List<SysRoleDto> selectTreeRoleByUserId(@Param("userId") Long userId);

	/**
	 * 获取根路径
	 * @return
	 */
    List<SysRoleDto> selectRootTreeRole();
}
