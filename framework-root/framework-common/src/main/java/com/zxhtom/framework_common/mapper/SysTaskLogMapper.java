package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysTaskLog;

import tk.mybatis.mapper.common.Mapper;
public interface SysTaskLogMapper extends Mapper<SysTaskLog>{

	/**
	 * 批量更新
	 * @param sysTaskLogs
	 * @return
	 */
	Integer updateSysTaskLogBatch(List<SysTaskLog> sysTaskLogs);

	/**
	 * 批量新增
	 * @param sysTaskLogs
	 * @return
	 */
	Integer insertSysTaskLogBatch(List<SysTaskLog> sysTaskLogs);
}
