package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRoleMenu;

public interface SysRoleMenuService {

	/**
	 * 根据主键查询角色菜单信息
	 * @param id 角色菜单主键
	 * @return
	 */
	PagedResult<SysRoleMenu> selectSysRoleMenusByPK(Long id,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除角色菜单信息
	 * @param id 角色菜单主键
	 * @return
	 */
	Integer deleteSysRoleMenuPK(Long id);

	/**
	 * 单条更新数据
	 * @param SysRoleMenu
	 * @return
	 */
	Integer updateSysRoleMenu(SysRoleMenu sysRoleMenu);

	/**
	 * 单条新增数据
	 * @param SysRoleMenu
	 * @return
	 */
	Integer insertSysRoleMenu(SysRoleMenu sysRoleMenu);
	
	/**
	 * 批量根据主键删除角色菜单信息
	 * @param id 角色菜单主键
	 * @return
	 */
	Integer deleteSysRoleMenuPKBatch(List<Long> ids);
	
	/**
	 * 批量更新数据
	 * @param SysRoleMenus
	 * @return
	 */
	Integer updateSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus);

	/**
	 * 批量新增数据
	 * @param SysRoleMenus
	 * @return
	 */
	Integer insertSysRoleMenuBatch(List<SysRoleMenu> sysRoleMenus);
}
