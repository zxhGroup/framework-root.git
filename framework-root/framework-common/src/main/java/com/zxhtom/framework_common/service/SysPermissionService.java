package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysPermission;

public interface SysPermissionService {

	/**
	 * 根据主键查询权限信息
	 * @param permissionId 权限主键
	 * @return
	 */
	PagedResult<SysPermission> selectSysPermissionsByPK(Long permissionId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除权限信息
	 * @param permissionId 权限主键
	 * @return
	 */
	Integer deleteSysPermissionPK(Long permissionId);

	/**
	 * 单条更新数据
	 * @param SysPermission
	 * @return
	 */
	Integer updateSysPermission(SysPermission sysPermission);

	/**
	 * 单条新增数据
	 * @param SysPermission
	 * @return
	 */
	Integer insertSysPermission(SysPermission sysPermission);
	
	/**
	 * 批量根据主键删除权限信息
	 * @param permissionId 权限主键
	 * @return
	 */
	Integer deleteSysPermissionPKBatch(List<Long> permissionIds);
	
	/**
	 * 批量更新数据
	 * @param SysPermissions
	 * @return
	 */
	Integer updateSysPermissionBatch(List<SysPermission> sysPermissions);

	/**
	 * 批量新增数据
	 * @param SysPermissions
	 * @return
	 */
	Integer insertSysPermissionBatch(List<SysPermission> sysPermissions);
}
