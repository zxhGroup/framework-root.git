package com.zxhtom.framework_common.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysPermission;

public interface SysPermissionRepository {

	/**
	 * 根据主键查询权限信息
	 * 
	 * @param permissionId
	 *            权限主键
	 * @return
	 */
	PagedResult<SysPermission> selectSysPermissionsByPK(Long permissionId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除权限信息
	 * @param permissionId
	 * @return
	 */
	Integer deleteSysPermissionsByPK(Long permissionId);
	
	/**
	 * 单条更新权限信息
	 * @param sysPermissions
	 * @return
	 */
	Integer updateSysPermission(SysPermission sysPermission);

	/**
	 * 单条新增信息
	 * @param sysPermissions
	 * @return
	 */
	Integer insertSysPermission(SysPermission sysPermission);
	
	/**
	 * 批量更新权限信息
	 * @param sysPermissions
	 * @return
	 */
	Integer updateSysPermissionBatch(List<SysPermission> sysPermissions);

	/**
	 * 批量新增信息
	 * @param sysPermissions
	 * @return
	 */
	Integer insertSysPermissionBatch(List<SysPermission> sysPermissions);
}
