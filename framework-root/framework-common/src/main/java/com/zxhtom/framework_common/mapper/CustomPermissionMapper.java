package com.zxhtom.framework_common.mapper;

import com.zxhtom.model.CustomPermission;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Set;

public interface CustomPermissionMapper extends Mapper<CustomPermission>{

	List<CustomPermission> selectPremissionSetByUserId(Long userId);

	Set<CustomPermission> selectPremissionsByRoleId(@Param("roleId") Long roleId, @Param("clientId") String clientId , @Param("clientSecret") String clientSecret);

}
