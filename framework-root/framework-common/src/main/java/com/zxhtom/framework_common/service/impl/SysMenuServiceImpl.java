package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysMenu;
import com.zxhtom.framework_common.repository.SysMenuRepository;
import com.zxhtom.framework_common.service.SysMenuService;

@Service
public class SysMenuServiceImpl implements SysMenuService {

	@Autowired
	private SysMenuRepository sysMenuRepository;

	@Override
	public PagedResult<SysMenu> selectSysMenusByPK(Long menuId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysMenu> result = new PagedResult<>();
		if(0==menuId){
			//查询所有
			menuId=null;
		}
		result = sysMenuRepository.selectSysMenusByPK(menuId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysMenuPK(Long menuId) {
		if(0==menuId){
			//删除所有
			menuId=null;
		}
		return sysMenuRepository.deleteSysMenusByPK(menuId);
	}

	@Override
	public Integer updateSysMenu(SysMenu sysMenu) {
		return sysMenuRepository.updateSysMenu(sysMenu);
	}

	@Override
	public Integer insertSysMenu(SysMenu sysMenu) {
		sysMenu.setMenuId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysMenuRepository.insertSysMenu(sysMenu);
	}
	
	@Override
	public Integer deleteSysMenuPKBatch(List<Long> menuIds){
		for(Long menuId : menuIds){
			sysMenuRepository.deleteSysMenusByPK(menuId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysMenuBatch(List<SysMenu> sysMenus) {
		return sysMenuRepository.updateSysMenuBatch(sysMenus);
	}

	@Override
	public Integer insertSysMenuBatch(List<SysMenu> sysMenus) {
		for(SysMenu sysMenu : sysMenus){
		sysMenu.setMenuId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysMenuRepository.insertSysMenuBatch(sysMenus);
	}

}
