package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.dto.SysMutilUserRole;
import com.zxhtom.framework_common.dto.SysUserRoleDto;
import com.zxhtom.framework_common.model.SysUserRole;

public interface SysUserRoleService {

	/**
	 * 根据主键查询用户角色关联信息
	 * @param userRoleId 用户角色关联主键
	 * @return
	 */
	PagedResult<SysUserRole> selectSysUserRolesByPK(Long userRoleId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除用户角色关联信息
	 * @param userRoleId 用户角色关联主键
	 * @return
	 */
	Integer deleteSysUserRolePK(Long userRoleId);

	/**
	 * 单条更新数据
	 * @param SysUserRole
	 * @return
	 */
	Integer updateSysUserRole(SysUserRole sysUserRole);

	/**
	 * 单条新增数据
	 * @param SysUserRole
	 * @return
	 */
	Integer insertSysUserRole(SysUserRole sysUserRole);
	
	/**
	 * 批量根据主键删除用户角色关联信息
	 * @param userRoleId 用户角色关联主键
	 * @return
	 */
	Integer deleteSysUserRolePKBatch(List<Long> userRoleIds);
	
	/**
	 * 批量更新数据
	 * @param SysUserRoles
	 * @return
	 */
	Integer updateSysUserRoleBatch(List<SysUserRole> sysUserRoles);

	/**
	 * 批量新增数据
	 * @param SysUserRoles
	 * @return
	 */
	Integer insertSysUserRoleBatch(List<SysUserRole> sysUserRoles);

	/**
	 * 根据主键获取所有用户角色关联详细信息列表
	 * @param userRoleId
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
    PagedResult<SysUserRoleDto> selectSysUserRolesDtoByPK(Long userRoleId, Integer pageNumber, Integer pageSize);

	/**
	 * 用户角色多对多新增
	 * @param sysMutilUserRole
	 * @return
	 */
	Integer insertMutilUserAndRole(SysMutilUserRole sysMutilUserRole);

	/**
	 * 通过用户ID删除用户的角色
	 * @param userIds
	 * @return
	 */
    Integer deleteBatchByUserId(List<Long> userIds);
}
