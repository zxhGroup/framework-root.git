package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysRoleModule;

import tk.mybatis.mapper.common.Mapper;
public interface SysRoleModuleMapper extends Mapper<SysRoleModule>{

	/**
	 * 批量更新
	 * @param sysRoleModules
	 * @return
	 */
	Integer updateSysRoleModuleBatch(List<SysRoleModule> sysRoleModules);

	/**
	 * 批量新增
	 * @param sysRoleModules
	 * @return
	 */
	Integer insertSysRoleModuleBatch(List<SysRoleModule> sysRoleModules);
}
