package com.zxhtom.framework_common.repository.impl;

import com.zxhtom.CoreConstants;
import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.dto.MenuDto;
import com.zxhtom.framework_common.mapper.*;
import com.zxhtom.model.*;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.vconstant.RedisLocalKey;
import com.zxhtom.vconstant.RoleList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Repository
@Primary
public class CustomRealRepositoryImpl implements CustomRepository {
    @Autowired
    private CustomUserMapper customUserMapper;

    @Autowired
    private GeneratorProperties properties;

    @Autowired
    private CustomRoleMapper customRoleMapper;

    @Autowired
    private CustomPermissionMapper customPermissionMapper;

    @Autowired
    private CustomMapper customMapper;

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private SysModuleMapper sysModuleMapper;

    @SuppressWarnings("rawtypes")
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public CustomUser selectUserByUserName(String userName) {
        Example example = new Example(CustomUser.class, false, false);
        example.createCriteria().andEqualTo("userName", userName);
        List<CustomUser> customUsers = customUserMapper.selectByExample(example);
        if (customUsers.size() > 0) {
            return customUsers.get(0);
        }
        return new CustomUser();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Set<String> selectPremissionNamesByUserId(Long userId,String clientId,String clientSecret) {
        String key = userId + ":" + clientId+clientSecret;
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(key, RedisLocalKey.USERPERMISSIONLIST)) {
            return (Set<String>) opsForHash.get(key, RedisLocalKey.USERPERMISSIONLIST);
        }
        List<CustomPermission> permissionList = new ArrayList<CustomPermission>();
        Set<String> resultSet = new HashSet<String>();
        Set<CustomRole> roleList = selectRolesByUserId(userId);
        for (CustomRole role : roleList) {
            permissionList.addAll(selectPremissionsByRoleId(role.getRoleId(),clientId,clientSecret));
        }
        for (CustomPermission permission : permissionList) {
            resultSet.add(permission.getPermissionName());
        }
        // 加入缓存
        opsForHash.put(key, RedisLocalKey.USERPERMISSIONLIST, resultSet);
        return resultSet;
    }

    private Set<CustomPermission> selectPremissionsByRoleId(Long roleId,String clientId,String clientSecret) {
        Set<CustomPermission> permissionList = new HashSet<CustomPermission>();
        if (null != roleId && roleId == -1) {
            permissionList.addAll(customPermissionMapper.selectAll());
        } else {
            permissionList = customPermissionMapper.selectPremissionsByRoleId(roleId,clientId,clientSecret);
        }
        return permissionList;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Set<String> selectRoleNamesByUserId(Long userId) {
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(userId, RedisLocalKey.USERROLELIST)) {
            return (Set<String>) opsForHash.get(userId, RedisLocalKey.USERROLELIST);
        }
        Set<CustomRole> roleList = selectRolesByUserId(userId);
        Set<String> resultSet = new HashSet<String>();
        for (CustomRole role : roleList) {
            resultSet.add(role.getRoleCode());
        }
        // 加入缓存
        opsForHash.put(userId, RedisLocalKey.USERROLELIST, resultSet);
        return resultSet;
    }

    @Override
    public Set<CustomRole> selectRolesByUserId(Long userId) {
        List<CustomRole> roleList = new ArrayList<CustomRole>();
        if (userId == -1) {
            // 超级管理员拥有所有权限
            roleList = customRoleMapper.selectAll();
        } else {
            roleList = selecttAllRolesByUserId(userId);
        }
        return new HashSet<CustomRole>(roleList);
    }

    /**
     * 获取用户角色，角色是继承关系
     *
     * @param userId
     * @return
     */
    private List<CustomRole> selecttAllRolesByUserId(Long userId) {
        // 获取直接关联的角色列表
        List<CustomRole> roleList = customRoleMapper.selectRolesByUserId(userId);
        Set<CustomRole> resultSet = new HashSet<CustomRole>();
        for (CustomRole customRole : roleList) {
            selectParentRoleListByRole(resultSet, customRole);
        }
        roleList.addAll(resultSet);
        return roleList;
    }

    private void selectParentRoleListByRole(Set<CustomRole> list, CustomRole role) {
        if (null != role && null != role.getParentRoleId()&&!role.getRoleId().equals(role.getParentRoleId())) {
            CustomRole customRole = customRoleMapper.selectByPrimaryKey(role.getParentRoleId());
            if (null != customRole) {
                list.add(customRole);
            }
            selectParentRoleListByRole(list, customRole);
        }
    }



    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<CustomRole> selectRolesByMenuId(Long menuId) {
        HashOperations hash = redisTemplate.opsForHash();
        if (redisTemplate.hasKey(RedisLocalKey.ROLEMENU)) {
            return (List<CustomRole>) hash.get(RedisLocalKey.ROLEMENU, menuId);
        }
        List<CustomRole> roleList = new ArrayList<CustomRole>();
        roleList = customMapper.selectRolesByMenuId(menuId);

        if (CollectionUtils.isNotEmpty(roleList)) {
            hash.put(RedisLocalKey.ROLEMENU, menuId, roleList);
        }
        redisTemplate.expire(RedisLocalKey.ROLEMENU, 18, TimeUnit.HOURS);
        return roleList;
    }

    @Override
    public CustomUser selectUserInfoByUserName(String userName) {
        if (redisTemplate.opsForHash().hasKey(RedisLocalKey.USERLIST, userName)) {
            return (CustomUser) redisTemplate.opsForHash().get(RedisLocalKey.USERLIST, userName);
        }
        List<CustomUser> userList = customUserMapper.selectUserInfoByUserName(userName);
        if (CollectionUtils.isNotEmpty(userList)) {
            CustomUser customUser = userList.get(0);
            redisTemplate.opsForHash().put(RedisLocalKey.USERLIST, userName,customUser );
            return customUser;
        }
        return new CustomUser();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public List<MenuDto> selectRootMenusByUserIdAndModeul(Long userId, String moduleCode,Long oauthClientId) {
        String key = userId + ":" + oauthClientId;
        List<CustomRole> roleList = selectRoleIdListByUserId(userId);
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(RedisLocalKey.USERMENULIST,key)) {
            return (List<MenuDto>) opsForHash.get(RedisLocalKey.USERMENULIST,key);
        }
        List<MenuDto> listemp = new ArrayList<MenuDto>();
        List<MenuDto> list = new ArrayList<MenuDto>();
        List<Long> menuIdList = new ArrayList<Long>();
        if (SecurityUtils.getSubject().hasRole(RoleList.SUPERADMIN)) {
            listemp = customMapper.selectRootMenusByRoleIdList(null, null,moduleCode, null);
        } else {
            listemp = customMapper.selectRootMenusByRoleIdList(roleList,oauthClientId, moduleCode, null);
        }
        if (CollectionUtils.isNotEmpty(listemp)) {
            for (MenuDto menuDto : listemp) {
                if (null == menuDto) {
                    continue;
                }
                if (!menuIdList.contains(menuDto.getMenuId())) {
                    menuIdList.add(menuDto.getMenuId());
                    list.add(menuDto);
                }
            }
            opsForHash.put(RedisLocalKey.USERMENULIST,key, list);
            return list;
        }
        return new ArrayList<MenuDto>();
    }

    @Override
    public List<MenuDto> selectChildMenuListMenuId(Long userId, Long menuId,Long oauthClientId) {
        List<CustomRole> roleList = selectRoleIdListByUserId(userId);
        if (SecurityUtils.getSubject().hasRole(RoleList.SUPERADMIN)) {
            userId = null;
        }
        List<MenuDto> listemp = customMapper.selectChildMenuListMenuId(roleList, menuId,oauthClientId);
        List<MenuDto> list = new ArrayList<MenuDto>();
        List<Long> menuIdList = new ArrayList<Long>();
        for (MenuDto menuDto : listemp) {
            if (null == menuDto) {
                continue;
            }
            if (!menuIdList.contains(menuDto.getMenuId())) {
                menuIdList.add(menuDto.getMenuId());
                list.add(menuDto);
            }
        }
        if (CollectionUtils.isNotEmpty(list)) {
            return list;
        }
        return new ArrayList<MenuDto>();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public List<MenuDto> selectRootMenusByUserId(Long userId,Long oauthClientId) {
        String key = userId + ":" + oauthClientId;
        List<CustomRole> roleList = selectRoleIdListByUserId(userId);
        List<String> moduleCodes = (List<String>) CoreConstants.map.get(CoreConstants.PROJECTNAMELIST);
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(RedisLocalKey.USERROOTMENULIST,key)) {
            return (List<MenuDto>) opsForHash.get(RedisLocalKey.USERROOTMENULIST,key);
        }
        List<MenuDto> listemp = new ArrayList<MenuDto>();
        List<MenuDto> list = new ArrayList<MenuDto>();
        List<Long> menuIdList = new ArrayList<Long>();
        if (SecurityUtils.getSubject().hasRole(RoleList.SUPERADMIN)) {
            listemp = customMapper.selectRootMenusByRoleIdList(null,null, null, null);
        } else {
            listemp = customMapper.selectRootMenusByRoleIdList(roleList, oauthClientId,null, moduleCodes);
        }
        if (CollectionUtils.isNotEmpty(listemp)) {
            for (MenuDto menuDto : listemp) {
                if (null == menuDto) {
                    continue;
                }
                if (!menuIdList.contains(menuDto.getMenuId())) {
                    menuIdList.add(menuDto.getMenuId());
                    list.add(menuDto);
                }
            }
            opsForHash.put(RedisLocalKey.USERROOTMENULIST,key, list);
            return list;
        }
        return new ArrayList<MenuDto>();
    }

    @Override
    public List<Module> selectModuleByCurrentUser(Long userId,Long oauthClientId) {
        String key = userId + ":" + oauthClientId;
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(RedisLocalKey.USERMODULELIST,key)) {
            return (List<Module>) opsForHash.get(RedisLocalKey.USERMODULELIST,key);
        }
        List<Module> modules = customMapper.selectModuleByUserId(userId,oauthClientId);
        opsForHash.put(RedisLocalKey.USERMODULELIST, key, modules);
        return modules;
    }

    @Override
    public CustomUser selectCurrentUserName(String accessToken) {
        //本地服务不需要具体实现。此方法是为shiro service准备的
        return null;
    }

    @Override
    public CustomUser selectUserByUserId(Long userId) {
        Example example = new Example(CustomUser.class, false, false);
        example.createCriteria().andEqualTo("userId", userId);
        List<CustomUser> customUsers = customUserMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(customUsers)) {
            CustomUser customUser = customUsers.get(0);
            customUser.setPassword("");
            return customUser;
        }
        return null;
    }

    @Override
    public Integer insertCustomUser(CustomUser user) {
        user.setUserId(IdUtil.getInstance().getId());
        user.setPassword(new Md5Hash(user.getPassword(),"zxh").toString());
        return customUserMapper.insert(user);
    }

    private List<CustomRole> selectRoleIdListByUserId(Long userId) {
        List<CustomRole> roleList = new ArrayList<CustomRole>();
        if (userId == -1) {
            // 超级管理员拥有所有权限
            roleList = customRoleMapper.selectAll();
        } else {
            roleList = selecttAllRolesByUserId(userId);
        }
        return roleList;
    }
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map<Long, Long> selectVistualMenu() {
        Map<Long, Long> vistualMenuMap = new HashMap<Long, Long>();
        Map<Long, Long> factMenuMap = new HashMap<Long, Long>();
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (redisTemplate.hasKey(RedisLocalKey.MENUPRE)) {
            return (Map<Long, Long>) opsForHash.get(CoreConstants.MENUPRE, CoreConstants.VISTUALMENUKEY);
        }
        Example example = new Example(Menu.class, false, false);
        example.createCriteria().andEqualTo("moduleId", properties.getModuleName());
        List<Menu> menuList = menuMapper.selectByExample(example);
        for (Menu menu : menuList) {
            Long id = IdUtil.getInstance().getId();
            factMenuMap.put(id, menu.getMenuId());
            vistualMenuMap.put(menu.getMenuId(), id);
        }
        if (factMenuMap.size() != 0) {
            opsForHash.put(CoreConstants.MENUPRE, CoreConstants.FACTMENUKEY, factMenuMap);
            opsForHash.put(CoreConstants.MENUPRE, CoreConstants.VISTUALMENUKEY, vistualMenuMap);
        }
        redisTemplate.expire(CoreConstants.MENUPRE, 30, TimeUnit.DAYS);
        return vistualMenuMap;
    }

    @Override
    public List<Module> selectAllModules() {
        return customMapper.selectAllModules();
    }

    @Override
    public List<CustomRole> selectRolesByModuleCode(String moduleName,String clientId,String clientSecret) {
        return customRoleMapper.selectRolesByModuleCode(moduleName,clientId,clientSecret);
    }
}
