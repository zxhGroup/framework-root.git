package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysRolePermission;
import com.zxhtom.framework_common.repository.SysRolePermissionRepository;
import com.zxhtom.framework_common.service.SysRolePermissionService;

@Service
public class SysRolePermissionServiceImpl implements SysRolePermissionService {

	@Autowired
	private SysRolePermissionRepository sysRolePermissionRepository;

	@Override
	public PagedResult<SysRolePermission> selectSysRolePermissionsByPK(Long rolePermissionId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysRolePermission> result = new PagedResult<>();
		if(0==rolePermissionId){
			//查询所有
			rolePermissionId=null;
		}
		result = sysRolePermissionRepository.selectSysRolePermissionsByPK(rolePermissionId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysRolePermissionPK(Long rolePermissionId) {
		if(0==rolePermissionId){
			//删除所有
			rolePermissionId=null;
		}
		return sysRolePermissionRepository.deleteSysRolePermissionsByPK(rolePermissionId);
	}

	@Override
	public Integer updateSysRolePermission(SysRolePermission sysRolePermission) {
		return sysRolePermissionRepository.updateSysRolePermission(sysRolePermission);
	}

	@Override
	public Integer insertSysRolePermission(SysRolePermission sysRolePermission) {
		sysRolePermission.setRolePermissionId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysRolePermissionRepository.insertSysRolePermission(sysRolePermission);
	}
	
	@Override
	public Integer deleteSysRolePermissionPKBatch(List<Long> rolePermissionIds){
		for(Long rolePermissionId : rolePermissionIds){
			sysRolePermissionRepository.deleteSysRolePermissionsByPK(rolePermissionId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions) {
		return sysRolePermissionRepository.updateSysRolePermissionBatch(sysRolePermissions);
	}

	@Override
	public Integer insertSysRolePermissionBatch(List<SysRolePermission> sysRolePermissions) {
		for(SysRolePermission sysRolePermission : sysRolePermissions){
		sysRolePermission.setRolePermissionId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysRolePermissionRepository.insertSysRolePermissionBatch(sysRolePermissions);
	}

}
