package com.zxhtom.framework_common.mapper;


import java.util.List;

import com.zxhtom.framework_common.model.SysTask;

import tk.mybatis.mapper.common.Mapper;
public interface SysTaskMapper extends Mapper<SysTask>{

	/**
	 * 批量更新
	 * @param sysTasks
	 * @return
	 */
	Integer updateSysTaskBatch(List<SysTask> sysTasks);

	/**
	 * 批量新增
	 * @param sysTasks
	 * @return
	 */
	Integer insertSysTaskBatch(List<SysTask> sysTasks);
}
