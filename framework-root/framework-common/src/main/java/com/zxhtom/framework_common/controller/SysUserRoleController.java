package com.zxhtom.framework_common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.zxhtom.framework_common.dto.SysMutilUserRole;
import com.zxhtom.framework_common.dto.SysUserRoleDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.framework_common.model.SysUserRole;
import com.zxhtom.framework_common.service.SysUserRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/framework_common/sysUserRole")
@Api(tags = "framework_common sysUserRole", description = "用户角色关联信息表管理")
public class SysUserRoleController {

	private Logger logger = LogManager.getLogger(SysUserRoleController.class);
	@Autowired
	private SysUserRoleService sysUserRoleService;

	@RequestMapping(value = "/mutil_insert_user_role", method = RequestMethod.POST)
	@ApiOperation(value = "用户角色多对多新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertMutilUserAndRole(@ApiParam(value="新增数据")@RequestBody SysMutilUserRole sysMutilUserRole) {
		return sysUserRoleService.insertMutilUserAndRole(sysMutilUserRole);
	}

	@RequestMapping(value = "/dto/{userRoleId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有用户角色关联信息列表 传0表示查询所有")
	public PagedResult<SysUserRoleDto> selectSysUserRolesDtoByPK(@ApiParam(value = "用户角色关联主键", required = true) @PathVariable @NotNull Long userRoleId,
															  @ApiParam(value = "页码", required = false) @RequestParam Integer pageNumber,
															  @ApiParam(value = "页量", required = false) @RequestParam Integer pageSize) {
		return sysUserRoleService.selectSysUserRolesDtoByPK(userRoleId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/{userRoleId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有用户角色关联信息列表 传0表示查询所有")
	public PagedResult<SysUserRole> selectSysUserRolesByPK(@ApiParam(value = "用户角色关联主键", required = true) @PathVariable @NotNull Long userRoleId,
	@ApiParam(value = "页码", required = false) @RequestParam Integer pageNumber, 
	@ApiParam(value = "页量", required = false) @RequestParam Integer pageSize) {
		return sysUserRoleService.selectSysUserRolesByPK(userRoleId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysUserRoleByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long userRoleId) {
		return sysUserRoleService.deleteSysUserRolePK(userRoleId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) SysUserRole sysUserRole) {
		return sysUserRoleService.updateSysUserRole(sysUserRole);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody SysUserRole sysUserRole) {
		return sysUserRoleService.insertSysUserRole(sysUserRole);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteSysUserRoleByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> userRoleIds) {
		return sysUserRoleService.deleteSysUserRolePKBatch(userRoleIds);
	}
	@RequestMapping(value = "/deleteBatchByUserId", method = RequestMethod.DELETE)
	@ApiOperation(value = "通过用户ID批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteBatchByUserId(@ApiParam(value = "用户ID集合", required = true) @RequestBody @NotNull List<Long> userIds) {
		return sysUserRoleService.deleteBatchByUserId(userIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<SysUserRole> sysUserRoles) {
		return sysUserRoleService.updateSysUserRoleBatch(sysUserRoles);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<SysUserRole> sysUserRoles) {
		return sysUserRoleService.insertSysUserRoleBatch(sysUserRoles);
	}
}
