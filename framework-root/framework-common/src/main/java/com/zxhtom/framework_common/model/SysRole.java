package com.zxhtom.framework_common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "用户表")
@Table(name = "SYS_ROLE")
public class SysRole {

	/**
	 * 角色ID
	 */
	@Id
	private Long roleId;
	/**
	 * 角色编号
	 */
	private String roleCode;
	/**
	 * 角色名称
	 */
	private String roleName;
	/**
	 * 父角色ID，根角色ID是自己-1
	 */
	private Long parentRoleId;
	
	/**
	 * 设置：角色ID
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID
	 */
	public Long getRoleId() {
		return roleId;
	}
	/**
	 * 设置：角色编号
	 */
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	/**
	 * 获取：角色编号
	 */
	public String getRoleCode() {
		return roleCode;
	}
	/**
	 * 设置：角色名称
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	/**
	 * 获取：角色名称
	 */
	public String getRoleName() {
		return roleName;
	}
	/**
	 * 设置：父角色ID，根角色ID是自己-1
	 */
	public void setParentRoleId(Long parentRoleId) {
		this.parentRoleId = parentRoleId;
	}
	/**
	 * 获取：父角色ID，根角色ID是自己-1
	 */
	public Long getParentRoleId() {
		return parentRoleId;
	}

	@Override
	public String toString() {
		return "SysRole [roleId=" + roleId+",roleCode=" + roleCode + ",roleName=" + roleName + ",parentRoleId=" + parentRoleId + "]";
	}

}
