package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.github.pagehelper.PageHelper;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.framework_common.dto.SysMutilUserRole;
import com.zxhtom.framework_common.dto.SysUserRoleDto;
import com.zxhtom.framework_common.repository.impl.SysUserRepositoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysUserRole;
import com.zxhtom.framework_common.repository.SysUserRoleRepository;
import com.zxhtom.framework_common.service.SysUserRoleService;

@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

	private static Logger logger = LogManager.getLogger(SysUserRepositoryImpl.class);
	@Autowired
	private SysUserRoleRepository sysUserRoleRepository;

	@Override
	public PagedResult<SysUserRole> selectSysUserRolesByPK(Long userRoleId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysUserRole> result = new PagedResult<>();
		if(0==userRoleId){
			//查询所有
			userRoleId=null;
		}
		result = sysUserRoleRepository.selectSysUserRolesByPK(userRoleId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysUserRolePK(Long userRoleId) {
		if(0==userRoleId){
			//删除所有
			userRoleId=null;
		}
		return sysUserRoleRepository.deleteSysUserRolesByPK(userRoleId);
	}

	@Override
	public Integer updateSysUserRole(SysUserRole sysUserRole) {
		return sysUserRoleRepository.updateSysUserRole(sysUserRole);
	}

	@Override
	public Integer insertSysUserRole(SysUserRole sysUserRole) {
		sysUserRole.setUserRoleId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysUserRoleRepository.insertSysUserRole(sysUserRole);
	}
	
	@Override
	public Integer deleteSysUserRolePKBatch(List<Long> userRoleIds){
		for(Long userRoleId : userRoleIds){
			sysUserRoleRepository.deleteSysUserRolesByPK(userRoleId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysUserRoleBatch(List<SysUserRole> sysUserRoles) {
		return sysUserRoleRepository.updateSysUserRoleBatch(sysUserRoles);
	}

	@Override
	public Integer insertSysUserRoleBatch(List<SysUserRole> sysUserRoles) {
		for(SysUserRole sysUserRole : sysUserRoles){
		sysUserRole.setUserRoleId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysUserRoleRepository.insertSysUserRoleBatch(sysUserRoles);
	}

	@Override
	public PagedResult<SysUserRoleDto> selectSysUserRolesDtoByPK(Long userRoleId, Integer pageNumber, Integer pageSize) {
		if (pageNumber != null && pageSize != null) {
			PageHelper.startPage(pageNumber, pageSize);
		}

		return new PagedResult<>(sysUserRoleRepository.selectSysUserRolesDtoByPK(userRoleId));
	}

	@Override
	public Integer insertMutilUserAndRole(SysMutilUserRole sysMutilUserRole) {
		if (null == sysMutilUserRole) {
			throw new BusinessException("参数不合法");
		}
		List<SysUserRole> list = new ArrayList<>();
		for (Long userId : sysMutilUserRole.getUserIds()) {
			sysUserRoleRepository.deleteSysUserRolesByUserId(userId);
			for (Long roleId : sysMutilUserRole.getRoleIds()) {
				SysUserRole sysUserRole = new SysUserRole();
				sysUserRole.setUserRoleId(IdUtil.getInstance().getId());
				sysUserRole.setUserId(userId);
				sysUserRole.setRoleId(roleId);
				list.add(sysUserRole);
			}
		}
		try {
			sysUserRoleRepository.insertSysUserRoleBatch(list);
		} catch (DuplicateKeyException e) {
			logger.warn("新增用户角色有重复");
		}
		return 1;
	}

	@Override
	public Integer deleteBatchByUserId(List<Long> userIds) {
		return sysUserRoleRepository.deleteSysUserRolesByUserIds(userIds);
	}

}
