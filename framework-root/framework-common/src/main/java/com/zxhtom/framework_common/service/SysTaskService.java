package com.zxhtom.framework_common.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysTask;

public interface SysTaskService {

	/**
	 * 根据主键查询任务信息
	 * @param taskId 任务主键
	 * @return
	 */
	PagedResult<SysTask> selectSysTasksByPK(Long taskId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除任务信息
	 * @param taskId 任务主键
	 * @return
	 */
	Integer deleteSysTaskPK(Long taskId);

	/**
	 * 单条更新数据
	 * @param SysTask
	 * @return
	 */
	Integer updateSysTask(SysTask sysTask);

	/**
	 * 单条新增数据
	 * @param SysTask
	 * @return
	 */
	Integer insertSysTask(SysTask sysTask);
	
	/**
	 * 批量根据主键删除任务信息
	 * @param taskId 任务主键
	 * @return
	 */
	Integer deleteSysTaskPKBatch(List<Long> taskIds);
	
	/**
	 * 批量更新数据
	 * @param SysTasks
	 * @return
	 */
	Integer updateSysTaskBatch(List<SysTask> sysTasks);

	/**
	 * 批量新增数据
	 * @param SysTasks
	 * @return
	 */
	Integer insertSysTaskBatch(List<SysTask> sysTasks);
}
