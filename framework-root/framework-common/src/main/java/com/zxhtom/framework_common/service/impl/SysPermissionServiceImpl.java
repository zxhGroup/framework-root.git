package com.zxhtom.framework_common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.model.SysPermission;
import com.zxhtom.framework_common.repository.SysPermissionRepository;
import com.zxhtom.framework_common.service.SysPermissionService;

@Service
public class SysPermissionServiceImpl implements SysPermissionService {

	@Autowired
	private SysPermissionRepository sysPermissionRepository;

	@Override
	public PagedResult<SysPermission> selectSysPermissionsByPK(Long permissionId,Integer pageNumber, Integer pageSize) {
		PagedResult<SysPermission> result = new PagedResult<>();
		if(0==permissionId){
			//查询所有
			permissionId=null;
		}
		result = sysPermissionRepository.selectSysPermissionsByPK(permissionId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteSysPermissionPK(Long permissionId) {
		if(0==permissionId){
			//删除所有
			permissionId=null;
		}
		return sysPermissionRepository.deleteSysPermissionsByPK(permissionId);
	}

	@Override
	public Integer updateSysPermission(SysPermission sysPermission) {
		return sysPermissionRepository.updateSysPermission(sysPermission);
	}

	@Override
	public Integer insertSysPermission(SysPermission sysPermission) {
		sysPermission.setPermissionId(Long.valueOf(IdUtil.getInstance().getId()));
		return sysPermissionRepository.insertSysPermission(sysPermission);
	}
	
	@Override
	public Integer deleteSysPermissionPKBatch(List<Long> permissionIds){
		for(Long permissionId : permissionIds){
			sysPermissionRepository.deleteSysPermissionsByPK(permissionId);
		}
		return 1;
	}
	
	@Override
	public Integer updateSysPermissionBatch(List<SysPermission> sysPermissions) {
		return sysPermissionRepository.updateSysPermissionBatch(sysPermissions);
	}

	@Override
	public Integer insertSysPermissionBatch(List<SysPermission> sysPermissions) {
		for(SysPermission sysPermission : sysPermissions){
		sysPermission.setPermissionId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return sysPermissionRepository.insertSysPermissionBatch(sysPermissions);
	}

}
