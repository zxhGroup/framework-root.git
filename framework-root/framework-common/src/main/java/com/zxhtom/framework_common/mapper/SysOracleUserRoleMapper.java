package com.zxhtom.framework_common.mapper;

import com.zxhtom.framework_common.dto.SysUserRoleDto;
import com.zxhtom.framework_common.model.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author John
 */
public interface SysOracleUserRoleMapper {
    /**
     * 通过流水号查询用户角色信息
     * @param userRoleId
     * @return
     */
    List<SysUserRoleDto> selectSysUserRoleDtoByPK(@Param("userRoleId") Long userRoleId);

    /**
     * 批量新增
     * @param sysUserRoles
     * @return
     */
    Integer insertSysUserRoleBatch(@Param("sysUserRoles") List<SysUserRole> sysUserRoles);
}
