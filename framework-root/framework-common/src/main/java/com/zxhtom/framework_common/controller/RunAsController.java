package com.zxhtom.framework_common.controller;

import com.zxhtom.core.service.CustomService;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.framework_common.service.UserRunAsService;
import com.zxhtom.model.CustomUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @package com.zxhtom.framework_common.controller
 * @Class RunAsController
 * @Description 托管管理控制层
 * @Author zhangxinhua
 * @Date 19-6-3 下午5:13
 */
@RestController
@Api(tags = "framework_common role", description = "切换")
@RequestMapping(value = "/framework_common/role")
public class RunAsController {

    @Autowired
    private CustomService customService;
    @Autowired
    private UserRunAsService userRunAsService;

    @RequestMapping(value = "/switch", method = RequestMethod.GET)
    @ApiOperation(value = "切换用户权限，前提是别人已经将权限赋予给你")
    public Integer runas(@ApiParam(value = "切换到谁的权限(Id)") @RequestParam Long fromUserId) {
        Subject subject = SecurityUtils.getSubject();
        CustomUser currentUser = (CustomUser) subject.getPrincipal();
        if (fromUserId == currentUser.getUserId()) {
            throw new BusinessException("不能给自己授权");
        }
        if (userRunAsService.exists(fromUserId, currentUser.getUserId())) {
            CustomUser customUser = customService.selectUserByUserId(-1L);
            subject.runAs(new SimplePrincipalCollection(customUser, ""));
        } else {
            throw new BusinessException("请先申请该用户的授权");
        }
        return 1;
    }

    @RequestMapping(value = "/switchBack",method = RequestMethod.GET)
    public Integer switchBack() {
        Subject subject = SecurityUtils.getSubject();
        if(subject.isRunAs()) {
            subject.releaseRunAs();
        }
        return 1;
    }
}
