package com.zxhtom.framework_common.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.framework_common.mapper.SysTaskLogMapper;
import com.zxhtom.framework_common.model.SysTaskLog;
import com.zxhtom.framework_common.repository.SysTaskLogRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class SysTaskLogRepositoryImpl implements SysTaskLogRepository{

	@Autowired
	private SysTaskLogMapper sysTaskLogMapper;
	@Override
	public PagedResult<SysTaskLog> selectSysTaskLogsByPK(Long taskLogId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(SysTaskLog.class,false,false);
		example.createCriteria().andEqualTo("taskLogId", taskLogId);
		return new PagedResult<>(sysTaskLogMapper.selectByExample(example));
	}
	@Override
	public Integer deleteSysTaskLogsByPK(Long taskLogId) {
		Example example = new Example(SysTaskLog.class,false,false);
		example.createCriteria().andEqualTo("taskLogId", taskLogId);
		return sysTaskLogMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateSysTaskLog(SysTaskLog sysTaskLog) {
		return sysTaskLogMapper.updateByPrimaryKeySelective(sysTaskLog);
	}
	@Override
	public Integer insertSysTaskLog(SysTaskLog sysTaskLog) {
		return sysTaskLogMapper.insert(sysTaskLog);
	}
	
	@Override
	public Integer updateSysTaskLogBatch(List<SysTaskLog> sysTaskLogs) {
		return sysTaskLogMapper.updateSysTaskLogBatch(sysTaskLogs);
	}
	@Override
	public Integer insertSysTaskLogBatch(List<SysTaskLog> sysTaskLogs) {
		return sysTaskLogMapper.insertSysTaskLogBatch(sysTaskLogs);
	}
}
