$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysRole/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'roleId',formatter:commonFormat,unformat:commonUnFormat, name: 'roleId', index: 'ROLE_ID', width: 50, key: true },
			{ label: '角色编号',formatter:commonFormat,unformat:commonUnFormat, name: 'roleCode', index: 'ROLE_CODE', width: 80 },
			{ label: '角色名称',formatter:commonFormat,unformat:commonUnFormat, name: 'roleName', index: 'ROLE_NAME', width: 80 },
			{ label: '父角色ID，根角色ID是自己-1',formatter:commonFormat,unformat:commonUnFormat, name: 'parentRoleId', index: 'PARENT_ROLE_ID', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysRole: {},
		sysRoleModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysRoleModalId = true;
			vm.title = "新增";
			vm.sysRole = {};
		},
		update: function (event) {
			var roleId = getSelectedRow();
			if(roleId == null){
				return ;
			}
            vm.sysRoleModalId = true;
            vm.title = "修改";
            
            vm.getInfo(roleId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysRole.roleId == null ? "framework_common/sysRole/insert" : "framework_common/sysRole/update";
			var ajaxType = vm.sysRole.roleId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysRole),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysRoleModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var roleIds = getSelectedRows();
			if(roleIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysRole/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(roleIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(roleId){
			$.get(baseURL + "framework_common/sysRole/"+roleId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysRole = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});