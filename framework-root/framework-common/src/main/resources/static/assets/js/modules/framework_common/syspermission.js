$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysPermission/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'permissionId',formatter:commonFormat,unformat:commonUnFormat, name: 'permissionId', index: 'PERMISSION_ID', width: 50, key: true },
			{ label: '权限名称',formatter:commonFormat,unformat:commonUnFormat, name: 'permissionName', index: 'PERMISSION_NAME', width: 80 },
			{ label: '权限简要描述',formatter:commonFormat,unformat:commonUnFormat, name: 'permissionRemark', index: 'PERMISSION_REMARK', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysPermission: {},
		sysPermissionModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysPermissionModalId = true;
			vm.title = "新增";
			vm.sysPermission = {};
		},
		update: function (event) {
			var permissionId = getSelectedRow();
			if(permissionId == null){
				return ;
			}
            vm.sysPermissionModalId = true;
            vm.title = "修改";
            
            vm.getInfo(permissionId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysPermission.permissionId == null ? "framework_common/sysPermission/insert" : "framework_common/sysPermission/update";
			var ajaxType = vm.sysPermission.permissionId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysPermission),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysPermissionModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var permissionIds = getSelectedRows();
			if(permissionIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysPermission/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(permissionIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(permissionId){
			$.get(baseURL + "framework_common/sysPermission/"+permissionId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysPermission = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});