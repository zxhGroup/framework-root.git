$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysTaskLog/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'taskLogId',formatter:commonFormat,unformat:commonUnFormat, name: 'taskLogId', index: 'TASK_LOG_ID', width: 50, key: true },
			{ label: '写日志时间',formatter:commonFormat,unformat:commonUnFormat, name: 'logTime', index: 'LOG_TIME', width: 80 },
			{ label: '任务名称',formatter:commonFormat,unformat:commonUnFormat, name: 'taskName', index: 'TASK_NAME', width: 80 },
			{ label: '任务类名',formatter:commonFormat,unformat:commonUnFormat, name: 'jobClass', index: 'JOB_CLASS', width: 80 },
			{ label: '调度表达式',formatter:commonFormat,unformat:commonUnFormat, name: 'cronExpression', index: 'CRON_EXPRESSION', width: 80 },
			{ label: '任务参数',formatter:commonFormat,unformat:commonUnFormat, name: 'param', index: 'PARAM', width: 80 },
			{ label: '任务处理状况',formatter:commonFormat,unformat:commonUnFormat, name: 'state', index: 'STATE', width: 80 },
			{ label: '任务处理IP',formatter:commonFormat,unformat:commonUnFormat, name: 'ip', index: 'IP', width: 80 },
			{ label: '任务处理开始时间',formatter:commonFormat,unformat:commonUnFormat, name: 'startTime', index: 'START_TIME', width: 80 },
			{ label: '任务处理结束时间',formatter:commonFormat,unformat:commonUnFormat, name: 'endTime', index: 'END_TIME', width: 80 },
			{ label: '处理结果',formatter:commonFormat,unformat:commonUnFormat, name: 'result', index: 'RESULT', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysTaskLog: {},
		sysTaskLogModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysTaskLogModalId = true;
			vm.title = "新增";
			vm.sysTaskLog = {};
		},
		update: function (event) {
			var taskLogId = getSelectedRow();
			if(taskLogId == null){
				return ;
			}
            vm.sysTaskLogModalId = true;
            vm.title = "修改";
            
            vm.getInfo(taskLogId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysTaskLog.taskLogId == null ? "framework_common/sysTaskLog/insert" : "framework_common/sysTaskLog/update";
			var ajaxType = vm.sysTaskLog.taskLogId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysTaskLog),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysTaskLogModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var taskLogIds = getSelectedRows();
			if(taskLogIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysTaskLog/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(taskLogIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(taskLogId){
			$.get(baseURL + "framework_common/sysTaskLog/"+taskLogId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysTaskLog = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});