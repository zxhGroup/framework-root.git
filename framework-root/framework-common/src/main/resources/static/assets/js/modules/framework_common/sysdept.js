$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysDept/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'deptId',formatter:commonFormat,unformat:commonUnFormat, name: 'deptId', index: 'DEPT_ID', width: 50, key: true },
			{ label: '部门编码',formatter:commonFormat,unformat:commonUnFormat, name: 'deptCode', index: 'DEPT_CODE', width: 80 },
			{ label: '部门类型',formatter:commonFormat,unformat:commonUnFormat, name: 'deptType', index: 'DEPT_TYPE', width: 80 },
			{ label: '部门名称',formatter:commonFormat,unformat:commonUnFormat, name: 'deptName', index: 'DEPT_NAME', width: 80 },
			{ label: '父部门，根部门父部门是自己',formatter:commonFormat,unformat:commonUnFormat, name: 'parentDeptId', index: 'PARENT_DEPT_ID', width: 80 },
			{ label: '部门负责人',formatter:commonFormat,unformat:commonUnFormat, name: 'leaderId', index: 'LEADER_ID', width: 80 },
			{ label: '部门描述',formatter:commonFormat,unformat:commonUnFormat, name: 'remark', index: 'REMARK', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysDept: {},
		sysDeptModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysDeptModalId = true;
			vm.title = "新增";
			vm.sysDept = {};
		},
		update: function (event) {
			var deptId = getSelectedRow();
			if(deptId == null){
				return ;
			}
            vm.sysDeptModalId = true;
            vm.title = "修改";
            
            vm.getInfo(deptId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysDept.deptId == null ? "framework_common/sysDept/insert" : "framework_common/sysDept/update";
			var ajaxType = vm.sysDept.deptId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysDept),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysDeptModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var deptIds = getSelectedRows();
			if(deptIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysDept/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(deptIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(deptId){
			$.get(baseURL + "framework_common/sysDept/"+deptId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysDept = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});