var data = {
    roleId:1,
    roleName: 'My Tree',
    children:
        [
        { roleId:2,roleName: 'hello' },
        { roleId:3,roleName: 'wat' },
        {
            roleId:4,
            roleName: 'child folder',
            children: [
                {
                    roleId:5,
                    roleName: 'child folder',
                    children: [
                        { roleId:6,roleName: 'hello' },
                        { roleId:7,roleName: 'wat' }
                    ]
                },
                { roleId:8,roleName: 'hello' },
                { roleId:9,roleName: 'wat' },
                {
                    roleId:10,
                    roleName: 'child folder',
                    children: [
                        { roleId:11,roleName: 'hello' },
                        { roleId:12,roleName: 'wat' }
                    ]
                }
            ]
        }
]
};
$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysUserRole/dto/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [
            {label: '角色ID', hidden: true, name: 'roleId', index: 'ROLE_ID', width: 50, key: true},
            {label: '用户ID', hidden: true, name: 'userId', index: 'USER_ID', width: 50, key: true},
            {label: '用户名称',formatter:commonFormat,unformat:commonUnFormat, name: 'userName', index: 'USER_NAME', width: 80},
            {label: '角色名称',formatter:commonFormat,unformat:commonUnFormat, name: 'roleName', index: 'ROLE_NAME', width: 80},
            {label: '产品id', formatter:commonFormat,unformat:commonUnFormat,name: 'oauthClientName', index: 'OAUTH_CLIENT_ID', width: 80}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data() {
	    return {
        showList: true,
        treeData:{},
        userIds: [],
        deptIds: [],
        userList:[],
        deptData:[],
        treeName:'name',
        title: null,
        sysUserRole: {},
        sysUserRoleModalId:false,
        sysUserLayer:false,
        sysRoleLayer:false,
        loading:true,
        treeUserLoading:false,
        treeLoading:false,
        userChange:false,
        change:false,
        userListMap:{}
    }
    },
    watch:{
        deptData(val, oldVal){//普通的watch监听
            console.log("a: "+val, oldVal);
            vm.getDeptMap(this.deptData);
            vm.updateSelectdDeptAndUserList(this.deptData);
        },
        b:{//深度监听，可监听到对象、数组的变化
            handler(val, oldVal){
                console.log("b.c: "+val.c, oldVal.c);//但是这两个值打印出来却都是一样的
            },
            deep:true
        }
    },
    created:function(){
	    console.log('角色创建页面')
    },
	methods: {
	    getSelectedUser:function(opt){
            vm.sysUserRole.userIds = [];
            vm.sysUserRole.userNames = "";
            for(var index in opt){
                vm.sysUserRole.userIds.push(opt[index].value);
                vm.sysUserRole.userNames+=','+opt[index].label;
            }
            vm.sysUserRole.userNames = vm.sysUserRole.userNames.substring(1);
        },
        saveUserIds:function(){
            vm.sysUserLayer = false;
        },
        saveRoleIds:function(){
            vm.sysRoleLayer = false;
        },
	    getDeptMap: function(objArr){
            for(var index in objArr){
                var obj = objArr[index];
                if(obj.userList !=undefined){
                    vm.userListMap[obj.deptId]=obj.userList;
                }
                if (obj.children != undefined){
                    vm.getDeptMap(obj.children);
                }
            }
        },
        selectedObj: function (opt) {
            var keys = Object.keys(opt);
            var roleIds=[];
            var roleNames="";
            for(let [key,value] of Object.entries(opt.key)){
                roleIds.push(key);
			}
            for(let [key,value] of Object.entries(opt.name)){
                roleNames+=","+key;
            }
			this.sysUserRole['roleIds']=roleIds;
            this.sysUserRole.roleNames = roleNames.substring(1);
        },
        selectedUserObj:function(opt){
	        vm.userList = [];
            var keys = Object.keys(opt);
            for(let [key,value] of Object.entries(opt.key)){
                // vm.userList = Object.assign(vm.userList, vm.deptData[index].userList);
                var userTempList=this.array_union(vm.userList, vm.userListMap[key]);
                vm.userList = userTempList;
                // vm.userList=vm.deptData[index].userList;
            }
            vm.$forceUpdate();
            vm.userList.sort(function(a,b){
                10000*(a.userName.length-b.userName.length)+(a.userName-b.userName);
            });
        },
        updateSelectdDeptAndUserList:function(deptData){
	        console.log("进入默认选中用户数");
            for(var userIndex in vm.sysUserRole.userIds){
                for(var key in vm.userListMap){
                    //key是部门ID
                    for(var userKeyIndex in vm.userListMap[key]){
                        if(vm.userListMap[key][userKeyIndex].userId == vm.sysUserRole.userIds[userIndex]){
                            //填充之前置空
                            vm.sysUserRole.deptIds=[];
                            vm.userList=[];
                            //选中部门
                            vm.sysUserRole.deptIds.push(key);
                            //填充部门下用户
                            vm.userList=this.array_union(vm.userList, vm.userListMap[key]);
                            break;
                        }
                    }
                }
            }
        },
        initUser:function(){
            $.ajax({
                type: 'get',
                url: '/framework_common/sysUserDept/dto/0',
                contentType: "application/json",
                success: function(r){
                    if(r.code === 0){
                        vm.deptData=r.data.datas;
                        vm.userChange=true;
                        vm.sysUserLayer=true;
                        vm.treeUserLoading=true;
                    }else{
                        alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                                vm.loading = true
                            })},20);
                    }
                }
            });
        },
        initRoleTree: function(){
            $.ajax({
                type: 'get',
                url: '/framework_common/sysRole/selectCurrentTreeRole',
                contentType: "application/json",
                success: function(r){
                    if(r.code === 0){
                        vm.treeData=r.data;
                        vm.change = true;
                        vm.sysRoleLayer=true;
                        vm.treeLoading=true;
                    }else{
                        alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                                vm.loading = true
                            })},20);
                    }
                }
            });
		},
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysUserRoleModalId = true;
			vm.title = "新增";
		},
		update: function (event) {
			var userRoleId = getSelectedRow();
			if(userRoleId == null){
				return ;
			}
            vm.sysUserRoleModalId = true;
            vm.title = "修改";
            var rowId=$('#jqGrid').jqGrid('getGridParam','selrow');
            var rowData = $("#jqGrid").jqGrid('getRowData',rowId);
            vm.initTreeInfo(rowData);
		},
		saveOrUpdate: function (event) {
			var url = vm.sysUserRole.userRoleId == null ? "framework_common/sysUserRole/mutil_insert_user_role" : "framework_common/sysUserRole/update";
			var ajaxType = vm.sysUserRole.userRoleId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysUserRole),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysUserRoleModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
        array_union :function(a, b) { // 并集
            return this.array_remove_repeat(a.concat(b));
        },
        array_remove_repeat: function(a) { // 去重
            var r = [];
            for(var i = 0; i < a.length; i ++) {
                var flag = true;
                var temp = a[i];
                for(var j = 0; j < r.length; j ++) {
                    if(temp.userId === r[j].userId) {
                        flag = false;
                        break;
                    }
                }
                if(flag) {
                    r.push(temp);
                }
            }
            return r;
        },
		del: function (event) {
			var userRoleIds = getSelectedRows();
			if(userRoleIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysUserRole/deleteBatchByUserId",
                    contentType: "application/json",
				    data: JSON.stringify(userRoleIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
        initUserRoleInfo:function(treeData,objId,objName){
            var roleIds=[];
            var roleNames="";
            if(treeData instanceof Array){
                for(var index in treeData){
                    roleIds.push(treeData[index][objId]);
                    roleNames+=","+treeData[index][objName];
                }
            }else{
                if (treeData[objId].indexOf(',')) {
                    var ra = treeData[objId].split(',');
                    for(var i in ra){
                        roleIds.push(ra[i]);
                    }
                } else {
                    roleIds.push(treeData[objId]);
                }
                roleNames+=","+treeData[objName];
            }
            vm.sysUserRole[objId+'s']=roleIds;
            vm.sysUserRole[objName+'s'] = roleNames.substring(1);
        },
        initTreeInfo:function(treeData){
	        vm.initUserRoleInfo(treeData,'roleId','roleName')
	        vm.initUserRoleInfo(treeData,'userId','userName')
            /*var roleIds=[];
            var roleNames="";
            if(treeData instanceof Array){
                for(var index in treeData){
                    roleIds.push(treeData[index].roleId);
                    roleNames+=","+treeData[index].roleName;
                }
            }else{
                if (treeData.roleId.indexOf(',')) {
                    var ra = treeData.roleId.split(',');
                    for(var i in ra){
                        roleIds.push(ra[i]);
                    }
                } else {
                    roleIds.push(treeData.roleId);
                }
                roleNames+=","+treeData.roleName;
            }
            vm.sysUserRole['roleIds']=roleIds;
            vm.sysUserRole.roleNames = roleNames.substring(1);*/
        },
		getInfo: function(userRoleId){
            $.ajax({
                type: "get",
                url: baseURL + "framework_common/sysUserRole/dto/"+userRoleId,
                data:{"pageNumber":1,  "pageSize":10},
                cache:false,
                async:false,
                success: function(r){
                    // vm.sysUserRole = r.data.datas[0];
                    vm.initTreeInfo(r.data.datas);
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});