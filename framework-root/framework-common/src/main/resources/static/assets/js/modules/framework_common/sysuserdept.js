$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysUserDept/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'userDeptId',formatter:commonFormat,unformat:commonUnFormat, name: 'userDeptId', index: 'USER_DEPT_ID', width: 50, key: true },
			{ label: '用户ID',formatter:commonFormat,unformat:commonUnFormat, name: 'userId', index: 'USER_ID', width: 80 },
			{ label: '部门ID',formatter:commonFormat,unformat:commonUnFormat, name: 'deptId', index: 'DEPT_ID', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysUserDept: {},
		sysUserDeptModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysUserDeptModalId = true;
			vm.title = "新增";
			vm.sysUserDept = {};
		},
		update: function (event) {
			var userDeptId = getSelectedRow();
			if(userDeptId == null){
				return ;
			}
            vm.sysUserDeptModalId = true;
            vm.title = "修改";
            
            vm.getInfo(userDeptId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysUserDept.userDeptId == null ? "framework_common/sysUserDept/insert" : "framework_common/sysUserDept/update";
			var ajaxType = vm.sysUserDept.userDeptId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysUserDept),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysUserDeptModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var userDeptIds = getSelectedRows();
			if(userDeptIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysUserDept/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(userDeptIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(userDeptId){
			$.get(baseURL + "framework_common/sysUserDept/"+userDeptId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysUserDept = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});