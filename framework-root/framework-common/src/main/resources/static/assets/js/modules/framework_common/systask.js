$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysTask/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'taskId',formatter:commonFormat,unformat:commonUnFormat, name: 'taskId', index: 'TASK_ID', width: 50, key: true },
			{ label: '任务名称',formatter:commonFormat,unformat:commonUnFormat, name: 'taskName', index: 'TASK_NAME', width: 80 },
			{ label: '任务类名',formatter:commonFormat,unformat:commonUnFormat, name: 'jobClass', index: 'JOB_CLASS', width: 80 },
			{ label: '调度表达式',formatter:commonFormat,unformat:commonUnFormat, name: 'cronExpression', index: 'CRON_EXPRESSION', width: 80 },
			{ label: '任务参数',formatter:commonFormat,unformat:commonUnFormat, name: 'param', index: 'PARAM', width: 80 },
			{ label: '定时任务处理服务器编号',formatter:commonFormat,unformat:commonUnFormat, name: 'serverId', index: 'SERVER_ID', width: 80 },
			{ label: '启用否',formatter:commonFormat,unformat:commonUnFormat, name: 'enable', index: 'ENABLE', width: 80 },
			{ label: '加载任务后立即执行',formatter:commonFormat,unformat:commonUnFormat, name: 'immediately', index: 'IMMEDIATELY', width: 80 },
			{ label: '备注',formatter:commonFormat,unformat:commonUnFormat, name: 'remark', index: 'REMARK', width: 80 },
			{ label: '任务状态',formatter:commonFormat,unformat:commonUnFormat, name: 'taskStatus', index: 'TASK_STATUS', width: 80 },
			{ label: '定时任务运行服务器IP',formatter:commonFormat,unformat:commonUnFormat, name: 'runIp', index: 'RUN_IP', width: 80 },
			{ label: '最后一次处理状况',formatter:commonFormat,unformat:commonUnFormat, name: 'lastState', index: 'LAST_STATE', width: 80 },
			{ label: '最后一次处理IP',formatter:commonFormat,unformat:commonUnFormat, name: 'lastIp', index: 'LAST_IP', width: 80 },
			{ label: '最后一次处理开始时间',formatter:commonFormat,unformat:commonUnFormat, name: 'lastStartTime', index: 'LAST_START_TIME', width: 80 },
			{ label: '最后一次处理结束时间',formatter:commonFormat,unformat:commonUnFormat, name: 'lastEndTime', index: 'LAST_END_TIME', width: 80 },
			{ label: '最后一次处理结果',formatter:commonFormat,unformat:commonUnFormat, name: 'lastResult', index: 'LAST_RESULT', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysTask: {},
		sysTaskModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysTaskModalId = true;
			vm.title = "新增";
			vm.sysTask = {};
		},
		update: function (event) {
			var taskId = getSelectedRow();
			if(taskId == null){
				return ;
			}
            vm.sysTaskModalId = true;
            vm.title = "修改";
            
            vm.getInfo(taskId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysTask.taskId == null ? "framework_common/sysTask/insert" : "framework_common/sysTask/update";
			var ajaxType = vm.sysTask.taskId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysTask),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysTaskModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var taskIds = getSelectedRows();
			if(taskIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysTask/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(taskIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(taskId){
			$.get(baseURL + "framework_common/sysTask/"+taskId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysTask = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});