$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysRolePermission/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'rolePermissionId',formatter:commonFormat,unformat:commonUnFormat, name: 'rolePermissionId', index: 'ROLE_PERMISSION_ID', width: 50, key: true },
			{ label: '权限ID',formatter:commonFormat,unformat:commonUnFormat, name: 'permissionId', index: 'PERMISSION_ID', width: 80 },
			{ label: '角色ID',formatter:commonFormat,unformat:commonUnFormat, name: 'roleId', index: 'ROLE_ID', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysRolePermission: {},
		sysRolePermissionModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysRolePermissionModalId = true;
			vm.title = "新增";
			vm.sysRolePermission = {};
		},
		update: function (event) {
			var rolePermissionId = getSelectedRow();
			if(rolePermissionId == null){
				return ;
			}
            vm.sysRolePermissionModalId = true;
            vm.title = "修改";
            
            vm.getInfo(rolePermissionId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysRolePermission.rolePermissionId == null ? "framework_common/sysRolePermission/insert" : "framework_common/sysRolePermission/update";
			var ajaxType = vm.sysRolePermission.rolePermissionId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysRolePermission),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysRolePermissionModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var rolePermissionIds = getSelectedRows();
			if(rolePermissionIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysRolePermission/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(rolePermissionIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(rolePermissionId){
			$.get(baseURL + "framework_common/sysRolePermission/"+rolePermissionId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysRolePermission = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});