$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysMenu/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'menuId',formatter:commonFormat,unformat:commonUnFormat, name: 'menuId', index: 'MENU_ID', width: 50, key: true },
			{ label: '菜单名称',formatter:commonFormat,unformat:commonUnFormat, name: 'menuName', index: 'MENU_NAME', width: 80 },
			{ label: '菜单编码',formatter:commonFormat,unformat:commonUnFormat, name: 'menuCode', index: 'MENU_CODE', width: 80 },
			{ label: '菜单链接',formatter:commonFormat,unformat:commonUnFormat, name: 'menuUrl', index: 'MENU_URL', width: 80 },
			{ label: '菜单类型 , 0真实菜单；1是父级虚拟菜单',formatter:commonFormat,unformat:commonUnFormat, name: 'menuType', index: 'MENU_TYPE', width: 80 },
			{ label: '模块ID',formatter:commonFormat,unformat:commonUnFormat, name: 'moduleId', index: 'MODULE_ID', width: 80 },
			{ label: '父级菜单ID，根菜单的父菜单是自己',formatter:commonFormat,unformat:commonUnFormat, name: 'parentMenuId', index: 'PARENT_MENU_ID', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysMenu: {},
		sysMenuModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysMenuModalId = true;
			vm.title = "新增";
			vm.sysMenu = {};
		},
		update: function (event) {
			var menuId = getSelectedRow();
			if(menuId == null){
				return ;
			}
            vm.sysMenuModalId = true;
            vm.title = "修改";
            
            vm.getInfo(menuId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysMenu.menuId == null ? "framework_common/sysMenu/insert" : "framework_common/sysMenu/update";
			var ajaxType = vm.sysMenu.menuId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysMenu),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysMenuModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var menuIds = getSelectedRows();
			if(menuIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysMenu/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(menuIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(menuId){
			$.get(baseURL + "framework_common/sysMenu/"+menuId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysMenu = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});