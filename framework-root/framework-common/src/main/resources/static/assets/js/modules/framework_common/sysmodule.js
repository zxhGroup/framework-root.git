$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'framework_common/sysModule/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'moduleId',formatter:commonFormat,unformat:commonUnFormat, name: 'moduleId', index: 'MODULE_ID', width: 50, key: true },
			{ label: '模块名称',formatter:commonFormat,unformat:commonUnFormat, name: 'moduleName', index: 'MODULE_NAME', width: 80 },
			{ label: '模块编码',formatter:commonFormat,unformat:commonUnFormat, name: 'moduleCode', index: 'MODULE_CODE', width: 80 },
			{ label: '模块链接',formatter:commonFormat,unformat:commonUnFormat, name: 'moduleUrl', index: 'MODULE_URL', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sysModule: {},
		sysModuleModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.sysModuleModalId = true;
			vm.title = "新增";
			vm.sysModule = {};
		},
		update: function (event) {
			var moduleId = getSelectedRow();
			if(moduleId == null){
				return ;
			}
            vm.sysModuleModalId = true;
            vm.title = "修改";
            
            vm.getInfo(moduleId)
		},
		saveOrUpdate: function (event) {
			var url = vm.sysModule.moduleId == null ? "framework_common/sysModule/insert" : "framework_common/sysModule/update";
			var ajaxType = vm.sysModule.moduleId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.sysModule),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.sysModuleModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var moduleIds = getSelectedRows();
			if(moduleIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "framework_common/sysModule/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(moduleIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(moduleId){
			$.get(baseURL + "framework_common/sysModule/"+moduleId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.sysModule = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});