package org.framework.common;

import com.zxhtom.framework_common.Application;
import com.zxhtom.utils.RPCClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @package org.framework.common
 * @Class SpringTest
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-19 下午2:31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SpringTest {

    @Test
    public void dispatchTest() {
        RPCClient.getResultFromUrlParam("/", null);
    }
}
