package com.zxhtom.framework_generator_code.service;

import java.util.List;
import java.util.Map;

import com.zxhtom.framework_generator_code.entity.Column;
import com.zxhtom.framework_generator_code.entity.Table;
import com.zxhtom.framework_generator_code.model.GeneratorData;

import javax.sql.DataSource;

public interface SysGeneratorService {

	public List<Table> queryList(Map<String, Object> map);

	public int queryTotal(Map<String, Object> map) ;

	public Table queryTable(String tableName) ;

	public List<Column> queryColumns(String tableName);

	public byte[] generatorCode(List<String> tableNames) ;

	/**
	 * 初始化数据库连接
	 * @param generatorData
	 * @return
	 */
    DataSource initDataSource(GeneratorData generatorData);
}
