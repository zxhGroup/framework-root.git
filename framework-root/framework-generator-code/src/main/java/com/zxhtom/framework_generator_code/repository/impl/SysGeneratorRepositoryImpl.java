package com.zxhtom.framework_generator_code.repository.impl;

import java.util.List;
import java.util.Map;

import com.zxhtom.CoreConstants;
import com.zxhtom.framework_generator_code.annotation.DataSource;
import com.zxhtom.framework_generator_code.annotation.OthersSource;
import com.zxhtom.framework_generator_code.service.SqlFactory;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.framework_generator_code.entity.Column;
import com.zxhtom.framework_generator_code.entity.Table;
import com.zxhtom.framework_generator_code.mapper.SysGeneratorMapper;
import com.zxhtom.framework_generator_code.mapper.SysGeneratorOracleMapper;
import com.zxhtom.framework_generator_code.repository.SysGeneratorRepository;
@Repository
@OthersSource
public class SysGeneratorRepositoryImpl implements SysGeneratorRepository {

	@Autowired
	private SqlFactory sqlFactory;
	@Autowired
	@DataSource
	private DruidDataSource dataSource;
	@Autowired
	@OthersSource
	private SysGeneratorMapper sysGeneratorMapper;
	@Autowired
	@OthersSource
	private SysGeneratorOracleMapper sysGeneratorOracleMapper;
	private void initMapper(){
		//SqlSession sqlSession = sqlFactory.getSqlFactory().openSession();
		//sysGeneratorOracleMapper = sqlSession.getMapper(SysGeneratorOracleMapper.class);
		//sysGeneratorMapper = sqlSession.getMapper(SysGeneratorMapper.class);
		//dataSource = (DruidDataSource) sqlSession.getConfiguration().getEnvironment().getDataSource();
	}

	private void closeMapper() {

	}
	@Override
	public List<Table> queryList(Map<String, Object> map) {
		initMapper();
		if(oracleDataSource()){
			return sysGeneratorOracleMapper.queryList(map);
		}
		List<Table> tables = sysGeneratorMapper.queryList(map);
		closeMapper();
		return tables;
	}

	@Override
	public int queryTotal(Map<String, Object> map) {
		initMapper();
		if(oracleDataSource()){
			return sysGeneratorOracleMapper.queryTotal(map);
		}
		int total = sysGeneratorMapper.queryTotal(map);
		closeMapper();
		return total;
	}

	private boolean oracleDataSource() {
		return CoreConstants.DATA_SOURCE.equals(this.dataSource.getDriverClassName());
	}

	@Override
	public Table queryTable(String tableName) {
		initMapper();
		if(oracleDataSource()){
			return sysGeneratorOracleMapper.queryTable(tableName);
		}
		Table table = sysGeneratorMapper.queryTable(tableName);
		closeMapper();
		return table;
	}

	@Override
	public List<Column> queryColumns(String tableName) {
		initMapper();
		if(oracleDataSource()){
			return sysGeneratorOracleMapper.queryColumns(tableName);
		}
		List<Column> columns = sysGeneratorMapper.queryColumns(tableName);
		closeMapper();
		return columns;
	}
	
}
