package com.zxhtom.framework_generator_code.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.zxhtom.CoreConstants;
import com.zxhtom.framework_generator_code.model.GeneratorData;
import com.zxhtom.framework_generator_code.service.SqlFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zxhtom.annotation.TokenNot;
import com.zxhtom.framework_generator_code.entity.Table;
import com.zxhtom.framework_generator_code.service.SysGeneratorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 代码生成器
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年12月19日 下午9:12:58
 */
@RestController
@RequestMapping("/framework_generator/generator")
@Api(tags = { "framework_generator generator" }, description = "代码生成器")
public class SysGeneratorController {
	@Autowired
	private SysGeneratorService sysGeneratorService;
	@Autowired
	private SqlFactory sqlFactory;

	/**
	 * 生成代码
	 */
	@RequestMapping(value = "/tableList",method=RequestMethod.GET)
	@ApiOperation(value = "获取数据源内的表")
	public List<Table> tableList(HttpServletRequest request , @ApiParam(value = "设置自动生成代码的数据源", required = false) @RequestBody GeneratorData generatorData) throws IOException {
		return sysGeneratorService.queryList(null);
	}

	/**
	 * 生成代码
	 */
	@RequestMapping(value = "/dataFrom",method=RequestMethod.POST)
	@ApiOperation(value = "设置自动生成代码的数据源")
	public List<Table> dataFrom(HttpServletRequest request , @ApiParam(value = "设置自动生成代码的数据源", required = false) @RequestBody GeneratorData generatorData) throws IOException {
		DataSource dataSource = sysGeneratorService.initDataSource(generatorData);
		HttpSession session = request.getSession();
		if (session != null) {
			session.setAttribute(CoreConstants.GENERATOR,generatorData);
		}
		return sysGeneratorService.queryList(null);
	}
	/**
	 * 生成代码
	 */
	@RequestMapping(value = "/code",method=RequestMethod.GET)
	@ApiOperation(value = "根据表名生成全套代码xml-->java-->html")
	@TokenNot
	public void code(HttpServletRequest request , HttpServletResponse response, @ApiParam(value = "表名集合system", required = false) @RequestParam List<String> tableNames) throws IOException {
		//先从缓存获取数据源
		GeneratorData dataSource = (GeneratorData) request.getSession().getAttribute(CoreConstants.GENERATOR);
		if (dataSource != null) {
			sysGeneratorService.initDataSource(dataSource);
		}
		if(null==tableNames||CollectionUtils.isEmpty(tableNames)||"system".equalsIgnoreCase(tableNames.get(0))){
			List<Table> tableList = sysGeneratorService.queryList(null);
			for (Table table : tableList) {
				if(table.getTableName().toLowerCase().matches("sys_.*")){
					//系统表
					tableNames.add(table.getTableName());
				}
			}
		} else if ("all".equalsIgnoreCase(tableNames.get(0))) {
			List<Table> tableList = sysGeneratorService.queryList(null);
			for (Table table : tableList) {
				if (table.getTableName().toLowerCase().matches("sys_.*")) {
					//系统表
					continue;
				}
				tableNames.add(table.getTableName());
			}
		}
		tableNames.remove("system");
		byte[] data = sysGeneratorService.generatorCode(tableNames);

		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\"zxhtom.zip\"");
		response.addHeader("Content-Length", "" + data.length);
		response.setContentType("application/octet-stream; charset=UTF-8");

		IOUtils.write(data, response.getOutputStream());
	}
}
