/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.zxhtom.framework_generator_code.util;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.zxhtom.Exception.CommonException;

import com.zxhtom.CoreConstants;
import com.zxhtom.framework_generator_code.entity.Column;
import com.zxhtom.framework_generator_code.entity.ColumnEntity;
import com.zxhtom.framework_generator_code.entity.Table;
import com.zxhtom.framework_generator_code.entity.TableEntity;
import com.zxhtom.framework_generator_code.model.GeneratorCodeProperties;
import com.zxhtom.utils.DateUtil;

/**
 * 代码生成器 工具类
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年12月19日 下午11:40:24
 */
public class GenUtils {

	public static List<String> getTemplates() {
		List<String> templates = new ArrayList<String>();
		templates.add("template/Model.java.vm");
		templates.add("template/Service.java.vm");
		templates.add("template/ServiceImpl.java.vm");
		templates.add("template/Repository.java.vm");
		templates.add("template/RepositoryImpl.java.vm");
		templates.add("template/Controller.java.vm");
		templates.add("template/list.html.vm");
		templates.add("template/list.js.vm");
		templates.add("template/menu.sql.vm");
		templates.add("template/Mapper.java.vm");
		templates.add("template/Mapper.xml.vm");
		templates.add("template/Application.java.vm");
		return templates;
	}

	/**
	 * 生成代码
	 * 
	 * @throws CommonException
	 */
	public static void generatorCode(GeneratorCodeProperties properties, Table table, List<Column> columns, ZipOutputStream zip) throws CommonException {
		// 配置信息
		Configuration config = getConfig(properties);
		boolean hasBigDecimal = false;
		// 表信息
		TableEntity tableEntity = new TableEntity();
		tableEntity.setTableName(table.getTableName());
		if (table.getTableComment() == null || "".equals(table.getTableComment())) {
			tableEntity.setComments(table.getTableName());
		} else {
			tableEntity.setComments(table.getTableComment());
		}
		// 表名转换成Java类名
		String className = tableToJava(tableEntity.getTableName(), config.getString("tablePrefix"));
		tableEntity.setClassName(className);
		tableEntity.setClassname(StringUtils.uncapitalize(className));

		// 列信息
		List<ColumnEntity> columsList = new ArrayList<>();
		for (Column column : columns) {
			ColumnEntity columnEntity = new ColumnEntity();
			columnEntity.setColumnName(column.getColumnName());
			columnEntity.setDataType(column.getDataType());
			if (column.getColumnComment() == null || "".equals(column.getColumnComment())) {
				columnEntity.setComments(column.getColumnName());
			} else {
				columnEntity.setComments(column.getColumnComment());
			}
			columnEntity.setExtra(column.getExtra());

			// 列名转换成Java属性名
			String attrName = columnToJava(columnEntity.getColumnName());
			columnEntity.setAttrName(attrName);
			columnEntity.setAttrname(StringUtils.uncapitalize(attrName));

			// 列的数据类型，转换成Java类型
			String attrType = "String";
			if("number".equalsIgnoreCase(columnEntity.getDataType().toString())){
				//>24 bigdevimal;>10 long ; >4 integer ; =1 boolean ; else short
				Integer precision = column.getDataPrecision();
				if(precision>24){
					//bigdevimal
					attrType=BigDecimal.class.getSimpleName();
				}else if(precision>10){
					//long
					attrType=Long.class.getSimpleName();
				}else if(precision>4){
					//integer
					attrType=Integer.class.getSimpleName();
				}else if(precision==1){
					//boolean
					attrType=Integer.class.getSimpleName();
				}else {
					//short
					attrType=Short.class.getSimpleName();
				}
			}else {
				String text = columnEntity.getDataType().toLowerCase();
				String regex = "[a-zA-Z0-9]*";
				Pattern compile = Pattern.compile(regex);
				Matcher matcher = compile.matcher(text);
				if(matcher.find()){
					text = matcher.group(0);
				}
				attrType = config.getString(text, "String");
			}
			columnEntity.setAttrType(attrType);
			if (!hasBigDecimal && attrType.equals("BigDecimal")) {
				hasBigDecimal = true;
			}
			// 是否主键
			if (column.getColumnKey()!=null&&!"".equalsIgnoreCase(column.getColumnKey().toString()) && tableEntity.getPk() == null) {
				tableEntity.setPk(columnEntity);
			}

			columsList.add(columnEntity);
		}
		tableEntity.setColumns(columsList);

		// 没主键，则第一个字段为主键
		if (tableEntity.getPk() == null) {
			tableEntity.setPk(tableEntity.getColumns().get(0));
		}

		// 设置velocity资源加载器
		Properties prop = new Properties();
		prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.init(prop);

		String mainPath = config.getString("mainPath");
		mainPath = StringUtils.isBlank(mainPath) ? "com.zxhtom" : mainPath;

		// 封装模板数据
		Map<String, Object> map = new HashMap<>();
		map.put("tableName", tableEntity.getTableName());
		map.put("comments", tableEntity.getComments());
		map.put("pk", tableEntity.getPk());
		map.put("className", tableEntity.getClassName());
		map.put("classname", tableEntity.getClassname());
		map.put("pathName", tableEntity.getClassname().toLowerCase());
		map.put("columns", tableEntity.getColumns());
		map.put("hasBigDecimal", hasBigDecimal);
		map.put("mainPath", mainPath);
		map.put("package", config.getString("package"));
		map.put("moduleName", config.getString("moduleName"));
		map.put("firstUpModuleName", firstUpName(config.getString("moduleName")));
		map.put("author", config.getString("author"));
		map.put("email", config.getString("email"));
		map.put("datetime", DateUtil.format(new Date(), CoreConstants.DATETIME_FORMAT));
		VelocityContext context = new VelocityContext(map);

		// 获取模板列表
		List<String> templates = getTemplates();
		for (int i = 0; i < templates.size(); i++) {
			String template = templates.get(i);
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);

			try {
				// 添加到zip
				zip.putNextEntry(new ZipEntry(getFileName(template, tableEntity.getClassName(), config.getString("package"), config.getString("moduleName"))));
				IOUtils.write(sw.toString(), zip, "UTF-8");
				IOUtils.closeQuietly(sw);
				zip.closeEntry();
			} catch (IOException e) {
				if ("duplicate".equals(e.getMessage().toString().substring(0, 9))) {
					return;
				}
				throw new CommonException("渲染模板失败，表名：" + tableEntity.getTableName(), e);
			}
		}
	}

	/**
	 * 首字母大写
	 */
	public static String firstUpName(String moduleName) {
		return moduleName.substring(0, 1).toUpperCase() + moduleName.substring(1);
	}

	/**
	 * 列名转换成Java属性名
	 */
	public static String columnToJava(String columnName) {
		return WordUtils.capitalizeFully(columnName, new char[] { '_' }).replace("_", "");
	}

	/**
	 * 表名转换成Java类名
	 */
	public static String tableToJava(String tableName, String tablePrefix) {
		if (StringUtils.isNotBlank(tablePrefix)) {
			tableName = tableName.replace(tablePrefix, "");
		}
		return columnToJava(tableName);
	}

	/**
	 * 获取配置信息
	 * 
	 * @throws CommonException
	 */
	public static Configuration getConfig(GeneratorCodeProperties properties) throws CommonException {
		try {
			Configuration configuration = new PropertiesConfiguration("generator.properties");
			if (StringUtils.isNotBlank(properties.getPackageName())) {
				configuration.setProperty("package", properties.getPackageName());
			}
			if (StringUtils.isNotBlank(properties.getCodeModuleName())) {
				configuration.setProperty("moduleName", properties.getCodeModuleName());
			}
			return configuration;
		} catch (ConfigurationException e) {
			throw new CommonException("获取配置文件失败，", e);
		}
	}

	/**
	 * 获取文件名
	 */
	public static String getFileName(String template, String className, String packageName, String moduleName) {
		String packagePath = "main" + File.separator + "java" + File.separator;
		if (StringUtils.isNotBlank(packageName)) {
			packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
		}
		if (template.contains("Application.java.vm")) {
			return packagePath + File.separator + firstUpName(moduleName) + "Application.java";
		}
		if (template.contains("Model.java.vm")) {
			return packagePath + "model" + File.separator + className + ".java";
		}

		if (template.contains("Mapper.java.vm")) {
			return packagePath + "mapper" + File.separator + className + "Mapper.java";
		}

		if (template.contains("Service.java.vm")) {
			return packagePath + "service" + File.separator + className + "Service.java";
		}

		if (template.contains("ServiceImpl.java.vm")) {
			return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
		}

		if (template.contains("Repository.java.vm")) {
			return packagePath + "repository" + File.separator + className + "Repository.java";
		}

		if (template.contains("RepositoryImpl.java.vm")) {
			return packagePath + "repository" + File.separator + "impl" + File.separator + className + "RepositoryImpl.java";
		}

		if (template.contains("Controller.java.vm")) {
			return packagePath + "controller" + File.separator + className + "Controller.java";
		}

		if (template.contains("Mapper.xml.vm")) {
			return packagePath + "mapper" + File.separator + className + "Mapper.xml";
			// return "main" + File.separator + "resources" + File.separator +
			// "mapper" + File.separator + moduleName + File.separator +
			// className + "Mapper.xml";
		}

		if (template.contains("list.html.vm")) {
			return "main" + File.separator + "resources" + File.separator + "templates" + File.separator + "modules" + File.separator + moduleName + File.separator + className.toLowerCase() + ".html";
		}

		if (template.contains("list.js.vm")) {
			return "main" + File.separator + "resources" + File.separator + "static" + File.separator+ "assets" + File.separator + "js" + File.separator + "modules" + File.separator + moduleName + File.separator + className.toLowerCase() + ".js";
		}

		if (template.contains("menu.sql.vm")) {
			return className.toLowerCase() + "_menu.sql";
		}

		return null;
	}
}
