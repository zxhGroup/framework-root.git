package com.zxhtom.framework_generator_code.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author John
 */
public class JdbcUtil {
    private static JdbcUtil jdbcUtil;
    private static Map<String , String> map = new HashMap<>();
    public static JdbcUtil getInstance(){
        if (jdbcUtil == null) {
            jdbcUtil = new JdbcUtil();
            initDataSourceUrl();
        }
        return jdbcUtil;
    }
    public static void initDataSourceUrl(){
        String oracleDataUrl = "jdbc:oracle:thin:@%1$s:%2$s:%3$s";
        String mysqlDataUrl = "jdbc:mysql://%1$s:%2$d/%3$s?useUnicode=true&amp;characterEncoding=utf8";
        String oracleDriver = "oracle.jdbc.driver.OracleDriver";
        String mysqlDriver = "com.mysql.jdbc.Driver";
        map.put(mysqlDriver, mysqlDataUrl);
        map.put(oracleDriver, oracleDataUrl);
    }
    public String getUrl(String ip , Integer port , String serverName,String driverClassName){
        String url = map.get(driverClassName);
        return String.format(url, ip, port, serverName);
    }

    public static void main(String[] args) {
        JdbcUtil jdbcUtil = JdbcUtil.getInstance();
        String chat = jdbcUtil.getUrl("192.168.0.103", 3306, "chat", "oracle.jdbc.driver.OracleDriver");
        System.out.println(chat);
    }
}
