package com.zxhtom.framework_generator_code.aspect;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.framework_generator_code.annotation.DataSource;
import com.zxhtom.framework_generator_code.annotation.OthersSource;
import com.zxhtom.framework_generator_code.service.SqlFactory;
import com.zxhtom.utils.ContextUtil;
import javassist.*;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class GeneratorAspect {
    private Logger logger = LogManager.getLogger(GeneratorAspect.class);

    @Pointcut("@within(com.zxhtom.framework_generator_code.annotation.OthersSource)")
    public void source() {
    }

    @Before("source()")
    public void before(JoinPoint joinPoint) throws Exception {
        SqlFactory sqlFactory = ContextUtil.getApplicationContext().getBean(SqlFactory.class);
        SqlSession sqlSession = sqlFactory.getSqlFactory().openSession();
        Object[] args = joinPoint.getArgs();
        System.out.println(joinPoint.getTarget());
        String classType = joinPoint.getTarget().getClass().getName();
        Class<?> clazz = Class.forName(classType);
        //获取类属性
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            OthersSource annotation = field.getAnnotation(OthersSource.class);
            if (annotation != null) {
                field.setAccessible(true);
                field.set(joinPoint.getTarget(), sqlSession.getMapper(field.getType()));
            }
            if (field.getAnnotation(DataSource.class) != null) {
                field.setAccessible(true);
                field.set(joinPoint.getTarget(), sqlSession.getConfiguration().getEnvironment().getDataSource());
            }
        }
        System.out.println("@@@@@@");
    }

    @AfterReturning(value = "source()", returning = "object")
    public void after(JoinPoint joinPoint, Object object) {
    }


    private void initMapper(){
        //SqlSession sqlSession = sqlFactory.getSqlFactory().openSession();
        //sysGeneratorOracleMapper = sqlSession.getMapper(SysGeneratorOracleMapper.class);
        //sysGeneratorMapper = sqlSession.getMapper(SysGeneratorMapper.class);
        //dataSource = (DruidDataSource) sqlSession.getConfiguration().getEnvironment().getDataSource();
    }
    /**
     * @return Map<String   ,   Object>
     * @Description 获取字段名和字段值
     * @Author 刘俊重
     * @date 2017年7月6日 
     */
    private Map<String, Object> getFieldsName(Class cls, String clazzName, String methodName, Object[] args) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        ClassPool pool = ClassPool.getDefault();
        ClassClassPath classPath = new ClassClassPath(cls);
        pool.insertClassPath(classPath);

        CtClass cc = pool.get(clazzName);
        CtMethod cm = cc.getDeclaredMethod(methodName);
        MethodInfo methodInfo = cm.getMethodInfo();
        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
        LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
        if (attr == null) {
            // exception    
        }
        int pos = Modifier.isStatic(cm.getModifiers()) ? 0 : 1;
        for (int i = 0; i < cm.getParameterTypes().length; i++) {
            if (args == null || args[i] == null) {
                continue;
            }
            map.put(attr.variableName(i + pos), args[i]);//paramNames即参数名    
        }
        return map;
    }
}
