package com.zxhtom.framework_generator_code.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.enums.*;
import com.zxhtom.framework_generator_code.service.SqlFactory;
import com.zxhtom.utils.ClassUtil;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.EnumOrdinalTypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.util.UUID;

public class SqlFactoryImpl implements SqlFactory {
    private UUID uuid;
    public SqlFactoryImpl(){
        uuid = UUID.randomUUID();
    }

    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void setDataSource(DruidDataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:com/zxhtom/**/mapper/*.xml"));
        sqlSessionFactory = sqlSessionFactoryBean.getObject();
        org.apache.ibatis.session.Configuration config = sqlSessionFactory.getConfiguration();
        // 自动注册
        for (String className : ClassUtil.getJsitsClasses()) {
            if (className.contains(".model.") || className.contains(".dto.") || className.contains(".enums.")) {
                Class<?> classType = Class.forName(className);
                if (classType.isEnum()) {
                    EnumIgnore enumIgnore = classType.getAnnotation(EnumIgnore.class);
                    if (enumIgnore != null && enumIgnore.value()) { // 忽略此枚举
                        continue;
                    }
                    if (IntValueEnum.class.isAssignableFrom(classType)) {
                        config.getTypeHandlerRegistry().register(classType, new IntValueEnumTypeHandler(classType));
                    } else if (StringValueEnum.class.isAssignableFrom(classType)) {
                        config.getTypeHandlerRegistry().register(classType, new StringValueEnumTypeHandler(classType));
                    }else {
                        config.getTypeHandlerRegistry().register(classType, new EnumOrdinalTypeHandler(classType));
                    }
                }
                config.getTypeAliasRegistry().registerAlias(classType);
            }
        }
    }

    @Override
    public SqlSessionFactory getSqlFactory() {
        return sqlSessionFactory;
    }

    @Override
    public void printUuid() {
        System.out.println(uuid);
    }
}
