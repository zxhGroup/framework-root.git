package com.zxhtom.framework_generator_code.service.impl;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.framework_generator_code.model.GeneratorData;
import com.zxhtom.framework_generator_code.service.SqlFactory;
import com.zxhtom.framework_generator_code.util.JdbcUtil;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zxhtom.framework_generator_code.entity.Column;
import com.zxhtom.framework_generator_code.entity.Table;
import com.zxhtom.framework_generator_code.model.GeneratorCodeProperties;
import com.zxhtom.framework_generator_code.repository.SysGeneratorRepository;
import com.zxhtom.framework_generator_code.repository.impl.SysGeneratorRepositoryImpl;
import com.zxhtom.framework_generator_code.service.SysGeneratorService;
import com.zxhtom.framework_generator_code.util.GenUtils;

import javax.sql.DataSource;

/**
 * 代码生成器
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年12月19日 下午3:33:38
 */
@Service
public class SysGeneratorServiceImpl implements SysGeneratorService {
	private Logger logger = LogManager.getLogger(SysGeneratorRepositoryImpl.class);
	@Autowired
	private SqlFactory sqlFactory;
	@Autowired
	private SysGeneratorRepository sysGeneratorRepository;
	@Autowired
	private GeneratorCodeProperties properties;

	public List<Table> queryList(Map<String, Object> map) {
		return sysGeneratorRepository.queryList(map);
	}

	public int queryTotal(Map<String, Object> map) {
		return sysGeneratorRepository.queryTotal(map);
	}

	public Table queryTable(String tableName) {
		return sysGeneratorRepository.queryTable(tableName);
	}

	public List<Column> queryColumns(String tableName) {
		return sysGeneratorRepository.queryColumns(tableName);
	}

	public byte[] generatorCode(List<String> tableNames) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);

		for (String tableName : tableNames) {
			// 查询表信息
			Table table = queryTable(tableName);
			// 查询列信息
			List<Column> columns = queryColumns(tableName);
			// 生成代码
			try {
				GenUtils.generatorCode(properties,table, columns, zip);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		IOUtils.closeQuietly(zip);
		return outputStream.toByteArray();
	}

    @Override
    public DataSource initDataSource(GeneratorData generatorData) {
		sqlFactory.printUuid();
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(JdbcUtil.getInstance().getUrl(generatorData.getIp(), generatorData.getPort(), generatorData.getServerName(), generatorData.getDriverClassName()));
		dataSource.setDriverClassName(generatorData.getDriverClassName());
		dataSource.setUsername(generatorData.getUsername());
		dataSource.setPassword(generatorData.getPassword());
		try {
			sqlFactory.setDataSource(dataSource);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataSource;
    }
}
