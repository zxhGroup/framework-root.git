package com.zxhtom.framework_generator_code.mapper;

import java.util.List;
import java.util.Map;

import com.zxhtom.framework_generator_code.entity.Column;
import com.zxhtom.framework_generator_code.entity.Table;

import tk.mybatis.mapper.common.Mapper;
public interface SysGeneratorOracleMapper {
	
	List<Table> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	Table queryTable(String tableName);
	
	List<Column> queryColumns(String tableName);

}
