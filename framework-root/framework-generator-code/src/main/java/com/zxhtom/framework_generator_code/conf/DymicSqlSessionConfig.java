package com.zxhtom.framework_generator_code.conf;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.framework_generator_code.service.SqlFactory;
import com.zxhtom.framework_generator_code.service.impl.SqlFactoryImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;

@Configuration
public class DymicSqlSessionConfig {

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
    public SqlFactory sqlSession(DruidDataSource dataSource){
        SqlFactoryImpl sqlFactory = new SqlFactoryImpl();
        try {
            sqlFactory.setDataSource(dataSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlFactory;
    }
}
