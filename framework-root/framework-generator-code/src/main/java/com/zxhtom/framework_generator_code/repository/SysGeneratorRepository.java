package com.zxhtom.framework_generator_code.repository;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.zxhtom.framework_generator_code.entity.Column;
import com.zxhtom.framework_generator_code.entity.Table;

public interface SysGeneratorRepository {
	
	List<Table> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	Table queryTable(String tableName);
	
	List<Column> queryColumns(String tableName);
}
