package com.zxhtom.framework_generator_code.service;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.sql.DataSource;

public interface SqlFactory{
    public void setDataSource(DruidDataSource dataSource) throws Exception;
    public SqlSessionFactory getSqlFactory();
    public void printUuid();
}
