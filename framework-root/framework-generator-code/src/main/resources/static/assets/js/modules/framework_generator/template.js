var vm = new Vue({
    el:'#template',
    data:function () {
        return {
            checkTableChange:false,
            dataList:['oracle.jdbc.driver.OracleDriver','com.mysql.jdbc.Driver'],
            dataName:'',
            allTableName:[],
            checkAll: false,
            tables:[],
            checkAllGroup:[],
            port:1,
            ip:'',
            serverName:'',
            userName:'',
            password:'',
            readonly:false
        }
    },
    methods:{
        checkAllTable :function() {
            this.checkAll = !this.checkAll;
            if (this.checkAll) {
                this.checkAllGroup = this.allTableName;
            } else {
                this.checkAllGroup = [];
            }
        },
        checkAllGroupChange :function(data) {
            if (data.length === 3) {
                this.indeterminate = false;
                this.checkAll = true;
            } else if (data.length > 0) {
                this.indeterminate = true;
                this.checkAll = false;
            } else {
                this.indeterminate = false;
                this.checkAll = false;
            }
        },
        change:function(vals){
            // alert(vals);
        },
        generator:function(){
            console.log(vm.checkAllGroup);
            window.open("/framework_generator/generator/code?tableNames="+vm.checkAllGroup);
        },
        update:function(){
            vm.readonly=false;
        },
        selectedData:function (opt) {
            vm.dataName=opt;
            if(opt=='com.mysql.jdbc.Driver'){
                vm.port=3306;
            }else{
                vm.port=1521;
            }
        },
        bindData:function () {
            //绑定数据库
            if(vm.isValidIP(vm.ip)==false){
                alert("IP不合法！");
            }
            $.ajax({
                type: 'POST',
                url: '/framework_generator/generator/dataFrom',
                contentType: "application/json",
                data:JSON.stringify({
                    ip:vm.ip,
                    port:vm.port,
                    username:vm.userName,
                    password:vm.password,
                    serverName:vm.serverName,
                    driverClassName:vm.dataName
                }),
                success: function(r){
                    if(r.code === 0){
                        vm.allTableName = r.data.map(function (item) {
                            return item.tableName;
                        });
                        vm.checkTableChange=true;
                    }else{
                        alert(r.message);
                        vm.loading = false;
                    }
                }
            });
            vm.readonly=true;
        },
        isValidIP:function(ip) {
            var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
            return reg.test(ip);
        }
    }
})