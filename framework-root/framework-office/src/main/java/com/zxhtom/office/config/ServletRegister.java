package com.zxhtom.office.config;

import com.zhuozhengsoft.pageoffice.poserver.Server;
import com.zxhtom.office.model.PageOffice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.office.config
 * @date 2020年04月17日, 0017 17:15
 * @Copyright © 2020 安元科技有限公司
 */
@Configuration
public class ServletRegister {

    @Autowired
    private PageOffice pageOffice;

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        Server poserver = new Server();
        //设置PageOffice注册成功后,license.lic文件存放的目录
        poserver.setSysPath(pageOffice.getPoSysPath());
        ServletRegistrationBean srb = new ServletRegistrationBean(poserver);
        // 下面是把资源文件暴露出来，必须配置，否则页面访问不了
        srb.addUrlMappings("/poserver.zz", "/posetup.exe", "/pageoffice.js", "/jquery.min.js", "/pobstyle.css", "/sealsetup.exe");
        return srb;
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean2() {
        com.zhuozhengsoft.pageoffice.poserver.AdminSeal adminSeal = new com.zhuozhengsoft.pageoffice.poserver.AdminSeal();
        adminSeal.setAdminPassword(pageOffice.getPoPassWord());//设置印章管理员admin的登录密码
        adminSeal.setSysPath(pageOffice.getPoSysPath());//设置印章数据库文件poseal.db存放的目录
        ServletRegistrationBean srb = new ServletRegistrationBean(adminSeal);
        srb.addUrlMappings("/adminseal.zz");
        srb.addUrlMappings("/sealimage.zz");
        srb.addUrlMappings("/loginseal.zz");
        return srb;
    }
}
