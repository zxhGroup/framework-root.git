package com.zxhtom.office.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.office.model
 * @date 2020年04月17日, 0017 17:13
 * @Copyright © 2020 安元科技有限公司
 */
@Component
@ConfigurationProperties(prefix = "framework.pageoffice")
@Data
public class PageOffice {

    private String poSysPath;

    private String poPassWord;

}
