package com.zxhtom.office;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.office
 * @date 2020年04月17日, 0017 17:02
 * @Copyright © 2020 安元科技有限公司
 */
@SpringBootApplication
public class OfficeApplication {
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "D:\\server\\hadoop-common-2.2.0-bin-master\\hadoop-common-2.2.0-bin-master");
        SpringApplication.run(OfficeApplication.class, args);
    }
}
