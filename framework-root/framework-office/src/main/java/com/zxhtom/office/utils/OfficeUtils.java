package com.zxhtom.office.utils;


import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.util.Map;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.office.utils
 * @date 2020年04月21日, 0021 13:48
 * @Copyright © 2020 安元科技有限公司
 */
public class OfficeUtils {

    /**
     * @return java.io.File
     * @author zxhtom
     * @Description 通过模板生成word
     * @Date 13:51 2020年04月21日, 0021
     * @Param 数据集合；模板名称；文件路径；文件名称
     */
    public static File createWordBaseOnTemplate(Map dataMap, String templateName, String filePath, String fileName) {
        try {
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(OfficeUtils.class, "/template");
            //获取模板
            Template template = configuration.getTemplate(templateName);
            //输出文件
            File outFile = new File(filePath + File.separator + fileName);
            //如果输出目标文件夹不存在，则创建
            if (!outFile.getParentFile().exists()) {
                outFile.getParentFile().mkdirs();
            }
            //将模板和数据模型合并生成文件
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"));
            //生成文件
            template.process(dataMap, out);
            //关闭流
            out.flush();
            out.close();
            return outFile;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
