package com.zxhtom.office.controller;

import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.office.controller
 * @date 2020年04月17日, 0017 17:10
 * @Copyright © 2020 安元科技有限公司
 */
@RestController
@RequestMapping(value = "/pageOffice")
public class PageOfficeController {

    @RequestMapping(value = "/word", method = RequestMethod.GET)
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        ModelAndView mv = new ModelAndView("word");
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //设置授权程序servlet
        poCtrl.setServerPage("/poserver.zz");
        //添加自定义按钮
        poCtrl.addCustomToolButton("保存", "Save", 1);
        poCtrl.addCustomToolButton("加盖印章", "InsertSeal()", 2);
        //保存接口地址
        poCtrl.setSaveFilePage("/pageOffice/save");
        //打开文件(打开的文件类型由OpenModeType决定，docAdmin是一个word，并且是管理员权限，如果是xls文件，则使用openModeType.xls开头的,其他的office格式同等)，最后一个参数是作者
        // TODO 这里有个坑，这里打开的文件是本地的，地址如果写成/结构的路径，页面就会找不到文件，会从http://xxxxx/G/id...去找，写成\\就是从本地找
        poCtrl.webOpen("/test.doc", OpenModeType.docNormalEdit, "光哥");
        //pageoffice 是文件的变量，前端页面通过这个变量加载出文件
        map.put("pageoffice", poCtrl.getHtmlCode("PageOfficeCtrl1"));
        //跳转到word.html
        return mv;
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView showIndex(@RequestParam String fileUrl) {
        ModelAndView mv = new ModelAndView("index");
        fileUrl = "http://10.0.20.128:90/Samples4/DrawExcel/doc/test.xls";
        mv.addObject("fileUrl", fileUrl);
        mv.addObject("extra", "test");

        return mv;
    }

    @RequestMapping("/save")
    public void saveFile(HttpServletRequest request, HttpServletResponse response){
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile("E:\\" + fs.getFileName());
        fs.close();
    }
}
