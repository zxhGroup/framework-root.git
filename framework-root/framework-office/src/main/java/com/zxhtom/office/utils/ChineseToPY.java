package com.zxhtom.office.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * @package com.ay.sdksm.sdk.common.utils
 * @Class ChineseToPY
 * @Description 汉字转英文
 * @Author zhangxinhua
 * @Date 19-8-22 下午1:55
 */
public class ChineseToPY {
    private static ChineseToPY chineseToPY;
    private static HanyuPinyinOutputFormat hypy ;
    public static ChineseToPY getInstance() {
        if (chineseToPY == null) {
            hypy = new HanyuPinyinOutputFormat();
            hypy.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            hypy.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            hypy.setVCharType(HanyuPinyinVCharType.WITH_V);
            return new ChineseToPY();
        }
        return chineseToPY;
    }

    public String getEnglish(String chinese) {
        String s = "";
        try {
            s = PinyinHelper.toHanYuPinyinString(chinese, hypy,"",false);
        } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
            badHanyuPinyinOutputFormatCombination.printStackTrace();
        }
        return s;
    }
}
