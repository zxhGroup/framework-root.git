import com.zxhtom.office.utils.ChineseToPY;
import com.zxhtom.office.utils.OfficeUtils;
import org.apache.shiro.crypto.hash.Hash;
import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zxhtom.utils.FileUtil;
import org.zxhtom.utils.MathUtil;
import sun.misc.BASE64Encoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * @author 张新华
 * @version V1.0
 * @Package PACKAGE_NAME
 * @date 2020年04月21日, 0021 13:58
 * @Copyright © 2020 安元科技有限公司
 */
public class OfficeTest {

    public static Map<String, Object> checkBoxMap = new HashMap<>();

    static {
        checkBoxMap.put("无证操作", "wuzhengcaozuo");
        checkBoxMap.put("指挥混乱", "a2");
        checkBoxMap.put("无警戒线或警示标志", "a3");
        checkBoxMap.put("未严格执行吊装作业“十不吊”", "a4");
        checkBoxMap.put("起重机与输电线路安全距离不符合规范要求", "a5");
        checkBoxMap.put("涉及危险作业组合，未落实相应安全措施，办理相应许可证", "a6");
        checkBoxMap.put("施工条件发生重大变化，未重新办理许可证", "a7");
        checkBoxMap.put("其它", "a8");
        checkBoxMap.put("工作服", "a9");
        checkBoxMap.put("安全鞋", "a10");
        checkBoxMap.put("安全帽", "a11");
        checkBoxMap.put("安全带", "a12");
        checkBoxMap.put("防尘口罩", "a13");
        checkBoxMap.put("防护手套", "a14");
        checkBoxMap.put("防毒面具", "a15");
        checkBoxMap.put("护目镜", "a16");
        checkBoxMap.put("护耳器具", "a17");
        checkBoxMap.put("防护服", "a18");
        checkBoxMap.put("其他", "a19");
        for (int j = 1; j <= 19; j++) {
            Integer i = MathUtil.getInstance().getRandom(0, 10);
            if (i % 2 == 0) {
                checkBoxMap.put("a" + j, "true");
            } else {
                checkBoxMap.put("a" + j, "false");
            }
        }
        checkBoxMap.put("wuzhengcaozuo", "true");
    }


    @Test
    public void replaceTest() {
        String text = "<w:sym w:font=\"Wingdings 2\" w:char=\"&lt;#if a1=='true'&gt;0052&lt;#else&gt;00A3&lt;/#if&gt;\"/></w:r>";
        text = text.replaceAll("<w:sym.*(&lt;)", "<");
        //text = text.replaceAll("&lt;", "<");
        System.out.println(text);
        String str = "总会在某一个回眸的时刻醉了流年，濡湿了柔软的心.总会有某一个回眸的时刻醉了流年，濡湿了柔软的心";
        str = str.replaceAll("总会在.+?流年", "213");
        System.out.println(str);
    }

    // 在指定位置添加一个节点
    @Test
    public void test3() throws Exception {
        SAXReader reader = new SAXReader();
        org.dom4j.Document document = reader.read(new File("src/main/resources/template/book.xml"));
        Element root = document.getRootElement();

//            获取第一个书节点
        Element shu = (Element) root.elements("书").get(0);
//            获取第一个节点下的所有节点
        List list = shu.elements();   //[书名，作者，价格]

//            创建一个节点
        Element miaoshu = DocumentHelper.createElement("miaoshu");
        //miaoshu.setText("一本好书");
        list.add(2, miaoshu);

//            格式化输出流，同时指定编码格式。也可以在FileOutputStream中指定。
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("utf-8");

        XMLWriter writer = new XMLWriter(new FileOutputStream("src/main/resources/template/book.xml"), format);
        writer.write(document);
        writer.close();
    }

    @Test
    public void doxml() {
        long lasting = System.currentTimeMillis();
        try {
            File f = new File("src/main/resources/template/受限空间作业许可证.xml");
            SAXReader reader = new SAXReader();
            org.dom4j.Document doc = reader.read(f);
            Element root = doc.getRootElement();
            System.out.println(root.getName());
            Element foo;
            List<Element> list = root.selectNodes("//w:binData");
            Integer index = 1;
            for (Element element : list) {
                //System.out.println(element.getText());
                element.setText("${img" + index + "}");
                //element.setText("abc");
                index++;
            }
            //修改集合
            List<Element> trList = root.selectNodes("//w:t");
            String regex = "\\$\\{(\\w+)\\.(\\w+)\\}";
            List<Element> rowList = new ArrayList<>();
            for (Element element : trList) {
                boolean matches = Pattern.matches(regex, element.getTextTrim());
                if (!matches) {
                    continue;
                }
                Pattern compile = Pattern.compile(regex);
                Matcher matcher = compile.matcher(element.getTextTrim());
                String tableName = "";
                String colName = "";
                while (matcher.find()) {
                    tableName = matcher.group(1);
                    colName = matcher.group(2);
                }
                //获取c.no所在w:tr标签
                List<Element> ancestorTrList = element.selectNodes("ancestor::w:tr[1]");
                Element ancestorTr = null;
                if (!ancestorTrList.isEmpty()) {
                    ancestorTr = ancestorTrList.get(0);
                    if (!rowList.contains(ancestorTr)) {
                        rowList.add(ancestorTr);
                        //List<Element> list1 = element.selectNodes("../child::*");
                        List<Element> list1 = ancestorTr.getParent().elements();
                        if (!list1.isEmpty()) {
                            Integer ino = 0;
                            Element foreach = null;
                            for (Element elemento : list1) {
                                if (ancestorTr.equals(elemento)) {
                                    foreach = DocumentHelper.createElement("zxhtom");
                                    foreach.addAttribute("name", "${"+tableName+"}");
                                    Element copy = ancestorTr.createCopy();
                                    //List<Element> list2 = copy.selectNodes("descendant::*/w:t");
                                    //for (Element o : list2) {
                                    //    boolean matches2 = Pattern.matches(regex, element.getTextTrim());
                                    //    if (!matches2) {
                                    //        continue;
                                    //    }
                                    //    Pattern compile2 = Pattern.compile(regex);
                                    //    Matcher matcher2 = compile.matcher(o.getTextTrim());
                                    //    String tableName2 = "";
                                    //    String colName2 = "";
                                    //    while (matcher2.find()) {
                                    //        tableName2 = matcher2.group(1);
                                    //        colName2 = matcher2.group(2);
                                    //    }
                                    //    o.setText("${"+colName2+"}");
                                    //}
                                    foreach.add(copy);
                                    break;
                                }
                                ino++;
                            }
                            if (foreach != null) {
                                list1.set(ino, foreach);
                            }
                        }
                    } else {
                        continue;
                    }
                }
                System.out.println(matches);
            }
            //替换原有表格集合标签
            if (!rowList.isEmpty()) {
                for (Element element : rowList) {
                    /*List<Element> list1 = element.selectNodes("../child::*");
                    if (!list1.isEmpty()) {
                        Integer ino = 0;
                        Element foreach = null;
                        for (Element elemento : list1) {
                            if (element.equals(elemento)) {
                                foreach = DocumentHelper.createElement("list");
                                break;
                            }
                            ino++;
                        }
                        if (foreach != null) {
                            Element o = (Element) element.selectNodes("../").get(0);
                            o.addElement("foreach");
                        }
                    }*/
                }
                //格式化输出流，同时指定编码格式。也可以在FileOutputStream中指定。
                /*OutputFormat format = OutputFormat.createPrettyPrint();
                format.setEncoding("utf-8");

                XMLWriter writer = new XMLWriter(new FileOutputStream("src/main/resources/template/book.xml"), format);
                writer.write(doc);
                writer.close();*/
            }
            //修改复选框
            List<Element> checkList = root.selectNodes("//w:sym");
            List<String> nameList = new ArrayList<>();
            Integer indext = 1;
            for (Element element : checkList) {
                Attribute aChar = element.attribute("char");
                List<Element> list1 = element.selectNodes("../following::*[1]/w:t[1]");
                String checkBoxName = list1.get(0).getText();
                System.out.println(aChar);
                String checkName = ChineseToPY.getInstance().getEnglish(checkBoxName);
                if (nameList.contains(checkName)) {
                    checkName = checkName+indext;
                    indext++;
                    nameList.add(checkName);
                }
                System.out.println(checkName);
                aChar.setData("<#if " + checkName+"??zxhtom_null"+checkName + "=='true'>0052<#else>00A3</#if>");
                /*if (checkBoxMap.containsKey(checkBoxName)) {
                    System.out.println(checkBoxMap.get(checkBoxName));
                    aChar.setData("<#if " + ChineseToPY.getInstance().getEnglish(checkBoxName) + "=='true'>0052<#else>00A3</#if>");
                } else {
                    aChar.setData("<#if a1=='true'>0052<#else>00A3</#if>");
                }*/

            }
            FileOutputStream out = new FileOutputStream("src/main/resources/template/受限空间作业许可证.xml");
            XMLWriter writer2 = new XMLWriter(out);
            writer2.write(doc);
            writer2.close();
            //doc.write(new FileWriter(new File("src/main/resources/template/was.xml")));
            //writer.write(doc);
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "&lt;#if", "<#if");
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "#if&gt;", "#if>");
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "\'&gt;", "'>");
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "&lt;#else", "<#else");
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "#else&gt;", "#else>");
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "&lt;/#if", "</#if");
            FileUtil.getInstance().replaceTextContent("src/main/resources/template/受限空间作业许可证.xml", "??zxhtom_null", "??&&");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Test
    public void test1() {
        String regex = "\\$\\{(\\w+)\\.(\\w+)\\}";
        Pattern compile = Pattern.compile(regex);
        Matcher matcher = compile.matcher("${s.s}");
        while (matcher.find()) {
            for (int i = 0; i < matcher.groupCount(); i++) {
                System.out.println(matcher.group(i));
            }
        }
        System.out.println(Pattern.matches(regex, "${s.n}"));
    }

    @Test
    public void readXml() throws ParserConfigurationException, IOException, SAXException {
        File file = new File("src/main/resources/template/test.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);
        NodeList value = doc.getElementsByTagName("VALUE");
        System.out.println(value);
    }

    @Test
    public void createWordTest() {
        Map<String, Object> dataMap = new HashMap<>();
        File file = new File("src/main/resources/pageoffice/index.jpg");
        dataMap.put("img", getImageStr(file.getAbsolutePath()));
        dataMap.put("name", "zxhtom");
        dataMap.put("sex", "true");
        OfficeUtils.createWordBaseOnTemplate(dataMap, "img.ftl", "D:\\", "img.doc");
    }

    @Test
    public void createWorkWordTest2() {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("no", "test");
        resultMap.put("depart", "test");
        resultMap.put("chargePerson", "test");
        resultMap.put("content", "test");
        resultMap.put("medium", "test");
        resultMap.put("workPeoples", "test");
        resultMap.put("guardian", "test");
        resultMap.put("year1", "test");
        resultMap.put("year2", "test");
        resultMap.put("month1", "test");
        resultMap.put("month2", "test");
        resultMap.put("day1", "test");
        resultMap.put("day2", "test");
        resultMap.put("hour1", "test");
        resultMap.put("hour2", "test");
        resultMap.put("min1", "test");
        resultMap.put("min2", "test");
        for (int i = 0; i < 15; i++) {
            int index = i + 1;
            resultMap.put("sign" + index, "suresign" + index);
            resultMap.put("suresign" + index, "zxh");
        }
        resultMap.put("sy1", "test");
        resultMap.put("sy2", "test");
        resultMap.put("sy3", "test");
        resultMap.put("sy4", "test");
        resultMap.put("sm1", "test");
        resultMap.put("sm2", "test");
        resultMap.put("sm3", "test");
        resultMap.put("sm4", "test");
        resultMap.put("sd1", "test");
        resultMap.put("sd2", "test");
        resultMap.put("sd3", "test");
        resultMap.put("sd4", "test");
        resultMap.put("shour1", "test");
        resultMap.put("shour2", "test");
        resultMap.put("shour3", "test");
        resultMap.put("shour4", "test");
        resultMap.put("smin1", "test");
        resultMap.put("smin2", "test");
        resultMap.put("smin3", "test");
        resultMap.put("smin4", "test");
        resultMap.put("endYear", "test");
        resultMap.put("endMonth", "test");
        resultMap.put("endDay", "test");
        resultMap.put("endHour", "test");
        resultMap.put("endMin", "test");
        resultMap.put("endActutor", "test");
        resultMap.put("eaYear", "test");
        resultMap.put("eaMonth", "test");
        resultMap.put("eaDay", "test");
        resultMap.put("eaHour", "test");
        resultMap.put("eaMin", "test");
        resultMap.put("jianhuoren", "test");
        resultMap.put("jYear", "test");
        resultMap.put("jMonth", "test");
        resultMap.put("jDay", "test");
        resultMap.put("jHour", "test");
        resultMap.put("jMin", "test");
        resultMap.put("wuzhengcaozuo", "true");
        for (int i = 0; i < 19; i++) {
            resultMap.put("a" + (i + 1), "true");
        }
        File imgFiles = new File("D:\\img");
        File[] files = imgFiles.listFiles();
        List<File> fileList = Arrays.asList(files);
        for (int i = 0; i < 4; i++) {
            resultMap.put("img" + (i + 1), getImageStr(fileList.get(i).getAbsolutePath()));
        }
        OfficeUtils.createWordBaseOnTemplate(resultMap, "受限空间作业许可证.xml", "D:\\", "work.doc");
    }

    @Test
    public void createWorkWordTest() {
        //doxml();
        Map<String, Object> dataMap = new HashMap<>();
        File file = new File("src/main/resources/pageoffice/index.jpg");
        File imgFiles = new File("D:\\img");
        File[] files = imgFiles.listFiles();
        List<File> fileList = Arrays.asList(files);
        Collections.shuffle(fileList);
        for (int i = 0; i < 20; i++) {
            dataMap.put("img" + (i + 1), getImageStr(fileList.get(i).getAbsolutePath()));
        }
        dataMap.put("img2", getImageStr(new File("D:\\img/xiegang.jpg").getAbsolutePath()));
        //dataMap.put("img2", "\\234");
        dataMap.putAll(checkBoxMap);

        dataMap.put("sex", "true");


        dataMap.put("level", "五");
        dataMap.put("depart", "安元科技");
        dataMap.put("applyPeople", "张新华");
        dataMap.put("workArea", "南京");
        dataMap.put("operator", "张新华");
        dataMap.put("commander", "程蕲");
        dataMap.put("swornPerson", "未知");
        dataMap.put("specialNo", "025489351456dfsg26");
        dataMap.put("cranes", "载重车");
        dataMap.put("craneNo", "2020");
        dataMap.put("guardian", "安元科技");
        dataMap.put("safer", "安元");
        dataMap.put("workContent", "南京市浦口区临江地铁站临时检修");
        dataMap.put("operateYear", "2020");
        dataMap.put("operateMonth", "04");
        dataMap.put("operateDay", "15");
        dataMap.put("operateHour", "05");
        dataMap.put("operateMin", "56");
        dataMap.put("eperateYear", "2020");
        dataMap.put("eperateMonth", "03");
        dataMap.put("eperateDay", "05");
        dataMap.put("eperateHour", "06");
        dataMap.put("eperateMin", "23");

        dataMap.put("advice1", "同意");
        dataMap.put("year1", "2020");
        dataMap.put("month1", "04");
        dataMap.put("day1", "21");
        dataMap.put("hour1", "14");
        dataMap.put("min1", "25");

        dataMap.put("advice2", "同意");
        dataMap.put("year2", "2020");
        dataMap.put("month2", "04");
        dataMap.put("day2", "21");
        dataMap.put("hour2", "14");
        dataMap.put("min2", "25");

        dataMap.put("advice3", "同意");
        dataMap.put("year3", "2020");
        dataMap.put("month3", "04");
        dataMap.put("day3", "21");
        dataMap.put("hour3", "14");
        dataMap.put("min3", "25");

        dataMap.put("advice4", "同意");
        dataMap.put("year4", "2020");
        dataMap.put("month4", "04");
        dataMap.put("day4", "21");
        dataMap.put("hour4", "14");
        dataMap.put("min4", "25");

        dataMap.put("advice5", "同意");
        dataMap.put("year5", "2020");
        dataMap.put("month5", "04");
        dataMap.put("day5", "21");
        dataMap.put("hour5", "14");
        dataMap.put("min5", "25");

        dataMap.put("entDay", "25");
        dataMap.put("endHour", "12");
        dataMap.put("workDepart", "南京安元科技");
        dataMap.put("wDay", "25");
        dataMap.put("wHour", "12");
        dataMap.put("wMin", "23");

        dataMap.put("seeDepart", "南京地铁公司");
        dataMap.put("sDay", "25");
        dataMap.put("sHour", "23");
        dataMap.put("sMin", "25");
        OfficeUtils.createWordBaseOnTemplate(dataMap, "was.xml", "D:\\", "work.doc");
    }

    public static String getImageStr(String imgFile) {
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }
}
