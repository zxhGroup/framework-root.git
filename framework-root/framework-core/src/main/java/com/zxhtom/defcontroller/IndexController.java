package com.zxhtom.defcontroller;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zxhtom.annotation.OnlyLogin;
import com.zxhtom.annotation.TokenNot;

import springfox.documentation.annotations.ApiIgnore;

@Controller("IndexController")
@ApiIgnore
public class IndexController {
	@RequestMapping(value = { "/index", "/","" })
	@OnlyLogin
	public String index(HttpServletRequest request, HttpServletResponse response) {
		try { response.sendRedirect("system.html"); } catch (Exception e) { }
		return "index";
	}

	/**
	 * 这个接口目的就是访问 xxx.html ,会经过此接口后在访问原来的xxx.html，接口后台后我们就可以使用freemark 来渲染了。
	 * 
	 * @param viewName
	 * @return date 2018年7月9日
	 */
	@RequestMapping(value = "/**/{viewName}.html")
	public String viewName(@PathVariable String viewName,HttpServletRequest request) {
		/*
		 * try { response.sendRedirect("system.html"); } catch (Exception e) { }
		 */
		viewName = request.getServletPath();
		if (viewName.indexOf(".") != -1) {
			viewName = viewName.substring(1, viewName.indexOf("."));
		}
		return viewName;
	}

	@RequestMapping(value = { "/swagger" }, method = RequestMethod.GET)
	@TokenNot
	public String swagger(HttpServletRequest request, HttpServletResponse response) {
		/*
		 * try { response.sendRedirect("system.html"); } catch (Exception e) { }
		 */
		return "swagger";
	}
}