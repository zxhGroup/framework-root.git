package com.zxhtom.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * keyF 和 keyS 是redis的两个key ， 可以设置，也可以用方法中的参数替换。如果想替换就需要制定成RedisKey.CURRENTPARAM, aop 中会进行严格的验证 
 * @ClassName: RedisForm 
 * @author zxhtom
 * @date 2018年7月13日
 * @since 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface RedisForm {

	String[] keyF() default {};

	String[] keyS() default {};

	boolean select() default false;
}
