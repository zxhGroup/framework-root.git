package com.zxhtom.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.core.util.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;

/**
 * 获取com.zxhtom下所有类型
 */
public class ClassUtil {
    private static final Logger logger = LoggerFactory.getLogger(ClassUtil.class);

    /**
     * com.zxhtom下所有类名
     */
    private volatile static List<String> jsitsClasses;

    private ClassUtil() {
    }

    /**
     * 获取com.zxhtom下所有类型
     *
     * @return
     */
    public static List<String> getJsitsClasses() {
        if (jsitsClasses == null) {
            synchronized (ClassUtil.class) {
                if (jsitsClasses == null) {
                    try {
                        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
                        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
                        Resource[] resources = resourcePatternResolver.getResources("classpath*:com/zxhtom/**/*.class");
                        jsitsClasses = new ArrayList<>(resources.length);
                        for (Resource resource : resources) {
                            MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
                            jsitsClasses.add(metadataReader.getClassMetadata().getClassName());
                        }
                    } catch (Exception e) {
//                        Throwables.propagate(e);
                    }
                }
            }
        }
        return jsitsClasses;
    }
}
