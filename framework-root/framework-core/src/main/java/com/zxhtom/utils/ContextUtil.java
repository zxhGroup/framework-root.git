package com.zxhtom.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 获取Spring应用上下文环境ApplicationContext
 */
public class ContextUtil implements ApplicationContextAware {
	/**
	 * Spring应用上下文环境
	 */
	private static ApplicationContext applicationContext;

	/**
	 * 实现ApplicationContextAware接口的回调方法，设置上下文环境
	 * @param applicationContext 上下文
	 * @throws BeansException 异常
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ContextUtil.applicationContext = applicationContext;
	}

	/**
	 * @return ApplicationContext 上下文
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 获取项目中模块名
	 * @return
	 */
	public static List<String> getProjectName(){
		Set<String> projectSet = new HashSet<>();
		List<String> requestUrl = getAllUrl();
		for (String url : requestUrl) {
			String regex = "\\/*(\\w+)";
			Pattern compile = Pattern.compile(regex);
			Matcher matcher = compile.matcher(url);
			if (matcher.find()) {
				String group = matcher.group(1);
				projectSet.add(group);
			}
		}
		return new ArrayList<>(projectSet);
	}
	/**
	 * 获取spring所有url
	 * @return
	 */
	public static List<String> getAllUrl(){
		RequestMappingHandlerMapping mapping = getApplicationContext().getBean(RequestMappingHandlerMapping.class);
		//获取url与类和方法的对应信息
		Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
		List<String> urlList = new ArrayList<>();
		for (RequestMappingInfo info : map.keySet()){
			//获取url的Set集合，一个方法可能对应多个url
			Set<String> patterns = info.getPatternsCondition().getPatterns();
			for (String url : patterns){
				urlList.add(url);
			}
		}
		return urlList;
	}
}
