package com.zxhtom.utils;

import java.util.List;

import javax.servlet.ServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.context.ApplicationContext;

import com.zxhtom.CoreConstants;
import com.zxhtom.core.service.CustomService;
import com.zxhtom.model.CustomRole;

public class ValidUtil {

	private static ValidUtil validUtil = new ValidUtil();

	private ValidUtil() {
	}

	public static ValidUtil getInstance() {
		return validUtil;
	}

	public static void validRequest(Subject subject, ServletRequest request) {
		// 如果之前过滤器已经验证不通过，则次验证器不需要继续验证了
		if (request.getAttribute(CoreConstants.MSG) != null && StringUtils.isNotEmpty(request.getAttribute(CoreConstants.MSG).toString())) {
			return;
		}
		// 验证menu 权限
		String menuIdStr = WebUtils.toHttp(request).getHeader(CoreConstants.MENUPRE);
		Long menuId = -2l;
		if (StringUtils.isNotEmpty(menuIdStr)) {
			menuId = Long.valueOf(menuIdStr);
		} else {
			request.setAttribute(CoreConstants.MSG, "request headers do not contain menuInfo , please check it");
		}
		ApplicationContext applicationContext = ContextUtil.getApplicationContext();
		CustomService customService = applicationContext.getBean(CustomService.class);
		// 如果是swagger 菜单，需要放行
		if (-1 == customService.selectFactMenuId(menuId)) {
			return;
		}
		// 更新真实菜单id
		List<CustomRole> roleList = customService.selectRolesVistualMenuId(menuId);
		if (CollectionUtils.isEmpty(roleList)) {
			request.setAttribute(CoreConstants.MSG, "the menu is wrongful , please give me right menu");
		} else {
			boolean hasOnceRole = true;
			for (CustomRole customRole : roleList) {
				if (subject.hasRole(customRole.getRoleCode())) {
					hasOnceRole = true;
					break;
				} else {
					hasOnceRole = false;
				}
			}
			if (!hasOnceRole) {
				request.setAttribute(CoreConstants.MSG, "You need to get one of the following roles[" + roleList.toString() + "]");
			}
		}
	}
}
