package com.zxhtom.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.Scanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.zxhtom.bean.ExecutorBean;

import com.zxhtom.CoreConstants;

/**
 * 注解管理器
 * 
 * @ClassName: AnnoManageUtil
 * @author zxhtom
 * @date 2018年4月26日
 * @since 1.0
 */
public final class AnnoManageUtil {

	/**
	 * 获取controller中的头部mapping地址
	 * 
	 * @param packageName
	 * @return date 2018年4月26日
	 */
	public static Map<String, Object> getControllerMapping(String packageName) {
		RequestMappingHandlerMapping requestMapping = ContextUtil.getApplicationContext().getBean(RequestMappingHandlerMapping.class);

		Map<String, Object> map = new HashMap<String, Object>();
		List<String> projectNameList = new ArrayList<String>();
		List<String> requestList = new ArrayList<String>();
		Reflections reflections = new Reflections(packageName);
		Set<Class<?>> classesList = reflections.getTypesAnnotatedWith(RestController.class);
		Class<?> cls = null;
		for (Class<?> class1 : classesList) {
			List<String> thisControllerMapping = new ArrayList<String>();
			cls = class1;
			// 获取controller上的request注解
			Annotation[] annotations = cls.getAnnotations();
			for (Annotation annotation : annotations) {
				if (annotation instanceof RequestMapping) {
					String[] value = ((RequestMapping) annotation).value();
					for (String route : value) {
						String regex = "\\/*(\\w+)";
						Pattern compile = Pattern.compile(regex);
						Matcher matcher = compile.matcher(route);
						if (matcher.find()) {
							String group = matcher.group(1);
							if(!projectNameList.contains(group)){
								projectNameList.add(group);
							}
							thisControllerMapping.add("/"+group);
						}
					}
				}
			}
			// 获取controller方法的request注解 废弃
			/*Method[] methods = cls.getMethods();
			for (Method method : methods) {
				RequestMapping annotation = method.getAnnotation(RequestMapping.class);
				if (null == annotation) {
					continue;
				}
				String[] value = annotation.value();
				for (String controller : thisControllerMapping) {
					for (String route : value) {
						requestList.add(initRoute(controller) + initRoute(route));
					}
				}
			}*/
		}
		map.put(CoreConstants.PROJECTNAMELIST, projectNameList);
		map.put(CoreConstants.REQUESTLIST, ContextUtil.getAllUrl());
		return map;
	}

	private static String initRoute(String route) {
		if (route == null || 0 == route.length()) {
			return "";
		}
		if (!"/".equals(route.substring(0, 1))) {
			route = "/" + route;
		}
		if ("/".equals(route.substring(route.length() - 1))) {
			route = route.substring(0, route.length() - 1);
		}
		return route;
	}

	public static Set<Method> getTokenNotMethod(String packageName, Class clazz) {
		List<Method> methodList = new ArrayList<Method>();
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setUrls(ClasspathHelper.forPackage(packageName));
		// 反射扫描器构造
		Set<Scanner> scanners = new HashSet<>();
		Scanner[] scanner = new Scanner[scanners.size()];
		scanners.add(new TypeAnnotationsScanner());
		scanners.add(new MethodAnnotationsScanner());
		scanners.add(new SubTypesScanner());
		configurationBuilder.setScanners(scanners.toArray(scanner));
		// 反射扫描枪构造结束
		Reflections reflections = new Reflections(configurationBuilder);
		// 获取标有TokenNot的注解类下的方法
		Set<Class<?>> classesList = reflections.getTypesAnnotatedWith(clazz);
		for (Class classes : classesList) {
			// 得到该类下面的所有方法
			Method[] methods = classes.getDeclaredMethods();
			methodList.addAll(Arrays.asList(methods));
		}
		// 获取标有TokenNot的注解的方法
		Set<Method> methodsAnnotatedWith = new HashSet<Method>(methodList);
		methodsAnnotatedWith.addAll(reflections.getMethodsAnnotatedWith(clazz));
		return methodsAnnotatedWith;
	}

	/**
	 * 获取指定文件下面的RequestMapping方法保存在mapp中
	 * 
	 * @param packageName
	 * @return
	 */
	public static Map<String, ExecutorBean> getRequestMappingMethod(String packageName) {
		Reflections reflections = new Reflections(packageName);
		Set<Class<?>> classesList = reflections.getTypesAnnotatedWith(RestController.class);

		// 存放url和ExecutorBean的对应关系
		Map<String, ExecutorBean> mapp = new HashMap<String, ExecutorBean>();
		for (Class classes : classesList) {
			// 得到该类下面的所有方法
			Method[] methods = classes.getDeclaredMethods();

			for (Method method : methods) {
				// 得到该类下面的RequestMapping注解
				RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
				if (null != requestMapping) {
					ExecutorBean executorBean = new ExecutorBean();
					try {
						executorBean.setObject(classes.newInstance());
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					executorBean.setMethod(method);
					mapp.put(requestMapping.value()[0], executorBean);

				}
			}
		}
		return mapp;
	}

	public static Set<Method> getAnnotationMethod(Class clazz, String packageName) {
		List<Method> methodList = new ArrayList<Method>();
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setUrls(ClasspathHelper.forPackage(packageName));
		// 反射扫描器构造
		Set<Scanner> scanners = new HashSet<>();
		Scanner[] scanner = new Scanner[scanners.size()];
		scanners.add(new TypeAnnotationsScanner());
		scanners.add(new MethodAnnotationsScanner());
		scanners.add(new SubTypesScanner());
		configurationBuilder.setScanners(scanners.toArray(scanner));
		// 反射扫描枪构造结束
		Reflections reflections = new Reflections(configurationBuilder);
		// 获取标有TokenNot的注解的方法
		Set<Method> methods = reflections.getMethodsAnnotatedWith(clazz);
		return methods;
	}
	public static Set<Class<?>> getAnnotationType(Class clazz, String packageName) {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setUrls(ClasspathHelper.forPackage(packageName));
		// 反射扫描器构造
		Set<Scanner> scanners = new HashSet<>();
		Scanner[] scanner = new Scanner[scanners.size()];
		scanners.add(new TypeAnnotationsScanner());
		scanners.add(new MethodAnnotationsScanner());
		scanners.add(new SubTypesScanner());
		configurationBuilder.setScanners(scanners.toArray(scanner));
		// 反射扫描枪构造结束
		Reflections reflections = new Reflections(configurationBuilder);
		// 获取标有TokenNot的注解的类
		Set<Class<?>> set = reflections.getTypesAnnotatedWith(clazz);
		return set;
	}
}