package com.zxhtom.utils;

import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.boot.web.server.WebServer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author John
 */
@Component
public class ServiceInfoUtil implements ApplicationListener<WebServerInitializedEvent> {
    private WebServer serverInfo;
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        this.serverInfo = event.getWebServer();
    }

    public WebServer getServerInfo() {
        return serverInfo;
    }
}
