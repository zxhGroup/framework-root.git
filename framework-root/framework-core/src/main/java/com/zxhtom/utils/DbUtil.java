package com.zxhtom.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ResourceUtils;

public class DbUtil {
	private static Logger LOG = LogManager.getLogger(DbUtil.class.getName());

	private static ScriptRunner runner = null;
	public static void main(String[] args) {
		try {
			/*DbUtil.execSqlFileByMysql("192.168.0.105", 3306, "shiro", "zxh", "025025",
					"D:\\init\\Documents\\server\\8082\\test.sql");*/
			DbUtil.execSqlFileByOracle("47.98.129.1", 1521, "xe", "shiro", "",
					"E:\\zxh\\test\\tew.sql");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void execSqlFileByOracle(String ip, Integer port,String serverName, String userName, String pwd, String sqlFilePath) throws Exception {
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@"+ip+":"+port+":"+serverName;
		SqlFileExecutor executor = new SqlFileExecutor();
		executor.execute(driver, url, userName, pwd, sqlFilePath);
//		exeSqlFile(userName, pwd, sqlFilePath, driver, url);
	}


	/**
	 * 
	 * @param ip
	 * @param port
	 * @param userName
	 * @param pwd
	 * @param sqlFilePath
	 * @throws Exception
	 */
	public static void execSqlFileByMysql(String ip, Integer port, String dataBase, String userName, String pwd,
			String sqlFilePath) throws Exception {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://" + ip + ":" + port + "/" + dataBase;
		SqlFileExecutor executor = new SqlFileExecutor();
		executor.execute(driver, url, userName, pwd, sqlFilePath);
//		exeSqlFile(userName, pwd, sqlFilePath, driver, url);
	}

	public static void exeSqlFile(Connection conn , List<File> fileList) throws Exception {
		Exception error = null;
		try {
			runner = new ScriptRunner(conn);
			// 下面配置不要随意更改，否则会出现各种问题
			runner.setAutoCommit(true);// 自动提交
			runner.setFullLineDelimiter(false);
			runner.setDelimiter(";");//// 每条命令间的分隔符
			runner.setSendFullScript(false);
			runner.setStopOnError(false);
			// runner.setLogWriter(null);//设置是否输出日志
			// 如果又多个sql文件，可以写多个runner.runScript(xxx),
			for (File file : fileList) {
				runner.runScript(new InputStreamReader(new FileInputStream(file), "utf-8"));
			}
		} catch (Exception e) {
			conn.rollback();
			LOG.error("执行sql文件进行数据库创建失败....", e);
			error = e;
		} finally {
			close(conn);
		}
		if (error != null) {
			throw error;
		}
	}
	
	private static void exeSqlFile(String userName, String pwd, String sqlFilePath, String driver, String url) throws Exception {
		Exception error = null;
		Connection conn = null;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, userName, pwd);
			runner = new ScriptRunner(conn);
			// 下面配置不要随意更改，否则会出现各种问题
			runner.setAutoCommit(true);// 自动提交
			runner.setFullLineDelimiter(false);
			runner.setDelimiter(";");//// 每条命令间的分隔符
			runner.setSendFullScript(false);
			runner.setStopOnError(false);
			// runner.setLogWriter(null);//设置是否输出日志
			// 如果又多个sql文件，可以写多个runner.runScript(xxx),
			runner.runScript(new InputStreamReader(new FileInputStream(sqlFilePath), "utf-8"));
		} catch (Exception e) {
			conn.rollback();
			LOG.error("执行sql文件进行数据库创建失败....", e);
			error = e;
		} finally {
			close(conn);
		}
		if (error != null) {
			throw error;
		}
	}
	private static void close(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			if (conn != null) {
				conn = null;
			}
		}
	}
}
