/*package com.zxhtom.utils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.PooledConnection;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

*//**
 * mybatis支持oracle geometry空间数据类型
 *//*
@MappedTypes({JGeometry.class})
@MappedJdbcTypes({JdbcType.STRUCT})
public class GeometryTypeHandler implements TypeHandler<JGeometry> {
    @Override
    public void setParameter(PreparedStatement ps, int i, JGeometry parameter, JdbcType jdbcType) throws SQLException {
        PooledConnection pooledConnection = (PooledConnection) ps.getConnection();
        if (pooledConnection != null) {
            STRUCT dbObject = JGeometry.store(parameter, pooledConnection.getConnection());
            ps.setObject(i, dbObject);
        } else {
            STRUCT dbObject = JGeometry.store(parameter, ps.getConnection());
            ps.setObject(i, dbObject);
        }
    }

    @Override
    public JGeometry getResult(ResultSet rs, String columnName) throws SQLException {
        STRUCT st = (STRUCT) rs.getObject(columnName);
        if (st != null) {
            return JGeometry.load(st);
        }
        return null;
    }

    @Override
    public JGeometry getResult(ResultSet rs, int columnIndex) throws SQLException {
        STRUCT st = (STRUCT) rs.getObject(columnIndex);
        if (st != null) {
            return JGeometry.load(st);
        }
        return null;
    }

    @Override
    public JGeometry getResult(CallableStatement cs, int columnIndex) throws SQLException {
        STRUCT st = (STRUCT) cs.getObject(columnIndex);
        if (st != null) {
            return JGeometry.load(st);
        }
        return null;
    }
}
*/