package com.zxhtom.utils;

import java.util.Date;

import org.joda.time.DateTime;

import com.zxhtom.CoreConfiguration;
import com.zxhtom.CoreConstants;

/**
 * 支持2种id, 特点不同, 使用场景不同, 请根据业务情况选择
 * <p>
 * 1. long id, 可以保证全局唯一, 包含: 时间+服务器号+序列号, 多台服务器唯一需设置不同的服务器号, 获取方法getId()
 * 参考twitter snowflake, 生成53bit有序自增id, 41bit timestamp + 3bit serverId + 9bit sequence, javascript数值型最大支持53bit
 * 41bit timestamp 服务器时间, 精确到毫秒
 * 3bit serverid 服务器号, 最多8台, 多台服务器不能重复
 * 9bit sequence 序列号, 1台服务器上1毫秒最多512个id, 1秒512000个id
 * <p>
 * 2. int id, 仅在当前进程中唯一, 服务重启后重新从0开始, 获取方法getIntId();
 *
 * @author wuchanglin
 */
public class IdUtil {
    /**
     * 序列号长度9bit
     */
    private static final int sequenceBits = 9;
    /**
     * 服务器号长度3bit
     */
    private static final int serverIdBits = 3;
    /**
     * 服务器号左移9bit
     */
    private static final int serverIdShift = sequenceBits;
    /**
     * 服务器时间左移12bit
     */
    private static final int timestampShift = sequenceBits + serverIdBits;
    /**
     * 基准时间, 2010-01-01
     */
    private static final long baseTimestamp = 1262275200000L;
    /**
     * javascript数值型最大支持53bit, 2^53=9007199254740992
     */
    private static final long jsNumberMax = 9007199254740992L;
    /**
     * 服务器时间
     */
    private volatile long lastTimestamp = -1L;
    /**
     * 序列号
     */
    private volatile int sequence = 0;
    /**
     * 序列号长度9bit, 最大512
     */
    private static final int sequenceMax = 512;
    /**
     * 单例实例
     */
    private static final IdUtil instance = new IdUtil();

    /**
     * int id, 仅在当前进程中唯一, 服务重启后重新从1开始
     */
    private volatile int intId = 1;

    /**
     * 隐藏构造方法
     */
    private IdUtil() {
    }

    /**
     * 应用服务器编号, 多台应用服务器时不能重复
     */
    private static int getServerId() {
        // 配置文件中的应用服务器编号, 多台服务器不能重复
        return CoreConfiguration.getServerId();
    }

    /**
     * 获取下一毫秒时间
     *
     * @param lastTimestamp 当前毫秒
     * @return 下一毫秒时间
     */
    private long getNextMillis(long lastTimestamp) {
        // 获取下一毫秒
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }

    /**
     * long id, 可以保证全局唯一, 包含: 时间+服务器号+序列号, 多台服务器唯一需设置不同的服务器号
     *
     * @return 自增Id
     */
    public synchronized Long getId() {
        long timestamp = System.currentTimeMillis();
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) % sequenceMax;
            // 序列号到达512, 时间取下一毫秒
            if (sequence == 0) {
                timestamp = getNextMillis(lastTimestamp);
            }
        } else {
            // 每毫秒序列号清零
            sequence = 0;
        }
        lastTimestamp = timestamp;
        long result = ((timestamp - baseTimestamp) << timestampShift) | (getServerId() << serverIdShift) | sequence;
        //取绝对值并求余
        return Math.abs(result) % jsNumberMax;
    }

    /**
     * 解析id信息
     *
     * @param id id
     * @return id信息
     */
    public synchronized String getIdInfo(long id) {
        long timestamp = ((0x1FFFFFFFFFF000L & id) >> timestampShift) + baseTimestamp;
        long srv = (0xE00 & id) >> serverIdShift;
        long seq = 0x1FF & id;

        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id);
        sb.append(", timestamp=").append(new DateTime(new Date(timestamp)).toString(CoreConstants.DATETIME_MS_FORMAT));
        sb.append(", server=").append(srv);
        sb.append(", sequence=").append(seq);
        return sb.toString();
    }

    /**
     * int id, 仅在当前进程中唯一, 服务重启后重新从1开始
     *
     * @return
     */
    public synchronized Integer getIntId() {
        return intId++;
    }

    /**
     * 返回单例 IdUtil
     *
     * @return IdUtil
     */
    public static IdUtil getInstance() {
        return instance;
    }
}
