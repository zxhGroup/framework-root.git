package com.zxhtom.utils;

import org.springframework.data.redis.core.RedisTemplate;

public class RedisUtil {
    /**
     * 单列模式(懒汉)
     */
    private static RedisUtil redisUtil;

    private RedisTemplate redisTemplate;
    private RedisUtil(){
        redisTemplate = ContextUtil.getApplicationContext().getBean(RedisTemplate.class);
    }

    public static RedisUtil getInstance(){
        if (redisUtil == null) {
            redisUtil = new RedisUtil();
        }
        return redisUtil;
    }

    public RedisTemplate getRedisTemplate(){
        return redisTemplate;
    }
}
