package com.zxhtom.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 字符串长度校验器(按字节)
 * Java字符串是Unicode, 中英文都是2字节, Oracle数据库字符串类型varchar2, 中文2字节, 英文1字节, 所以定制此校验器
 */
@Documented
@Constraint(validatedBy = ByteLengthValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface ByteLength {
    int min() default 0;

    int max() default Integer.MAX_VALUE;

    String message() default "字符串字节长度必须在 {min} 到 {max} 之间";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    public @interface List {
        ByteLength[] value();
    }
}
