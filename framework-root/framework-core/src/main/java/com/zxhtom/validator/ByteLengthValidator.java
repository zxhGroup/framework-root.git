package com.zxhtom.validator;

import java.io.UnsupportedEncodingException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import com.zxhtom.exception.BusinessException;

public class ByteLengthValidator implements ConstraintValidator<ByteLength, CharSequence> {
    private Logger logger = LogManager.getLogger(ByteLengthValidator.class);
    private int min;
    private int max;

    @Override
    public void initialize(ByteLength parameters) {
        min = parameters.min();
        max = parameters.max();
        validateParameters();
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) {
            return true;
        }

        String str = value.toString();
        if (str == null) {
            return true;
        }

        try {
            int length = str.getBytes("GBK").length;
            return length >= min && length <= max;
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("ByteLength校验出错, GBK编码不支持!", e);
        }
    }

    private void validateParameters() {
        if (min < 0) {
            logger.warn("getMinCannotBeNegativeException");
        }
        if (max < 0) {
            logger.warn("getMaxCannotBeNegativeException");
        }
        if (max < min) {
            logger.warn("getLengthCannotBeNegativeException");
        }
    }
}
