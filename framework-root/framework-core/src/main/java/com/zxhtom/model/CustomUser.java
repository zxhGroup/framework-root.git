package com.zxhtom.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author John
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customUser", propOrder = {
		"password",
		"userCode",
		"userId",
		"userName",
		"sex",
		"photo"
})
@ApiModel("学生表")
@Table(name = "SYS_USER")
public class CustomUser implements Serializable{

	@Id
	private Long userId;
	private String userCode;
	private String userName;
	private String password;
	private Integer sex;
	private String photo;

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
