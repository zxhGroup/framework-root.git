package com.zxhtom.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "zxhtom.config.remote")
public class WebServiceProperties {

    private String base;
    private String url;
    private String appUrl;
    private String userName;

    private String password;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getAppUrl() {
        if (appUrl == null) {
            appUrl = base + "/services/app?wsdl";
        }
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        if (url == null) {
            url = base + "/services/user?wsdl";
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
