package com.zxhtom.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "zxhtom.config")
public class SysConfig {

	private Boolean viewHandler;

	private String logLevel;

	private Boolean monitor;

	public Boolean getMonitor() {
		return monitor;
	}

	public void setMonitor(Boolean monitor) {
		this.monitor = monitor;
	}

	public String getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	public Boolean getViewHandler() {
		return viewHandler;
	}

	public void setViewHandler(Boolean viewHandler) {
		this.viewHandler = viewHandler;
	}
}
