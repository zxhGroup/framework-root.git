package com.zxhtom.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.datasource.druid")
public class DruidStatProperties {
    private String aopPatterns;

    public String getAopPatterns() {
        return aopPatterns;
    }

    public void setAopPatterns(String aopPatterns) {
        this.aopPatterns = aopPatterns;
    }
}
