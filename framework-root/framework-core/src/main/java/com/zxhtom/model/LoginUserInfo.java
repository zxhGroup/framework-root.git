package com.zxhtom.model;

import java.io.Serializable;
import java.util.Set;

public class LoginUserInfo implements Serializable{

	private Set<String> roles;
	
	private Set<String> permissions;
	
	private CustomUser userInfo;
	
	public CustomUser getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(CustomUser userInfo) {
		this.userInfo = userInfo;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	public Set<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}
}
