package com.zxhtom.model;

import java.security.Principal;
import java.util.Objects;

/**
 * 简易版的用户信息
 * @author John
 */
public class User implements Principal {
    private Long userId;
    private String name;


    public User() {

    }
    public User(Long userId, String name) {
        this.userId=userId;
        this.name=name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId) &&
                Objects.equals(name, user.name);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
