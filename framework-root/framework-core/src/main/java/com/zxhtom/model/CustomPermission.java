package com.zxhtom.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Table;

@ApiModel("权限表")
@Table(name = "SYS_PERMISSION")
public class CustomPermission {

	private Long permissionId;
	private String permissionName;
	private String permissionRemark;
	public Long getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	public String getPermissionRemark() {
		return permissionRemark;
	}
	public void setPermissionRemark(String permissionRemark) {
		this.permissionRemark = permissionRemark;
	}

	
}
