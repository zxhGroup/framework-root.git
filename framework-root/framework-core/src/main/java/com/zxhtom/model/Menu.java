package com.zxhtom.model;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.zxhtom.dto.MenuDto;
import io.swagger.annotations.ApiModel;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "menu", propOrder = {
		"menuCode",
		"menuId",
		"menuName",
		"menuType",
		"menuUrl",
		"icon",
		"parentMenuId"
})
@XmlSeeAlso({
		MenuDto.class
})
@ApiModel("菜单表")
@Table(name = "SYS_MENU")
public class Menu {

	@Id
	private Long menuId;
	private String menuName;
	private String menuCode;
	private Integer menuType;
	private String menuUrl;
	private String icon;
	private Long parentMenuId;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getMenuType() {
		return menuType;
	}

	public void setMenuType(Integer menuType) {
		this.menuType = menuType;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public Long getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Long parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	@Override
	public String toString() {
		return "Menu [menuId=" + menuId + ", menuName=" + menuName + ", menuCode=" + menuCode + ", menuType=" + menuType
				+ ", menuUrl=" + menuUrl + ", parentMenuId=" + parentMenuId + "]";
	}
	
}
