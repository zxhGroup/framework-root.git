package com.zxhtom.model;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


import io.swagger.annotations.ApiModel;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "module", propOrder = {
		"moduleCode",
		"icon",
		"moduleId",
		"moduleName",
		"moduleUrl"
})
@ApiModel(description = "模块表")
@Table(name = "SYS_MODULE")
public class Module {

	/**
	 * 模块ID
	 */
	@Id
	private Long moduleId;
	/**
	 * 模块名称
	 */
	private String moduleName;
	/**
	 * 模块编码
	 */
	private String moduleCode;
	/**
	 * 模块链接
	 */
	private String moduleUrl;
	/**
	 * 图标
	 */
	private String icon;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * 设置：模块ID
	 */
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * 获取：模块ID
	 */
	public Long getModuleId() {
		return moduleId;
	}
	/**
	 * 设置：模块名称
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/**
	 * 获取：模块名称
	 */
	public String getModuleName() {
		return moduleName;
	}
	/**
	 * 设置：模块编码
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	/**
	 * 获取：模块编码
	 */
	public String getModuleCode() {
		return moduleCode;
	}
	/**
	 * 设置：模块链接
	 */
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
	/**
	 * 获取：模块链接
	 */
	public String getModuleUrl() {
		return moduleUrl;
	}

	@Override
	public String toString() {
		return "SysModule [moduleId=" + moduleId+",moduleName=" + moduleName + ",moduleCode=" + moduleCode + ",moduleUrl=" + moduleUrl + "]";
	}

}
