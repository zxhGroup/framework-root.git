package com.zxhtom.core.service.impl;

import com.zxhtom.CoreConstants;
import com.zxhtom.annotation.RedisForm;
import com.zxhtom.config.shiro.CustomRealm;
import com.zxhtom.core.repository.BaseCustomRepository;
import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.core.repository.CustomShiroRepository;
import com.zxhtom.core.service.CustomService;
import com.zxhtom.dto.MenuDto;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;
import com.zxhtom.utils.ContextUtil;
import com.zxhtom.vconstant.PackageName;
import com.zxhtom.vconstant.RedisKey;
import com.zxhtom.vconstant.RedisLocalKey;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CustomServiceImpl implements CustomService {

	@SuppressWarnings("rawtypes")
	@Autowired
	private RedisTemplate redisTemplate;

	private BaseCustomRepository customRepository;

	private VistualCustomService vistualCustomService;


	public VistualCustomService getVistualCustomService() {
		Session session = SecurityUtils.getSubject().getSession();
		if(session.getAttribute(PackageName.REALM).toString().equals(PackageName.SERVICE)){
			customRepository = ContextUtil.getApplicationContext().getBean(CustomShiroRepository.class);
		}else if(session.getAttribute(PackageName.REALM).toString().equals(PackageName.LOCAL)){
			customRepository = ContextUtil.getApplicationContext().getBean(CustomRepository.class);
		}else{
			throw new BusinessException("请配置realm 对应的数据请求");
		}
		if (vistualCustomService == null) {
			vistualCustomService = new VistualCustomService();
		}
		return vistualCustomService;
	}

	public RedisTemplate getRedisTemplate() {
		return redisTemplate;
	}

	public void setRedisTemplate(RedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public BaseCustomRepository getCustomRepository() {
		return customRepository;
	}

	public void setCustomRepository(CustomRepository customRepository) {
		this.customRepository = customRepository;
	}

	@Override
	public Map<Long, Long> selectVistualMenu() {
		return getVistualCustomService().selectVistualMenu();
	}

	@Override
	public Long selectVistualMenuId(Long menuId) {
		return getVistualCustomService().selectVistualMenuId(menuId);
	}

	@Override
	public Long selectFactMenuId(Long menuId) {
		return getVistualCustomService().selectFactMenuId(menuId);
	}

	@Override
	public List<CustomRole> selectRolesVistualMenuId(Long menuId) {
		return getVistualCustomService().selectRolesVistualMenuId(menuId);
	}

	@Override
	public CustomUser selectUserInfoByUserName(String userName) {
		return getVistualCustomService().selectUserInfoByUserName(userName);
	}

	@Override
	public List<MenuDto> selectMenuListByUserId(Long userId) {
		return getVistualCustomService().selectMenuListByUserId(userId);
	}

	@Override
	public List<MenuDto> selectMenuListByUserName(String userName) {
		return getVistualCustomService().selectMenuListByUserName(userName);
	}

	@Override
	public CustomUser selectUserByUserId(Long userId) {
		return getVistualCustomService().selectUserByUserId(userId);
	}

    @Override
    public AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return getVistualCustomService().doGetAuthorizationInfo(principals);
    }

    @Override
    public List<Module> selectModuleByCurrentUser() {
        return getVistualCustomService().selectModuleByCurrentUser();
    }

    @Override
    public Integer insertCustomUser(CustomUser user) {
        return getVistualCustomService().insertCustomUser(user);
    }


    class VistualCustomService implements CustomService{
		@Override
		public Map<Long, Long> selectVistualMenu() {
			return customRepository.selectVistualMenu();
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		private Map<Long, Long> getVistualMenuMap() {
			HashOperations hash = redisTemplate.opsForHash();
			Map<Long, Long> menuMap = new HashMap<Long, Long>();
			if (hash.hasKey(CoreConstants.MENUPRE, CoreConstants.VISTUALMENUKEY)) {
				menuMap = (Map<Long, Long>) hash.get(CoreConstants.MENUPRE, CoreConstants.VISTUALMENUKEY);
			} else {
				menuMap = selectVistualMenu();
			}
			return menuMap;
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		private Map<Long, Long> getFactMenuMap() {
			HashOperations hash = redisTemplate.opsForHash();
			Map<Long, Long> menuMap = new HashMap<Long, Long>();
			if (!hash.hasKey(CoreConstants.MENUPRE, CoreConstants.FACTMENUKEY)) {
				selectVistualMenu();
			}
			menuMap = (Map<Long, Long>) hash.get(CoreConstants.MENUPRE, CoreConstants.FACTMENUKEY);
			return menuMap;
		}

		@Override
		public Long selectVistualMenuId(Long menuId) {
			Map<Long, Long> menuMap = getVistualMenuMap();
			if (menuMap.containsKey(menuId.toString())) {
				return menuMap.get(menuId.toString());
			}
			return -2L;
		}

		@Override
		public Long selectFactMenuId(Long menuId) {
			Map<Long, Long> menuMap = getFactMenuMap();
			if (menuMap.containsKey(menuId.toString())) {
				return menuMap.get(menuId.toString());
			}
			return -2L;
		}

		@Override
		public List<CustomRole> selectRolesVistualMenuId(Long menuId) {
			menuId = selectFactMenuId(menuId);
			return customRepository.selectRolesByMenuId(menuId);
		}

		@Override
		public CustomUser selectUserInfoByUserName(String userName) {
			return customRepository.selectUserInfoByUserName(userName);
		}

		@Override
		@RedisForm(keyF={RedisKey.USERMENULIST},keyS={RedisKey.CURRENTPARAM},select=true)
		public List<MenuDto> selectMenuListByUserId(Long userId) {
            Object attribute = SecurityUtils.getSubject().getSession().getAttribute(CoreConstants.OAUTHCLIENTID);
            String key = "";
            Long lKey=null;
            if (attribute != null) {
                key = attribute.toString();
                lKey = Long.valueOf(key);
            }
			String redisKey = key+":";
			if (SecurityUtils.getSubject().getSession().getAttribute(PackageName.REALM).toString().equals(PackageName.LOCAL)) {
				redisKey += RedisLocalKey.USERMENULIST;
			} else {
				redisKey += RedisKey.USERMENULIST;
			}
			HashOperations hashOperations = redisTemplate.opsForHash();
			if (hashOperations.hasKey(redisKey, userId)) {
				return (List<MenuDto>) hashOperations.get(redisKey, userId);
			}
			// 首先获取用户权限内的跟菜单 这里的moduleName 是后台的moduleCode
			List<MenuDto> rootMenuList = customRepository.selectRootMenusByUserId(userId,lKey);
			// 递归获取子菜单数据
			selectMenuListByRootMenuList(userId,rootMenuList);
			hashOperations.put(redisKey, userId, rootMenuList);
			return rootMenuList;
		}

		@Override
		public List<MenuDto> selectMenuListByUserName(String userName) {

			CustomUser customUser = selectUserInfoByUserName(userName);
			return selectMenuListByUserId(customUser.getUserId());
		}

		@Override
		public CustomUser selectUserByUserId(Long userId) {

			return customRepository.selectUserByUserId(userId);
		}

		@Override
		public AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
			CustomRealm realm = ContextUtil.getApplicationContext().getBean(CustomRealm.class);
			return realm.doGetAuthorizationInfo(principals);
		}

		@Override
		public List<Module> selectModuleByCurrentUser() {
			CustomUser customUser = (CustomUser) SecurityUtils.getSubject().getPrincipal();
			return customRepository.selectModuleByCurrentUser(customUser.getUserId(),Long.valueOf(SecurityUtils.getSubject().getSession().getAttribute(CoreConstants.OAUTHCLIENTID).toString()));
		}

		@Override
		public Integer insertCustomUser(CustomUser user) {
			return customRepository.insertCustomUser(user);
		}

		public List<MenuDto> selectMenuListByRootMenuList(Long userId,List<MenuDto> rootMenuList) {
            Object attribute = SecurityUtils.getSubject().getSession().getAttribute(CoreConstants.OAUTHCLIENTID);
            Long lkey = null;
            if (attribute != null) {
                lkey = Long.parseLong(attribute.toString());
            }
            if (rootMenuList == null) {
				return null;
			}
			for (MenuDto menuDto : rootMenuList) {
				List<MenuDto> childMenuList = customRepository.selectChildMenuListMenuId(userId,menuDto.getMenuId(),lkey);
				menuDto.setChildList(childMenuList);
				selectMenuListByRootMenuList(userId,childMenuList);
			}
			return null;
		}
	}
}
