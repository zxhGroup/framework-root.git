package com.zxhtom.core.repository.impl;

import com.zxhtom.annotation.RedisForm;
import com.zxhtom.core.repository.AppOriginRepository;
import com.zxhtom.core.repository.CustomOriginRepository;
import com.zxhtom.core.repository.CustomShiroRepository;
import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.vconstant.RedisKey;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author John
 */
@Repository
public class CustomShiroRepositoryImpl implements CustomShiroRepository {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired(required = false)
    private CustomOriginRepository customOriginRepository;
    @Autowired(required = false)
    private AppOriginRepository appOriginRepository;

    @Override
    public List<Module> selectModuleByCurrentUser(Long userId, Long oauthClientId) {
        return customOriginRepository.selectModuleByCurrentUser(userId,oauthClientId);
    }

    @Override
    public CustomUser selectCurrentUserName(String accessToken) {
        return customOriginRepository.selectCurrentUserName(accessToken);
    }

    @Override
    @RedisForm
    public CustomUser selectUserByUserName(String userName) {
        return customOriginRepository.selectUserByUserName(userName);

    }

    @Override
    public Set<String> selectPremissionNamesByUserId(Long userId, String clientId , String clientSecrect) {
        String key = userId + ":" + clientId+clientSecrect;
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(key, RedisKey.USERPERMISSIONLIST)) {
            return (Set<String>) opsForHash.get(key, RedisKey.USERPERMISSIONLIST);
        }
        HashSet<String> resultSet = new HashSet<>(customOriginRepository.selectPremissionNamesByUserId(userId,clientId,clientSecrect));
        // 加入缓存
        opsForHash.put(key, RedisKey.USERPERMISSIONLIST, resultSet);
        return resultSet;
    }

    @Override
    public Set<String> selectRoleNamesByUserId(Long userId) {
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(userId, RedisKey.USERROLELIST)) {
            return (Set<String>) opsForHash.get(userId, RedisKey.USERROLELIST);
        }
        HashSet<String> resultSet = new HashSet<>(customOriginRepository.selectRoleNamesByUserId(userId));
        // 加入缓存
        opsForHash.put(userId, RedisKey.USERROLELIST, resultSet);
        return resultSet;
    }

    @Override
    public Set<CustomRole> selectRolesByUserId(Long userId) {
        return new HashSet<>(customOriginRepository.selectRolesByUserId(userId));
    }

    @Override
    public Map<Long, Long> selectVistualMenu() {
        return (Map<Long, Long>) appOriginRepository.selectVistualMenu();
    }

    @Override
    public List<CustomRole> selectRolesByMenuId(Long menuId) {
        HashOperations hash = redisTemplate.opsForHash();
        if (redisTemplate.hasKey(RedisKey.ROLEMENU)) {
            return (List<CustomRole>) hash.get(RedisKey.ROLEMENU, menuId);
        }
        List<CustomRole> roleList = customOriginRepository.selectRolesByMenuId(menuId);
        if (CollectionUtils.isNotEmpty(roleList)) {
            hash.put(RedisKey.ROLEMENU, menuId, roleList);
        }
        return roleList;
    }

    @Override
    public CustomUser selectUserInfoByUserName(String userName) {
        return customOriginRepository.selectUserInfoByUserName(userName);
    }

    @Override
    public List<MenuDto> selectRootMenusByUserIdAndModeul(Long userId, String moduleCode, Long oauthClientId) {
        String key = userId + ":" + oauthClientId;
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(RedisKey.USERMENULIST,key)) {
            return (List<MenuDto>) opsForHash.get(RedisKey.USERMENULIST,key);
        }
        List<MenuDto> menuList = customOriginRepository.selectRootMenusByUserIdAndModeul(userId, moduleCode,oauthClientId);
        opsForHash.put(RedisKey.USERMENULIST,key, menuList);
        return menuList;
    }

    @Override
    public List<MenuDto> selectChildMenuListMenuId(Long userId, Long menuId, Long oauthClientId) {
        return customOriginRepository.selectChildMenuListMenuId(userId,menuId,oauthClientId);
    }

    @Override
    public List<MenuDto> selectRootMenusByUserId(Long userId, Long oauthClientId) {
        String key = userId + ":" + oauthClientId;
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(RedisKey.USERROOTMENULIST,key)) {
            return (List<MenuDto>) opsForHash.get(RedisKey.USERROOTMENULIST,key);
        }
        List<MenuDto> menuList = customOriginRepository.selectRootMenusByUserId(userId,oauthClientId);
        opsForHash.put(RedisKey.USERROOTMENULIST,key, menuList);
        return menuList;
    }

    @Override
    public List<Module> selectAllModules() {
        return appOriginRepository.selectAllModules();
    }

    @Override
    public List<CustomRole> selectRolesByModuleCode(String moduleName, String clientId, String clientSecret) {
        return appOriginRepository.selectRolesByModuleCode(moduleName, clientId,clientSecret);
    }

    @Override
    public CustomUser selectUserByUserId(Long userId) {
        return customOriginRepository.selectUserByUserId(userId);
    }

    @Override
    public Integer insertCustomUser(CustomUser user) {
        user.setUserId(IdUtil.getInstance().getId());
        user.setPassword(new Md5Hash(user.getPassword(),"zxh").toString());
        return customOriginRepository.insertCustomUser(user);
    }
}
