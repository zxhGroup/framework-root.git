package com.zxhtom.core.repository;

import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface BaseCustomRepository {
    /**
     * 通过用户获取该用户在该商户下的模块列表
     * @param userId
     * @param oauthClientId
     */
    public List<Module> selectModuleByCurrentUser(Long userId , Long oauthClientId);
    /**
     * 获取当前登录的用户
     * @return
     */
    CustomUser selectCurrentUserName(String accessToken);
    /**
     * 通过用户名查询用户
     *
     * @param userName
     *            用户名
     * @return date 2018年5月22日
     */
    CustomUser selectUserByUserName(String userName);

    /**
     * 通过用户名查询权限列表
     *
     * @param userId 用户名
     * @param clientId 商户id
     * @param clientSecret 商户秘钥
     * @return date 2018年5月22日
     */
    Set<String> selectPremissionNamesByUserId(Long userId , String clientId , String clientSecret);

    /**
     * 通过用户名查询角色名列表
     *
     * @param userId 用户名
     * @return date 2018年5月22日
     */
    Set<String> selectRoleNamesByUserId(Long userId);

    /**
     * 通过用户名查询角色列表
     *
     * @param userId
     *            用户名
     * @return date 2018年5月22日
     */
    Set<CustomRole> selectRolesByUserId(Long userId);

    /**
     * 将menuID虚拟
     *
     * @return
     */
    Map<Long, Long> selectVistualMenu();

    /**
     * 通过菜单id获取对应的权限列表集合
     *
     * @param menuId
     * @return date 2018年7月6日
     */
    List<CustomRole> selectRolesByMenuId(Long menuId);

    /**
     * 通过用户名获取完整信息
     *
     * @param userName
     * @return date 2018年7月11日
     */
    CustomUser selectUserInfoByUserName(String userName);

    /**
     * 根据角色获取对应模块下的菜单根列表
     *
     * @param userId 用户id
     * @param moduleCode 模块编号
     * @param oauthClientId 商户id
     * @return date 2018年7月11日
     */
    List<MenuDto> selectRootMenusByUserIdAndModeul(Long userId, String moduleCode,Long oauthClientId);

    /**
     * 获取菜单下的子菜单
     * @param userId 用户id
     * @param menuId 菜单id
     * @param oauthClientId 商户id
     * @return date 2018年7月11日
     */
    List<MenuDto> selectChildMenuListMenuId(Long userId , Long menuId,Long oauthClientId);

    /**
     * 根据用户获取商户菜单，模块将会系统确定
     * @param userId 用户id
     * @param userId 商户id
     * @return
     */
    List<MenuDto> selectRootMenusByUserId(Long userId,Long oauthClientId);

    /**
     * 获取所有模块
     * @return
     */
    List<Module> selectAllModules();

    /**
     * 通过模块编号查询角色
     * @param moduleName 模块编码
     * @param clientId 商户id
     * @return
     */
    List<CustomRole> selectRolesByModuleCode(String moduleName,String clientId , String clientSecret);

    /**
     * 获取用户信息
     * @param userId
     * @return
     */
    CustomUser selectUserByUserId(Long userId);

    /**
     * 新增用户
     * @param user
     * @return
     */
    Integer insertCustomUser(CustomUser user);
}
