package com.zxhtom.core.repository;

import com.zxhtom.model.CustomRole;
import com.zxhtom.model.Module;

import javax.jws.WebService;
import java.util.List;
import java.util.Map;

@WebService
public interface AppOriginRepository {
    /**
     * 将menuID虚拟
     *
     * @return
     */
    Map<Long, Long> selectVistualMenu();
    /**
     * 获取所有模块
     * @return
     */
    List<Module> selectAllModules();

    /**
     * 通过模块编号查询角色
     * @param moduleName
     * @return
     */
    List<CustomRole> selectRolesByModuleCode(String moduleName, String clientId ,String clientSecret);
}
