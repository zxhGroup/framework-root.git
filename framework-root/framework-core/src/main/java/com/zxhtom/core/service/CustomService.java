package com.zxhtom.core.service;

import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.List;
import java.util.Map;

public interface CustomService {

	Map<Long, Long> selectVistualMenu();
	
	Long selectVistualMenuId(Long menuId);
	
	Long selectFactMenuId(Long menuId);

	/**
	 * 通过前台的虚拟菜单查询权限
	 * @param menuId
	 * @return   
	 * date 2018年7月6日
	 */
	List<CustomRole> selectRolesVistualMenuId(Long menuId);

	/**
	 * 通过用户名获取完整信息
	 * @param userName
	 * @return   
	 * date 2018年7月11日
	 */
	CustomUser selectUserInfoByUserName(String userName);

	/**
	 * 通过用户id获取用户界面左侧的菜单树数据
	 * @param userId
	 * @return   
	 * date 2018年7月11日
	 */
	List<MenuDto> selectMenuListByUserId(Long userId);

	/**
	 * 通过用户名查询菜单信息
	 * @param userName
	 * @return
	 */
    List<MenuDto> selectMenuListByUserName(String userName);

	/**
	 * 获取用户
	 * @param userId
	 * @return
	 */
    CustomUser selectUserByUserId(Long userId);

	/**
	 * 为了抽离realm和controller
	 * @param principals
	 * @return
	 */
    AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals);

	/**
	 * 获取用户模块
	 * @return
	 */
	List<Module> selectModuleByCurrentUser();

    Integer insertCustomUser(CustomUser user);


}
