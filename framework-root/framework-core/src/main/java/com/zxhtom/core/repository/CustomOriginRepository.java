package com.zxhtom.core.repository;

import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;

import javax.jws.WebService;
import java.util.List;
import java.util.Set;

/**
 * 服务端 客户端用的是同一个文件，及包名类名相同，如果服务端和客户端用的不一样，
 * 那么客户端这个地方需要指定服务端的位置
 * @WebService(name = "CustomOriginRepository", targetNamespace = "http://repository.shiro.zxhtom.com/")
 */
@WebService
public interface CustomOriginRepository {

	/***
	 *
	 * @Method : selectModuleByCurrentUser
	 * @Description : 通过用户获取该用户在该商户下的模块列表 
	 * @param userId : 用户id
	 * @param oauthClientId : 商户id
	 * @return : java.util.List<com.zxhtom.model.Module>
	 * @author : zhangxinhua
	 * @CreateDate : 2019-05-05 13:48:58
	 * 
	 */
	List<Module> selectModuleByCurrentUser(Long userId, Long oauthClientId);
	/**
	 * 通过用户名查询用户
	 * 
	 * @param userName
	 *            用户名
	 * @return date 2018年5月22日
	 */
	CustomUser selectUserByUserName(String userName);

	/**
	 * 通过用户名查询权限列表
	 * 
	 * @param userId
	 *            用户名
	 * @return date 2018年5月22日
	 */
	Set<String> selectPremissionNamesByUserId(Long userId, String clientId,String clientSecret);

	/**
	 * 通过用户名查询角色名列表
	 * 
	 * @param userId
	 *            用户名
	 * @return date 2018年5月22日
	 */
	Set<String> selectRoleNamesByUserId(Long userId);

	/**
	 * 通过用户名查询角色列表
	 * 
	 * @param userId
	 *            用户名
	 * @return date 2018年5月22日
	 */
	Set<CustomRole> selectRolesByUserId(Long userId);

	/**
	 * 通过菜单id获取对应的权限列表集合
	 * 
	 * @param menuId
	 * @return date 2018年7月6日
	 */
	List<CustomRole> selectRolesByMenuId(Long menuId);

	/**
	 * 通过用户名获取完整信息
	 * 
	 * @param userName
	 * @return date 2018年7月11日
	 */
	CustomUser selectUserInfoByUserName(String userName);

	/**
	 * 根据角色获取对应模块下的菜单根列表
	 * 
	 * @param userId
	 * @param moduleCode
	 * @return date 2018年7月11日
	 */
	List<MenuDto> selectRootMenusByUserIdAndModeul(Long userId, String moduleCode, Long oauthClientId);

	/**
	 * 获取菜单下的子菜单
	 * 
	 * @param menuId
	 * @return date 2018年7月11日
	 */
	List<MenuDto> selectChildMenuListMenuId(Long userId, Long menuId, Long oauthClientId);

	/**
	 * 根据用户获取菜单，模块将会系统确定
	 * @param userId
	 * @return
	 */
	List<MenuDto> selectRootMenusByUserId(Long userId, Long oauthClientId);


	/**
	 * 获取当前登录用户
	 * @return
	 */
	CustomUser selectCurrentUserName(String accessToken);

	/**
	 * 获取用户、信息
	 * @param userId
	 * @return
	 */
    CustomUser selectUserByUserId(Long userId);

	Integer insertCustomUser(CustomUser user);
}
