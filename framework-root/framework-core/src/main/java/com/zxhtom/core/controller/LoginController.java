package com.zxhtom.core.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.zxhtom.CoreConstants;
import com.zxhtom.annotation.OnlyLogin;
import com.zxhtom.annotation.TokenNot;
import com.zxhtom.config.shiro.CustomRealm;
import com.zxhtom.config.shiro.ShiroUtils;
import com.zxhtom.config.shiro.client.OAuth2Token;
import com.zxhtom.config.shiro.token.CustomUserPasswordToken;
import com.zxhtom.core.service.impl.CustomServiceImpl;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.model.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONObject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Controller
@RequestMapping(value="/framework_core/login")
@Api(tags="framework_core login",description="系统的用户登录登出常用接口")
public class LoginController {

	@Autowired
	private CustomServiceImpl customService;
	@Autowired
	private Client client;
	@Autowired
	private WebServiceProperties properties;
	@Autowired
	private RedisProperties redisProperties;

	@Autowired
	private Producer producer;

	@RequestMapping(value="/captcha.jpg")
	@TokenNot
	public void captcha(HttpServletResponse response)throws IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");

		//生成文字验证码
		String text = producer.createText();
		//生成图片验证码
		BufferedImage image = producer.createImage(text);
		//保存到shiro session
		SecurityUtils.getSubject().getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, text);

		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
	}

    @RequestMapping(value = "/oauth2", method = RequestMethod.GET)
    @ResponseBody
    @TokenNot
    @ApiOperation(value="服务登录处理接口")
    public JSONObject serviceLogin(HttpServletRequest request , HttpServletResponse response){
		JSONObject jsonObject = new JSONObject();
		LoginUserInfo userInfo = new LoginUserInfo();
		Subject subject = SecurityUtils.getSubject();
		String code = request.getParameter("code");
		//保存服务端code
		CoreConstants.map.put("code", code);
		OAuth2Token token = new OAuth2Token(code);
		try {
			subject.login(token);
			AuthorizationInfo authorizationInfo = customService.doGetAuthorizationInfo(subject.getPrincipals());
			userInfo.setPermissions(new HashSet<>(authorizationInfo.getStringPermissions()));
			userInfo.setRoles(new HashSet<>(authorizationInfo.getRoles()));
			userInfo.setUserInfo(customService.selectUserInfoByUserName("admin"));
			jsonObject=JSONObject.fromObject(userInfo);
			jsonObject.put("x-token", subject.getSession().getId());
			request.getSession().setAttribute(CoreConstants.USERNAME, userInfo.getUserInfo().getUserName());
			request.getSession().setAttribute(CoreConstants.USERID, userInfo.getUserInfo().getUserId());
			request.getSession().setAttribute("userInfo", jsonObject);
			Cookie cookie = new Cookie("login","true");  //对比入参数据
			response.addCookie(cookie);
		} catch (IncorrectCredentialsException e) {
			throw new BusinessException("密码错误test");
		} catch (LockedAccountException e) {
			throw new BusinessException("登录失败，该用户已被冻结");
		} catch (AuthenticationException e) {
			throw new BusinessException("该用户不存在");
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		if (subject.hasRole("admin")) {
//			return "有admin权限";
		}
		return jsonObject;
    }

	@RequestMapping(value = "/startLogin", method = RequestMethod.POST)
	@ResponseBody
	@TokenNot
	@ApiOperation(value="登录接口，返回凭证用于前后分离")
	public JSONObject subLogin(HttpServletRequest request , HttpServletResponse response , @ApiParam(value="登录的用户信息",required=true) @RequestBody CustomUser user) throws BusinessException {
    	//登录前先清空权限缓存
		ShiroUtils.clearAuth();
		JSONObject jsonObject = new JSONObject();
		LoginUserInfo userInfo = new LoginUserInfo();
		Subject subject = SecurityUtils.getSubject();
		CustomUserPasswordToken token = new CustomUserPasswordToken(user, user.getPassword());
		try {
			subject.login(token);
			AuthorizationInfo authorizationInfo = customService.doGetAuthorizationInfo(subject.getPrincipals());
			userInfo.setPermissions(new HashSet<>(authorizationInfo.getStringPermissions()));
			userInfo.setRoles(new HashSet<>(authorizationInfo.getRoles()));
			userInfo.setUserInfo(customService.selectUserInfoByUserName(user.getUserName()));
			jsonObject=JSONObject.fromObject(userInfo);
			jsonObject.put("x-token", subject.getSession().getId());
			request.getSession().setAttribute(CoreConstants.USERNAME, userInfo.getUserInfo().getUserName());
			request.getSession().setAttribute(CoreConstants.USERID, userInfo.getUserInfo().getUserId());
			request.getSession().setAttribute("userInfo", jsonObject);
			//将如x-token , 暂时没有加入请求头
			AtomicReference<Cookie> cookie;  //对比入参数据
            cookie = new AtomicReference<>(new Cookie("x-token", subject.getSession().getId().toString()));
            cookie.get().setMaxAge(redisProperties.getTimeout()*1000);
			cookie.get().setPath("/");
			response.addCookie(cookie.get());
			SavedRequest savedRequest = WebUtils.getAndClearSavedRequest(request);
			if (savedRequest != null && savedRequest.getRequestUrl() != null) {
				//response.sendRedirect(savedRequest.getRequestUrl());
				jsonObject.put("backUrl", savedRequest.getRequestUrl());
			}
		} catch (IncorrectCredentialsException e) {
	        throw new BusinessException("密码错误");
	    } catch (LockedAccountException e) {  
	        throw new BusinessException("登录失败，该用户已被冻结");
	    } catch (AuthenticationException e) {  
	        throw new BusinessException("用户名密码不匹配");
	    } catch (Exception e) {  
	    	throw new BusinessException(e);  
	    } 
		if (subject.hasRole("admin")) {
//			return "有admin权限";
		}
		return jsonObject;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	@TokenNot
	@ApiOperation(value="登录接口，返回凭证用于前后分离")
	public Integer register(@ApiParam(value = "注册用户") @RequestBody CustomUser user){
		user.setUserCode(user.getUserName());
		user.setSex(1);
        customService.insertCustomUser(user);
		return 1;
	}
	@RequestMapping(value="/login",method = RequestMethod.GET)
	@TokenNot
	@ApiIgnore
	public String login(Model model){
		model.addAttribute("client", client);
		model.addAttribute("remote", properties);
		return "login";
	}
	@RequestMapping(value="/logout",method = RequestMethod.GET)
	@OnlyLogin
	@ApiIgnore
	public void logout(HttpServletResponse response){
		SecurityUtils.getSubject().logout();
		try {
			response.sendRedirect("/framework_core/login/login");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@RequestMapping(value="/403",method = RequestMethod.GET)
	@TokenNot
	@ApiIgnore
	public String failPermission(){
		return "403";
	}
}
