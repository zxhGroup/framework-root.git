package com.zxhtom.core.controller;

import com.zxhtom.annotation.OnlyLogin;
import com.zxhtom.core.service.CustomService;
import com.zxhtom.model.CustomUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONObject;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController("SysCoreUserController")
@RequestMapping(value = "/framework_core/user")
@Api(tags="framework_core user",description="用户常用接口")
@OnlyLogin
public class SysCoreUserController {
	@Autowired
	private CustomService customService;

	@RequestMapping(value = "/{userId}" , method = RequestMethod.GET)
	@ApiOperation(value = "获取用户信息")
	public CustomUser selectUserByUserId(@ApiParam(value = "用户iD") @PathVariable Long userId){
		return customService.selectUserByUserId(userId);
	}
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@ApiOperation(value="获取登录用户的基本信息")
	public Object selectUserInfo(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		CustomUser userInfo = new CustomUser();
		try {
			userInfo = (CustomUser) SecurityUtils.getSubject().getPrincipal();
		} catch (Exception e) {
			e.printStackTrace();
		}
		json.put("userInfo", userInfo);
		return json;
	}
}
