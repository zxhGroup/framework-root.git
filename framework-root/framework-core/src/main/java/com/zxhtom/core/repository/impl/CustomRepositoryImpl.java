package com.zxhtom.core.repository.impl;

import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.Module;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;
@Repository
public class CustomRepositoryImpl implements CustomRepository {
    @Override
    public List<Module> selectModuleByCurrentUser(Long userId,Long oauthClientId) {
        return null;
    }

    @Override
    public CustomUser selectCurrentUserName(String accessToken) {
        return null;
    }

    @Override
    public CustomUser selectUserByUserName(String userName) {
        return null;
    }

    @Override
    public Set<String> selectPremissionNamesByUserId(Long userId,String clientId,String clientSecret) {
        return null;
    }

    @Override
    public Set<String> selectRoleNamesByUserId(Long userId) {
        return null;
    }

    @Override
    public Set<CustomRole> selectRolesByUserId(Long userId) {
        return null;
    }

    @Override
    public Map<Long, Long> selectVistualMenu() {
        return null;
    }

    @Override
    public List<CustomRole> selectRolesByMenuId(Long menuId) {
        return null;
    }

    @Override
    public CustomUser selectUserInfoByUserName(String userName) {
        return null;
    }

    @Override
    public List<MenuDto> selectRootMenusByUserIdAndModeul(Long userId, String moduleCode,Long oauthClientId) {
        return null;
    }

    @Override
    public List<MenuDto> selectChildMenuListMenuId(Long userId, Long menuId,Long oauthClientId) {
        return null;
    }

    @Override
    public List<MenuDto> selectRootMenusByUserId(Long userId,Long oauthClientId) {
        return null;
    }

    @Override
    public List<Module> selectAllModules() {
        return null;
    }

    @Override
    public List<CustomRole> selectRolesByModuleCode(String moduleName,String clientId,String clientSecret) {
        return null;
    }

    @Override
    public CustomUser selectUserByUserId(Long userId) {
        return null;
    }

    @Override
    public Integer insertCustomUser(CustomUser user) {
        return null;
    }
}
