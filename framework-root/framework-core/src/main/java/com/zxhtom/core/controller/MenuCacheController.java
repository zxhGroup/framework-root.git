package com.zxhtom.core.controller;

import com.zxhtom.model.Module;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.mgt.SecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.annotation.InitData;
import com.zxhtom.annotation.OnlyLogin;
import com.zxhtom.core.service.CustomService;
import com.zxhtom.dto.MenuDto;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.LoginUserInfo;
import com.zxhtom.utils.ContextUtil;
import com.zxhtom.vconstant.RoleList;

@RestController("MenuCacheController")
@RequestMapping("/framework_core/menu_init")
@Api(tags = "framework_core menu_init", description = "菜单缓存关系")
public class MenuCacheController {

	@Autowired
	private CustomService customService;
	
	@RequestMapping(value = "/vistual_menuId", method = RequestMethod.GET)
	@ApiOperation(value = "将菜单ID进行虚拟化，目的是防止恶意攻击")
	@InitData(value = MenuCacheController.class, order = 1)
	@RequiresRoles(value = { RoleList.SUPERADMIN })
	public Map<Long, Long> vistualMenu() {
		ApplicationContext applicationContext = ContextUtil.getApplicationContext();
		CustomService customService = applicationContext.getBean(CustomService.class);
		return customService.selectVistualMenu();
	}

	@RequestMapping(value = "/nav", method = RequestMethod.GET)
	@OnlyLogin
	@ApiOperation(value = "获取登录用户的菜单列表")
	public List<MenuDto> selectMenuListByLoginUser(HttpServletRequest request) {
		//获取当前登录的的用户id
		CustomUser customUser = (CustomUser)SecurityUtils.getSubject().getPrincipal();
		List<MenuDto> list = customService.selectMenuListByUserId(customUser.getUserId());
		if(CollectionUtils.isNotEmpty(list)){
			return list;
		}
		return new ArrayList<MenuDto>();
	}

    @RequestMapping(value = "/selectModuleByCurrentUser", method = RequestMethod.GET)
    @ApiOperation(value = "根据用户获取模块")
    public List<Module> selectModuleByCurrentUser() {
        return customService.selectModuleByCurrentUser();
    }
}
