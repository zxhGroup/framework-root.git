package com.zxhtom;

import com.github.pagehelper.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页数据对象
 *
 * @author wuchanglin
 */
@ApiModel(description = "分页查询返回值")
@Data
public class PagedResult<T> {
    /**
     * 总笔数
     */
    @ApiModelProperty("总笔数")
    private Long totalRecords;
    /**
     * 总页数
     */
    @ApiModelProperty("总页数")
    private Integer totalPages;
    /**
     * 页码
     */
    @ApiModelProperty("页码")
    private Integer pageNumber;
    /**
     * 每页笔数
     */
    @ApiModelProperty("每页笔数")
    private Integer pageSize;
    /**
     * 分页数据
     */
    @ApiModelProperty("分页数据")
    private List<T> datas = new ArrayList<>();

    /**
     * 构造方法
     */
    public PagedResult() {
    }

    /**
     * 构造方法
     *
     * @param totalRecords 总笔数
     * @param totalPages   总页数
     * @param pageNumber   页码
     * @param pageSize     每页笔数
     * @param datas        分页数据
     */
    public PagedResult(Long totalRecords, Integer totalPages, Integer pageNumber, Integer pageSize, List<T> datas) {
        this.totalRecords = totalRecords;
        this.totalPages = totalPages;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.datas = datas;
    }

    /**
     * 构造方法
     *
     * @param list 泛型列表
     */
    public PagedResult(List<T> list) {
        if (list instanceof Page) {
            Page<T> page = (Page<T>) list;
            this.totalRecords = page.getTotal();
            this.totalPages = page.getPages();
            this.pageNumber = page.getPageNum();
            this.pageSize = page.getPageSize();
            this.datas = page;
        } else {
            this.totalRecords = (long) list.size();
            this.totalPages = this.totalRecords > 0 ? 1 : 0;
            this.pageNumber = this.totalRecords > 0 ? 1 : 0;
            this.pageSize = list.size();
            this.datas = list;
        }
    }

    /**
     * 构造方法, 取list中某一页
     *
     * @param list
     * @param pageNumber
     * @param pageSize
     */
    public PagedResult(List<T> list, int pageNumber, int pageSize) {
        if (pageSize == 0) {
            // pageSize为0表示全取
            this.totalRecords = (long) list.size();
            this.totalPages = this.totalRecords > 0 ? 1 : 0;
            this.pageNumber = this.totalRecords > 0 ? 1 : 0;
            this.pageSize = list.size();
            this.datas = list;
        } else {
            this.totalRecords = (long) list.size();
            //如果每页笔数小于0, 取默认值10笔
            this.pageSize = pageSize > 0 ? pageSize : Integer.parseInt(CoreConstants.PAGE_SIZE);
            //总页数
            this.totalPages = (int) (this.totalRecords / this.pageSize + ((this.totalRecords % this.pageSize == 0) ? 0 : 1));
            //如果页数<1, 取第1页, 如果页数>总页数, 取最后页
            if (pageNumber < 1) {
                this.pageNumber = 1;
            } else if (pageNumber > this.totalPages) {
                this.pageNumber = this.totalPages;
            } else {
                this.pageNumber = pageNumber;
            }
            if (CollectionUtils.isEmpty(list)) {
                this.datas = list;
            } else {
                //分页起始笔数
                int from = (this.pageNumber - 1) * this.pageSize;
                //分页结束笔数
                int to = this.pageNumber * this.pageSize;
                //如果超出list范围
                if (from < 0) {
                    from = 0;
                }
                if (to < 0) {
                    to = 0;
                }
                if (from >= list.size()) {
                    from = list.size() - 1;
                }
                if (to > list.size()) {
                    to = list.size();
                }
                this.datas = list.subList(from, to);
            }
        }
    }

	public Long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public List<T> getDatas() {
		return datas;
	}

	public void setDatas(List<T> datas) {
		this.datas = datas;
	}
    
    
}
