package com.zxhtom.dto;

import java.util.List;

import com.zxhtom.model.Menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "menuDto", propOrder = {
		"childList",
		"type"
})
public class MenuDto extends Menu{

	private List<MenuDto> childList;
	private Integer type;

	public List<MenuDto> getChildList() {
		return childList;
	}

	public void setChildList(List<MenuDto> childList) {
		this.childList = childList;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "MenuDto [childList=" + childList + ", type=" + type + "]";
	}
	
	
}
