package com.zxhtom;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.liquibase.LiquibaseServiceLocatorApplicationListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Core配置类
 */
public class CoreConfiguration {
	/**
	 * 应用服务器编号, 多台应用服务器时不能重复, 允许值0..7
	 */
	private static Integer serverId = 0;
	/**
	 * 文件URL前缀
	 */
	private static String fileUrlPrefix;

	/**
	 * 根据客户端请求的主机, 获取文件URL前缀
	 */
	private static Map<String, String> fileUrlPrefixMap = new HashMap<>();

	/**
	 * 缓存过期时间(秒)
	 */
	private static Integer cacheExpiration = 3600;

	public static void setServerId(Integer serverId) {
		Preconditions.checkArgument(serverId != null && serverId >= 0 && serverId <= 7,
				"配置项CoreConfig.serverId表示应用服务器编号, 只能在0..7之间, 请检查!");
		CoreConfiguration.serverId = serverId;
	}

	public static Integer getServerId() {
		Preconditions.checkArgument(serverId != null && serverId >= 0 && serverId <= 7,
				"配置项CoreConfig.serverId表示应用服务器编号, 只能在0..7之间, 请检查!");
		return serverId;
	}

	public static void setFileUrlPrefix(String fileUrlPrefix) {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(fileUrlPrefix),
				"配置项CoreConfig.fileUrlPrefix表示文件URL前缀, 不可为空, 请检查!");
		CoreConfiguration.fileUrlPrefix = StringUtils.endsWith(fileUrlPrefix, "/") ? fileUrlPrefix
				: fileUrlPrefix + "/";
	}

	public static String getFileUrlPrefix() {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(fileUrlPrefix),
				"配置项CoreConfig.fileUrlPrefix表示文件URL前缀, 不可为空, 请检查!");
		return fileUrlPrefix;
	}

	public void setFileUrlPrefixMap(Map<String, String> fileUrlPrefixMap) {
		CoreConfiguration.fileUrlPrefixMap = fileUrlPrefixMap;
	}

	public Map<String, String> getFileUrlPrefixMap() {
		return fileUrlPrefixMap;
	}

	public static void setCacheExpiration(Integer cacheExpiration) {
		Preconditions.checkArgument(cacheExpiration != null && cacheExpiration >= 0,
				"配置项CoreConfig.cacheExpiration表示缓存过期时间, 不能小于0, 请检查!");
		CoreConfiguration.cacheExpiration = cacheExpiration;
	}

	public static Integer getCacheExpiration() {
		Preconditions.checkArgument(cacheExpiration != null && cacheExpiration >= 0,
				"配置项CoreConfig.cacheExpiration表示缓存过期时间, 不能小于0, 请检查!");
		return cacheExpiration;
	}

	/**
	 * 根据客户端请求的服务器地址, 获取文件URL前缀
	 *
	 * @param hostName
	 * @return
	 */
	public static String getFileUrlPrefix(String hostName) {
		if (Strings.isNullOrEmpty(hostName) || fileUrlPrefixMap.isEmpty()) {
			return fileUrlPrefix;
		}

		String mapValue = fileUrlPrefixMap.get(hostName);
		if (!Strings.isNullOrEmpty(mapValue)) {
			return mapValue;
		}

		return fileUrlPrefix;
	}

	/**
	 * 去除文件URL中的前缀
	 *
	 * @param fileUrl
	 * @return
	 */
	public static String cutFileUrlPrefix(String fileUrl) {
		if (fileUrl.contains(fileUrlPrefix)) {
			return fileUrl.replace(fileUrlPrefix, "");
		}
		for (String item : fileUrlPrefixMap.values()) {
			if (fileUrl.contains(item)) {
				return fileUrl.replace(item, "");
			}
		}
		return fileUrl;
	}
}
