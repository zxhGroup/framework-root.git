package com.zxhtom.proxy;

import java.lang.reflect.Method;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import com.zxhtom.config.shiro.MySessionManager;
import com.zxhtom.config.shiro.RedisSessionDao;
import com.zxhtom.utils.ContextUtil;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ProxyFactory implements MethodInterceptor {

	private static Logger logger = LogManager.getLogger(ProxyFactory.class);
	// 维护目标对象
	private Object target;

	public ProxyFactory(Object target) {
		this.target = target;
	}

	// 给目标对象创建一个代理对象
	public Object getProxyInstance() {
		// 1.工具类
		Enhancer en = new Enhancer();
		// 2.设置父类
		en.setSuperclass(target.getClass());
		// 3.设置回调函数
		en.setCallback(this);
		// 4.创建子类(代理对象)
		return en.create();

	}

	@Override
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) {
		Object returnValue=null;
		try {
			// 执行目标对象的方法
			//在redisSessionDao方法执行前后更改redistemplate的编码
			if(target instanceof RedisSessionDao){
				JdkSerializationRedisSerializer serializerPre = new JdkSerializationRedisSerializer();
				RedisSessionDao redisSessionDao = (RedisSessionDao) target;
				redisSessionDao.getRedisTemplate().setValueSerializer(serializerPre);
				returnValue = method.invoke(target, args);
//				Jackson2JsonRedisSerializer serializerFix = new Jackson2JsonRedisSerializer(Object.class);
//				redisSessionDao.getRedisTemplate().setValueSerializer(serializerFix);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return returnValue;
	}

}
