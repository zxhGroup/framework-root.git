package com.zxhtom;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 公用常量定义
 * 
 * @author zxhtom
 * @date 2016-6-25
 */
public class CoreConstants {
	public static final String DATA_SOURCE = "oracle.jdbc.OracleDriver";
	/**
	 * 日期格式字符串
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * 时间格式字符串
	 */
	public static final String TIME_FORMAT = "HH:mm:ss";

	/**
	 * 日期时间格式字符串
	 */
	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 日期时间格式字符串毫秒
	 */
	public static final String DATETIME_MS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

	/**
	 * 客户端有两种方式传递令牌: Header中X-Token, 或Cookie中X-Token
	 */
	public static final String X_TOKEN = "X-Token";
	/**
	 * 组织名称
	 */
	public static final String GROUP_NAME = "zxhtom.oschina.io";
	/**
	 * 包前缀
	 */
	public static final String PACKAGE_PREFIX = "com.zxhtom";
	/**
	 * 数据分页默认页码
	 */
	public static final String PAGE_NUMBER = "1";
	/**
	 * 数据分页默认笔数
	 */
	public static final String PAGE_SIZE = "10";
	/**
	 * 分隔符
	 */
	public static final String SEPARATE_CHAR = "|";

	public static final ObjectMapper OBJECT_MAPPER;

	/**
	 * 全局共用JSON转换工具, 动态类型
	 */
	public static final ObjectMapper OBJECT_MAPPER_WITH_TYPE;
	
	/**
	 * 菜单缓存关系
	 */
	public static final String MENUPRE="MENU";
	public static final String FACTMENUKEY = "FACTMENUKEY";
	public static final String VISTUALMENUKEY = "VISTUALMENUKEY";
	
	/**
	 * 请求缓存key
	 */
	public static final String PROJECTNAMELIST="projectNameList";
	public static final String REQUESTLIST = "requestList";
	/**
	 * 登录过程产生的用户信息
	 */
	public static final String USERID = "userId";
	public static final String USERNAME = "userName";
	
	public static final String MSG="MSG";

	/*用户密码盐*/
	public static final Object SLAT = "zxh";

	public static final String NULL = "zxhtom:null:value:key";
	public static final String USERINFO = "userInfo";
	public static final String USERLIST = "USERLIST";
    public static final String KAPTCHA_SESSION_KEY ="KAPTCHA_SESSION_KEY" ;
	public static final String GENERATOR = "generator_datasource";
	public static final String OAUTHCLIENTID = "oauthClientId";
    /**
	 * 全局参数
	 */
	public static Map<String, Object> map = new HashMap<String, Object>();
	/**
	 * 静态构造函数
	 */
	static {
		// json序列化配置, 非动态类型
		OBJECT_MAPPER = new ObjectMapper();
		OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OBJECT_MAPPER.setDateFormat(new SimpleDateFormat(CoreConstants.DATETIME_FORMAT));
		OBJECT_MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

		// json序列化配置, 动态类型
		OBJECT_MAPPER_WITH_TYPE = new ObjectMapper();
		OBJECT_MAPPER_WITH_TYPE.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		OBJECT_MAPPER_WITH_TYPE.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OBJECT_MAPPER_WITH_TYPE.setDateFormat(new SimpleDateFormat(CoreConstants.DATETIME_FORMAT));
		OBJECT_MAPPER_WITH_TYPE.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		OBJECT_MAPPER_WITH_TYPE.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL,
				JsonTypeInfo.As.WRAPPER_OBJECT);
	}
}
