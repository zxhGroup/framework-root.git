package com.zxhtom.vconstant;

public class PackageName {
    public final static String BASKPACKAGE = "com.zxhtom";

    //webservice请求头中参数名称
    public final static String USERNAME = "app_key";
    public final static String PASSWORD = "app_secret";
    public final static String CODE = "code";

    public static final String ACCESSTOKEN = "access_token";
    public static final String REALM = "realm";
    public static final String SERVICE = "service";
    public static final String LOCAL = "local";
}
