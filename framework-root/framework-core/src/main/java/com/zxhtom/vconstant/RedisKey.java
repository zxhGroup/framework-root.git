package com.zxhtom.vconstant;

public class RedisKey {

	public static final String ROLEMENU="role_menu";
	public static final String MENU="menu";
	public static final String CURRENTPARAM="zxhtom:1102540492009472";
	public static final String CURRENTUSERID="zxhtom:1112540492009472";
	public static final String CURRENTUSERNAME="zxhtom:1102549445668864";
	
	/*用户菜单缓存*/
	public static final String USERMENULIST="USERMENULIST";
	public static final String USERROOTMENULIST="USERROOTMENULIST";
	/*用户角色缓存*/
	public static final String USERROLELIST = "USERROLELIST";
	/*用户权限缓存*/
	public static final String USERPERMISSIONLIST = "USERPERMISSIONLIST";
	/*spring cache*/
	public static final String SPRINGCACHE = "cache";
    public static final String USERMODULELIST = "MODULE";
}
