package com.zxhtom.vconstant;

public class RedisLocalKey {
    public static final String ROLEMENU="LOCAL_ROLE_MENU";
    public static final String MENU="LOCAL_MENU";
    public static final String CURRENTPARAM="local_zxhtom:1102540492009472";
    public static final String CURRENTUSERID="local_zxhtom:1112540492009472";
    public static final String CURRENTUSERNAME="local_zxhtom:1102549445668864";

    /*用户菜单缓存*/
    public static final String USERMENULIST="LOCAL_USERMENULIST";
    public static final String USERROOTMENULIST="LOCAL_USERROOTMENULIST";
    /*用户角色缓存*/
    public static final String USERROLELIST = "LOCAL_USERROLELIST";
    /*用户权限缓存*/
    public static final String USERPERMISSIONLIST = "LOCAL_USERPERMISSIONLIST";
    public static final String USERLIST = "LOCAL_USERLIST";
    public static final String MENUPRE = "MENU";
    public static final String USERMODULELIST = "LOCAL_MODULE";
}
