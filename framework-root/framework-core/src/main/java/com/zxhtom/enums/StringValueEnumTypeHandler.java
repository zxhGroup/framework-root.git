package com.zxhtom.enums;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Java枚举类型保存到数据库默认使用EnumOrdinalTypeHandler
 * 数据库中保存的是枚举值的Ordinal属性, 但Ordinal固定从0开始, 无法指定
 * 接口StringValueEnum用来定义可以指定保存到数据库的字符串的枚举类型
 * 实现了StringValueEnum接口的枚举类型, 保存数据库时使用IntValueEnumTypeHandler
 */
public class StringValueEnumTypeHandler<E extends Enum<E>> extends BaseTypeHandler<E> {
    private Class<E> type;
    private Map<String, E> map = new HashMap<>();

    public StringValueEnumTypeHandler(Class<E> type) {
        if (type == null) {
            throw new IllegalArgumentException("Type argument cannot be null");
        }
        this.type = type;
        E[] enums = type.getEnumConstants();
        if (enums == null) {
            throw new IllegalArgumentException(type.getSimpleName() + " does not represent an enum type.");
        }

        for (E e : enums) {
            StringValueEnum enumValue = (StringValueEnum) e;
            map.put(enumValue.getValue(), e);
        }
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, E parameter, JdbcType jdbcType) throws SQLException {
        StringValueEnum enumValue = (StringValueEnum) parameter;
        ps.setString(i, enumValue.getValue());
    }

    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
        if (rs.wasNull()) {
            return null;
        } else {
            return getValuedEnum(rs.getString(columnName));
        }
    }

    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        if (rs.wasNull()) {
            return null;
        } else {
            return getValuedEnum(rs.getString(columnIndex));
        }
    }

    @Override
    public E getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        if (cs.wasNull()) {
            return null;
        } else {
            return getValuedEnum(cs.getString(columnIndex));
        }
    }

    private E getValuedEnum(String value) {
        try {
            return map.get(value);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Cannot convert " + value + " to " + type.getSimpleName() + " by value.", ex);
        }
    }
}