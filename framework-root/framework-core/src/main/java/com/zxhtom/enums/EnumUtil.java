
package com.zxhtom.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zxhtom.utils.ClassUtil;

/**
 * 获取com.jsits下所有枚举类型信息
 */
public class EnumUtil {
    private static final Logger logger = LoggerFactory.getLogger(EnumUtil.class);

    /**
     * com.jsits下所有枚举类型信息
     */
    private static Map<String, List<EnumInfo>> enumInfos;

    /**
     * 获取com.jsits下所有枚举类型信息
     *
     * @return 枚举类型信息
     */
    public static Map<String, List<EnumInfo>> getEnumInfos() {
        if (enumInfos == null) {
            enumInfos = new HashMap<>();

            try {
                for (String className : ClassUtil.getJsitsClasses()) {
                    Class<?> classType = Class.forName(className);
                    if (classType.isEnum()) { // 枚举类型
                        EnumIgnore enumIgnore = classType.getAnnotation(EnumIgnore.class);
                        if (enumIgnore != null && enumIgnore.value()) { // 忽略此枚举
                            continue;
                        }

                        List<EnumInfo> info = new ArrayList<>();
                        for (Object obj : classType.getEnumConstants()) {
                            Enum<?> enumValue = (Enum<?>) obj;
                            String valueDesc = null;
                            try {
                                EnumDesc enumDesc = classType.getField(enumValue.name()).getAnnotation(EnumDesc.class);
                                if (enumDesc != null) {
                                    valueDesc = enumDesc.value();
                                }
                            } catch (NoSuchFieldException e) {
                                if (logger.isErrorEnabled()) {
                                    logger.error(ExceptionUtils.getStackTrace(e));
                                }
                            }

                            if (IntValueEnum.class.isAssignableFrom(classType)) {
                                IntValueEnum<Integer> intValueEnum = (IntValueEnum) enumValue;
                                info.add(new EnumInfo(enumValue.ordinal(), enumValue.name(), Integer.toString(intValueEnum.getValue()), valueDesc));
                            } else if (StringValueEnum.class.isAssignableFrom(classType)) {
                                StringValueEnum stringValueEnum = (StringValueEnum) enumValue;
                                info.add(new EnumInfo(enumValue.ordinal(), enumValue.name(), stringValueEnum.getValue(), valueDesc));
                            } else {
                                info.add(new EnumInfo(enumValue.ordinal(), enumValue.name(), Integer.toString(enumValue.ordinal()), valueDesc));
                            }
                        }
                        enumInfos.put(classType.getSimpleName(), info);
                    }
                }
            } catch (Exception e) {
//                Throwables.propagate(e);
            }
        }
        return enumInfos;
    }
}
