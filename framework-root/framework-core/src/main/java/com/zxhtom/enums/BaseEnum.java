package com.zxhtom.enums;

public interface BaseEnum<T> {
	T getValue();
	
	Object getCode();
}
