package com.zxhtom.enums;

public enum Operator {

	INSERT("insert"),
	UPDATE("update"),
	DELETE("delete"),
	SELECT("select");
	
	private String code;
	
	private Operator(String code){
		this.code = code;
	}
	public String getCode(){
		return code;
	}
}
