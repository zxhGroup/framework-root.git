package com.zxhtom.enums;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 枚举值信息
 */
@ApiModel(description = "枚举值信息对象")
@Data
@AllArgsConstructor
public class EnumInfo {
	/**
	 * 枚举值序数
	 */
	@ApiModelProperty("枚举值序数")
	private Integer ordinal;

	/**
	 * 枚举值名称
	 */
	@ApiModelProperty("枚举值名称")
	private String name;

	/**
	 * 枚举值数据库存储值
	 */
	@ApiModelProperty("枚举值数据库存储值")
	private String value;

	/**
	 * 枚举值描述
	 */
	@ApiModelProperty("枚举值描述")
	private String desc;

	public EnumInfo(int ordinal, String name, String value, String desc) {
		this.ordinal=ordinal;
		this.name=name;
		this.value=value;
		this.desc=desc;
	}
}
