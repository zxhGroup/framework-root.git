package com.zxhtom.enums;


/**
 * Java枚举类型保存到数据库默认使用EnumOrdinalTypeHandler
 * 数据库中保存的是枚举值的Ordinal属性, 但Ordinal固定从0开始, 无法指定
 * 本接口用来定义可以指定保存到数据库的字符串的枚举类型
 */
public interface StringValueEnum extends BaseEnum{
    String getValue();
}
