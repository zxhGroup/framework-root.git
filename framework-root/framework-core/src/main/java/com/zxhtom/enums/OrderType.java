package com.zxhtom.enums;


/**
 * 排序方式
 * @author Tony
 */
public enum OrderType {
	/**
	 * 升序
	 */
	@EnumDesc("升序")
	ASC,
	/**
	 * 降序
	 */
	@EnumDesc("降序")
	DESC
}
