package com.zxhtom.enums;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 是否忽略此枚举类型
 * @author wuchanglin
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnumIgnore {
	/**
	 * 是否忽略此枚举类型
	 * @return true忽略, false不忽略
	 */
	boolean value() default false;
}
