package com.zxhtom.enums;


/**
 * 布尔枚举类型, TRUE: 是/真, FALSE: 否/假
 */
public enum Bool {
	/**
	 * 否/假
	 */
	@EnumDesc("否")
	FALSE,
	/**
	 * 是/真
	 */
	@EnumDesc("是")
	TRUE
}
