package com.zxhtom.enums;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解枚举值的中文描述
 * @author wuchanglin
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnumDesc {
	/**
	 * 注解枚举值的中文描述
	 * @return 枚举值中文描述
	 */
	String value() default "";
}
