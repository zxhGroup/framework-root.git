package com.zxhtom.exception;


/**
 * 参数验证异常
 * @author zxhtom
 */
public class ParamValidException extends RuntimeException {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;

	public ParamValidException(String msg){
		super(msg);
	}
}
