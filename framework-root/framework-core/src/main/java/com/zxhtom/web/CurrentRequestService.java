package com.zxhtom.web;

import com.zxhtom.model.CustomUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 当前http请求相关信息
 */
public interface CurrentRequestService {
	/**
	 * 获取HttpServletRequest
	 * @return request
	 */
	HttpServletRequest getRequest();

	/**
	 * 设置HttpServletRequest
	 * @param request
	 */
	void setRequest(HttpServletRequest request);

	/**
	 * 获取HttpServletResponse
	 * @return response
	 */
	HttpServletResponse getResponse();

	/**
	 * 设置HttpServletResponse
	 * @param response
	 */
	void setResponse(HttpServletResponse response);

	/**
	 * 获取客户端IP地址
	 * @return
	 */
	String getClientIp();

	/**
	 * 获取当前服务器IP地址
	 * @return
	 */
	String getLocalIp();

	/**
	 * 获取客户端请求的主机名
	 * @return
	 */
	String getHostName();

}
