/**
 * 
 */
package com.zxhtom.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 * 后端服务返回统一格式对象, 序列化成json字符串返回
 * { "code": 0, "message": null, "data": null }
 * code: 状态码, 0成功, 1一般业务异常, -1其他异常, -2非法访问
 * message: 错误消息
 * data: 数据
 * @author zxhtom
 * @see ResponseBodyHandler
 */
@JsonInclude(Include.ALWAYS)
public class ActionResult {
	/**
	 * code: 状态码, 0成功, 1一般业务异常, -1其他异常, -2非法访问
	 */
	private Integer code;

	/**
	 * message: 错误消息
	 */
	private String message;

	/**
	 * data: 数据
	 */
	private Object data;
	
	public ActionResult(Integer code, String message, Object data) {
		this.code=code;
		this.message=message;
		this.data=data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
