/**
 * 
 */
package com.zxhtom.web;

import com.zxhtom.annotation.NoResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.zxhtom.CoreConstants;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.utils.ErrorUtil;

/**
 * 后端服务返回统一格式对象, 序列化成json字符串返回 { "code": 0, "message": null, "data": null }
 * code: 状态码ActionResultCode, 0成功, 1业务异常, -1其他异常 message: 错误消息 data: 数据
 * 
 * @author zxhtom
 */
@ControllerAdvice
public class ResponseBodyHandler implements ResponseBodyAdvice<Object> {
	private static final Logger logger = LoggerFactory.getLogger(ResponseBodyHandler.class);

	/**
	 * 仅处理包头前缀为com.zxhtom的Controller请求返回值
	 */
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		if (returnType == null) {
			return false;
		}
		if (returnType.getContainingClass().getPackage().getName().startsWith("com.zxhtom.qinxuewu")) {
			return false;
		}
        // 仅处理包头前缀为com.zxhtom的Controller请求返回值
		return returnType.getContainingClass().getPackage().getName().startsWith(CoreConstants.PACKAGE_PREFIX);
	}

	/**
	 * 对返回的对象进行包装
	 */
	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		if (body == null) {
			return new ActionResult(ActionResultCode.SUCCESS.getValue(), null, null);
		}
        NoResult annotation = returnType.getContainingClass().getAnnotation(NoResult.class);
        if (annotation!=null) {
            return body;
        }
		// body是异常, 代表失败
		if (body instanceof Exception) {
			// 异常写日志
			Exception ex = (Exception) body;
			if (logger.isErrorEnabled()) {
				logger.error(ExceptionUtils.getStackTrace(ex));
			}

			// BusinessException业务异常返回ErrorCode, message
			if (body instanceof BusinessException) {
				BusinessException bex = (BusinessException) body;
				return new ActionResult(bex.getErrorCode(), bex.getMessage(), null);
			}
			// 如果是shiro异常我们封装成我们的业务异常
			if (body instanceof UnauthenticatedException || body instanceof UnauthorizedException) {
				if (true == SecurityUtils.getSubject().isAuthenticated()) {
					String message = ex.getMessage();
					if (StringUtils.isNotEmpty(message)) {
						// 登录状态，则返回原生的shiro报错信息
						return new ActionResult(ActionResultCode.BUSINESS_ERROR.getValue(), ex.getMessage(), null);
					}
				}
				return new ActionResult(ActionResultCode.INVALID_SHIRO_TOKEN.getValue(), "shiro need login", null);
			}
			// IllegalArgumentException / NullPointerException /
			// IndexOutOfBoundsException / IllegalStateException
			// 错误消息不为空, 参数检查抛出的异常, 认为也是业务异常
			if (body instanceof IllegalArgumentException || body instanceof NullPointerException || body instanceof IndexOutOfBoundsException || body instanceof IllegalStateException) {
				String message = ex.getMessage();
				if (StringUtils.isNotEmpty(message)) {
					return new ActionResult(ActionResultCode.BUSINESS_ERROR.getValue(), message, null);
				}
			}

			// BindException / MethodArgumentNotValidException 是 Spring MVC
			// Controller 参数实体校验异常, 也认为是业务异常
			BindingResult bindingResult = null;
			if (body instanceof BindException) {
				bindingResult = ((BindException) body).getBindingResult();
			}
			if (body instanceof MethodArgumentNotValidException) {
				bindingResult = ((MethodArgumentNotValidException) body).getBindingResult();
			}
			if (bindingResult != null && bindingResult.hasErrors()) {
				return new ActionResult(ActionResultCode.BUSINESS_ERROR.getValue(), ErrorUtil.getErrorsMessage(bindingResult), null);
			}

			return new ActionResult(ActionResultCode.OTHER_ERROR.getValue(), ex.toString(), null);
		}

		// 返回数据
		ActionResult result = new ActionResult(ActionResultCode.SUCCESS.getValue(), null, body);
		return result;
	}
}
