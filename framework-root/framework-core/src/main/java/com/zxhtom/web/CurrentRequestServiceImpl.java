package com.zxhtom.web;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zxhtom.CoreConstants;
import com.zxhtom.model.CustomUser;
import org.apache.shiro.SecurityUtils;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * 当前http请求相关信息, 会有2份 1. 生命周期同http请求, 部分信息从http请求中获取 2. 生命周期为单例,
 * 用于定时任务等非http请求发起的调用, 部分从http请求中获取的信息为空
 */
public class CurrentRequestServiceImpl implements CurrentRequestService {
	private HttpServletRequest request;

	private HttpServletResponse response;

	private volatile String clientIp = null;

	private volatile String localIp = null;

	private volatile String hostName = null;

	private CustomUser currentUser;

	@Override
	public HttpServletRequest getRequest() {
		return request;
	}

	@Override
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public HttpServletResponse getResponse() {
		return response;
	}

	@Override
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public String getClientIp() {
		if (clientIp != null)
			return clientIp;
		if (request == null)
			return null;
		clientIp = getClientIp(request);
		return clientIp == null ? "" : clientIp;
	}

	@Override
	public String getLocalIp() {
		if (localIp != null)
			return localIp;
//		localIp = getLoalIP();
		localIp=getNetIp();//获取外网ip
		return localIp == null ? "" : localIp;
	}

	@Override
	public String getHostName() {
		if (hostName != null)
			return hostName;
		if (request == null)
			return null;
		hostName = request.getServerName();
		return hostName == null ? "" : hostName;
	}
	/**
	 * 获取客户端IP地址
	 * 
	 * @return
	 */
	private static String getClientIp(HttpServletRequest request) {
		String ipAddress = request.getHeader("X-Forwarded-For");
		if (Strings.isNullOrEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (Strings.isNullOrEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (Strings.isNullOrEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
			if ("127.0.0.1".equals(ipAddress) || "0:0:0:0:0:0:0:1".equals(ipAddress)) {
				// 根据网卡取本机配置的IP
				ipAddress = getLoalIP();
			}
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (!Strings.isNullOrEmpty(ipAddress) && ipAddress.contains(",")) {
			List<String> ips = Lists.newArrayList(Splitter.on(",").trimResults().omitEmptyStrings().split(ipAddress));
			if (!CollectionUtils.isEmpty(ips)) {
				ipAddress = ips.get(0);
			}
		}
		return ipAddress;
	}

	/**
	 * 获取本机IP地址
	 */
	private static String getLoalIP() {
		InetAddress inetAddress = getLocalHostLANAddress();
		if (null == inetAddress) {
			return "127.0.0.1";
		}
		return inetAddress.getHostAddress();
	}

	public static String getNetIp() {
		try {
			String localip = null;// 本地IP，如果没有配置外网IP则返回它
			String netip = null;// 外网IP
			Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
			InetAddress ip = null;
			boolean finded = false;// 是否找到外网IP
			while (netInterfaces.hasMoreElements() && !finded) {
				NetworkInterface ni = netInterfaces.nextElement();
				Enumeration<InetAddress> address = ni.getInetAddresses();
				while (address.hasMoreElements()) {
					ip = address.nextElement();
					if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1) {// 外网IP
						netip = ip.getHostAddress();
						finded = true;
						break;
					} else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1) {// 内网IP
						localip = ip.getHostAddress();
					}
				}
			}

			if (netip != null && !"".equals(netip)) {
				return netip;
			} else {
				return localip;
			}
		} catch (Exception e) {
		}
		return "127.0.0.1";
	}

	public static InetAddress getLocalHostLANAddress() {
		try {
			InetAddress candidateAddress = null;
			// 遍历所有的网络接口
			for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
				NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
				// 在所有的接口下再遍历IP
				for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements();) {
					InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
					if (!inetAddr.isLoopbackAddress()) {// 排除loopback类型地址
						if (inetAddr.isSiteLocalAddress()) {
							// 如果是site-local地址，就是它了
							return inetAddr;
						} else if (candidateAddress == null) {
							// site-local类型的地址未被发现，先记录候选地址
							candidateAddress = inetAddr;
						}
					}
				}
			}
			if (candidateAddress != null) {
				return candidateAddress;
			}
			// 如果没有发现 non-loopback地址.只能用最次选的方案
			InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
			return jdkSuppliedAddress;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
