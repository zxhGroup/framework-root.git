package com.zxhtom.web;

import com.zxhtom.CoreConstants;
import com.zxhtom.config.shiro.MySessionManager;
import com.zxhtom.utils.ContextUtil;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.web.session.mgt.WebSessionKey;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.BeansException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

public class UrlFilter implements Filter {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UrlFilter(String name) {
		this.name = name;
	}

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain filterChain) throws IOException {
		MySessionManager sessionManager =null;
		try {
			sessionManager = ContextUtil.getApplicationContext().getBean(MySessionManager.class);
		} catch (BeansException e) {
			//说明shiro没有配置 ， 放行
			try {
				filterChain.doFilter(arg0, arg1);
			} catch (ServletException se) {
				se.printStackTrace();
			}
		}
		HttpServletResponse response = (HttpServletResponse) arg1;
		HttpServletRequest request = (HttpServletRequest) arg0;
		Serializable sessionId = sessionManager.getPubSessionId(arg0, arg1);
		String requestUrl = request.getRequestURI();
		boolean flag = false;
		Object userName = request.getSession().getAttribute(CoreConstants.USERNAME);
		userName = null;
		try {
			if (requestUrl.matches("/druid.*")) {
				if (null != sessionId) {
					Session readSession = sessionManager.retrieveSession(new WebSessionKey(sessionId, request, response));
					if (readSession instanceof SimpleSession) {
						userName = readSession.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
						if (null != userName) {
							flag = true;
							// CoreConstants.map.put("userName",userName);
							filterChain.doFilter(arg0, arg1);
						}
					}
				}
				// 最终会重定向登录页
				if (!flag) {
					// response.sendRedirect("/index");
					response.sendRedirect("/framework_core/login/login");
				}
			} else {
				//request.getRequestDispatcher("index").forward(request,response );
				filterChain.doFilter(arg0, arg1);
			}
		} catch (ServletException e) {
			response.sendRedirect("/framework_core/login/login");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
