package com.zxhtom.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 当前http请求相关信息, 会有2份
 * 1. 生命周期同http请求, 部分信息从http请求中获取
 * 2. 生命周期为单例, 用于定时任务等非http请求发起的调用, 部分从http请求中获取的信息为空
 * 优先取serviceByRequest, 再取serviceBySingleton
 */
public class CurrentRequestServiceProxy implements CurrentRequestService {
    @Autowired
    @Qualifier("currentRequestServiceByRequest")
    private CurrentRequestService serviceByRequest;

    @Autowired
    @Qualifier("currentRequestServiceBySingleton")
    private CurrentRequestService serviceBySingleton;

    @Override
    public HttpServletRequest getRequest() {
        try {
            return serviceByRequest.getRequest();
        } catch (Exception e) {
            return serviceBySingleton.getRequest();
        }
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        try {
            serviceByRequest.setRequest(request);
        } catch (Exception e) {
            serviceBySingleton.setRequest(request);
        }
    }

    @Override
    public HttpServletResponse getResponse() {
        try {
            return serviceByRequest.getResponse();
        } catch (Exception e) {
            return serviceBySingleton.getResponse();
        }
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        try {
            serviceByRequest.setResponse(response);
        } catch (Exception e) {
            serviceBySingleton.setResponse(response);
        }
    }

    @Override
    public String getClientIp() {
        try {
            return serviceByRequest.getClientIp();
        } catch (Exception e) {
            return serviceBySingleton.getClientIp();
        }
    }

    @Override
    public String getLocalIp() {
        try {
            return serviceByRequest.getLocalIp();
        } catch (Exception e) {
            return serviceBySingleton.getLocalIp();
        }
    }

    @Override
    public String getHostName() {
        try {
            return serviceByRequest.getHostName();
        } catch (Exception e) {
            return serviceBySingleton.getHostName();
        }
    }
}
