package com.zxhtom.config.websocket;

import com.zxhtom.CoreConstants;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.model.CustomUser;
import com.zxhtom.model.User;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;


/**
 * 用户拦截器
 */
public class UserInterceptor extends ChannelInterceptorAdapter {
    @Autowired
    private SimpUserRegistry userRegistry;
    /**
     * 获取包含在stomp中的用户信息
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        Boolean add = true;
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        CustomUser userInfo = (CustomUser) accessor.getSessionAttributes().get(CoreConstants.USERINFO);
        if (userInfo == null) {
            return message;
        }
        if (StompCommand.CONNECT.equals(accessor.getCommand())) {
            Object raw = message.getHeaders().get(SimpMessageHeaderAccessor.NATIVE_HEADERS);
            if (raw instanceof Map) {
                Object userId = ((Map) raw).get(CoreConstants.USERID);
                //暂时将登陆用户赋值
                if (userId instanceof LinkedList) { // 设置当前访问器的认证用户
                    if (!((LinkedList)userId).get(0).toString().equals(userInfo.getUserId().toString())) {
                        throw new BusinessException(userInfo.getUserName() + "用户与登陆用户不符");
                    }
                    User user = new User();
                    try {
                        BeanUtils.copyProperties(user, userInfo);
                        user.setName(user.getUserId().toString());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    Iterator<SimpUser> iterator = userRegistry.getUsers().iterator();
                    while (iterator.hasNext()) {
                        if (iterator.next().getName().equals(user.getName().toString())) {
                            //throw new BusinessException("重复");
                            add=false;
                        }
                    }
                    if (add) {
                    }
                    accessor.setUser(user);
                }
            }
        } else if (StompCommand.SUBSCRIBE.equals(accessor.getCommand())) {
            //订阅的时候检查权限
        }
        return message;
    }

    @Override
    public boolean preReceive(MessageChannel channel) {
        return super.preReceive(channel);
    }

    @Override
    public Message<?> postReceive(Message<?> message, MessageChannel channel) {
        return super.postReceive(message, channel);
    }

    @Override
    public void afterReceiveCompletion(Message<?> message, MessageChannel channel, Exception ex) {
        super.afterReceiveCompletion(message, channel, ex);
    }
    @Override
    public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
        super.afterSendCompletion(message,channel ,sent,ex );
    }
}