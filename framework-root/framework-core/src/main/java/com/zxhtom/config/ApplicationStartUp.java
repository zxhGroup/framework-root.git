
package com.zxhtom.config;

import java.lang.reflect.Method;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.zxhtom.CoreConstants;
import com.zxhtom.annotation.InitData;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.model.GeneratorProperties;
import com.zxhtom.utils.AnnoManageUtil;
import com.zxhtom.utils.CollectionUtil;
import com.zxhtom.utils.ContextUtil;

@Component
public class ApplicationStartUp implements ApplicationListener<ContextRefreshedEvent> {

	private static Logger logger = LogManager.getLogger(ApplicationStartUp.class);
	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		CoreConstants.map = AnnoManageUtil.getControllerMapping("com.zxhtom");
		Map<String, Object> tempMap = AnnoManageUtil.getControllerMapping("com.zxhtom");
		for (Map.Entry<String, Object> entry : tempMap.entrySet()) {
			List<Object> list = (List<Object>) CoreConstants.map.get(entry.getKey());
			list.addAll((Collection<?>) entry.getValue());
		}
		ApplicationContext applicationContext = ContextUtil.getApplicationContext();
		GeneratorProperties properties = applicationContext.getBean(GeneratorProperties.class);
		if(properties.isCheck()&&!((List<String>)CoreConstants.map.get(CoreConstants.PROJECTNAMELIST)).contains(properties.getModuleName())){
			throw new BusinessException("moduleName is not valid code");
		}
		Set<Method> orderMethods = new HashSet<Method>();
		Integer order = -1;
		try {
			Set<Method> methods = AnnoManageUtil.getAnnotationMethod(InitData.class, "com.zxhtom");
			List<Method> methodList = new ArrayList<Method>(methods);
			// 按InitData中的order排序，没有指定的最后默认加入
			for (int i = 0; i < methodList.size(); i++) {
				for (int j = i; j < methodList.size(); j++) {
					if(methodList.get(i).getAnnotation(InitData.class).order()>methodList.get(j).getAnnotation(InitData.class).order()){
						CollectionUtil.swap(methodList, i, j);
					}
				}
			}
			for (Method method : methodList) {
				if (method.getParameterTypes().length == 0) {
					InitData annotation = method.getAnnotation(InitData.class);
					Class clazz = annotation.value();
					method.invoke(clazz.newInstance());
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("eror" + e.getMessage());
		}
	}

}