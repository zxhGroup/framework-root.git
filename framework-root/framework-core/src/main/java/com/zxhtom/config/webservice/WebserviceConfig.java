package com.zxhtom.config.webservice;

import com.zxhtom.core.repository.AppOriginRepository;
import com.zxhtom.core.repository.CustomOriginRepository;
import com.zxhtom.model.Client;
import com.zxhtom.model.WebServiceProperties;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebserviceConfig {

    @Bean
    public AppOriginRepository appOriginRepository(WebServiceProperties properties, Client client){
        // 代理工厂
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        // 设置代理地址
        jaxWsProxyFactoryBean.setAddress(properties.getAppUrl());
        // 设置接口类型
        jaxWsProxyFactoryBean.setServiceClass(AppOriginRepository.class);
        // 创建一个代理接口实现
        AppOriginRepository cs = (AppOriginRepository) jaxWsProxyFactoryBean.create();
        return  cs;
    }

    @Bean
    public CustomOriginRepository customOriginRepository(WebServiceProperties properties, Client client){
        // 代理工厂
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        // 设置代理地址
        jaxWsProxyFactoryBean.setAddress(properties.getUrl());
        //添加用户名密码拦截器.
        jaxWsProxyFactoryBean.getOutInterceptors().add(new LoginInterceptor(client.getAppKey(),client.getAppSecret()));;
        // 设置接口类型
        jaxWsProxyFactoryBean.setServiceClass(CustomOriginRepository.class);
        // 创建一个代理接口实现
        CustomOriginRepository cs = (CustomOriginRepository) jaxWsProxyFactoryBean.create();
        return  cs;
    }

    /**
     * 此种方式适合Java生成的客户端类型的代码
     */
	/*@Bean
	public CustomOriginRepository customOriginRepository(WebServiceProperties properties){
		URL url = null;
		WebServiceException e = null;
		try {
			if (properties.getUrl() != null) {

				url = new URL(properties.getUrl());
			}
		} catch (MalformedURLException ex) {
			e = new WebServiceException(ex);
		}
		CustomOriginRepositoryImplService.setCustomoriginrepositoryimplserviceWsdlLocation(url);
		CustomOriginRepositoryImplService.setCustomoriginrepositoryimplserviceException(e);
		CustomOriginRepositoryImplService customOriginRepositoryImplService = new CustomOriginRepositoryImplService();
		CustomOriginRepository customOriginRepository = customOriginRepositoryImplService.getCustomOriginRepositoryImplPort();
		return customOriginRepository;
	}*/

}
