package com.zxhtom.config.shiro;

import com.zxhtom.model.CustomUser;
import com.zxhtom.model.RedisProperties;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisCache<K,V> implements Cache<K,V> {
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisProperties redisProperties;

    private final String CACHE_PREFIX = "zxhtom-cache:";

    private String getKey(K k){
        String fix = "";
        if (k instanceof String) {
            fix = k.toString();
        }
        fix = ((CustomUser) ((SimplePrincipalCollection) k).getPrimaryPrincipal()).getUserName();
        return (CACHE_PREFIX+fix);
    }

    @Override
    public V get(K k) throws CacheException {
        System.out.println("get from redis cache ");
        Object object = redisTemplate.opsForValue().get(getKey(k));
        if (object != null) {
            return (V) SerializationUtils.deserialize((byte[]) object);
        }
        return null;
    }

    @Override
    public V put(K k, V v) throws CacheException {
        String key = getKey(k);
        byte[] value = SerializationUtils.serialize(v);
        redisTemplate.opsForValue().set(key,value,redisProperties.getSessionTimeout(), TimeUnit.SECONDS);
        return v;
    }

    @Override
    public V remove(K k) throws CacheException {
        String key = getKey(k);
        byte[] value = (byte[]) redisTemplate.opsForValue().get(key);
        redisTemplate.delete(key);
        if (value != null) {
            return (V) SerializationUtils.deserialize(value);
        }
        return null;
    }

    @Override
    public void clear() throws CacheException {

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Set<K> keys() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }
}
