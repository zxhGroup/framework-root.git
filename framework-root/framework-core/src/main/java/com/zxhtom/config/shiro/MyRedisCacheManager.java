package com.zxhtom.config.shiro;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

import javax.annotation.Resource;

/**
 * shiro缓存管理 每次授权不需要查询数据库 避免数据库压力太大
 * @author John
 */
public class MyRedisCacheManager implements CacheManager {
    @Resource
    private RedisCache redisCache;
    /**
     * 返回cache实例
     * @param s
     * @param <K>
     * @param <V>
     * @return
     * @throws CacheException
     */
    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return redisCache;
    }
}
