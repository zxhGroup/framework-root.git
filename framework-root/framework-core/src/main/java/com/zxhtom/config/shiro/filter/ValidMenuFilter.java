package com.zxhtom.config.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;

import com.zxhtom.utils.ValidUtil;

public class ValidMenuFilter extends AccessControlFilter  {

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
		return false;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		Subject subject = getSubject(request, response);
		//验证请求头中的menu的权限
		ValidUtil.getInstance().validRequest(subject,request);
		return true;
	}

}
