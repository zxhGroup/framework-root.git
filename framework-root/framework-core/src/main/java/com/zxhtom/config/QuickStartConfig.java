package com.zxhtom.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackages = {"com.zxhtom.**.model","com.zxhtom.**.dto","com.zxhtom.**.repository"})
public class QuickStartConfig {

}
