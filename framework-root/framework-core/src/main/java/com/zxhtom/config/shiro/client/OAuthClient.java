package com.zxhtom.config.shiro.client;

import org.apache.oltu.oauth2.client.HttpClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

import java.util.HashMap;
import java.util.Map;

public class OAuthClient extends org.apache.oltu.oauth2.client.OAuthClient {
    public OAuthClient(HttpClient oauthClient) {
        super(oauthClient);
    }

    @Override
    public <T extends OAuthAccessTokenResponse> T accessToken(OAuthClientRequest request, String requestMethod, Class<T> responseClass) throws OAuthSystemException, OAuthProblemException {
        Map<String, String> headers = new HashMap();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        return (T) this.httpClient.execute(request, headers, requestMethod, responseClass);
    }
}
