package com.zxhtom.config;

import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

import com.zxhtom.config.aspect.DataRepositoryAspect;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.zxhtom.Exception.CommonException;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zxhtom.model.RedisProperties;

import redis.clients.jedis.JedisPoolConfig;

/**
 * redis配置类
 * 
 * @author zcc ON 2018/3/19
 **/
@Configuration
@EnableCaching
// 开启注解
@Import(DataRepositoryAspect.class)
public class RedisConfig {

	private Logger logger = LogManager.getLogger(RedisConfig.class);
	@Autowired
	private RedisProperties redisProperties;

	@Bean
	@ConfigurationProperties(prefix = "spring.redis.pool")
	public JedisPoolConfig getJedisPoolConfig() {
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		return jedisPoolConfig;
	}

	@Bean
	public RedisClusterConfiguration getClusterConfig() {
		RedisClusterConfiguration clusterConfig = new RedisClusterConfiguration();
		try {
			if (redisProperties == null || StringUtils.isEmpty(redisProperties.getCluster().getNodes())) {
				return null;
			}
			String[] serverArray = redisProperties.getCluster().getNodes().split(",");
			Set<RedisNode> nodes = new HashSet<RedisNode>();
			for (String ipPort : serverArray) {
				String[] ipPortPair = ipPort.split(":");
				nodes.add(new RedisNode(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())));
			}
			clusterConfig.setClusterNodes(nodes);
			clusterConfig.setMaxRedirects(6);
		} catch (Exception e) {
			logger.error("redis cluster init failed");
		}
		return clusterConfig;
	}

	@Bean
	public JedisConnectionFactory getRedisConnectionFactory() throws CommonException {
		JedisConnectionFactory connectionFactory = null;
		if (connectionFactory == null && redisProperties != null && redisProperties.getHost() != null) {
			// 单机
			connectionFactory = new JedisConnectionFactory();
		}
		if (connectionFactory == null && redisProperties != null && redisProperties.getCluster().getNodes() != null) {
			// 集群
			connectionFactory = new JedisConnectionFactory(getClusterConfig());
		}
		if (null == connectionFactory) {
			throw new CommonException("无法初始化redis连接池");
		}
		try {
			connectionFactory.setUsePool(true);
			JedisPoolConfig config = getJedisPoolConfig();
			connectionFactory.setPoolConfig(config);
			connectionFactory.setHostName(redisProperties.getHost());
			connectionFactory.setPort(redisProperties.getPort());
			connectionFactory.setDatabase(redisProperties.getDatabase());
		} catch (Exception e) {
			logger.error("redis connection factory init failed");
		}
		return connectionFactory;
	}
	private class zxhHandler{

	}
	@Bean
	public CacheManager cacheManager(RedisTemplate<?, ?> redisTemplate) throws CommonException {
		RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
				.entryTtl(Duration.ofHours(1)); // 设置缓存有效期一小时
		return RedisCacheManager
				.builder(RedisCacheWriter.nonLockingRedisCacheWriter(getRedisConnectionFactory()))
				.cacheDefaults(redisCacheConfiguration).build();
		/*
		 * RedisCacheManager rcm = new RedisCacheManager(redisTemplate); //
		 * 多个缓存的名称,目前只定义了一个 rcm.setCacheNames(Arrays.asList("thisredis"));
		 * //设置缓存默认过期时间(秒) rcm.setDefaultExpiration(600); return rcm;
		 */
	}

	// 以下两种redisTemplate自由根据场景选择
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	@Primary
	public RedisTemplate<Object, Object> redisTemplate() throws CommonException {
		RedisTemplate template = new RedisTemplate();
		template.setConnectionFactory(getRedisConnectionFactory());

		// 使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值（默认使用JDK的序列化方式）
		Jackson2JsonRedisSerializer serializer = new Jackson2JsonRedisSerializer(Object.class);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		serializer.setObjectMapper(mapper);
		// shiro默认使用jdk，所以这个序列化采用默认jdk，方便shirosession的 处理
		template.setValueSerializer(serializer);
		// 使用StringRedisSerializer来序列化和反序列化redis的key值
		template.setKeySerializer(serializer);
		//template.setValueSerializer(serializer);
		template.setHashKeySerializer(serializer);
		template.setHashValueSerializer(serializer);
		template.afterPropertiesSet();
		return template;
	}

	@Bean
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory factory) {
		StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
		stringRedisTemplate.setConnectionFactory(factory);
		return stringRedisTemplate;
	}
}