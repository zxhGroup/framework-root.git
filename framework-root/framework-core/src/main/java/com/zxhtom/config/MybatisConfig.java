package com.zxhtom.config;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidAbstractDataSourceMBean;
import com.alibaba.druid.pool.DruidDataSourceMBean;
import com.zxhtom.model.SessionDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.EnumOrdinalTypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.MybatisProperties;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;
import com.zxhtom.enums.EnumIgnore;
import com.zxhtom.enums.IntValueEnum;
import com.zxhtom.enums.IntValueEnumTypeHandler;
import com.zxhtom.enums.StringValueEnum;
import com.zxhtom.enums.StringValueEnumTypeHandler;
import com.zxhtom.utils.ClassUtil;

import org.springframework.web.context.WebApplicationContext;
import tk.mybatis.mapper.code.Style;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Config;
import tk.mybatis.mapper.mapperhelper.MapperHelper;

/**
 * Mybatis配置类
 */
@Configuration
// 扫描注入mybatis mapper接口
@MapperScan(basePackages = {"com.zxhtom.**.mapper", "com.zxhtom.**.dao"}, sqlSessionFactoryRef = "primarySqlSessionFactory")
// 导入声明式事务xml配置
@ImportResource("classpath:transaction-primary.xml")
public class MybatisConfig {


    /**
     * 注入数据源
     *
     * @return
     */
    @Bean("primaryDataSource")
    @ConfigurationProperties("spring.datasource")
    public DataSource primaryDataSource() {
        return new DruidDataSource();
    }

    /**
     * 注入事务管理器
     *
     * @return
     */
    @Bean("primaryTransactionManager")
    @Primary
    public DataSourceTransactionManager primaryTransactionManager() {
        return new DataSourceTransactionManager(primaryDataSource());
    }

    /**
     * 注入SqlSessionFactory, 同时注册业务类型
     *
     * @return SqlSessionFactory
     * @throws Exception
     */
    @Bean("primarySqlSessionFactory")
    @Primary
    public SqlSessionFactory primarySqlSessionFactory() {
        SqlSessionFactory factory = null;

        try {
            SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
            bean.setDataSource(primaryDataSource());
            //mybatis配置
            bean.setConfigLocation(new DefaultResourceLoader().getResource("classpath:mybatis-primary.xml"));
            System.out.println("datasource:"+new DefaultResourceLoader().getResource("classpath:mybatis-primary.xml"));
            factory = bean.getObject();
            org.apache.ibatis.session.Configuration config = factory.getConfiguration();

            // 自动注册

            for (String className : ClassUtil.getJsitsClasses()) {
                if (className.contains(".model.") || className.contains(".dto.") || className.contains(".enums.")) {
                    Class<?> classType = Class.forName(className);
                    if (classType.isEnum()) {
                        EnumIgnore enumIgnore = classType.getAnnotation(EnumIgnore.class);
                        if (enumIgnore != null && enumIgnore.value()) { // 忽略此枚举
                            continue;
                        }
                        if (IntValueEnum.class.isAssignableFrom(classType)) {
                            config.getTypeHandlerRegistry().register(classType, new IntValueEnumTypeHandler(classType));
                        } else if (StringValueEnum.class.isAssignableFrom(classType)) {
                            config.getTypeHandlerRegistry().register(classType, new StringValueEnumTypeHandler(classType));
						}else {
                            config.getTypeHandlerRegistry().register(classType, new EnumOrdinalTypeHandler(classType));
                        }
                    }
                    config.getTypeAliasRegistry().registerAlias(classType);
                }
            }

            // 支持oracle geometry空间数据类型
//            config.getTypeHandlerRegistry().register(Object.class, new GeometryTypeHandler());
//            config.getTypeAliasRegistry().registerAlias(Object.class);
        } catch (Exception e) {
            System.out.println("@@@@@@@"+e.getMessage());
        }

        return factory;
    }

    /**
     * mybatis通用mapper配置
     *
     * @return
     * @throws Exception
     */
    @Bean("primaryMapperHelper")
    @Primary
    public MapperHelper primaryMapperHelper() throws Exception {
        MapperHelper mapperHelper = new MapperHelper();

        Config config = mapperHelper.getConfig();
        //表名字段名使用驼峰转下划线大写形式
        config.setStyle(Style.camelhumpAndUppercase);

        mapperHelper.registerMapper(Mapper.class);
        mapperHelper.processConfiguration(primarySqlSessionFactory().getConfiguration());
        return mapperHelper;
    }

    private class zxhHandler{

    }
}
