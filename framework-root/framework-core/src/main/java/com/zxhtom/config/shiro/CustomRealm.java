package com.zxhtom.config.shiro;

import com.zxhtom.config.shiro.token.CustomUserPasswordToken;
import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.model.Client;
import com.zxhtom.model.CustomUser;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.vconstant.PackageName;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 该realm 废弃了，因为采用单点登录这里不能授权登录，
 * 客户端需要使用的可以重新自定义realm,
 * 客户端只需要保证名称是customRealm，加上@Primary生效,继承该类CustomRealm
 */
@Component
@Order(value = Integer.MAX_VALUE)
public class CustomRealm extends BaseShiroRealm{

	@Autowired
	private RedisSessionDao sessionDao;
	@Autowired
	private Client client;
	@Autowired
	private CustomRepository customRepository;

	{
		super.setName("customRealm");
	}
	/**
	 * 此方法会保证其他的token进来不会进来验证
	 * @param token
	 * @return
	 */
	@Override
	public boolean supports(AuthenticationToken token) {
		return token instanceof CustomUserPasswordToken;//表示此Realm只支持CustomUserPasswordToken类型
	}
	/**
	 * 获取角色
	 */
	@Override
	public AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		if (SecurityUtils.getSubject().getSession().getAttribute(PackageName.REALM).toString().equals(PackageName.SERVICE)) {
			//说明是远程登录，本地不进行授权
			return null;
		}
		//获取用户名
		Long userId = ((CustomUser) principals.getPrimaryPrincipal()).getUserId();
		Set<String> roles = selectRoleNamesByUserId(userId);
		Set<String> premission=selectPremissionNamesByUserId(userId);
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		authorizationInfo.setRoles(roles);
		authorizationInfo.setStringPermissions(premission);
		return authorizationInfo;
	}

	/**
	 * 认证用户方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		//从主体传过来的认证信息中获取用户名
		CustomUser customUser = (CustomUser) token.getPrincipal();
		//补全用户信息
        customUser = customRepository.selectUserInfoByUserName(customUser.getUserName());
		//通过用户名到数据库中获取凭证
		String password = selectPasswordByUserName(customUser.getUserName());
		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(customUser,password,"customRealm");
		//设置盐
		authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes("zxh"));
		SecurityUtils.getSubject().getSession().setAttribute(PackageName.REALM, PackageName.LOCAL);
		return authenticationInfo;
	}
	
	
	private String selectPasswordByUserName(String username) {
		CustomUser user = customRepository.selectUserByUserName(username);
		if(null!=user){
			return user.getPassword();
		}
		return null;
	}

	private Set<String> selectPremissionNamesByUserId(Long userId) {
		Set<String> set = customRepository.selectPremissionNamesByUserId(userId,null,null);
		return set;
	}

	private Set<String> selectRoleNamesByUserId(Long userId) {
		Set<String> set = customRepository.selectRoleNamesByUserId(userId);
		return set;
	}
	
	
	public static void main(String[] args) {
		System.out.println(IdUtil.getInstance().getId());
		Md5Hash md5Hash= new Md5Hash("123456","zxh");
		System.out.println(md5Hash.toString());
	}

}
