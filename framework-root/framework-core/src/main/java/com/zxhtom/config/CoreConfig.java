package com.zxhtom.config;


import com.zxhtom.CoreConfiguration;
import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.core.repository.impl.CustomRepositoryImpl;
import com.zxhtom.model.WebServiceProperties;
import com.zxhtom.utils.ContextUtil;
import com.zxhtom.web.CurrentRequestService;
import com.zxhtom.web.CurrentRequestServiceImpl;
import com.zxhtom.web.CurrentRequestServiceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;

import javax.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 核心配置类, 扫描所有业务服务和仓储服务
 */
@Configuration
@ComponentScan(basePackages = {"com.zxhtom.**.impl"})
public class CoreConfig {


    /**
     * 注入CoreConfiguration
     *
     * @return CoreConfiguration
     */
    @Bean
    @ConfigurationProperties("coreconfiguration")
    public CoreConfiguration coreConfiguration() {
        return new CoreConfiguration();
    }

    /**
     * 注入ContextUtil
     *
     * @return ContextUtil
     */
    @Bean
    public ContextUtil contextUtil() {
        return new ContextUtil();
    }

    /**
     * 注入CurrentRequestService, 代理
     *
     * @return CurrentRequestService
     */
    @Bean
    @Primary
    public CurrentRequestService currentRequestService() {
        return new CurrentRequestServiceProxy();
    }

    /**
     * 生命周期同request
     *
     * @return CurrentRequestService
     */
    @Bean("currentRequestServiceByRequest")
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public CurrentRequestService currentRequestServiceByRequest() {
        return new CurrentRequestServiceImpl();
    }
    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }

    /**
     * 生命周期同单例
     *
     * @return CurrentRequestService
     */
    @Bean("currentRequestServiceBySingleton")
    public CurrentRequestService currentRequestServiceBySingleton() {
        return new CurrentRequestServiceImpl();
    }
}
