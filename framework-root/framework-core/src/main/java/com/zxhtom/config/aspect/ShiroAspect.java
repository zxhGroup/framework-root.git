package com.zxhtom.config.aspect;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * shiro中需要进行请求前后处理
 * 
 * @author John
 *
 */
@Aspect
@Component
public class ShiroAspect {

	@Autowired
	private RedisTemplate redisTemplate;
	
	private void setSeria() {
		JdkSerializationRedisSerializer serializer = new JdkSerializationRedisSerializer();
		 redisTemplate.setKeySerializer(serializer);
	}

	@Pointcut("@annotation(com.zxhtom.annotation.TokenNot)")
	// @Pointcut("execution(* com.zxhtom.shiro.model..*.*(..))")
	public void validRequest() {
	}

	@Before("validRequest()")
	public void deBefore(JoinPoint joinPoint) throws Throwable {
		System.out.println("ShiroAspect@@@@@@@@@@@@@@");
		// 接收到请求，记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		System.out.println("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "."
				+ joinPoint.getSignature().getName());
		System.out.println("ARGS : " + Arrays.toString(joinPoint.getArgs()));
		HttpServletRequest request = attributes.getRequest();
		request.setAttribute("token", "");
//		if ("OPTIONS".equals(request.getMethod())) {
//			return;
//		}
//		// 记录下请求内容
//		System.out.println("URL : " + request.getRequestURL().toString());
//		System.out.println("HTTP_METHOD : " + request.getMethod());
//		System.out.println("IP : " + request.getRemoteAddr());
//		System.out.println("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "."
//				+ joinPoint.getSignature().getName());
//		System.out.println("ARGS : " + Arrays.toString(joinPoint.getArgs()));

	}
}
