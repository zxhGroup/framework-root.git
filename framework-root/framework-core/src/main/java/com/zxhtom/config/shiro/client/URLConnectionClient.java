package com.zxhtom.config.shiro.client;

import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.client.response.OAuthClientResponseFactory;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;

public class URLConnectionClient extends org.apache.oltu.oauth2.client.URLConnectionClient {

    @Override
    public <T extends OAuthClientResponse> T execute(OAuthClientRequest request, Map<String, String> headers, String requestMethod, Class<T> responseClass) throws OAuthSystemException, OAuthProblemException {
        String responseBody = null;
        URLConnection c = null;
        boolean var7 = false;

        int responseCode;
        try {
            URL url = new URL(request.getLocationUri());
            c = url.openConnection();
            responseCode = -1;
            if (c instanceof HttpURLConnection) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) c;
                Iterator i$;
                Map.Entry header;
                if (headers != null && !headers.isEmpty()) {
                    i$ = headers.entrySet().iterator();

                    while (i$.hasNext()) {
                        header = (Map.Entry) i$.next();
                        httpURLConnection.addRequestProperty((String) header.getKey(), (String) header.getValue());
                    }
                }

                if (request.getHeaders() != null) {
                    i$ = request.getHeaders().entrySet().iterator();

                    while (i$.hasNext()) {
                        header = (Map.Entry) i$.next();
                        httpURLConnection.addRequestProperty((String) header.getKey(), (String) header.getValue());
                    }
                }

                if (!OAuthUtils.isEmpty(requestMethod)) {
                    httpURLConnection.setRequestMethod(requestMethod);
                    if (requestMethod.equals("POST")) {
                        httpURLConnection.setDoOutput(true);
                        OutputStream ost = httpURLConnection.getOutputStream();
                        PrintWriter pw = new PrintWriter(ost);
                        pw.print(request.getBody());
                        pw.flush();
                        pw.close();
                    }
                } else {
                    httpURLConnection.setRequestMethod("GET");
                }

                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                InputStream inputStream;
                if (responseCode != 400 && responseCode != 401) {
                    inputStream = httpURLConnection.getInputStream();
                } else {
                    inputStream = httpURLConnection.getErrorStream();
                }

                responseBody = toString(inputStream,"UTF-8");
            }
        } catch (IOException var12) {
            throw new OAuthSystemException(var12);
        }
        return OAuthClientResponseFactory.createCustomResponse(responseBody, c.getContentType(), responseCode, responseClass);
    }

    public static String toString(InputStream is, String defaultCharset) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        int count = -1;
        int index=0;
        while((count = is.read()) != -1) {
            if (count != 92) {
                data[index++]= (byte) count;
            }
        }
        is.close();
        return new String(data,"UTF-8").substring(1);
    }

    public static void main(String[] args) {
        String string = "{\\\"code\\\", : 123}";
        System.out.println(string.replace("\\", ""));
    }
}
