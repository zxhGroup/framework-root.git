package com.zxhtom.config.aspect;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.zxhtom.CoreConstants;
import com.zxhtom.exception.BusinessException;
import com.zxhtom.web.ActionResultCode;

/**
 * shiro中需要进行请求前后处理
 * 
 * @author John
 *
 */
@Aspect
@Component
public class RequestAspect {
//	@Pointcut("execution(public * com.zxhtom..*.*Controller(..))")  
	@Pointcut("execution(* com.zxhtom..*controller.*.*(..))")
	// @Pointcut("execution(* com.zxhtom.shiro.model..*.*(..))")
	public void validRequest() {
	}

	@Before("validRequest()")
	public void deBefore(JoinPoint joinPoint) throws Throwable {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if (attributes == null) {
			return ;
		}
		HttpServletRequest request = attributes.getRequest();
		if(request.getAttribute("x-token-status")!=null&&false==(Boolean)request.getAttribute("x-token-status")){
			throw new BusinessException(ActionResultCode.INVALID_SHIRO_TOKEN.getValue(),"need login");
		}
		if(request.getAttribute("x-token-roles")!=null&&StringUtils.isNotEmpty(request.getAttribute("x-token-roles").toString())){
			throw new BusinessException(ActionResultCode.INVALID_SHIRO_TOKEN.getValue(),"need reLogin to get right roles["+request.getAttribute("x-token-roles").toString()+"]");
		}
		if(request.getAttribute(CoreConstants.MSG)!=null&&StringUtils.isNotEmpty(request.getAttribute(CoreConstants.MSG).toString())){
			throw new BusinessException(ActionResultCode.INVALID_SHIRO_TOKEN.getValue(),request.getAttribute(CoreConstants.MSG).toString());
		}
	}
}
