package com.zxhtom.config;

import org.springframework.context.annotation.Bean;

import com.google.common.base.Predicates;
import com.zxhtom.CoreConstants;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Springfox配置类
 */
@EnableSwagger2
@Configuration
public class SpringfoxConfig {
	// 配置swagger
	@Bean
	public Docket restApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName(CoreConstants.GROUP_NAME).apiInfo(new ApiInfoBuilder().title("后端服务说明").version("1.0").build()).forCodeGeneration(true).useDefaultResponseMessages(false).select()
				.paths(Predicates.not(PathSelectors.regex("/error.*"))).build();
	}
}
