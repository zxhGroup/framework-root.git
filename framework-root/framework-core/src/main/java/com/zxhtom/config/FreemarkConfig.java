package com.zxhtom.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.zxhtom.config.shiro.ShiroTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;


import freemarker.template.TemplateException;

@Import(ShiroTag.class)
@Configuration
public class FreemarkConfig {

	@Autowired
	protected freemarker.template.Configuration configuration;
	@Autowired
	protected org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver resolver;
	@Autowired
	protected org.springframework.web.servlet.view.InternalResourceViewResolver springResolver;

	@Bean
	public FreeMarkerConfigurer freeMarkerConfigurer(ShiroTag shiroTag) throws IOException, TemplateException {
		FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
		configurer.setTemplateLoaderPath("classpath:/templates");
		configurer.setPreferFileSystemAccess(false);
		Map<String, Object> variables = new HashMap<String, Object>(1);
		//variables.put("shiros", new ShiroTags());
		variables.put("shiro", shiroTag);
		configurer.setFreemarkerVariables(variables);
		Properties settings = new Properties();
		settings.setProperty("default_encoding", "utf-8");
		settings.setProperty("number_format", "0.##");
		configurer.setFreemarkerSettings(settings);
		return configurer;
		/*FreeMarkerConfigurationFactory factory = new FreeMarkerConfigurationFactory();
	    factory.setTemplateLoaderPath("classpath:/templates/");
	    factory.setDefaultEncoding("UTF-8");
	    factory.setPreferFileSystemAccess(false);
	    FreeMarkerConfigurer result = new FreeMarkerConfigurer();
	    freemarker.template.Configuration configuration = factory.createConfiguration();
	    Map<String, Object> variables = new HashMap<String, Object>(1);
	    variables.put("shiro", shiroTag);
	    result.setFreemarkerVariables(variables);
	    configuration.setClassicCompatible(true);
	    result.setConfiguration(configuration);
	    Properties settings = new Properties();
	    settings.put("template_update_delay", "0");
	    settings.put("default_encoding", "UTF-8");
	    settings.put("number_format", "0.######");
	    settings.put("classic_compatible", true);
	    settings.put("template_exception_handler", "ignore");
	    result.setFreemarkerSettings(settings);
	    return result;*/
	}

}
