package com.zxhtom.config.shiro.client;

import com.zxhtom.config.shiro.BaseShiroRealm;
import com.zxhtom.core.repository.CustomShiroRepository;
import com.zxhtom.model.CustomUser;
import com.zxhtom.vconstant.PackageName;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-2-18
 * <p>Version: 1.0
 * @author John
 */
@Component
public class OAuth2Realm extends BaseShiroRealm {

    @Autowired
    private CustomShiroRepository customShiroRepository;

    private String clientId;
    private String clientSecret;
    private String accessTokenUrl;
    private String userInfoUrl;
    private String redirectUrl;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setAccessTokenUrl(String accessTokenUrl) {
        this.accessTokenUrl = accessTokenUrl;
    }

    public void setUserInfoUrl(String userInfoUrl) {
        this.userInfoUrl = userInfoUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    /**
     * 此方法会保证其他的token进来不会进来验证
     * @param token
     * @return
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof OAuth2Token;//表示此Realm只支持OAuth2Token类型
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if (SecurityUtils.getSubject().getSession().getAttribute(PackageName.REALM).toString().equals(PackageName.LOCAL)) {
            //说明不是远程登录，本地不进行授权
            return null;
        }
        //获取用户名
        Long userId = ((CustomUser) principals.getPrimaryPrincipal()).getUserId();
        Set<String> roles = selectRoleNamesByUserId(userId);
        Set<String> premission=selectPremissionNamesByUserId(userId);
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(roles);
        authorizationInfo.setStringPermissions(premission);
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) {
        OAuth2Token oAuth2Token = (OAuth2Token) token;
        String code = oAuth2Token.getAuthCode();
        //String username = extractUsername(code);
        CustomUser customUser = customShiroRepository.selectCurrentUserName(SecurityUtils.getSubject().getSession().getAttribute(PackageName.ACCESSTOKEN).toString());
        SimpleAuthenticationInfo authenticationInfo =
                new SimpleAuthenticationInfo(customUser, code, getName());
        return authenticationInfo;
    }

    private Set<String> selectPremissionNamesByUserId(Long userId) {
        Set<String> set = customShiroRepository.selectPremissionNamesByUserId(userId,clientId,clientSecret);
        return set;
    }

    private Set<String> selectRoleNamesByUserId(Long userId) {
        Set<String> set = customShiroRepository.selectRoleNamesByUserId(userId);
        return set;
    }
}

