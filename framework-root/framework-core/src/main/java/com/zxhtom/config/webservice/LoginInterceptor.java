package com.zxhtom.config.webservice;

import com.zxhtom.utils.RedisUtil;
import com.zxhtom.vconstant.PackageName;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.shiro.SecurityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import java.util.List;

/**
 * Created by Administrator on 2018/2/27.
 */
public class LoginInterceptor extends AbstractPhaseInterceptor<SoapMessage> {
    private static String username="root";
    private static String password="admin";
    public LoginInterceptor(String username, String password) {
        //设置在发送请求前阶段进行拦截
        super(Phase.PREPARE_SEND);
        this.username=username;
        this.password=password;
    }

    @Override
    public void handleMessage(SoapMessage soapMessage) throws Fault {
        List<Header> headers = soapMessage.getHeaders();
        Document doc = DOMUtils.createDocument();
        Element auth = doc.createElementNS("http://repository.shiro.zxhtom.com/","SecurityHeader");
        Element UserName = doc.createElement(PackageName.USERNAME);
        Element UserPass = doc.createElement(PackageName.PASSWORD);
        Element AccessToken = doc.createElement(PackageName.ACCESSTOKEN);

        UserName.setTextContent(username);
        UserPass.setTextContent(password);
        //AccessToken.setTextContent(SecurityUtils.getSubject().getSession().getAttribute(PackageName.ACCESSTOKEN).toString());
        AccessToken.setTextContent("");
        auth.appendChild(UserName);
        auth.appendChild(UserPass);
        //auth.appendChild(AccessToken);

        headers.add(0, new Header(new QName("SecurityHeader"),auth));
    }
}
