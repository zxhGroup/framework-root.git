package com.zxhtom.config.shiro;

import com.zxhtom.model.RedisProperties;
import com.zxhtom.utils.ContextUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class RedisSessionDao extends AbstractSessionDAO {

	@Autowired
	private RedisTemplate redisTemplate;

	private RedisProperties redisProperties;
	// redis session操作的前缀

	public RedisTemplate getRedisTemplate() {
		return redisTemplate;
	}

	public void setRedisTemplate(RedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public String getSHIRO_SESSION_PREFIX() {
		return SHIRO_SESSION_PREFIX;
	}

	private final String SHIRO_SESSION_PREFIX = "zxhtom-session:";

	private String getKey(String key) {

		return SHIRO_SESSION_PREFIX + key;

	}

	@Override
	public void update(Session session) throws UnknownSessionException {
		saveSession(session);
	}

	@Override
	public void delete(Session session) {
		setSeria();
		if (null == session || null == session.getId()) {
			return;
		}
		redisTemplate.delete(getKey(session.getId().toString()));
		resetSeria();

	}
	@Override
	public Collection<Session> getActiveSessions() {
		// 通过前缀获取所有的session
		setSeria();
		Set<String> keys = redisTemplate.keys(SHIRO_SESSION_PREFIX + "*");
		Set<Session> sessions = new HashSet<Session>();
		if (CollectionUtils.isEmpty(keys)) {
			return sessions;
		}
		for (String key : keys) {
			Session session = (Session) (redisTemplate.opsForValue().get(key));
			sessions.add(session);
		}
		resetSeria();
		return sessions;
	}
	@Override
	protected Serializable doCreate(Session session) {
		// 通过session获取sessionID
		Serializable sessionId = generateSessionId(session);
		assignSessionId(session, sessionId);
		saveSession(session);
		return sessionId;
	}

	private void saveSession(Session session) {
		if (redisProperties == null) {
			redisProperties = ContextUtil.getApplicationContext().getBean(RedisProperties.class);
		}
		// 通过sessionID保存对应的session
		setSeria();
		redisTemplate.opsForValue().set(getKey(session.getId().toString()), session);
		if (redisTemplate.hasKey(getKey(session.getId().toString()))) {
			redisTemplate.expire(getKey(session.getId().toString()), redisProperties.getSessionTimeout() - 1, TimeUnit.SECONDS);
		}
		resetSeria();
		// redisTemplate.expire(session.getId().toString(), 600,
		// TimeUnit.SECONDS);

	}

	private void setSeria() {
		JdkSerializationRedisSerializer serializer = new JdkSerializationRedisSerializer();
		 //redisTemplate.setKeySerializer(serializer);
		 redisTemplate.setValueSerializer(serializer);
	}
	private void resetSeria() {
		/*Jackson2JsonRedisSerializer serializer = new Jackson2JsonRedisSerializer(Object.class);
		redisTemplate.setKeySerializer(serializer);
		redisTemplate.setValueSerializer(serializer);*/
	}
	@Override
	protected Session doReadSession(Serializable sessionId) {
		// SimpleSession
		setSeria();
		if (null == sessionId) {
			return null;
		}
		if (!redisTemplate.hasKey(getKey(sessionId.toString()))) {
			return null;
		}
		Object value = redisTemplate.opsForValue().get(getKey(sessionId.toString()));
		resetSeria();
		return (Session) value;
	}

}
