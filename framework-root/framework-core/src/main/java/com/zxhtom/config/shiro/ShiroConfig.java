package com.zxhtom.config.shiro;

import com.zxhtom.annotation.OnlyLogin;
import com.zxhtom.annotation.TokenNot;
import com.zxhtom.config.CoreConfig;
import com.zxhtom.config.WebMvcConfig;
import com.zxhtom.config.shiro.client.OAuth2AuthenticationFilter;
import com.zxhtom.config.shiro.client.OAuth2Realm;
import com.zxhtom.config.shiro.filter.RoleAuthorizationFilter;
import com.zxhtom.config.shiro.filter.ShiroLoginFilter;
import com.zxhtom.config.shiro.filter.ValidMenuFilter;
import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.model.*;
import com.zxhtom.utils.AnnoManageUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionFactory;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.Filter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

@Configuration
@ComponentScan(basePackages = {"com.zxhtom.config.aspect","com.zxhtom.config.shiro"})
@Import({WebMvcConfig.class, CoreConfig.class})
public class ShiroConfig {
	/**
	 * 一共配置两个。这一个是不支持前后分离的。
	 *
	 * @param securityManager
	 * @return date 2018年5月24日
	 */
//	@Bean("shiroFilter")
//	public ShiroFilterFactoryBean shiroFilterNo(SecurityManager securityManager) {
//		// 构建shiro请求过滤器
//		ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
//		// 设置安全管理
//		shiroFilter.setSecurityManager(securityManager);
//		// 设置登录页面
//		shiroFilter.setLoginUrl("/login.html");
//		// 授权失败，未授权的跳转页面
//		shiroFilter.setUnauthorizedUrl("/403.html");
//		// 授权成功跳转页面
//		shiroFilter.setSuccessUrl("/system.html");
//		Map<String, String> filterMap = new LinkedHashMap<>();
//		// 获取不需要登录的接口地址
//		Set<Method> methods = AnnoManageUtil.getTokenNotMethod("com.zxhtom");
//		for (Method method : methods) {
//			RequestMapping controller = method.getDeclaringClass().getAnnotation(RequestMapping.class);
//			RequestMapping annotation = method.getAnnotation(RequestMapping.class);
//			RequiresRoles requiresRoles = method.getAnnotation(RequiresRoles.class);
//			RequiresPermissions requiresPermissions = method.getAnnotation(RequiresPermissions.class);
//			if (null != requiresRoles || null != requiresPermissions) {
//				continue;
//			}
//			filterMap.put(controller.value()[0].toString() + annotation.value()[0].toString(), "anon");
//		}
//		// 放开swagger接口权限
//		filterMap.put("/swagger/**", "anon");
//		filterMap.put("/v2/api-docs", "anon");
//		filterMap.put("/swagger-ui.html", "anon");
//		filterMap.put("/webjars/**", "anon");
//		filterMap.put("/swagger-resources/**", "anon");
//
//		filterMap.put("/static/**", "anon");
//		// 放开所有的文件
//		filterMap.put("/system.html", "anon");
////		filterMap.put("/**.*", "anon");
//
//		filterMap.put("/**", "authc");
//		Map<String, Filter> filters = new HashMap<String, Filter>();
//
////		filters.put("authc", new ShiroLoginFilter());
//		shiroFilter.setFilters(filters);
//		shiroFilter.setFilterChainDefinitionMap(filterMap);
//		return shiroFilter;
//	}
	@Bean("shiroFilter")
	@Order(-10)
	@Primary
	@ConfigurationProperties("zxhtom.config")
	public ShiroFilterFactoryBean shiroFilter(Client client , WebServiceProperties webServiceProperties , CustomRepository customRepository , GeneratorProperties properties , SecurityManager securityManager, SysConfig sysConfig, DruidProperties druidProperties) {
		// 构建shiro请求过滤器
		ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
		// 设置安全管理
		shiroFilter.setSecurityManager(securityManager);
		// 设置登录页面
		shiroFilter.setLoginUrl("/framework_core/login/login");
		// 授权失败，未授权的跳转页面
		shiroFilter.setUnauthorizedUrl("/framework_core/login/403");
		// 授权成功跳转页面
		shiroFilter.setSuccessUrl("/index.html");
		Map<String, String> filterMap = new LinkedHashMap<>();
		/**
		 * 登录页面 不需要登录
		 */
		filterMap.put("/login.*","anon");// 获取不需要登录的接口地址
		setShiroByInterfaceClass(filterMap,TokenNot.class,"anon");
		setShiroByInterfaceClass(filterMap,OnlyLogin.class,"authc");
		// 放开swagger接口权限
		filterMap.put("/framework_core/login/oauth2", "oauth2");
		filterMap.put("/services", "anon");
		filterMap.put("/swagger/**", "anon");
		filterMap.put("/v2/api-docs", "anon");
		filterMap.put("/swagger-ui.html", "anon");
		filterMap.put("/webjars/**", "anon");
		filterMap.put("/druid/**", "authc");
		filterMap.put("/support/http/resources/**", "anon");
		filterMap.put("/swagger-resources/**", "anon");
		filterMap.put("/statics/**", "anon");
		filterMap.put("/assets/**", "anon");
		filterMap.put("/jquery/**", "anon");
        filterMap.put("favicon.ico", "anon");
		//所有html、jsp、ftl页面需要登录(除去登录页) , 开发期间注释掉
		filterMap.put("/**/*.html","authc");
		filterMap.put("/**/*.jsp","authc");
		filterMap.put("/**/*.ftl","authc");
		// 放开所有的文件
		filterMap.put("/**", "anon");
		String roleStr="";
		try {
			List<Module> moduleList = customRepository.selectAllModules();
			for (Module module : moduleList) {
				//权限角色控制
				String frameworkCommonRoleStr="";
				frameworkCommonRoleStr = getRoleStrByModuleName(customRepository, module.getModuleCode(), frameworkCommonRoleStr,client);
				filterMap.put("/framework_common/**", "role["+frameworkCommonRoleStr+"],authc");
			}
			roleStr = getRoleStrByModuleName(customRepository, properties.getModuleName(), roleStr,client);
		} catch (Exception e) {
			System.out.println(e+"允许第一次出错");
		}
		filterMap.put("/**", "anon");
		Map<String, Filter> filters = new HashMap<String, Filter>();
		//根据子系统配置选择加载不同响应器  json--权限等相应重返回json;html--权限响应页面
		//覆盖默认的登录过滤器
		if(null==sysConfig.getViewHandler()||true==sysConfig.getViewHandler()){
			filters.put("authc", new ShiroLoginFilter());
		}

		filters.put("menu", new ValidMenuFilter());
		filters.put("role", new RoleAuthorizationFilter());
		filters.put("oauth2", new OAuth2AuthenticationFilter(client,webServiceProperties));
		shiroFilter.setFilters(filters);
		shiroFilter.setFilterChainDefinitionMap(filterMap);
		return shiroFilter;
	}


	private void setShiroByInterfaceClass(Map<String, String> filterMap,Class clazz , String filterName) {
		Set<Method> methods = AnnoManageUtil.getTokenNotMethod("com.zxhtom",clazz);
		for (Method method : methods) {
			String controllerMapping="";
			String methodMapping="";
			RequestMapping controller = method.getDeclaringClass().getAnnotation(RequestMapping.class);
			RequestMapping annotation = method.getAnnotation(RequestMapping.class);
			RequiresRoles requiresRoles = method.getAnnotation(RequiresRoles.class);
			RequiresPermissions requiresPermissions = method.getAnnotation(RequiresPermissions.class);
			if(TokenNot.class==clazz){
				if (null != requiresRoles || null != requiresPermissions) {
					continue;
				}
			}
			if(null!=controller&&controller.value()!=null){
				controllerMapping = controller.value()[0].toString();
			}
			if(null!=annotation&&annotation.value()!=null){
				methodMapping = annotation.value()[0].toString();
			}
			//在rest请求地址中会出现参数在地址中，在controller中会 xxx/{xx}这样设置
			if(Pattern.matches(".*\\{.*\\}.*", methodMapping)){
				//需要将之前的地址加入后面用**匹配
				String regex = "(\\{[^\\}]*\\})";
				filterMap.put(controllerMapping + methodMapping.replaceAll(regex, "**") , filterName);
			}else if(StringUtils.isNoneBlank(methodMapping)){
				filterMap.put(controllerMapping + methodMapping , filterName);
			}else {
				filterMap.put(controllerMapping + "/**" , filterName);
			}
		}
	}

	private String getRoleStrByModuleName(CustomRepository customRepository, String moduleName, String roleStr,Client client) {
		try {
			List<CustomRole> roleList = customRepository.selectRolesByModuleCode(moduleName,client.getAppKey(),client.getAppSecret());
			for (CustomRole customRole : roleList) {
				roleStr+=customRole.getRoleCode()+",";
			}
			if(StringUtils.isNotEmpty(roleStr)){
				roleStr=roleStr.substring(0,roleStr.length()-1);
			}
		} catch (Exception e) {
			System.out.println("@@@error");
		}
		return roleStr;
	}

	//这里不能讲自定义过滤器注入spring，在spring注册了，每次请求会先走这里的过滤器
	/*
	 * @Bean public ShiroLoginFilter loginFilter(){ return new
	 * ShiroLoginFilter(); }
	 */
    @Bean
    public MyRedisCacheManager getEhCacheManager() {
		MyRedisCacheManager ehcacheManager = new MyRedisCacheManager();
        //ehcacheManager.setCacheManagerConfigFile("classpath:ehcache-shiro.xml");
        return ehcacheManager;
    }
	@Bean
	public SecurityManager securityManager(OAuth2Realm oAuth2Realm,CustomRealm userRealm, SessionManager sessionManager) {
		List<Realm> realms = new ArrayList<>();
		realms.add(userRealm);
		realms.add(oAuth2Realm);

		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		userRealm.setCredentialsMatcher(getMatcher());
		securityManager.setRealms(realms);
		//securityManager.setRealm(userRealm);
		securityManager.setSessionManager(sessionManager);
		securityManager.setCacheManager(getEhCacheManager());
		return securityManager;
	}

	@Bean
	public RedisSessionDao getRedisSessionDao(RedisTemplate redisTemplate) {
		RedisSessionDao redisSessionDao = new RedisSessionDao();
		redisSessionDao.setRedisTemplate(redisTemplate);
		return redisSessionDao;
		//return (RedisSessionDao) new ProxyFactory(redisSessionDao).getProxyInstance();
	}
	
	@Bean
	public SessionManager getSessionManager(RedisSessionDao sessionDao) {
		MySessionManager sessionManager = new MySessionManager();
		sessionManager.setSessionDAO(sessionDao);
		sessionManager.setSessionFactory(getSessionFactory());
		return sessionManager;
//		return (MySessionManager) new ProxyFactory(sessionManager).getProxyInstance();
	}

	@Bean
	public SessionFactory getSessionFactory() {
		SessionFactory sessionFactory = new MySessionFactory();
		return sessionFactory;
	}

	// 设置md5加密。
	@Bean
	public HashedCredentialsMatcher getMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
		matcher.setHashAlgorithmName("md5");
		matcher.setHashIterations(1);
		return matcher;
	}

	// 以下设置通过注解设置shiro权限
	@Bean("lifecycleBeanPostProcessor")
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}

	@Bean
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator proxyCreator = new DefaultAdvisorAutoProxyCreator();
		proxyCreator.setProxyTargetClass(true);
		return proxyCreator;
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
		advisor.setSecurityManager(securityManager);
		return advisor;
	}
}
