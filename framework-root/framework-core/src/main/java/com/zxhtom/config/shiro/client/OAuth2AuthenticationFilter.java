package com.zxhtom.config.shiro.client;

import com.zxhtom.CoreConstants;
import com.zxhtom.model.Client;
import com.zxhtom.model.RedisProperties;
import com.zxhtom.model.WebServiceProperties;
import com.zxhtom.utils.ContextUtil;
import com.zxhtom.vconstant.PackageName;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-2-18
 * <p>Version: 1.0
 */
public class OAuth2AuthenticationFilter extends AuthenticatingFilter {

    private Client client;
    private WebServiceProperties properties;
    //oauth2 authc code参数名
    private String authcCodeParam = "code";
    //客户端id
    private String clientId;
    //服务器端登录成功/失败后重定向到的客户端地址
    private String redirectUrl;
    //oauth2服务器响应类型
    private String responseType = "code";

    private String failureUrl;

    public OAuth2AuthenticationFilter(Client client , WebServiceProperties properties){
        this.client = client;
        this.properties=properties;
    }
    public void setAuthcCodeParam(String authcCodeParam) {
        this.authcCodeParam = authcCodeParam;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public void setFailureUrl(String failureUrl) {
        this.failureUrl = failureUrl;
    }

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String code = httpRequest.getParameter(authcCodeParam);
        return new OAuth2Token(code);
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponese = (HttpServletResponse) response;

        String error = request.getParameter("error");
        String errorDescription = request.getParameter("error_description");
        if(!StringUtils.isEmpty(error)) {//如果服务端返回了错误
            WebUtils.issueRedirect(request, response, failureUrl + "?error=" + error + "error_description=" + errorDescription);
            return false;
        }

        Subject subject = getSubject(request, response);
        if(!subject.isAuthenticated()) {
            if(StringUtils.isEmpty(request.getParameter(authcCodeParam))) {
                //如果用户没有身份验证，且没有auth code，则重定向到服务端授权
                saveRequestAndRedirectToLogin(request, response);
                return false;
            }
        }
        //保存服务端code
        httpRequest.getSession().setAttribute(authcCodeParam, request.getParameter(authcCodeParam));
        try {
            OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());

            OAuthClientRequest accessTokenRequest = OAuthClientRequest
                    .tokenLocation(properties.getBase()+"/accessToken")
                    .setGrantType(GrantType.AUTHORIZATION_CODE)
                    .setClientId(client.getAppKey())
                    .setClientSecret(client.getAppSecret())
                    .setCode(String.valueOf(httpRequest.getSession().getAttribute(authcCodeParam)))
                    .setRedirectURI("localhost:8886/framework_core/login/oauth2")
                    .buildQueryMessage();

            OAuthAccessTokenResponse oAuthResponse = oAuthClient.accessToken(accessTokenRequest, OAuth.HttpMethod.POST);
            String accessToken = oAuthResponse.getAccessToken();
            String oauthClientId = oAuthResponse.getParam(CoreConstants.OAUTHCLIENTID);
            //保存服务器端的accesstoken
            httpRequest.getSession().setAttribute(PackageName.ACCESSTOKEN,accessToken );
            httpRequest.getSession().setAttribute(CoreConstants.OAUTHCLIENTID,oauthClientId );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return executeLogin(request, response);
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,
                                     ServletResponse response) throws Exception {
        RedisProperties redisProperties = ContextUtil.getApplicationContext().getBean(RedisProperties.class);
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        //将如x-token , 暂时没有加入请求头
        AtomicReference<Cookie> cookie;  //对比入参数据
        cookie = new AtomicReference<>(new Cookie("x-token", subject.getSession().getId().toString()));
        cookie.get().setMaxAge(redisProperties.getTimeout()*1000);
        cookie.get().setPath("/");
        httpResponse.addCookie(cookie.get());
        SecurityUtils.getSubject().getSession().setAttribute(PackageName.REALM,PackageName.SERVICE );
        WebUtils.getAndClearSavedRequest(request);
        issueSuccessRedirect(request, response);
        return false;
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException ae, ServletRequest request,
                                     ServletResponse response) {
        Subject subject = getSubject(request, response);
        if (subject.isAuthenticated() || subject.isRemembered()) {
            try {
                issueSuccessRedirect(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                WebUtils.issueRedirect(request, response, failureUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}
