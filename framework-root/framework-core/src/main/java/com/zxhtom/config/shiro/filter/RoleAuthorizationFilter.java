package com.zxhtom.config.shiro.filter;

import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import com.zxhtom.CoreConstants;

public class RoleAuthorizationFilter extends AuthorizationFilter {

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
		Subject subject = getSubject(request, response);
        String[] rolesArray = (String[]) mappedValue;

        if (rolesArray == null || rolesArray.length == 0) {
        	request.setAttribute(CoreConstants.MSG, "此模块尚未对外公布！");
            return true;
        }

        Set<String> roles = CollectionUtils.asSet(rolesArray);
        String roleStr="";
        for (String customRole : roles) {
			roleStr+=customRole+",";
		}
		if(StringUtils.isNotEmpty(roleStr)){
			roleStr=roleStr.substring(0,roleStr.length()-1);
		}
        for (String role : roles) {
			if(subject.hasRole(role)){
				return true;
			}
		}
        request.setAttribute("x-token-roles", roleStr);
		return true;
	}

}
