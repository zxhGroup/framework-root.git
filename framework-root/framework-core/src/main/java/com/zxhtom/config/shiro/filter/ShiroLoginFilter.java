package com.zxhtom.config.shiro.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import com.zxhtom.utils.ValidUtil;

/**
 * 重写shiro自带的登录过滤器。我们需要对OPTIONS的请求进行放行
 * 
 * @author John
 * 
 */
public class ShiroLoginFilter extends FormAuthenticationFilter {

	@SuppressWarnings("static-access")
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		if ("OPTIONS".equals(httpRequest.getMethod())) {
			return true;
		}
		request.setAttribute("x-token-status", super.onAccessDenied(request, response));
		return true;
	}

	// 重写 禁用重定向登录页/登录请求。在前后分离只需要返回json。具体有前台进行跳转
	@Override
	protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
	}
}
