package com.zxhtom.config.shiro.token;

import com.zxhtom.model.CustomUser;
import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

public class CustomUserPasswordToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
    private CustomUser customUser;
    private char[] password;
    private boolean rememberMe;
    private String host;

    public CustomUserPasswordToken() {
        this.rememberMe = false;
    }

    public CustomUserPasswordToken(CustomUser customUser, char[] password) {
        this(customUser, (char[])password, false, (String)null);
    }

    public CustomUserPasswordToken(CustomUser customUser, String password) {
        this(customUser, (char[])(password != null ? password.toCharArray() : null), false, (String)null);
    }

    public CustomUserPasswordToken(CustomUser customUser, char[] password, String host) {
        this(customUser, password, false, host);
    }

    public CustomUserPasswordToken(CustomUser customUser, String password, String host) {
        this(customUser, password != null ? password.toCharArray() : null, false, host);
    }

    public CustomUserPasswordToken(CustomUser customUser, char[] password, boolean rememberMe) {
        this(customUser, (char[])password, rememberMe, (String)null);
    }

    public CustomUserPasswordToken(CustomUser customUser, String password, boolean rememberMe) {
        this(customUser, (char[])(password != null ? password.toCharArray() : null), rememberMe, (String)null);
    }

    public CustomUserPasswordToken(CustomUser customUser, char[] password, boolean rememberMe, String host) {
        this.rememberMe = false;
        this.customUser = customUser;
        this.password = password;
        this.rememberMe = rememberMe;
        this.host = host;
    }

    public CustomUserPasswordToken(CustomUser customUser, String password, boolean rememberMe, String host) {
        this(customUser, password != null ? password.toCharArray() : null, rememberMe, host);
    }

    public CustomUser getCustomUser() {
        return customUser;
    }

    public void setCustomUser(CustomUser customUser) {
        this.customUser = customUser;
    }

    public char[] getPassword() {
        return this.password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public Object getPrincipal() {
        return this.getCustomUser();
    }

    public Object getCredentials() {
        return this.getPassword();
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isRememberMe() {
        return this.rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public void clear() {
        this.customUser = null;
        this.host = null;
        this.rememberMe = false;
        if (this.password != null) {
            for(int i = 0; i < this.password.length; ++i) {
                this.password[i] = 0;
            }

            this.password = null;
        }

    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append(" - ");
        sb.append(this.customUser);
        sb.append(", rememberMe=").append(this.rememberMe);
        if (this.host != null) {
            sb.append(" (").append(this.host).append(")");
        }

        return sb.toString();
    }
}
