package com.zxhtom.config.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.RealmSecurityManager;

/**
 * @author John
 */
public class ShiroUtils {
    /**
     *
     * @Title: clearAuth
     * @Description: TODO 清空所有资源权限
     * @return void    返回类型
     */
    public static void clearAuth(){
        RealmSecurityManager rsm = (RealmSecurityManager) SecurityUtils.getSecurityManager();
        BaseShiroRealm realm = (BaseShiroRealm)rsm.getRealms().iterator().next();
        realm.doClearCache();
    }
}
