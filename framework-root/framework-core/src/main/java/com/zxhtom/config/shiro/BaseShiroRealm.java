package com.zxhtom.config.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;

public abstract class BaseShiroRealm extends AuthorizingRealm {
    /**
     *
     * @Title: clearAuth
     * @Description: TODO 清空所有资源权限
     * @return void    返回类型
     */
    public void doClearCache(){
        super.doClearCache(SecurityUtils.getSubject().getPrincipals());
    }
}
