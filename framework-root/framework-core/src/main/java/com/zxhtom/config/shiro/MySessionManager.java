package com.zxhtom.config.shiro;

import java.io.Serializable;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.servlet.ShiroHttpSession;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.session.mgt.WebSessionKey;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.zxhtom.model.RedisProperties;

/**
 * Created by Administrator on 2017/12/11. 自定义sessionId获取
 */
public class MySessionManager extends DefaultWebSessionManager {

	@Autowired
	private RedisProperties redisProperties ;
	
	private static final String TOKEN = "x-token";

	private static final String REFERENCED_SESSION_ID_SOURCE = "Stateless request";

	public MySessionManager() {
		super();
	}

	/**暴露给外部使用*/
	public Serializable getPubSessionId(ServletRequest request, ServletResponse response){
		String id = WebUtils.toHttp(request).getHeader(TOKEN);
		// 如果请求头中有 Authorization 则其值为sessionId
		if (!StringUtils.isEmpty(id)) {
			request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, REFERENCED_SESSION_ID_SOURCE);
			request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, id);
			request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);
			return id;
		} else {
			// 否则按默认规则从cookie取sessionId
			Cookie cookie = new SimpleCookie(ShiroHttpSession.DEFAULT_SESSION_ID_NAME);
			return cookie.readValue((HttpServletRequest)request, (HttpServletResponse)response);
		}
	}
	@Override
	public Serializable getSessionId(ServletRequest request, ServletResponse response) {
		String id = WebUtils.toHttp(request).getHeader(TOKEN);
		if (StringUtils.isEmpty(id)) {
			javax.servlet.http.Cookie[] cookies = WebUtils.toHttp(request).getCookies();
			if (null != cookies) {
				for (javax.servlet.http.Cookie cookie : cookies) {
					if (cookie.getName().toString().equals(TOKEN)) {
						id=cookie.getValue();
					}
				}
			}
		}
		// 如果请求头中有 Authorization 则其值为sessionId
		if (!StringUtils.isEmpty(id)) {
			request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, REFERENCED_SESSION_ID_SOURCE);
			request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, id);
			request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);
			return id;
		} else {
			// 否则按默认规则从cookie取sessionId
			return super.getSessionId(request, response);
		}
	}

	@Override
	public Session retrieveSession(SessionKey sessionKey) {
		Serializable sessionId = getSessionId(sessionKey);
		ServletRequest request = null;
		HttpServletResponse response=null;
		Session session =null;
		if (sessionKey instanceof WebSessionKey) {
			request = ((WebSessionKey) sessionKey).getServletRequest();
			response = (HttpServletResponse) ((WebSessionKey) sessionKey).getServletResponse();
		}
		if (request != null && sessionId != null) {
			session = (Session) request.getAttribute(sessionId.toString());
		}
		try {
			session = super.retrieveSession(sessionKey);
		} catch (UnknownSessionException e) {
		}
		if(null!=session){
			session.setTimeout(redisProperties.getSessionTimeout()*1000);
		}
		if (request != null && sessionId != null) {
			request.setAttribute(sessionId.toString(), session);
		}
		return session;
	}

}
