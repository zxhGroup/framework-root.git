package com.zxhtom.config.aspect;

import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.zxhtom.CoreConstants;
import com.zxhtom.annotation.RedisForm;
import com.zxhtom.exception.ParamValidException;
import com.zxhtom.vconstant.RedisKey;

/**
 * 主要拦截repository中数据的来源，标有RedisFrom注解的会对redis进行curd
 * 
 * @author John
 * 
 */
@Aspect
@Component
public class DataRepositoryAspect {
	private Logger logger = LogManager.getLogger(DataRepositoryAspect.class);

	@SuppressWarnings("rawtypes")
	@Autowired
	private RedisTemplate redisTemplate;

	@Pointcut("@annotation(com.zxhtom.annotation.RedisForm)")
	public void redisCURD() {
	}

	@Around("redisCURD()")
	public Object aroud(ProceedingJoinPoint pjp) throws Throwable {
		// 获取拦截的方法对象
		Signature sig = pjp.getSignature();
		MethodSignature msig = null;
		if (!(sig instanceof MethodSignature)) {
			throw new IllegalArgumentException("该注解只能用于方法");
		}
		msig = (MethodSignature) sig;
		Object target = pjp.getTarget();
		Object[] args = pjp.getArgs();
		Method currentMethod = target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
		// 获取方法上RedisFrom注解的详细信息
		RedisForm annotation = currentMethod.getAnnotation(RedisForm.class);
		Object[] keyFT = annotation.keyF();
		Object[] keyST = annotation.keyS();
		Object[] keyF = new Object[annotation.keyF().length];
		Object[] keyS = new Object[annotation.keyS().length];
		// 提前申请好object类型。直接复制会改变数据内部接受的结构。keyFT会表面是object实际已经是keyF的数组的类型了。string
		for (int i = 0; i < keyFT.length; i++) {
			keyF[i] = keyFT[i];
		}
		for (int i = 0; i < keyST.length; i++) {
			keyS[i] = keyST[i];
		}
		analyseArgs(keyF, keyS, args);
		if (true == annotation.select()) {
			Object object = null;
			// 这个时候只去每个数组中的第一个key
			object = judgeKey(0, redisTemplate, keyF, keyS, object);
			if (null != object) {
				return object;
			}
		} else {
			// 不能轻易查询数据给别人，但是redis数据可以随意的删除
			deleteCacheByKeys(redisTemplate, keyF, keyS);
		}
		logger.info("环绕通知的目标方法名：" + pjp.getSignature().getName());
		try {
			Object obj = pjp.proceed();
			if (true == annotation.select()) {
				judgeKey(1, redisTemplate, keyF, keyS, obj);
			}
			return obj;
		} catch (Throwable throwable) {
			throwable.printStackTrace();
		}
		return null;
	}

	/**
	 * 分析注入参数合法性
	 * 
	 * @param keyF
	 * @param keyS
	 * @param args
	 *            date 2018年7月13日
	 */
	private void analyseArgs(Object[] keyF, Object[] keyS, Object[] args) {
		if (keyF.length < keyS.length) {
			throw new ParamValidException("redis key pair is not right");
		}
		// 将注解中keyF 中标志用参数代替的值 进行替换
		Integer index = 0;
		index = replaceKey(keyF, args, index);
		index = replaceKey(keyS, args, index);
	}

	private Integer replaceKey(Object[] key, Object[] args, Integer index) {
		for (int i = 0; i < key.length; i++) {
			if (key[i].equals(RedisKey.CURRENTPARAM)) {
				if (args.length <= index) {
					throw new ParamValidException("args error , params not enough");
				}
				key[i] = args[index++];
			} else if (key[i].equals(RedisKey.CURRENTUSERID)) {
				key[i] = Long.valueOf((String) SecurityUtils.getSubject().getSession().getAttribute(CoreConstants.USERID));
			} else if (key[i].equals(RedisKey.CURRENTUSERNAME)) {
				key[i] = (String) SecurityUtils.getSubject().getSession().getAttribute(CoreConstants.USERNAME);
			}
		}
		return index;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object judgeKey(Integer type, RedisTemplate redisTemplate, Object[] keyF, Object[] keyS, Object object) {
		// 0 是获取 1是设置
		if (0 != keyF.length && 0 != keyS.length) {
			if (0 == type) {
				object = redisTemplate.opsForHash().get(keyF[0], keyS[0]);
			} else if (1 == type) {
				redisTemplate.opsForHash().put(keyF[0], keyS[0], object);
			}
		} else if (0 != keyF.length && keyF.length > 0) {
			if (0 == type) {
				object = redisTemplate.opsForValue().get(keyF[0]);
			} else if (1 == type) {
				redisTemplate.opsForValue().set(keyF[0], object);
			}
		} else if (0 != keyS.length && keyS.length > 0) {
			if (0 == type) {
				object = redisTemplate.opsForValue().get(keyS[0]);
			} else if (1 == type) {
				redisTemplate.opsForValue().set(keyS[0], object);
			}
		}
		return object;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void deleteCacheByKeys(RedisTemplate redisTemplate, Object[] keyF, Object[] keyS) {
		if (0 != keyF.length && 0 != keyS.length) {
			if (keyF.length < keyS.length) {
				throw new ParamValidException("redis key pair is not right");
			}
			// 前key 比后key多，则前key中多的key 不纳入考虑范围
			for (int i = 0; i < keyS.length; i++) {
				redisTemplate.opsForHash().delete(keyF[i].toString(), keyS[i].toString());
			}
		} else if (0 != keyF.length) {
			for (int i = 0; i < keyF.length; i++) {
				redisTemplate.delete(keyF[i]);
			}
		} else if (0 != keyS.length) {
			for (int i = 0; i < keyS.length; i++) {
				redisTemplate.delete(keyS[i]);
			}
		}
	}
}
