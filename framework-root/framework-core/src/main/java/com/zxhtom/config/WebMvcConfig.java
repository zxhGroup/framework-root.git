package com.zxhtom.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.zxhtom.CoreConstants;
import com.zxhtom.enums.UniversalEnumConverterFactory;
import com.zxhtom.model.DruidProperties;
import com.zxhtom.model.GeneratorProperties;
import com.zxhtom.web.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Web配置类
 */
@Configuration
@Import({FreemarkConfig.class,KaptchaConfig.class})
@ComponentScan(basePackages = "com.zxhtom.**.controller")
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	private Logger logger = LogManager.getLogger(WebMvcConfig.class);

	@Autowired
	private DruidProperties druidProperties;

	// 配置JSP视图解析器
//	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/");
		resolver.setSuffix(".html");
		resolver.setExposeContextBeansAsAttributes(true);
		return resolver;
	}

	@Bean
	public ViewResolver setSharedVariable(FreeMarkerViewResolver resolver) {
		resolver.setPrefix("/");
		resolver.setSuffix(".html");
		resolver.setCache(false);
		resolver.setRequestContextAttribute("request"); // 为模板调用时，调用request对象的变量名</span>
		resolver.setOrder(0);
		resolver.setExposeRequestAttributes(true);
		resolver.setExposeSessionAttributes(true);
		return resolver;
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/")
		.addResourceLocations("classpath:/statics/")
		.addResourceLocations("classpath:/templates/");
		super.addResourceHandlers(registry);
		// registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	// 注册全局异常处理器
	@Bean
	public ControllerExceptionHandler controllerExceptionHandler() {
		return new ControllerExceptionHandler();
	}

	// 注册返回数据处理器
	@Bean
	public ResponseBodyHandler responseBodyHandler() {
		return new ResponseBodyHandler();
	}


	// 定制消息转换器, 控制对象序列化格式
	/*@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.clear();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Arrays.asList(new MediaType("application", "json", Charset.forName("UTF-8")),
				new MediaType("application", "x-www-form-urlencoded", Charset.forName("UTF-8")),
				new MediaType("text", "json", Charset.forName("UTF-8")),
				new MediaType("text", "plain", Charset.forName("UTF-8")),
				new MediaType("text", "html", Charset.forName("UTF-8"))));
		ObjectMapper objectMapper = new ObjectMapper();
		// 对象序列化时, 值为null的属性不序列化 这里设置会出现前台传递的json 后台无法解析错误 切记
//		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		// 反序列化对象时, 忽略对象没有的属性
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		// 日期默认格式
		objectMapper.setDateFormat(new SimpleDateFormat(CoreConstants.DATETIME_FORMAT));
		converter.setObjectMapper(objectMapper);
		converters.add(converter);
	}*/

	 /**
     * 表单数据转换器
     */
    @Bean
    public AllEncompassingFormHttpMessageConverter getFormHttpMessageConverter() {
        return new AllEncompassingFormHttpMessageConverter();
    }

    /**
     * Json数据转换器
     */
    @Bean
    public MappingJackson2HttpMessageConverter getJsonHttpMessageConverter() {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        jsonConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED,MediaType.APPLICATION_JSON_UTF8, MediaType.TEXT_PLAIN));
        jsonConverter.setObjectMapper(CoreConstants.OBJECT_MAPPER);
        return jsonConverter;
    }

    /**
     * 定制消息转换器, 控制对象序列化格式
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.clear();
        converters.add(getFormHttpMessageConverter());
        converters.add(getJsonHttpMessageConverter());
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new UniversalEnumConverterFactory());
        registry.addFormatter(new DateFormatter(CoreConstants.DATETIME_FORMAT));
        registry.addFormatterForFieldType(Date.class,new DateFormatter(CoreConstants.DATETIME_FORMAT));
    }

    /**
     * 配置body数据格式, 默认json
     */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false).favorParameter(false).ignoreAcceptHeader(false).defaultContentType(MediaType.APPLICATION_JSON_UTF8)
                .mediaType(MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_PLAIN)
                .mediaType(MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON)
                .mediaType(MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_JSON_UTF8)
                .mediaType(MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_FORM_URLENCODED)
                .mediaType(MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_FORM_DATA);
    }

    /**
     * 配置路径匹配参数
     */
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseRegisteredSuffixPatternMatch(false).setUseSuffixPatternMatch(false).setUseTrailingSlashMatch(true);
    }
    
	// Cross Origin Resource Sharing允许跨域访问
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*")
				.allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "TRACE", "OPTIONS", "CONNECT")
				.allowedHeaders("Content-Type", "Cookie", "x-requested-with", "X-Token", "Access-Control-Allow-Origin")
				.allowCredentials(true).maxAge(86400);
	}

	// 注册druid sql分析servlet
	@Bean
	public ServletRegistrationBean druidServlet() {
		ServletRegistrationBean servletBean = new ServletRegistrationBean();
		String username = "admin";
		String password = "admin";
		boolean enable=druidProperties.isEnable();
		servletBean.setServlet(new StatViewServlet());
		servletBean.addUrlMappings("/druid/*");
		// 获取配置文件中的用户名和密码 没有配置则注入默认用户名和密码
		if (StringUtils.isNoneEmpty(druidProperties.getUsername())
				&& StringUtils.isNoneEmpty(druidProperties.getPassword())) {
			username = druidProperties.getUsername();
			password = druidProperties.getPassword();
		}
		if(enable){
			servletBean.addInitParameter("loginUsername", username);
			servletBean.addInitParameter("loginPassword", password);
		}
		// 白名单
		servletBean.addInitParameter("allow", "");
		// 黑名单
		servletBean.addInitParameter("deny", "");
		// 禁用html页面行的reset all功能
		servletBean.addInitParameter("resetEnable", "false");
		return servletBean;
		// 可以完全由下面一段替换
		// return new ServletRegistrationBean(new StatViewServlet(),
		// "/druid/*");
	}
	@Bean
	public WebStatFilter getWebStatFilter(){
		WebStatFilter webStatFilter = new WebStatFilter();
		webStatFilter.setProfileEnable(true);
		webStatFilter.setSessionStatEnable(true);
		return webStatFilter;
	}
	// 注册druid sql分析filter
	@Bean
	public FilterRegistrationBean druidFilter(WebStatFilter webStatFilter) {
		FilterRegistrationBean filterBean = new FilterRegistrationBean();
		filterBean.setFilter(webStatFilter);
		filterBean.setEnabled(true);
		filterBean.addUrlPatterns("/*");
		filterBean.addInitParameter("exclusions", "*.js,*.pdf,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return filterBean;
	}

	@Bean
	public FilterRegistrationBean validRoleFilter() {
		FilterRegistrationBean filterBean = new FilterRegistrationBean();
		filterBean.setFilter(new ValidRoleFilter());
		filterBean.addUrlPatterns("/*");
		return filterBean;
	}
	/*
	 * @Bean public FilterRegistrationBean getInitFilter(){
	 * FilterRegistrationBean registration = new FilterRegistrationBean();
	 * registration.setFilter(new InitFilter());
	 * registration.addUrlPatterns("/*"); return registration; }
	 */
	/**
	 * 配置过滤器
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean someFilterRegistration(GeneratorProperties properties) {
		/** 注册过滤器之前获取所有的请求地址。用于分析出项目地址 */
		/* 此段注释目的是根据请求地址随机分析出项目名，现在已经在application.yml中指定
		String requestURI = AnnoManageUtil.getControllerMapping("com.zxhtom");
		String regex = "(/[^/]*)";
		Pattern compile = Pattern.compile(regex);
		Matcher matcher = compile.matcher(requestURI);
		String prefix = "";
		while (matcher.find()) {
			prefix = matcher.group(1);
			break;
		}*/
		String prefix = "/"+properties.getModuleName();
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setOrder(Integer.MAX_VALUE);
		registration.setFilter(new UrlFilter(prefix));
		registration.addUrlPatterns(prefix);
		registration.addUrlPatterns(prefix + "/");
		//registration.addUrlPatterns("/druid/*");
		registration.setName("urlFilter");
		return registration;
	}

	// 注册拦截器, 注入CurrentRequestService
	@Bean
	public CurrentRequestInterceptor currentRequestInterceptor() {
		return new CurrentRequestInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		registry.addInterceptor(currentRequestInterceptor()).addPathPatterns("/**");
	}

	private class zxhHandler{

	}
}
