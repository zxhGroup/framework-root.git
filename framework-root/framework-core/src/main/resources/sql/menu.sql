-- 接口菜单
INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(1,'辅助管理','interfaceManager','interface/manage',-1,1,0,'fa fa-cart-arrow-down');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(101,'swagger','swaggerManager','swagger.html',-1,1,1,'fa fa-user-secret');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(102,'druid','druidManager','druid',-1,1,1,'fa fa-chevron-circle-up');

INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(2,'系统管理','systemManager','system/manage',-1,2,0,'fa fa-star-half-empty');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(201,'用户管理','userManager','modules/framework_common/sysuser.html',-1,2,1,'fa fa-frown-o');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(202,'角色管理','roleManager','modules/framework_common/sysrole.html',-1,2,1,'fa fa-keyboard-o');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(203,'权限管理','permissionManager','modules/framework_common/syspermission.html',-1,2,1,'fa fa-flag-checkered');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(204,'用户角色管理','userRoleManager','modules/framework_common/sysuserrole.html',-1,2,1,'fa fa-location-arrow');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(205,'角色权限管理','userManager','modules/framework_common/sysrolepermission.html',-1,2,1,'fa fa-cloud-upload');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(206,'部门管理','deptManager','modules/framework_common/sysdept.html',-1,2,1,'fa fa-bell-o');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(207,'模块管理','moduleManager','modules/framework_common/sysmodule.html',-1,2,1,'fa fa-hand-o-right');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(208,'菜单管理','menuManager','modules/framework_common/sysmenu.html',-1,2,1,'fa fa-square-o');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(209,'用户部门管理','userDeptManager','modules/framework_common/sysuserdept.html',-1,2,1,'fa fa-github-square');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(210,'角色模块管理','roleModuleManager','modules/framework_common/sysrolemodule.html',-1,2,1,'fa fa-minus-circle');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(211,'角色菜单管理','roleMenuManager','modules/framework_common/sysrolemenu.html',-1,2,1,'fa fa-map-signs');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(212,'定时器管理','taskManager','modules/framework_common/systask.html',-1,2,1,'fa fa-black-tie');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(213,'定时器日志管理','tasklogManager','modules/framework_common/systasklog.html',-1,2,1,'fa fa-gg-circle');

INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(3,'代码管理','codeManager','code/manage',-1,3,0,'fa fa-chevron-up');
	INSERT INTO SYS_MENU(MENU_ID,MENU_NAME,MENU_CODE,MENU_URL,MODULE_ID,PARENT_MENU_ID,MENU_TYPE,ICON) VALUES(301,'自动生成','autoCode','modules/framework_generator/template.html',-1,3,1,'fa fa-thumbs-o-down');