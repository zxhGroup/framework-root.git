insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-101,'sys:select','系统查询接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-102,'sys:delete','系统删除接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-103,'sys:update','系统更新接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-104,'sys:insert','系统新增接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-111,'user:select','用户查询接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-112,'user:delete','用户删除接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-113,'user:update','用户更新接口');
insert into SYS_PERMISSION(PERMISSION_ID,PERMISSION_NAME,PERMISSION_REMARK) values(-114,'user:insert','用户新增接口');
