package com.zxhtom.shiro.service.impl;

import com.zxhtom.model.CustomUser;
import com.zxhtom.model.LoginUserInfo;
import com.zxhtom.model.RedisProperties;
import com.zxhtom.shiro.service.ClientService;
import com.zxhtom.shiro.service.OAuthService;
import com.zxhtom.vconstant.RedisKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-2-17
 * <p>Version: 1.0
 */
@Service
public class OAuthServiceImpl implements OAuthService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ClientService clientService;
    @Autowired
    private RedisProperties redisProperties;

    private HashOperations cache;
    @PostConstruct
    public void init() {
        cache = redisTemplate.opsForHash();
    }
    @Override
    public void addAuthCode(String authCode, Object username) {
        cache.put(RedisKey.SPRINGCACHE,authCode, username);
        //cache.
    }

    @Override
    public void addAccessToken(String accessToken, Object username) {
        cache.put(RedisKey.SPRINGCACHE,accessToken, username);
    }

    @Override
    public Object getByAuthCode(String authCode) {
        return cache.get(RedisKey.SPRINGCACHE,authCode);
    }

    @Override
    public Object getByAccessToken(String accessToken) {
        return cache.get(RedisKey.SPRINGCACHE,accessToken);
    }

    @Override
    public String getUsernameByAuthCode(String authCode) {
        LoginUserInfo user = (LoginUserInfo) cache.get(RedisKey.SPRINGCACHE,authCode);
        return user.getUserInfo().getUserName();
    }

    @Override
    public CustomUser getUsernameByAccessToken(String accessToken) {
        LoginUserInfo user = (LoginUserInfo) cache.get(RedisKey.SPRINGCACHE,accessToken);
        return user.getUserInfo();
    }

    @Override
    public boolean checkAuthCode(String authCode) {
        return cache.get(RedisKey.SPRINGCACHE,authCode) != null;
    }

    @Override
    public boolean checkAccessToken(String accessToken) {
        return cache.get(RedisKey.SPRINGCACHE,accessToken) != null;
    }

    @Override
    public boolean checkClientId(String clientId) {
        return clientService.findByClientId(clientId) != null;
    }

    @Override
    public boolean checkClientSecret(String clientSecret) {
        return clientService.findByClientSecret(clientSecret) != null;
    }

    @Override
    public long getExpireIn() {
        return 3600L;
    }
}
