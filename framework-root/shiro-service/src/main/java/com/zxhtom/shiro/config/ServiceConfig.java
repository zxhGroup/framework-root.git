package com.zxhtom.shiro.config;

import com.zxhtom.config.shiro.CustomRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

//@Configuration
public class ServiceConfig {

    //@Bean
    //@Primary
    public com.zxhtom.config.shiro.CustomRealm getCustomRealm(HashedCredentialsMatcher hashedCredentialsMatcher) {
        com.zxhtom.config.shiro.CustomRealm realm = new CustomServiceRealm();
        realm.setCredentialsMatcher(hashedCredentialsMatcher);
        return realm;
    }
}
