package com.zxhtom.shiro.mapper;


import com.zxhtom.shiro.model.Oauth2Client;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
public interface Oauth2ClientMapper extends Mapper<Oauth2Client>{

	/**
	 * 批量更新
	 * @param oauth2Clients
	 * @return
	 */
	Integer updateOauth2ClientBatch(List<Oauth2Client> oauth2Clients);

	/**
	 * 批量新增
	 * @param oauth2Clients
	 * @return
	 */
	Integer insertOauth2ClientBatch(List<Oauth2Client> oauth2Clients);

    Long selectIdByClientIdAndSecret(@Param("clientId") String clientId, @Param("clientSecret") String clientSecret);
}
