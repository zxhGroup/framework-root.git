package com.zxhtom.shiro.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.shiro.model.Oauth2Client;

public interface Oauth2ClientService {

	/**
	 * 根据主键查询oauth2_client信息
	 * @param id oauth2_client主键
	 * @return
	 */
	PagedResult<Oauth2Client> selectOauth2ClientsByPK(Long id,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除oauth2_client信息
	 * @param id oauth2_client主键
	 * @return
	 */
	Integer deleteOauth2ClientPK(Long id);

	/**
	 * 单条更新数据
	 * @param Oauth2Client
	 * @return
	 */
	Integer updateOauth2Client(Oauth2Client oauth2Client);

	/**
	 * 单条新增数据
	 * @param oauth2Client
	 * @return
	 */
	Integer insertOauth2Client(Oauth2Client oauth2Client);
	
	/**
	 * 批量根据主键删除oauth2_client信息
	 * @param ids oauth2_client主键
	 * @return
	 */
	Integer deleteOauth2ClientPKBatch(List<Long> ids);
	
	/**
	 * 批量更新数据
	 * @param oauth2Clients
	 * @return
	 */
	Integer updateOauth2ClientBatch(List<Oauth2Client> oauth2Clients);

	/**
	 * 批量新增数据
	 * @param Oauth2Clients
	 * @return
	 */
	Integer insertOauth2ClientBatch(List<Oauth2Client> oauth2Clients);
}
