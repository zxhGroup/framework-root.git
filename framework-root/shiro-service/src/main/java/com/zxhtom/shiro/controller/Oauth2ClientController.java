package com.zxhtom.shiro.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.shiro.model.Oauth2Client;
import com.zxhtom.shiro.service.Oauth2ClientService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/shiro/oauth2Client")
@Api(tags = "shiro oauth2Client", description = "oauth2_client信息表管理")
public class Oauth2ClientController {

	private Logger logger = LogManager.getLogger(Oauth2ClientController.class);
	@Autowired
	private Oauth2ClientService oauth2ClientService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有oauth2_client信息列表 传0表示查询所有")
	public PagedResult<Oauth2Client> selectOauth2ClientsByPK(@ApiParam(value = "oauth2_client主键", required = true) @PathVariable @NotNull Long id,
	@ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
	@ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return oauth2ClientService.selectOauth2ClientsByPK(id, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteOauth2ClientByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long id) {
		return oauth2ClientService.deleteOauth2ClientPK(id);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) Oauth2Client oauth2Client) {
		return oauth2ClientService.updateOauth2Client(oauth2Client);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody Oauth2Client oauth2Client) {
		return oauth2ClientService.insertOauth2Client(oauth2Client);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteOauth2ClientByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> ids) {
		return oauth2ClientService.deleteOauth2ClientPKBatch(ids);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<Oauth2Client> oauth2Clients) {
		return oauth2ClientService.updateOauth2ClientBatch(oauth2Clients);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<Oauth2Client> oauth2Clients) {
		return oauth2ClientService.insertOauth2ClientBatch(oauth2Clients);
	}
}
