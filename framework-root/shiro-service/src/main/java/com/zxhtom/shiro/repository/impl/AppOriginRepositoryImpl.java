package com.zxhtom.shiro.repository.impl;

import com.zxhtom.CoreConstants;
import com.zxhtom.core.repository.AppOriginRepository;
import com.zxhtom.framework_common.mapper.CustomMapper;
import com.zxhtom.framework_common.mapper.CustomPermissionMapper;
import com.zxhtom.framework_common.mapper.CustomRoleMapper;
import com.zxhtom.framework_common.mapper.MenuMapper;
import com.zxhtom.model.CustomRole;
import com.zxhtom.model.GeneratorProperties;
import com.zxhtom.model.Menu;
import com.zxhtom.model.Module;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Repository
@Primary
public class AppOriginRepositoryImpl implements AppOriginRepository {

    @SuppressWarnings("rawtypes")
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private GeneratorProperties properties;

    @Autowired
    private CustomRoleMapper customRoleMapper;

    @Autowired
    private CustomPermissionMapper customPermissionMapper;

    @Autowired
    private CustomMapper customMapper;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map<Long, Long> selectVistualMenu() {
        Map<Long, Long> vistualMenuMap = new HashMap<Long, Long>();
        Map<Long, Long> factMenuMap = new HashMap<Long, Long>();
        HashOperations opsForHash = redisTemplate.opsForHash();
        if (redisTemplate.hasKey(CoreConstants.MENUPRE)) {
            return (Map<Long, Long>) opsForHash.get(CoreConstants.MENUPRE, CoreConstants.VISTUALMENUKEY);
        }
        Example example = new Example(Menu.class, false, false);
        example.createCriteria().andEqualTo("moduleId", properties.getModuleName());
        List<Menu> menuList = menuMapper.selectByExample(example);
        for (Menu menu : menuList) {
            Long id = IdUtil.getInstance().getId();
            factMenuMap.put(id, menu.getMenuId());
            vistualMenuMap.put(menu.getMenuId(), id);
        }
        if (factMenuMap.size() != 0) {
            opsForHash.put(CoreConstants.MENUPRE, CoreConstants.FACTMENUKEY, factMenuMap);
            opsForHash.put(CoreConstants.MENUPRE, CoreConstants.VISTUALMENUKEY, vistualMenuMap);
        }
        redisTemplate.expire(CoreConstants.MENUPRE, 30, TimeUnit.DAYS);
        return vistualMenuMap;
    }

    @Override
    public List<Module> selectAllModules() {
        return customMapper.selectAllModules();
    }

    @Override
    public List<CustomRole> selectRolesByModuleCode(String moduleName,String clientId,String clientSecret) {
        return customRoleMapper.selectRolesByModuleCode(moduleName,clientId,clientSecret);
    }
}
