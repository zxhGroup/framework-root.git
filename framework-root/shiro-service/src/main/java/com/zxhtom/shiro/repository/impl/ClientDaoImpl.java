package com.zxhtom.shiro.repository.impl;

import com.zxhtom.shiro.model.Client;
import com.zxhtom.shiro.repository.ClientDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
@Repository
public class ClientDaoImpl implements ClientDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public Client createClient(final Client client) {
        final String sql = "insert into OAUTH2_CLIENT(CLIENT_NAME, CLIENT_ID, CLIENT_SECRET) values(?,?,?)";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement psst = connection.prepareStatement(sql, new String[]{"id"});
                int count = 1;
                psst.setString(count++, client.getClientName());
                psst.setString(count++, client.getClientId());
                psst.setString(count++, client.getClientSecret());
                return psst;
            }
        }, keyHolder);

        client.setId(keyHolder.getKey().longValue());
        return client;
    }

    public Client updateClient(Client client) {
        String sql = "update OAUTH2_CLIENT SET CLIENT_NAME=?, CLIENT_ID=?, CLIENT_SECRET=? where id=?";
        jdbcTemplate.update(
                sql,
                client.getClientName(), client.getClientId(), client.getClientSecret(), client.getId());
        return client;
    }

    public void deleteClient(Long clientId) {
        String sql = "delete from OAUTH2_CLIENT where id=?";
        jdbcTemplate.update(sql, clientId);
    }

    @Override
    public Client findOne(Long clientId) {
        String sql = "select ID, CLIENT_NAME, CLIENT_ID, CLIENT_SECRET FROM OAUTH2_CLIENT where id=?";
        List<Client> clientList = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Client.class), clientId);
        if(clientList.size() == 0) {
            return null;
        }
        return clientList.get(0);
    }

    @Override
    public List<Client> findAll() {
        String sql = "select ID, CLIENT_NAME, CLIENT_ID, CLIENT_SECRET FROM OAUTH2_CLIENT";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Client.class));
    }


    @Override
    public Client findByClientId(String clientId) {
        String sql = "select ID, CLIENT_NAME, CLIENT_ID, CLIENT_SECRET FROM OAUTH2_CLIENT WHERE CLIENT_ID=?";
        List<Client> clientList = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Client.class), clientId);
        if(clientList.size() == 0) {
            return null;
        }
        return clientList.get(0);
    }


    @Override
    public Client findByClientSecret(String clientSecret) {
        String sql = "select id, client_name, client_id, client_secret from OAUTH2_CLIENT where client_secret=?";
        List<Client> clientList = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Client.class), clientSecret);
        if(clientList.size() == 0) {
            return null;
        }
        return clientList.get(0);
    }
}
