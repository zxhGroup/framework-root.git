package com.zxhtom.shiro.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.shiro.model.Oauth2Client;
import com.zxhtom.shiro.repository.Oauth2ClientRepository;
import com.zxhtom.shiro.service.Oauth2ClientService;

@Service
public class Oauth2ClientServiceImpl implements Oauth2ClientService {

	@Autowired
	private Oauth2ClientRepository oauth2ClientRepository;

	@Override
	public PagedResult<Oauth2Client> selectOauth2ClientsByPK(Long id,Integer pageNumber, Integer pageSize) {
		PagedResult<Oauth2Client> result = new PagedResult<>();
		if(0==id){
			//查询所有
			id=null;
		}
		result = oauth2ClientRepository.selectOauth2ClientsByPK(id, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteOauth2ClientPK(Long id) {
		if(0==id){
			//删除所有
			id=null;
		}
		return oauth2ClientRepository.deleteOauth2ClientsByPK(id);
	}

	@Override
	public Integer updateOauth2Client(Oauth2Client oauth2Client) {
		return oauth2ClientRepository.updateOauth2Client(oauth2Client);
	}

	@Override
	public Integer insertOauth2Client(Oauth2Client oauth2Client) {
		oauth2Client.setId(Long.valueOf(IdUtil.getInstance().getId()));
		oauth2Client.setClientId(UUID.randomUUID().toString());
		oauth2Client.setClientSecret(UUID.randomUUID().toString());
		return oauth2ClientRepository.insertOauth2Client(oauth2Client);
	}
	
	@Override
	public Integer deleteOauth2ClientPKBatch(List<Long> ids){
		for(Long id : ids){
			oauth2ClientRepository.deleteOauth2ClientsByPK(id);
		}
		return 1;
	}
	
	@Override
	public Integer updateOauth2ClientBatch(List<Oauth2Client> oauth2Clients) {
		return oauth2ClientRepository.updateOauth2ClientBatch(oauth2Clients);
	}

	@Override
	public Integer insertOauth2ClientBatch(List<Oauth2Client> oauth2Clients) {
		for(Oauth2Client oauth2Client : oauth2Clients){
		oauth2Client.setId(Long.valueOf(IdUtil.getInstance().getId()));
		}
		return oauth2ClientRepository.insertOauth2ClientBatch(oauth2Clients);
	}

}
