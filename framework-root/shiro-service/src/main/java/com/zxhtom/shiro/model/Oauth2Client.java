package com.zxhtom.shiro.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Id;
import javax.persistence.Table;

@ApiModel(description = "OAUTH2_CLIENT表")
@Table(name = "OAUTH2_CLIENT")
public class Oauth2Client {

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	@Id
	private Long id;
	/**
	 * client_name
	 */
	@ApiModelProperty(value = "client_name")
	private String clientName;
	/**
	 * client_id
	 */
	@ApiModelProperty(value = "client_id")
	private String clientId;
	/**
	 * client_secret
	 */
	@ApiModelProperty(value = "client_secret")
	private String clientSecret;
	
	/**
	 * 设置：id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：client_name
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	/**
	 * 获取：client_name
	 */
	public String getClientName() {
		return clientName;
	}
	/**
	 * 设置：client_id
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * 获取：client_id
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * 设置：client_secret
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	/**
	 * 获取：client_secret
	 */
	public String getClientSecret() {
		return clientSecret;
	}

	@Override
	public String toString() {
		return "Oauth2Client [id=" + id+",clientName=" + clientName + ",clientId=" + clientId + ",clientSecret=" + clientSecret + "]";
	}

}
