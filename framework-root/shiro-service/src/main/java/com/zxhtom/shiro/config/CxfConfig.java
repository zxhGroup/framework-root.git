package com.zxhtom.shiro.config;

import com.zxhtom.core.repository.AppOriginRepository;
import com.zxhtom.core.repository.CustomOriginRepository;
import com.zxhtom.shiro.service.OAuthService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class CxfConfig {
    @Autowired
    private OAuthService oAuthService;
    @Autowired
    private CustomOriginRepository customOriginRepository;
    @Autowired
    private AppOriginRepository appOriginRepository;
    //默认servlet路径/*,如果覆写则按照自己定义的来
    @Bean
    public ServletRegistrationBean dispatcherServlet1() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new CXFServlet(), "/services/*");
        //名字和顺序必须指定，名字不指定会覆盖spring 默认的dispatcher导致我们的http服务不可用
        servletRegistrationBean.setName("services");
        servletRegistrationBean.setLoadOnStartup(1);
        return servletRegistrationBean;
    }
    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    //终端路径
    @Bean
    public Endpoint customOriginRepositoryEndpoint() {
        /*Set<Class<?>> annotationType = AnnoManageUtil.getAnnotationType(WebService.class, PackageName.BASKPACKAGE);
        if (CollectionUtils.isNotEmpty(annotationType)) {
            for (Class<?> aClass : annotationType) {
                Object bean = ContextUtil.getApplicationContext().getBean(aClass);
                EndpointImpl endpoint = new EndpointImpl(springBus(), customOriginRepository);
                //endpoint.getInInterceptors().add(new AuthInterceptor());//添加校验拦截器
                endpoint.publish("/"+aClass.getSimpleName());
            }
        }*/
        EndpointImpl endpoint = new EndpointImpl(springBus(), customOriginRepository);
        endpoint.getInInterceptors().add(new AuthInterceptor(oAuthService));//添加校验拦截器
        endpoint.publish("/user");
        return endpoint;
    }
    @Bean
    public Endpoint appOriginRepositoryEndpoint() {
        /*Set<Class<?>> annotationType = AnnoManageUtil.getAnnotationType(WebService.class, PackageName.BASKPACKAGE);
        if (CollectionUtils.isNotEmpty(annotationType)) {
            for (Class<?> aClass : annotationType) {
                Object bean = ContextUtil.getApplicationContext().getBean(aClass);
                EndpointImpl endpoint = new EndpointImpl(springBus(), customOriginRepository);
                //endpoint.getInInterceptors().add(new AuthInterceptor());//添加校验拦截器
                endpoint.publish("/"+aClass.getSimpleName());
            }
        }*/
        EndpointImpl endpoint = new EndpointImpl(springBus(), appOriginRepository);
        endpoint.publish("/app");
        return endpoint;
    }
}
