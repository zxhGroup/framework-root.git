package com.zxhtom.shiro.config;

import com.zxhtom.shiro.service.OAuthService;
import com.zxhtom.vconstant.PackageName;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.service.invoker.MethodDispatcher;
import org.apache.cxf.service.model.BindingOperationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.soap.SOAPException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * 定义服务端用户名密码校验的拦截器
 * Created by sky on 2018/2/27.
 */
public class AuthInterceptor extends AbstractPhaseInterceptor<SoapMessage> {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    private OAuthService oAuthService;

    public AuthInterceptor(OAuthService oAuthService) {
        //定义在哪个阶段进行拦截
        super(Phase.PRE_PROTOCOL);
        this.oAuthService=oAuthService;
    }

    @Override
    public void handleMessage(SoapMessage soapMessage) throws Fault {

        List<Header> headers = null;
        String username=null;
        String password=null;
        String accessToken=null;
        try {
           headers = soapMessage.getHeaders();
        } catch (Exception e) {
            logger.error("getSOAPHeader error: {}",e.getMessage(),e);
        }

        if (headers == null) {
            throw new Fault(new IllegalArgumentException("找不到Header，无法验证用户信息"));
        }
        //获取用户名,密码
        for (Header header : headers) {
            SoapHeader soapHeader = (SoapHeader) header;
            Element e = (Element) soapHeader.getObject();
            NodeList usernameNode = e.getElementsByTagName(PackageName.USERNAME);
            NodeList pwdNode = e.getElementsByTagName(PackageName.PASSWORD);
            //NodeList accessNode = e.getElementsByTagName(PackageName.ACCESSTOKEN);
             username=usernameNode.item(0).getTextContent();
             password=pwdNode.item(0).getTextContent();
            // accessToken=accessNode.item(0).getTextContent();
            if( StringUtils.isEmpty(username)|| StringUtils.isEmpty(password)){
                throw new Fault(new IllegalArgumentException("用户信息为空"));
            }
        }
        //校验用户名密码
        if((username == null || password == null)){
            SOAPException soapExc = new SOAPException("认证失败");
            logger.debug("用户认证信息错误");
            throw new Fault(soapExc);
        }
        //验证code是否是服务服务端发布的
        /*if (!oAuthService.checkAccessToken(accessToken)) {
            SOAPException soapExc = new SOAPException("code失效");
            logger.debug("code失效");
            throw new Fault(soapExc);
        }*/
    }

}
