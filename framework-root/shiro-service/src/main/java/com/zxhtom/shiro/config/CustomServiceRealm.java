package com.zxhtom.shiro.config;

import com.zxhtom.config.shiro.CustomRealm;
import com.zxhtom.config.shiro.RedisSessionDao;
import com.zxhtom.core.repository.CustomRepository;
import com.zxhtom.model.Client;
import com.zxhtom.model.CustomUser;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.vconstant.PackageName;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@Primary
public class CustomServiceRealm extends CustomRealm {

	@Autowired
	private RedisSessionDao sessionDao;
	@Autowired
	private CustomRepository customRepository;
	@Autowired
	private Client client;

	{
		super.setName("customRealm");
	}
	/**
	 * 获取角色
	 */
	@Override
	public AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		//获取用户名
		Long userId = ((CustomUser) principals.getPrimaryPrincipal()).getUserId();
		Set<String> roles = selectRoleNamesByUserId(userId);
		Set<String> premission=selectPremissionNamesByUserId(userId);
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		authorizationInfo.setRoles(roles);
		authorizationInfo.setStringPermissions(premission);
		return authorizationInfo;
	}

	/**
	 * 认证用户方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		//从主体传过来的认证信息中获取用户名
		CustomUser customUser = (CustomUser) token.getPrincipal();
        customUser = customRepository.selectUserInfoByUserName(customUser.getUserName());
		//通过用户名到数据库中获取凭证
		String password=selectPasswordByUserName(customUser.getUserName());
		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(customUser,password,"customRealm");
		//设置盐
		authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes("zxh"));
		SecurityUtils.getSubject().getSession().setAttribute(PackageName.REALM, PackageName.LOCAL);
		return authenticationInfo;
	}


	private String selectPasswordByUserName(String username) {
		CustomUser user = customRepository.selectUserByUserName(username);
		if(null!=user){
			return user.getPassword();
		}
		return null;
	}

	private Set<String> selectPremissionNamesByUserId(Long userId) {
		Set<String> set = customRepository.selectPremissionNamesByUserId(userId,client.getAppKey(),client.getAppSecret());
		return set;
	}

	private Set<String> selectRoleNamesByUserId(Long userId) {
		System.out.println("get from database");
		Set<String> set = customRepository.selectRoleNamesByUserId(userId);
		return set;
	}
	
	
	public static void main(String[] args) {
		System.out.println(IdUtil.getInstance().getId());
		Md5Hash md5Hash= new Md5Hash("vistor","zxh");
		System.out.println(md5Hash.toString());
	}

}
