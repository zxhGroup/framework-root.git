package com.zxhtom.shiro.repository;

import com.zxhtom.PagedResult;
import com.zxhtom.shiro.model.Oauth2Client;

import java.util.List;

public interface Oauth2ClientRepository {

	/**
	 * 根据主键查询oauth2_client信息
	 * 
	 * @param id
	 *            oauth2_client主键
	 * @return
	 */
	PagedResult<Oauth2Client> selectOauth2ClientsByPK(Long id,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除oauth2_client信息
	 * @param id
	 * @return
	 */
	Integer deleteOauth2ClientsByPK(Long id);
	
	/**
	 * 单条更新oauth2_client信息
	 * @param oauth2Client
	 * @return
	 */
	Integer updateOauth2Client(Oauth2Client oauth2Client);

	/**
	 * 单条新增信息
	 * @param oauth2Client
	 * @return
	 */
	Integer insertOauth2Client(Oauth2Client oauth2Client);
	
	/**
	 * 批量更新oauth2_client信息
	 * @param oauth2Clients
	 * @return
	 */
	Integer updateOauth2ClientBatch(List<Oauth2Client> oauth2Clients);

	/**
	 * 批量新增信息
	 * @param oauth2Clients
	 * @return
	 */
	Integer insertOauth2ClientBatch(List<Oauth2Client> oauth2Clients);

	/***
	 *
	 * @Method : selectIdByClientIdAndSecret
	 * @Description : 通过商户秘钥查询商户id
	 * @param clientId : 
	 * @param clientSecret : 
	 * @return : java.lang.Long
	 * @author : zhangxinhua
	 * @CreateDate : 2019-05-05 14:24:42
	 *
	 */
	Long selectIdByClientIdAndSecret(String clientId, String clientSecret);
}
