package com.zxhtom.shiro.repository.impl;

import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.shiro.mapper.Oauth2ClientMapper;
import com.zxhtom.shiro.model.Oauth2Client;
import com.zxhtom.shiro.repository.Oauth2ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Repository
public class Oauth2ClientRepositoryImpl implements Oauth2ClientRepository{

	@Autowired
	private Oauth2ClientMapper oauth2ClientMapper;
	@Override
	public PagedResult<Oauth2Client> selectOauth2ClientsByPK(Long id,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(Oauth2Client.class,false,false);
		example.createCriteria().andEqualTo("id", id);
		return new PagedResult<>(oauth2ClientMapper.selectByExample(example));
	}
	@Override
	public Integer deleteOauth2ClientsByPK(Long id) {
		Example example = new Example(Oauth2Client.class,false,false);
		example.createCriteria().andEqualTo("id", id);
		return oauth2ClientMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateOauth2Client(Oauth2Client oauth2Client) {
		return oauth2ClientMapper.updateByPrimaryKeySelective(oauth2Client);
	}
	@Override
	public Integer insertOauth2Client(Oauth2Client oauth2Client) {
		return oauth2ClientMapper.insert(oauth2Client);
	}
	
	@Override
	public Integer updateOauth2ClientBatch(List<Oauth2Client> oauth2Clients) {
		return oauth2ClientMapper.updateOauth2ClientBatch(oauth2Clients);
	}
	@Override
	public Integer insertOauth2ClientBatch(List<Oauth2Client> oauth2Clients) {
		return oauth2ClientMapper.insertOauth2ClientBatch(oauth2Clients);
	}

    @Override
    public Long selectIdByClientIdAndSecret(String clientId, String clientSecret) {
		return oauth2ClientMapper.selectIdByClientIdAndSecret(clientId, clientSecret);
    }
}
