package com.zxhtom.shiro;

import com.zxhtom.config.*;

import com.zxhtom.config.aspect.DataRepositoryAspect;
import com.zxhtom.config.shiro.ShiroConfig;
import com.zxhtom.shiro.config.CxfConfig;
import com.zxhtom.shiro.config.ServiceConfig;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * 入口类, 扫描并注入其他配置类和服务
 */
@SpringBootApplication
@ComponentScan(value = "com.zxhtom")
@Import({FreemarkConfig.class, DataRepositoryAspect.class, ServiceConfig.class,ShiroConfig.class,QuickStartConfig.class, ShiroConfig.class,SpringfoxConfig.class,RedisConfig.class,MybatisConfig.class})
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return configureApplication(builder);
    }

    public static void main(String[] args) throws Exception {
        configureApplication(new SpringApplicationBuilder()).run(args);
        //Server.start();
    }

    private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
        return builder.sources(Application.class).bannerMode(Banner.Mode.OFF).web(true);
    }
}
