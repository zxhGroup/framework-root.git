package com.zxhtom.shiro.service;

import com.zxhtom.model.CustomUser;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-2-17
 * <p>Version: 1.0
 */
public interface OAuthService {

    //添加 auth code
    public void addAuthCode(String authCode, Object username);
    //添加 access token
    public void addAccessToken(String accessToken, Object username);

    public Object getByAuthCode(String authCode);
    public Object getByAccessToken(String accessToken);
    //验证auth code是否有效
    boolean checkAuthCode(String authCode);
    //验证access token是否有效
    boolean checkAccessToken(String accessToken);

    String getUsernameByAuthCode(String authCode);
    CustomUser getUsernameByAccessToken(String accessToken);


    //auth code / access token 过期时间
    long getExpireIn();


    public boolean checkClientId(String clientId);

    public boolean checkClientSecret(String clientSecret);


}
