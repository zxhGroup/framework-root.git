package com.zxhtom.framework_task.quartz;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;

import com.google.common.base.Strings;
import com.zxhtom.enums.Bool;
import com.zxhtom.framework_task.model.Task;
import com.zxhtom.framework_task.repository.TaskRepository;
import com.zxhtom.web.CurrentRequestService;

/**
 * 刷新数据库中定义的定时任务
 */
@DisallowConcurrentExecution
public class DatabaseTaskRefreshJob extends AbstractJob {
    private static final Logger logger = LogManager.getLogger(DatabaseTaskRefreshJob.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private CurrentRequestService currentRequestService;

    @Autowired
    @Qualifier("databaseScheduler")
    private Scheduler databaseScheduler;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        //记录刷新数据库定时任务时的错误信息
        StringBuilder result = new StringBuilder();
        try {
            //查询数据库中所有定时任务
            List<Task> dbTasks = taskRepository.selectList(1, 0, null, null, null, null).getDatas();

            //遍历定时任务调度器的定时任务列表
            //如果不在数据库定时任务列表中, 说明数据库定时任务被删除或修改了, 就把调度器定时任务删除
            //如果在数据库定时任务列表中, 说明数据库定时任务没有被改动, 则把数据库定时任务删除
            for (JobKey jobKey : databaseScheduler.getJobKeys(GroupMatcher.anyJobGroup())) {
                JobDetail jobDetail = databaseScheduler.getJobDetail(jobKey);

                //排除DatabaseTaskRefreshJob
                if (DatabaseTaskRefreshJob.class.isAssignableFrom(jobDetail.getJobClass())) {
                    continue;
                }

                //获取定时任务信息
                Task task = (Task) jobDetail.getJobDataMap().get(TaskConstants.TASK_OBJECT);
                if (task != null) {
                    int index = dbTasks.indexOf(task);
                    if (index >= 0) {
                        //如果在数据库定时任务列表中, 说明数据库定时任务没有被改动, 则把数据库定时任务删除
                        dbTasks.remove(index);
                    } else {
                        //如果不在数据库定时任务列表中, 说明数据库定时任务被删除或修改了, 就把调度器定时任务删除
                        if (databaseScheduler.checkExists(jobKey)) {
                            synchronized (this) {
                                if (databaseScheduler.checkExists(jobKey)) {
                                    databaseScheduler.deleteJob(jobKey);
                                    logger.debug("移除数据库定时任务, taskName: {}, jobClass: {}, cronExpression: {}",
                                            task.getTaskName(), task.getJobClass(), task.getCronExpression());
                                }
                            }
                        }
                    }
                }
            }

            //数据库定时任务列表中剩下的定时任务, 就是新增或修改的定时任务, 加到定时任务调试器中
            String localIp = currentRequestService.getLocalIp();
            for (Task task : dbTasks) {
                //未启用
                if (task.getEnable() != Boolean.TRUE) {
                    continue;
                }

                //任务指定运行服务器IP
                if (StringUtils.hasLength(task.getRunIp()) && !task.getRunIp().equals(localIp)) {
                    continue;
                }
                try {
                	//检测该定时任务是否是本项目的定时任务
                    String jobClazz = task.getJobClass();
                    Class<?> forName = Class.forName(jobClazz);
				} catch (Exception e) {
					logger.warn("task is not this system ,so we can not do it");
					continue;
				}
                //工作类名为空
                if (Strings.isNullOrEmpty(task.getJobClass())) {
                    String msg = String.format("数据库定时任务的工作类名(jobClass)不可为空, taskName: %s, jobClass: %s, cronExpression: %s",
                            task.getTaskName(), task.getJobClass(), task.getCronExpression());
                    logger.error(msg);
                    result.append(msg + System.lineSeparator());
                    continue;
                }

                //调度表达式为空
                if (Strings.isNullOrEmpty(task.getCronExpression())) {
                    String msg = String.format("数据库定时任务的调度表达式(cronExpression)不可为空, taskName: %s, jobClass: %s, cronExpression: %s",
                            task.getTaskName(), task.getJobClass(), task.getCronExpression());
                    logger.error(msg);
                    result.append(msg + System.lineSeparator());
                    continue;
                }

                //工作类不是AbstractJob子类
                Class<?> jobClass;
                try {
                    jobClass = Class.forName(task.getJobClass());
                    if (!AbstractJob.class.isAssignableFrom(jobClass)) {
                        String msg = String.format("数据库定时任务的工作类(jobClass)必须是AbstractJob的子类, taskName: %s, jobClass: %s, cronExpression: %s",
                                task.getTaskName(), task.getJobClass(), task.getCronExpression());
                        logger.error(msg);
                        result.append(msg + System.lineSeparator());
                        continue;
                    }
                } catch (Throwable e) {
                    String msg = String.format("数据库定时任务获取工作类(jobClass)类型信息出错, taskName: %s, jobClass: %s, cronExpression: %s",
                            task.getTaskName(), task.getJobClass(), task.getCronExpression());
                    logger.error(msg, e);
                    result.append(msg + System.lineSeparator());
                    result.append(ExceptionUtils.getStackTrace(e) + System.lineSeparator());
                    continue;
                }

                try {
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(TaskConstants.TASK_OBJECT, task); //定时任务对象
                    jobDataMap.put(TaskConstants.TASK_PARAM, task.getParam()); //定时任务参数

                    //定义JobDetail
                    String id = task.getTaskId().toString();
                    JobDetail jobDetail = newJob((Class<? extends Job>) jobClass)
                            .withIdentity(id)
                            .usingJobData(jobDataMap)
                            .build();
                    //定义Trigger
                    Trigger trigger = newTrigger()
                            .withIdentity(id)
                            .withSchedule(cronSchedule(task.getCronExpression()))
                            .build();
                    //加到调度器
                    if (!databaseScheduler.checkExists(jobDetail.getKey())) {
                        synchronized (this) {
                            if (!databaseScheduler.checkExists(jobDetail.getKey())) {
                                databaseScheduler.scheduleJob(jobDetail, trigger);
                                logger.debug("添加数据库定时任务, taskName: {}, jobClass: {}, cronExpression: {}",
                                        task.getTaskName(), task.getJobClass(), task.getCronExpression());

                                //定时任务立即执行
                                if (task.getImmediately() == Boolean.TRUE) {
                                    databaseScheduler.triggerJob(jobDetail.getKey());
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    String msg = String.format("将数据库定时任务加到调度器出错, taskName: %s, jobClass: %s, cronExpression: %s",
                            task.getTaskName(), task.getJobClass(), task.getCronExpression());
                    logger.error(msg, e);
                    result.append(msg + System.lineSeparator());
                    result.append(ExceptionUtils.getStackTrace(e) + System.lineSeparator());
                    continue;
                }
            }
        } catch (Exception e) {
            String msg = "刷新数据库定时任务列表出错!";
            logger.error(msg, e);
            result.append(msg + System.lineSeparator());
            result.append(ExceptionUtils.getStackTrace(e) + System.lineSeparator());
        }
        //刷新数据库定时任务列表过程的错误信息返回
        context.setResult(result.toString());
    }
}
