package com.zxhtom.framework_task.quartz;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

import com.google.common.base.Strings;
import com.zxhtom.utils.ClassUtil;
import com.zxhtom.utils.IdUtil;

/**
 * 启动注解定时任务调度器, 并添加注解定时任务
 */
public class AnnotationTaskStarter implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger logger = LogManager.getLogger(AnnotationTaskStarter.class);

    @Autowired
    private TaskConfiguration taskConfiguration;

    @Autowired
    @Qualifier("annotationScheduler")
    private Scheduler annotationScheduler;

    /**
     * 依据注解, 将工作类加到定时任务调试器
     *
     * @param scheduler 定时任务调试器
     * @param jobClass  工作类
     * @param schedule  工作类上的注解
     * @throws SchedulerException
     */
    private void addJob2Scheduler(Scheduler scheduler, Class<? extends Job> jobClass, Schedule schedule) {
        if (schedule.enable()) {
            if (Strings.isNullOrEmpty(schedule.cronExpression())) {
                logger.error("注解定时任务工作类: {} 没有定义调度表达式(cronExpression)", jobClass.getCanonicalName());
            } else {
                try {
                    //定义JobDetail
                    JobDetail jobDetail = newJob(jobClass)
                            .withIdentity(IdUtil.getInstance().getId().toString())
                            .usingJobData(TaskConstants.TASK_PARAM, schedule.param()) //参数
                            .usingJobData(TaskConstants.TASK_NAME, schedule.taskName()) //任务名称
                            .usingJobData(TaskConstants.CRON_EXPRESSION, schedule.cronExpression()) //调度表达式
                            .build();
                    //定义Trigger
                    Trigger trigger = newTrigger()
                            .withIdentity(IdUtil.getInstance().getId().toString())
                            .withSchedule(cronSchedule(schedule.cronExpression())) //注解中的调度表达式
                            .build();
                    //加到调度器
                    scheduler.scheduleJob(jobDetail, trigger);
                    //立即执行
                    if (schedule.immediately()) {
                        scheduler.triggerJob(jobDetail.getKey());
                    }
                } catch (Exception e) {
                    logger.error("将注解定时任务加到调度器出错, jobClass: {}, cronExpression: {}", jobClass.getCanonicalName(), schedule.cronExpression(), e);
                }
            }
        }
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            //启用注解定时任务
            if (taskConfiguration.isAnnotationTaskEnable()) {
                //启动注解定时任务调度器
                annotationScheduler.start();

                //添加所有注解定时任务到注解定时任务调试器
                for (String className : ClassUtil.getJsitsClasses()) {
                    Class<?> jobClass;
                    try {
                        jobClass = Class.forName(className);
                    } catch (Throwable e) {
                        continue; //跳过
                    }
                    //AbstractJob子类
                    if (AbstractJob.class.isAssignableFrom(jobClass)) {
                        //有定义Schedule注解
                        Schedule schedule = jobClass.getAnnotation(Schedule.class);
                        if (schedule != null) {
                            addJob2Scheduler(annotationScheduler, (Class<? extends Job>) jobClass, schedule);
                        }
                        //有定义Schedules注解
                        Schedules schedules = jobClass.getAnnotation(Schedules.class);
                        if (schedules != null) {
                            for (Schedule item : schedules.value()) {
                                addJob2Scheduler(annotationScheduler, (Class<? extends Job>) jobClass, item);
                            }
                        }
                    }
                }
                logger.debug("注解定时任务调度器启动成功");
            }
        } catch (Exception e) {
            logger.error("启动注解定时任务调度器出错!", e);
        }
    }
}
