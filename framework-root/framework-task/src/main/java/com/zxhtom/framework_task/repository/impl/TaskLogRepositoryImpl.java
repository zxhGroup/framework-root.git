package com.zxhtom.framework_task.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.github.pagehelper.PageHelper;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.mapper.TaskLogMapper;
import com.zxhtom.framework_task.model.TaskLog;
import com.zxhtom.framework_task.repository.TaskLogRepository;

import tk.mybatis.mapper.entity.Example;

/**
 * 定时任务日志Repository
 */
@Repository
public class TaskLogRepositoryImpl implements TaskLogRepository {
    @Autowired
    private TaskLogMapper taskLogMapper;

    /**
     * 新增定时任务日志
     * @param taskLog
     * @return
     */
    @Override
    public Integer insert(TaskLog taskLog) {
        return taskLogMapper.insertSelective(taskLog);
    }

    /**
     * 批量删除日志
     *
     * @param ids 要删除的日志Id列表
     * @return
     */
    @Override
    public Integer delete(List<Long> ids) {
        return taskLogMapper.deleteByIds(Joiner.on(",").skipNulls().join(ids));
    }

    /**
     * 按条件批量删除定时任务日志
     *
     * @param taskName     定时任务名称
     * @param jobClass     工作类名
     * @param startLogTime 日志起始时间
     * @param endLogTime   日志截止时间
     * @return
     */
    @Override
    public Integer delete(String taskName, String jobClass, Date startLogTime, Date endLogTime) {
        Example example = new Example(TaskLog.class);
        Example.Criteria criteria = example.createCriteria();
        if (!Strings.isNullOrEmpty(taskName)) {
            criteria.andEqualTo("taskName", taskName);
        }
        if (!Strings.isNullOrEmpty(jobClass)) {
            criteria.andEqualTo("jobClass", jobClass);
        }
        if (startLogTime != null) {
            criteria.andGreaterThanOrEqualTo("logTime", startLogTime);
        }
        if (endLogTime != null) {
            criteria.andLessThanOrEqualTo("logTime", endLogTime);
        }
        return taskLogMapper.deleteByExample(example);
    }

    /**
     * 分页查询定时任务日志
     *
     * @param pageNumber    页码
     * @param pageSize      每页笔数
     * @param orderProperty 排序字段
     * @param orderType     排序方式
     * @param taskName      定时任务名称
     * @param jobClass      工作类名
     * @param startLogTime  日志起始时间
     * @param endLogTime    日志截止时间
     * @return
     */
    @Override
    public PagedResult<TaskLog> selectList(Integer pageNumber, Integer pageSize, String orderProperty, OrderType orderType, String taskName, String jobClass, Date startLogTime, Date endLogTime) {
        Example example = new Example(TaskLog.class);
        Example.Criteria criteria = example.createCriteria();
        if (!Strings.isNullOrEmpty(taskName)) {
            criteria.andEqualTo("taskName", taskName);
        }
        if (!Strings.isNullOrEmpty(jobClass)) {
            criteria.andEqualTo("jobClass", jobClass);
        }
        if (startLogTime != null) {
            criteria.andGreaterThanOrEqualTo("logTime", startLogTime);
        }
        if (endLogTime != null) {
            criteria.andLessThanOrEqualTo("logTime", endLogTime);
        }
        if (!Strings.isNullOrEmpty(orderProperty)) {
            if (OrderType.DESC.equals(orderType)) {
                example.orderBy(orderProperty).desc();
            } else {
                example.orderBy(orderProperty).asc();
            }
        } else {
            example.orderBy("logTime").desc();
        }
        PageHelper.startPage(pageNumber, pageSize);
        return new PagedResult<>(taskLogMapper.selectByExample(example));
    }
}
