package com.zxhtom.framework_task.quartz;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

import com.zxhtom.utils.IdUtil;

/**
 * 启动数据库定时任务调度器
 */
public class DatabaseTaskStarter implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseTaskStarter.class);

    @Autowired
    private TaskConfiguration taskConfiguration;

    @Autowired
    @Qualifier("databaseScheduler")
    private Scheduler databaseScheduler;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            //启用数据库定时任务
            if (taskConfiguration.isDatabaseTaskEnable()) {
                //启动数据库定时任务调度器
                databaseScheduler.start();

                //把刷新数据库的定时任务加到调度器, 在定时任务中增删改数据库中定义的定时任务
                String id = IdUtil.getInstance().getId().toString();
                JobDetail jobDetail = newJob(DatabaseTaskRefreshJob.class)
                        .withIdentity(id)
                        .build();
                Trigger trigger = newTrigger()
                        .withIdentity(id)
                        //调度使用定时任务配置中的刷新秒数
                        .withSchedule(simpleSchedule().withIntervalInSeconds(taskConfiguration.getDatabaseTaskRefreshInterval()).repeatForever())
                        .build();
                //加到调度器
                databaseScheduler.scheduleJob(jobDetail, trigger);
                //触发任务立即执行
                databaseScheduler.triggerJob(jobDetail.getKey());
                logger.debug("数据库定时任务调度器启动成功");
            }
        } catch (Exception e) {
            logger.error("启动数据库定时任务调度器出错!", e);
        }
    }
}
