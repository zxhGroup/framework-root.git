package com.zxhtom.framework_task.quartz;

/**
 * 定时任务常量定义
 */
public final class TaskConstants {
    private TaskConstants() {
    }

    /**
     * 注解定时任务调度器名称
     */
    public static final String ANNOTATION_SCHEDULER = "ANNOTATION_SCHEDULER";

    /**
     * 数据库定时任务调度器名称
     */
    public static final String DATABASE_SCHEDULER = "DATABASE_SCHEDULER";

    /**
     * 注解定时任务监听器名称
     */
    public static final String ANNOTATION_TASK_LISTENER = "ANNOTATION_TASK_LISTENER";

    /**
     * 数据库定时任务监听器名称
     */
    public static final String DATABASE_TASK_LISTENER = "DATABASE_TASK_LISTENER";

    /**
     * 定时任务传入参数
     */
    public static final String TASK_PARAM = "TASK_PARAM";

    /**
     * 定时任务对象
     */
    public static final String TASK_OBJECT = "TASK_OBJECT";

    /**
     * 定时任务名称
     */
    public static final String TASK_NAME = "TASK_NAME";

    /**
     * 调度表达式
     */
    public static final String CRON_EXPRESSION = "CRON_EXPRESSION";
}
