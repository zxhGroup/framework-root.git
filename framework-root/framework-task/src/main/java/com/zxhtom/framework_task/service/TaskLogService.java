package com.zxhtom.framework_task.service;

import java.util.Date;
import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.model.TaskLog;

/**
 * 定时任务日志Service
 */
public interface TaskLogService {
	/**
	 * 新增定时任务日志
	 * @param taskLog
	 * @return
	 */
	TaskLog insert(TaskLog taskLog);

	/**
	 * 批量删除日志
	 *
	 * @param ids 要删除的日志Id列表
	 * @return
	 */
	void delete(List<Long> ids);

	/**
	 * 按条件批量删除定时任务日志
	 *
	 * @param taskName     定时任务名称
	 * @param jobClass     工作类名
	 * @param startLogTime 日志起始时间
	 * @param endLogTime   日志截止时间
	 * @return
	 */
	void delete(String taskName, String jobClass, Date startLogTime, Date endLogTime);

	/**
	 * 分页查询定时任务日志
	 *
	 * @param pageNumber    页码
	 * @param pageSize      每页笔数
	 * @param orderProperty 排序字段
	 * @param orderType     排序方式
	 * @param taskName      定时任务名称
	 * @param jobClass      工作类名
	 * @param startLogTime  日志起始时间
	 * @param endLogTime    日志截止时间
	 * @return
	 */
	PagedResult<TaskLog> selectList(Integer pageNumber, Integer pageSize, String orderProperty, OrderType orderType, String taskName, String jobClass, Date startLogTime, Date endLogTime);
}
