package com.zxhtom.framework_task.quartz;

import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.listeners.JobListenerSupport;

import com.zxhtom.CoreConstants;
import com.zxhtom.framework_task.enums.TaskState;

/**
 * 监听注解定时任务执行事件, 记录日志
 */
public class AnnotationTaskListener extends JobListenerSupport {
    private static final Logger logger = LogManager.getLogger(AnnotationTaskListener.class);

    @Override
    public String getName() {
        return TaskConstants.ANNOTATION_TASK_LISTENER;
    }

    /**
     * 开始执行任务
     *
     * @param context
     */
    @Override
    public void jobToBeExecuted(JobExecutionContext context) {
        if (logger.isDebugEnabled()) {
            String taskName = context.getMergedJobDataMap().getString(TaskConstants.TASK_NAME);
            String jobClass = context.getJobInstance().getClass().getCanonicalName();
            String cronExpression = context.getMergedJobDataMap().getString(TaskConstants.CRON_EXPRESSION);
            logger.debug("注解定时任务开始执行, taskName: {}, jobClass: {}, cronExpression: {}", taskName, jobClass, cronExpression);
        }
    }

    /**
     * 执行任务结束
     *
     * @param context
     * @param jobException
     */
    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        if (logger.isDebugEnabled()) {
            String taskName = context.getMergedJobDataMap().getString(TaskConstants.TASK_NAME);
            String jobClass = context.getJobInstance().getClass().getCanonicalName();
            String cronExpression = context.getMergedJobDataMap().getString(TaskConstants.CRON_EXPRESSION);
            String param = context.getMergedJobDataMap().getString(TaskConstants.TASK_PARAM);
            //如果工作类执行没有发生异常, 则任务状态为成功, 否则为失败
            TaskState state = jobException == null ? TaskState.SUCCESS : TaskState.FAILURE;
            Date startTime = context.getFireTime();
            Date now = new Date();
            String result = "";
            //任务状态为失败时, 获取异常堆栈作为执行结果, 任务状态为成功时, 取上下文中result作为执行结果
            if (state == TaskState.FAILURE) {
                result = ExceptionUtils.getStackTrace(jobException);
            } else if (context.getResult() != null) {
                result = context.getResult().toString();
            }
            logger.debug("注解定时任务结束执行, taskName: {}, jobClass: {}, cronExpression: {}, param: {}, state: {}, startTime: {}, endTime: {}, result: {}",
                    taskName, jobClass, cronExpression, param, state, new DateTime(startTime).toString(CoreConstants.DATETIME_FORMAT), new DateTime(now).toString(CoreConstants.DATETIME_FORMAT), result);
        }
    }
}
