package com.zxhtom.framework_task.service.schedule;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.ResourceUtils;

import com.zxhtom.framework_task.quartz.AbstractJob;
import com.zxhtom.framework_task.quartz.Schedule;
import com.zxhtom.utils.DbUtil;
import com.zxhtom.utils.SqlFileExecutor;

@DisallowConcurrentExecution
@Schedule(enable = true, taskName = "定时还原数据", cronExpression = "0 0 2 * * ?", param = "", immediately = false)
public class CleanDataSchedule extends AbstractJob {

	private Logger logger = LogManager.getLogger(CleanDataSchedule.class);
	@Autowired
	private SqlSession sqlSession;
	@Autowired
	private RedisTemplate redisTemplate;

	public Connection getConnection(){  
        Connection conn = null;  
        try {  
            conn =  sqlSession.getConfiguration().getEnvironment().getDataSource().getConnection();  
            logger.info("===This Connection isClosed ? "+conn.isClosed());  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return conn;  
    } 
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		SqlFileExecutor executor = new SqlFileExecutor();
        redisTemplate.delete(redisTemplate.keys("*"));
		try {
			List<File> fileList = new ArrayList<File>();
			fileList.add(ResourceUtils.getFile("classpath:sql/delete.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/user.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/role.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/permission.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/user_role.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/role_permission.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/module.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/menu.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/role_menu.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/role_module.sql"));
			fileList.add(ResourceUtils.getFile("classpath:sql/dept.sql"));
			DbUtil.exeSqlFile(getConnection(),fileList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
