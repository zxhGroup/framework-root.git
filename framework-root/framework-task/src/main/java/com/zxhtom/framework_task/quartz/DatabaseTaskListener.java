package com.zxhtom.framework_task.quartz;

import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.listeners.JobListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Strings;
import com.zxhtom.CoreConstants;
import com.zxhtom.framework_task.enums.TaskState;
import com.zxhtom.framework_task.model.Task;
import com.zxhtom.framework_task.model.TaskLog;
import com.zxhtom.framework_task.repository.TaskRepository;
import com.zxhtom.framework_task.service.TaskLogService;
import com.zxhtom.web.CurrentRequestService;

/**
 * 监听数据库定时任务执行事件, 更新数据库中定时任务状态及记录定时任务日志
 */
public class DatabaseTaskListener extends JobListenerSupport {
    private static final Logger logger = LogManager.getLogger(DatabaseTaskListener.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskLogService taskLogService;

    @Autowired
    private TaskConfiguration taskConfiguration;

    @Autowired
    private CurrentRequestService currentRequestService;

    @Override
    public String getName() {
        return TaskConstants.DATABASE_TASK_LISTENER;
    }

    /**
     * 开始执行任务
     *
     * @param context
     */
    @Override
    public void jobToBeExecuted(JobExecutionContext context) {
        //DatabaseTaskRefreshJob
    	logger.info("定时任务开始准备");
        if (context.getJobInstance() instanceof DatabaseTaskRefreshJob) {
            return;
        }
        logger.info("判断数据库是否可以执行");
        //获取定时任务信息
        Task task = (Task) context.getMergedJobDataMap().get(TaskConstants.TASK_OBJECT);
        if (task != null) {
            logger.debug("数据库定时任务开始执行, taskName: {}, jobClass: {}, cronExpression: {}, param: {}",
                    task.getTaskName(), task.getJobClass(), task.getCronExpression(), task.getParam());
        }
    }

    /**
     * 执行任务结束
     *
     * @param context
     * @param jobException
     */
    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        //如果工作类执行没有发生异常, 则任务状态为成功, 否则为失败
        TaskState state = jobException == null ? TaskState.SUCCESS : TaskState.FAILURE;
        String ip = currentRequestService.getLocalIp();
        String param = context.getMergedJobDataMap().getString(TaskConstants.TASK_PARAM);
        Date startTime = context.getFireTime();
        Date now = new Date();
        String result = "";
        //任务状态为失败时, 获取异常堆栈作为执行结果, 任务状态为成功时, 取上下文中result作为执行结果
        if (state == TaskState.FAILURE) {
            result = ExceptionUtils.getStackTrace(jobException);
        } else if (context.getResult() != null) {
            result = context.getResult().toString();
        }

        //DatabaseTaskRefreshJob
        if (context.getJobInstance() instanceof DatabaseTaskRefreshJob) {
            //刷新数据库定时任务时, 如果发生异常或返回结果不为空, 则写一笔日志, 其他不写日志
            if (state == TaskState.FAILURE || !Strings.isNullOrEmpty(result)) {
                TaskLog taskLog = new TaskLog();
                taskLog.setTaskName("刷新数据库定时任务");
                taskLog.setJobClass(DatabaseTaskRefreshJob.class.getCanonicalName());
                taskLog.setCronExpression(Integer.toString(taskConfiguration.getDatabaseTaskRefreshInterval()));
                taskLog.setParam(param);
                taskLog.setState(state);
                taskLog.setIp(ip);
                taskLog.setStartTime(startTime);
                taskLog.setEndTime(now);
                taskLog.setResult(result);
                taskLogService.insert(taskLog);
            }
        } else {
            //获取定时任务信息
            Task task = (Task) context.getMergedJobDataMap().get(TaskConstants.TASK_OBJECT);
            if (task != null) {
                //更新数据库中定时任务最后一次执行情况
                Task updateTask = new Task();
                updateTask.setTaskId(task.getTaskId());
                updateTask.setLastState(state);
                updateTask.setLastIp(ip);
                updateTask.setLastStartTime(startTime);
                updateTask.setLastEndTime(now);
                updateTask.setLastResult(result);
                taskRepository.update(updateTask);

                //写定时任务日志
                TaskLog taskLog = new TaskLog();
                taskLog.setTaskName(task.getTaskName());
                taskLog.setJobClass(task.getJobClass());
                taskLog.setCronExpression(task.getCronExpression());
                taskLog.setParam(param);
                taskLog.setState(state);
                taskLog.setIp(ip);
                taskLog.setStartTime(startTime);
                taskLog.setEndTime(now);
                taskLog.setResult(result);
                taskLogService.insert(taskLog);

                logger.debug("数据库定时任务结束执行, taskName: {}, jobClass: {}, cronExpression: {}, param: {}, state: {}, startTime: {}, endTime: {}, result: {}",
                        task.getTaskName(), task.getJobClass(), task.getCronExpression(), param, state,
                        new DateTime(startTime).toString(CoreConstants.DATETIME_FORMAT), new DateTime(now).toString(CoreConstants.DATETIME_FORMAT), result);
            }
        }
    }
}
