package com.zxhtom.framework_task.quartz;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.Min;

/**
 * 定时任务参数配置
 */
@Data
@ConfigurationProperties("taskconfiguration")
public class TaskConfiguration {
    /**
     * 是否启用注解定时任务
     */
    private boolean annotationTaskEnable = true;
    /**
     * 注解定时任务线程池数
     */
    @Range(min = 1, max = 100, message = "注解定时任务线程池数量必须在 1 - 100 之间")
    private int annotationTaskThreadCount = 5;

    /**
     * 数据库定时任务是否启用
     */
    private boolean databaseTaskEnable = true;
    /**
     * 数据库定时任务线程池数
     */
    @Range(min = 1, max = 100, message = "数据库定时任务线程池数量必须在 1 - 100 之间")
    private int databaseTaskThreadCount = 5;
    /**
     * 数据库定时任务刷新时间间隔, 单位秒
     */
    @Min(value = 1, message = "数据库定时任务刷新时间间隔必须大于0")
    private int databaseTaskRefreshInterval = 30;
	public boolean isAnnotationTaskEnable() {
		return annotationTaskEnable;
	}
	public void setAnnotationTaskEnable(boolean annotationTaskEnable) {
		this.annotationTaskEnable = annotationTaskEnable;
	}
	public int getAnnotationTaskThreadCount() {
		return annotationTaskThreadCount;
	}
	public void setAnnotationTaskThreadCount(int annotationTaskThreadCount) {
		this.annotationTaskThreadCount = annotationTaskThreadCount;
	}
	public boolean isDatabaseTaskEnable() {
		return databaseTaskEnable;
	}
	public void setDatabaseTaskEnable(boolean databaseTaskEnable) {
		this.databaseTaskEnable = databaseTaskEnable;
	}
	public int getDatabaseTaskThreadCount() {
		return databaseTaskThreadCount;
	}
	public void setDatabaseTaskThreadCount(int databaseTaskThreadCount) {
		this.databaseTaskThreadCount = databaseTaskThreadCount;
	}
	public int getDatabaseTaskRefreshInterval() {
		return databaseTaskRefreshInterval;
	}
	public void setDatabaseTaskRefreshInterval(int databaseTaskRefreshInterval) {
		this.databaseTaskRefreshInterval = databaseTaskRefreshInterval;
	}
    
    
}
