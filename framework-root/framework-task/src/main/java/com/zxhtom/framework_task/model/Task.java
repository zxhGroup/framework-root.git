package com.zxhtom.framework_task.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import com.zxhtom.CoreConstants;
import com.zxhtom.framework_task.enums.TaskState;
import com.zxhtom.validator.ByteLength;
import com.zxhtom.validator.Insert;
import com.zxhtom.validator.Update;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 定时任务
 */
@Data
@EqualsAndHashCode(of = {"taskId", "taskName", "jobClass", "cronExpression", "param", "enable", "immediately", "runIp"})
@ApiModel(description = "定时任务对象")
@Table(name="SYS_TASK")
public class Task {
    /**
     * 定时任务流水号
     */
    @Id
    @ApiModelProperty("定时任务Id")
    private Long taskId;

    /**
     * 定时任务名称
     */
    @Column
    @ApiModelProperty("定时任务名称, 必填")
    @NotBlank(message = "定时任务名称不可空白", groups = {Insert.class})
    @ByteLength(max = 100, message = "定时任务名称长度不能超过{max}", groups = {Insert.class, Update.class})
    private String taskName;

    /**
     * 工作类名
     */
    @Column
    @ApiModelProperty("工作类名, 必填")
    @NotBlank(message = "工作类名不可空白", groups = {Insert.class})
    @ByteLength(max = 100, message = "工作类名长度不能超过{max}", groups = {Insert.class, Update.class})
    private String jobClass;

    /**
     * 调度表达式
     */
    @Column
    @ApiModelProperty("调度表达式, 必填")
    @NotBlank(message = "调度表达式不可空白", groups = {Insert.class})
    @ByteLength(max = 100, message = "调度表达式长度不能超过{max}", groups = {Insert.class, Update.class})
    private String cronExpression;

    /**
     * 定时任务参数
     */
    @ApiModelProperty("定时任务参数")
    private String param;

    /**
     * 是否启用
     */
    @Column
    @ApiModelProperty("是否启用")
    @NotNull(message = "是否启用不可为空", groups = {Insert.class})
    private Boolean enable;

    /**
     * 加载定时任务后立即执行
     */
    @Column
    @ApiModelProperty("加载定时任务后立即执行")
    @NotNull(message = "加载定时任务后立即执行不可为空", groups = {Insert.class})
    private Boolean immediately;

    /**
     * 指定定时任务运行服务器IP, 如果不指定会在所有服务器上执行
     */
    @Column
    @ApiModelProperty("指定定时任务运行服务器IP, 如果不指定会在所有服务器上执行")
    @ByteLength(max = 100, message = "定时任务运行服务器IP长度不能超过{max}", groups = {Insert.class, Update.class})
    private String runIp;

    /**
     * 备注
     */
    @Column
    @ApiModelProperty("备注")
    @ByteLength(max = 500, message = "备注长度不能超过{max}", groups = {Insert.class, Update.class})
    private String remark;

    /**
     * 最后一次处理状况
     */
    @Column
    @ApiModelProperty("最后一次处理状况")
    private TaskState lastState;

    /**
     * 最后一次处理IP
     */
    @Column
    @ApiModelProperty("最后一次处理IP")
    @ByteLength(max = 100, message = "任务最后一次处理IP长度不能超过{max}", groups = {Insert.class, Update.class})
    private String lastIp;

    /**
     * 最后一次处理开始时间
     */
    @ApiModelProperty("最后一次处理开始时间")
    @DateTimeFormat(pattern=CoreConstants.DATETIME_FORMAT)
    private Date lastStartTime;

    /**
     * 最后一次处理结束时间
     */
    @ApiModelProperty("最后一次处理结束时间")
    private Date lastEndTime;

    /**
     * 最后一次处理结果
     */
    @Column
    @ApiModelProperty("最后一次处理结果")
    private String lastResult;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public Boolean getImmediately() {
		return immediately;
	}

	public void setImmediately(Boolean immediately) {
		this.immediately = immediately;
	}

	public String getRunIp() {
		return runIp;
	}

	public void setRunIp(String runIp) {
		this.runIp = runIp;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public TaskState getLastState() {
		return lastState;
	}

	public void setLastState(TaskState lastState) {
		this.lastState = lastState;
	}

	public String getLastIp() {
		return lastIp;
	}

	public void setLastIp(String lastIp) {
		this.lastIp = lastIp;
	}

	public Date getLastStartTime() {
		return lastStartTime;
	}

	public void setLastStartTime(Date lastStartTime) {
		this.lastStartTime = lastStartTime;
	}

	public Date getLastEndTime() {
		return lastEndTime;
	}

	public void setLastEndTime(Date lastEndTime) {
		this.lastEndTime = lastEndTime;
	}

	public String getLastResult() {
		return lastResult;
	}

	public void setLastResult(String lastResult) {
		this.lastResult = lastResult;
	}
    
    
}
