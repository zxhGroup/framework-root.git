/**
 *
 */
package com.zxhtom.framework_task.enums;

import com.zxhtom.enums.EnumDesc;
import com.zxhtom.enums.StringValueEnum;

import java.lang.reflect.Field;

/**
 * 定时任务处理状况
 */
public enum TaskState implements StringValueEnum {

    /**
     * 成功
     */
    @EnumDesc("成功")
    SUCCESS(0, "success"),
    /**
     * 失败
     */
    @EnumDesc("失败")
    FAILURE(1, "failure");

    private TaskState(Integer code, String value) {
        this.value = value;
        this.code = code;
    }

    private Integer code;
    private String value;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public Object getCode() {
        return code;
    }

    @Override
    public String toString() {
        return value;
    }
}
