package com.zxhtom.framework_task.model;

import io.swagger.annotations.ApiModelProperty;

public class Info {

	/**
	 * 配置定时器需要制定服务的ip
	 */
	@ApiModelProperty("配置定时器需要制定服务的ip")
	private String runIp;

	public String getRunIp() {
		return runIp;
	}

	public void setRunIp(String runIp) {
		this.runIp = runIp;
	}
	
	
}
