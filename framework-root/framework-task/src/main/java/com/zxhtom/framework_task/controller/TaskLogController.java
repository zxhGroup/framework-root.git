package com.zxhtom.framework_task.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.CoreConstants;
import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.model.TaskLog;
import com.zxhtom.framework_task.service.TaskLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 定时任务日志Controller
 */
@RestController
@RequestMapping("/framework_task/task_log")
@Api(tags = "framework_task task_log", description = "定时任务日志")
public class TaskLogController {
    @Autowired
    private TaskLogService taskLogService;

    /**
     * 分页查询定时任务日志
     *
     * @param pageNumber    页码
     * @param pageSize      每页笔数
     * @param orderProperty 排序字段
     * @param orderType     排序方式
     * @param taskName      定时任务名称
     * @param jobClass      工作类名
     * @param startLogTime  日志起始时间
     * @param endLogTime    日志截止时间
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation("分页查询定时任务日志")
    public PagedResult<TaskLog> selectList(
            @ApiParam("页码") @RequestParam(required = false, defaultValue = CoreConstants.PAGE_NUMBER) Integer pageNumber,
            @ApiParam("每页笔数") @RequestParam(required = false, defaultValue = CoreConstants.PAGE_SIZE) Integer pageSize,
            @ApiParam("排序字段") @RequestParam(required = false) String orderProperty,
            @ApiParam("排序方式") @RequestParam(required = false) OrderType orderType,
            @ApiParam("定时任务名称") @RequestParam(required = false) String taskName,
            @ApiParam("工作类名") @RequestParam(required = false) String jobClass,
            @ApiParam("日志起始时间") @RequestParam(required = false) Date startLogTime,
            @ApiParam("日志截止时间") @RequestParam(required = false) Date endLogTime) {
        return taskLogService.selectList(pageNumber, pageSize, orderProperty, orderType, taskName, jobClass, startLogTime, endLogTime);
    }

    /**
     * 按条件批量删除定时任务日志
     *
     * @param taskName     定时任务名称
     * @param jobClass     工作类名
     * @param startLogTime 日志起始时间
     * @param endLogTime   日志截止时间
     */
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ApiOperation("按条件批量删除日志")
    public void delete(@ApiParam("定时任务名称") @RequestParam(required = false) String taskName,
                       @ApiParam("工作类名") @RequestParam(required = false) String jobClass,
                       @ApiParam("日志起始时间") @RequestParam(required = false) Date startLogTime,
                       @ApiParam("日志截止时间") @RequestParam(required = false) Date endLogTime) {
        taskLogService.delete(taskName, jobClass, startLogTime, endLogTime);
    }

    /**
     * 批量删除日志
     *
     * @param ids 要删除的日志Id列表
     */
    @RequestMapping(value = "/ids", method = RequestMethod.DELETE)
    @ApiOperation("批量删除日志")
    public void delete(@ApiParam(value = "要删除的日志Id列表", required = true) @RequestBody List<Long> ids) {
        taskLogService.delete(ids);
    }
}
