package com.zxhtom.framework_task.mapper;

import com.zxhtom.framework_task.model.TaskLog;

import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * 定时任务日志Mapper
 */
public interface TaskLogMapper extends Mapper<TaskLog>, IdsMapper<TaskLog> {
}
