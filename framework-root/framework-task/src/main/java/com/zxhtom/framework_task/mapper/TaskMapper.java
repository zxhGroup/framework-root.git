package com.zxhtom.framework_task.mapper;

import com.zxhtom.framework_task.model.Task;
import com.zxhtom.framework_task.model.TaskLog;

import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;

public interface TaskMapper extends Mapper<Task>, IdsMapper<Task> {

}
