package com.zxhtom.framework_task.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.model.Task;
import com.zxhtom.framework_task.repository.TaskRepository;
import com.zxhtom.framework_task.service.TaskService;
import com.zxhtom.utils.IdUtil;

/**
 * 定时任务Service
 */
@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;

    /**
     * 查询定时任务
     *
     * @param id 定时任务id
     * @return
     */
    @Override
    public Task selectById(Long id) {
        return taskRepository.selectById(id);
    }

    /**
     * 分页查询定时任务
     *
     * @param pageNumber    页码
     * @param pageSize      每页笔数
     * @param orderProperty 排序字段
     * @param orderType     排序方式
     * @param taskName      定时任务名称
     * @param jobClass      工作类名
     * @return
     */
    @Override
    public PagedResult<Task> selectList(Integer pageNumber, Integer pageSize, String orderProperty, OrderType orderType, String taskName, String jobClass) {
        return taskRepository.selectList(pageNumber, pageSize, orderProperty, orderType, taskName, jobClass);
    }

    /**
     * 创建定时任务
     *
     * @param task
     * @return
     */
    @Override
    public Task insert(Task task) {
        //赋Id
        task.setTaskId(IdUtil.getInstance().getId());
        taskRepository.insert(task);
        return task;
    }

    /**
     * 修改定时任务
     *
     * @param task
     * @return
     */
    @Override
    public Task update(Task task) {
        //更新返回不为1, 则说明没有此数据
        Assert.state(taskRepository.update(task) == 1, "定时任务不存在!");
        //更新成功后, 重查一下
        return taskRepository.selectById(task.getTaskId());
    }

    /**
     * 删除定时任务
     *
     * @param id
     */
    @Override
    public void delete(Long id) {
        //删除返回不为1, 则说明没有此数据
        Assert.state(taskRepository.delete(id) == 1, "定时任务不存在!");
    }
}
