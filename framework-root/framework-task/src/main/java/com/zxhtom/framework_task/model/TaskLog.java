package com.zxhtom.framework_task.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.zxhtom.framework_task.enums.TaskState;
import com.zxhtom.validator.ByteLength;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 定时任务日志
 */
@Data
@ApiModel(description = "定时任务日志对象")
@Table(name="SYS_TASK_LOG")
public class TaskLog {
    /**
     * 定时任务日志流水号
     */
    @Id
    @ApiModelProperty("定时任务日志Id")
    private Long taskLogId;

    /**
     * 日志写入时间
     */
    @Column
    @ApiModelProperty("日志写入时间")
    @NotNull(message = "日志写入时间不可空白")
    private Date logTime;

    /**
     * 定时任务名称
     */
    @Column
    @ApiModelProperty("定时任务名称")
    @NotBlank(message = "定时任务名称不可空白")
    @ByteLength(max = 100, message = "定时任务名称长度不能超过{max}")
    private String taskName;

    /**
     * 工作类名
     */
    @Column
    @ApiModelProperty("工作类名")
    @NotBlank(message = "工作类名不可空白")
    @ByteLength(max = 100, message = "工作类名长度不能超过{max}")
    private String jobClass;

    /**
     * 调度表达式
     */
    @Column
    @ApiModelProperty("调度表达式")
    @NotBlank(message = "调度表达式不可空白")
    @ByteLength(max = 100, message = "调度表达式长度不能超过{max}")
    private String cronExpression;

    /**
     * 定时任务参数
     */
    @Column
    @ApiModelProperty("定时任务参数")
    private String param;

    /**
     * 处理状况
     */
    @Column
    @ApiModelProperty("处理状况")
    @NotNull(message = "处理状况不可空白")
    private TaskState state;

    /**
     * 处理IP
     */
    @Column
    @ApiModelProperty("处理IP")
    @NotBlank(message = "处理IP不可空白")
    @ByteLength(max = 100, message = "处理IP长度不能超过{max}")
    private String ip;

    /**
     * 处理开始时间
     */
    @Column
    @ApiModelProperty("处理开始时间")
    @NotNull(message = "处理开始时间不可空白")
    private Date startTime;

    /**
     * 处理结束时间
     */
    @Column
    @ApiModelProperty("处理结束时间")
    @NotNull(message = "处理结束时间不可空白")
    private Date endTime;

    /**
     * 处理结果
     */
    @Column
    @ApiModelProperty("处理结果")
    private String result;

	public Long getTaskLogId() {
		return taskLogId;
	}

	public void setTaskLogId(Long taskLogId) {
		this.taskLogId = taskLogId;
	}

	public Date getLogTime() {
		return logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public TaskState getState() {
		return state;
	}

	public void setState(TaskState state) {
		this.state = state;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
    
    
}
