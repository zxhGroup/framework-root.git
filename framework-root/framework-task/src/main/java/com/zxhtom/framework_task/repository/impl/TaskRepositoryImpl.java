package com.zxhtom.framework_task.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.github.pagehelper.PageHelper;
import com.google.common.base.Strings;
import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.mapper.TaskMapper;
import com.zxhtom.framework_task.model.Task;
import com.zxhtom.framework_task.repository.TaskRepository;

import tk.mybatis.mapper.entity.Example;

/**
 * 定时任务Repository
 */
@Repository
public class TaskRepositoryImpl implements TaskRepository {
    @Autowired
    private TaskMapper taskMapper;

    /**
     * 查询定时任务
     *
     * @param id 定时任务id
     * @return
     */
    @Override
    public Task selectById(Long id) {
        return taskMapper.selectByPrimaryKey(id);
    }

    /**
     * 创建定时任务
     *
     * @param task
     * @return
     */
    @Override
    public Integer insert(Task task) {
        return taskMapper.insertSelective(task);
    }

    /**
     * 修改定时任务
     *
     * @param task
     * @return
     */
    @Override
    public Integer update(Task task) {
        return taskMapper.updateByPrimaryKeySelective(task);
    }

    /**
     * 删除定时任务
     *
     * @param id
     * @return
     */
    @Override
    public Integer delete(Long id) {
        return taskMapper.deleteByPrimaryKey(id);
    }

    /**
     * 分页查询定时任务
     *
     * @param pageNumber    页码
     * @param pageSize      每页笔数
     * @param orderProperty 排序字段
     * @param orderType     排序方式
     * @param taskName      定时任务名称
     * @param jobClass      工作类名
     * @return
     */
    @Override
    public PagedResult<Task> selectList(Integer pageNumber, Integer pageSize, String orderProperty, OrderType orderType, String taskName, String jobClass) {
        Example example = new Example(Task.class);
        Example.Criteria criteria = example.createCriteria();
        if (!Strings.isNullOrEmpty(taskName)) {
            criteria.andLike("taskName", "%" + taskName + "%");
        }
        if (!Strings.isNullOrEmpty(jobClass)) {
            criteria.andLike("jobClass", "%" + jobClass + "%");
        }
        if (!Strings.isNullOrEmpty(orderProperty)) {
            if (OrderType.DESC.equals(orderType)) {
                example.orderBy(orderProperty).desc();
            } else {
                example.orderBy(orderProperty).asc();
            }
        }
        PageHelper.startPage(pageNumber, pageSize);
        return new PagedResult<>(taskMapper.selectByExample(example));
    }
}
