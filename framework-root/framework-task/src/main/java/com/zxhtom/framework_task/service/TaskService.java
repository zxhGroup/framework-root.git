package com.zxhtom.framework_task.service;

import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.model.Task;

/**
 * 定时任务Service
 */
public interface TaskService {
	/**
	 * 查询定时任务
	 *
	 * @param id 定时任务id
	 * @return
	 */
	Task selectById(Long id);

	/**
	 * 创建定时任务
	 *
	 * @param task
	 * @return
	 */
	Task insert(Task task);

	/**
	 * 修改定时任务
	 *
	 * @param task
	 * @return
	 */
	Task update(Task task);

	/**
	 * 删除定时任务
	 *
	 * @param id
	 */
	void delete(Long id);

	/**
	 * 分页查询定时任务
	 *
	 * @param pageNumber    页码
	 * @param pageSize      每页笔数
	 * @param orderProperty 排序字段
	 * @param orderType     排序方式
	 * @param taskName      定时任务名称
	 * @param jobClass      工作类名
	 * @return
	 */
	PagedResult<Task> selectList(Integer pageNumber, Integer pageSize, String orderProperty, OrderType orderType, String taskName, String jobClass);
}
