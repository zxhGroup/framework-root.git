package com.zxhtom.framework_task.quartz;

import java.lang.annotation.*;

/**
 * 定时任务调度注解, 多个调度
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Schedules {
    Schedule[] value();
}
