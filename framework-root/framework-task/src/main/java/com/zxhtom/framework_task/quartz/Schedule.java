package com.zxhtom.framework_task.quartz;

import java.lang.annotation.*;

/**
 * 定时任务调度注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Schedule {
    /**
     * 是否启用定时任务
     *
     * @return
     */
    boolean enable() default true;

    /**
     * 任务名称
     *
     * @return
     */
    String taskName() default "";

    /**
     * 调度表达式, 必填
     *
     * @return
     */
    String cronExpression() default "";

    /**
     * 传入参数
     *
     * @return
     */
    String param() default "";

    /**
     * 加载定时任务时立即执行
     *
     * @return
     */
    boolean immediately() default false;
}
