package com.zxhtom.framework_task.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.CoreConstants;
import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.model.Info;
import com.zxhtom.framework_task.model.Task;
import com.zxhtom.framework_task.service.TaskService;
import com.zxhtom.validator.Insert;
import com.zxhtom.validator.Update;
import com.zxhtom.web.CurrentRequestService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 定时任务Controller
 */
@RestController
@RequestMapping("/framework_task/task")
@Api(tags = "framework_task task", description = "定时任务")
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    private CurrentRequestService currentRequestService;
    /**
     * 获取配置定时器的必要参数
     * @return
     */
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ApiOperation("查询定时任务参数")
    public Info selectNeccessInfo(){
    	Info info = new Info();
    	info.setRunIp(currentRequestService.getLocalIp());
    	System.out.println(currentRequestService.getLocalIp());
    	System.out.println(currentRequestService.getClientIp());
    	System.out.println(currentRequestService.getHostName());
    	return info;
    }
    /**
     * 查询定时任务
     *
     * @param id 定时任务id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("查询定时任务")
    public Task selectById(@ApiParam(value = "要查询的定时任务Id", required = true) @PathVariable Long id) {
        return taskService.selectById(id);
    }

    /**
     * 分页查询定时任务
     *
     * @param pageNumber    页码
     * @param pageSize      每页笔数
     * @param orderProperty 排序字段
     * @param orderType     排序方式
     * @param taskName      定时任务名称
     * @param jobClass      工作类名
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation("分页查询定时任务")
    public PagedResult<Task> selectList(
            @ApiParam("页码") @RequestParam(required = false, defaultValue = CoreConstants.PAGE_NUMBER) Integer pageNumber,
            @ApiParam("每页笔数") @RequestParam(required = false, defaultValue = CoreConstants.PAGE_SIZE) Integer pageSize,
            @ApiParam("排序字段") @RequestParam(required = false) String orderProperty,
            @ApiParam("排序方式") @RequestParam(required = false) OrderType orderType,
            @ApiParam("定时任务名称") @RequestParam(required = false) String taskName,
            @ApiParam("工作类名") @RequestParam(required = false) String jobClass) {
        return taskService.selectList(pageNumber, pageSize, orderProperty, orderType, taskName, jobClass);
    }

    /**
     * 创建定时任务
     *
     * @param task
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation("创建定时任务")
    public Task insert(@ApiParam(value = "要创建的定时任务对象", required = true) @Validated({Insert.class}) @RequestBody Task task) {
        return taskService.insert(task);
    }

    /**
     * 修改定时任务
     *
     * @param task
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ApiOperation("修改定时任务")
    public Task update(@ApiParam(value = "要修改的定时任务对象", required = true) @Validated({Update.class}) @RequestBody Task task) {
        return taskService.update(task);
    }

    /**
     * 删除定时任务
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation("删除定时任务")
    public void delete(@ApiParam(value = "要删除的定时任务Id", required = true) @PathVariable Long id) {
        taskService.delete(id);
    }
}
