package com.zxhtom.framework_task.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 抽象工作类, 具体工作类需继承 AbstractJob
 */
public abstract class AbstractJob implements Job {
    /**
     * 具体工作类需实现 execute 方法, 在方法中撰写具体业务逻辑
     *
     * @param context 上下文, 可以获取传入参数及回传执行结果
     * @throws JobExecutionException
     */
    @Override
    public abstract void execute(JobExecutionContext context) throws JobExecutionException;
}
