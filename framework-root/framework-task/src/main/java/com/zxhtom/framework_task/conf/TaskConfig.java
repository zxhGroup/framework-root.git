package com.zxhtom.framework_task.conf;

import com.zxhtom.framework_task.quartz.*;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.DirectSchedulerFactory;
import org.quartz.simpl.RAMJobStore;
import org.quartz.simpl.SimpleThreadPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 注入定时任务相关对象
 */
@Configuration
@Import({TaskConfiguration.class, TaskJobFactory.class,
        AnnotationTaskListener.class, DatabaseTaskListener.class,
        AnnotationTaskStarter.class, DatabaseTaskStarter.class})
public class TaskConfig {
    /**
     * 定时任务参数配置
     */
    @Autowired
    private TaskConfiguration taskConfiguration;

    /**
     * 工作类工厂
     */
    @Autowired
    private TaskJobFactory taskJobFactory;

    /**
     * 注解定时任务监听器
     */
    @Autowired
    private AnnotationTaskListener annotationTaskListener;

    /**
     * 数据库定时任务监听器
     */
    @Autowired
    private DatabaseTaskListener databaseTaskListener;

    /**
     * 创建注解定时任务调度器
     */
    @Bean("annotationScheduler")
    public Scheduler annotationScheduler() throws SchedulerException {
        if (taskConfiguration.isAnnotationTaskEnable()) {
            //创建注解定时任务线程池
            SimpleThreadPool threadPool = new SimpleThreadPool(taskConfiguration.getAnnotationTaskThreadCount(), Thread.NORM_PRIORITY);
            threadPool.initialize();
            //创建注解定时任务调度器
            DirectSchedulerFactory.getInstance().createScheduler(TaskConstants.ANNOTATION_SCHEDULER, TaskConstants.ANNOTATION_SCHEDULER, threadPool, new RAMJobStore());
            Scheduler scheduler = DirectSchedulerFactory.getInstance().getScheduler(TaskConstants.ANNOTATION_SCHEDULER);
            //设置工作类工厂
            scheduler.setJobFactory(taskJobFactory);
            //添加注解定时任务监听器
            scheduler.getListenerManager().addJobListener(annotationTaskListener);
            return scheduler;
        } else {
            return new EmptySchedule();
        }
    }

    /**
     * 创建数据库定时任务调度器
     *
     * @return
     */
    @Bean("databaseScheduler")
    public Scheduler databaseScheduler() throws SchedulerException {
        if (taskConfiguration.isDatabaseTaskEnable()) {
            //创建数据库定时任务线程池
            SimpleThreadPool threadPool = new SimpleThreadPool(taskConfiguration.getDatabaseTaskThreadCount(), Thread.NORM_PRIORITY);
            threadPool.initialize();
            //创建数据库定时任务调度器
            DirectSchedulerFactory.getInstance().createScheduler(TaskConstants.DATABASE_SCHEDULER, TaskConstants.DATABASE_SCHEDULER, threadPool, new RAMJobStore());
            Scheduler scheduler = DirectSchedulerFactory.getInstance().getScheduler(TaskConstants.DATABASE_SCHEDULER);
            //设置工作类工厂
            scheduler.setJobFactory(taskJobFactory);
            //添加数据库定时任务监听器
            scheduler.getListenerManager().addJobListener(databaseTaskListener);
            return scheduler;
        } else {
            return new EmptySchedule();
        }
    }
}
