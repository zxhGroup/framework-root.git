package com.zxhtom.framework_task.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zxhtom.PagedResult;
import com.zxhtom.enums.OrderType;
import com.zxhtom.framework_task.model.TaskLog;
import com.zxhtom.framework_task.repository.TaskLogRepository;
import com.zxhtom.framework_task.service.TaskLogService;
import com.zxhtom.utils.IdUtil;

/**
 * 定时任务日志Service
 */
@Service
public class TaskLogServiceImpl implements TaskLogService {
    @Autowired
    private TaskLogRepository taskLogRepository;

    /**
     * 分页查询定时任务日志
     *
     * @param pageNumber    页码
     * @param pageSize      每页笔数
     * @param orderProperty 排序字段
     * @param orderType     排序方式
     * @param taskName      定时任务名称
     * @param jobClass      工作类名
     * @param startLogTime  日志起始时间
     * @param endLogTime    日志截止时间
     * @return
     */
    @Override
    public PagedResult<TaskLog> selectList(Integer pageNumber, Integer pageSize, String orderProperty, OrderType orderType, String taskName, String jobClass, Date startLogTime, Date endLogTime) {
        return taskLogRepository.selectList(pageNumber, pageSize, orderProperty, orderType, taskName, jobClass, startLogTime, endLogTime);
    }

    /**
     * 新增定时任务日志
     *
     * @param taskLog
     * @return
     */
    @Override
    public TaskLog insert(TaskLog taskLog) {
        // 赋Id
        taskLog.setTaskLogId(IdUtil.getInstance().getId());
        taskLog.setLogTime(new Date());
        taskLogRepository.insert(taskLog);
        return taskLog;
    }

    /**
     * 批量删除日志
     *
     * @param ids 要删除的日志Id列表
     */
    @Override
    public void delete(List<Long> ids) {
        taskLogRepository.delete(ids);
    }

    /**
     * 按条件批量删除定时任务日志
     *
     * @param taskName     定时任务名称
     * @param jobClass     工作类名
     * @param startLogTime 日志起始时间
     * @param endLogTime   日志截止时间
     */
    @Override
    public void delete(String taskName, String jobClass, Date startLogTime, Date endLogTime) {
        taskLogRepository.delete(taskName, jobClass, startLogTime, endLogTime);
    }
}
