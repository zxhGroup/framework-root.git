var users={
};
/**全局变量*/
var socket;
var stompClient;
/**
 * socket远程ip+端口
 */
var host;
/**
 * 当前用户
 */
var currentUser;
var directionJson = {'left':'left','right':'right'};
var errorTimes =0;
/**全局变量*/

$(document).ready(function() {
    host = getHost();
    /**获取登录用户信息*/
    currentUser=getCurrentUser();
    if(currentUser==null || typeof(currentUser) == undefined){
        window.location.href = host + "/";
    }
    //init
    connect();
    initFriendsGroup();
});


/**
 * 初始化好友列表数据
 */
function initFriendsGroup(){
    $.ajax({
        url: '/tcp/friends/selectFriends',
        type: 'get',
        async: false,
        dataType: 'json',
        success:function (data) {
            var myTemplate = Handlebars.compile($("#friend-template").html());
            //将json对象用刚刚注册的Handlebars模版封装，得到最终的html，插入到基础table中。
            $('#friends').append(myTemplate(data.data));
            initializeMenu();
        },
        error:function (data) {
            console.log('error');
        }
    });
}
function initializeMenu() {
    $(".actuator").click(function(){
        var display = $(this).next().css('display');
        $(this).parent().css('background-image',(display == "block") ? "url()" : "url()");
        $(this).next().css('display',(display == "block") ? "none" : "block");
    });
}

/**
 *
 * @param userId 用户聊天框id
 * @param innerHTML 文本
 * @param direction 显示位置0左侧 1右侧
 */
function setMessageInnerHTML(userId,result,direction){
    var data={};
    var innerHTML = result.content;
    var createTime=formatDate(new Date(result.createTime),'hh:mm');
    var realDirection;
    if (direction == 0) {
        realDirection=directionJson.left;
        data['photo']=users[userId].photo;
    } else {
        realDirection=directionJson.right;
        data['photo']=currentUser.photo;
    }
    data['innerHTML']=innerHTML;
    data['createTime']= createTime;
    data['direction'] =realDirection;
    $('#'+userId+' .content').append(Handlebars.compile($("#content-template").html())(data));
    $('#'+userId+' .content').scrollTop($('#'+userId+' .content').height());
    console.log(innerHTML);
}
//订阅消息
function subscribe() {
    stompClient.subscribe('/user/queue/message', function (response) {
        var returnData = JSON.parse(response.body);
        if (returnData.fromUserId == returnData.toUserId) {
            //自己发送的消息需要自己渲染到聊天框中
            setMessageInnerHTML(currentUser.userId, returnData, 0);
        } else if (returnData.fromUserId == currentUser.userId) {
            //自己发送信息给别，自己收到的信息
            setMessageInnerHTML(returnData.toUserId, returnData, 1);
        } else {
            //别人发送的信息
            setMessageInnerHTML(returnData.fromUserId, returnData, 0);
        }
    });
}
function send(userId) {
    var chat_content = $('#'+userId+' .footer .text:eq(0)');
    var content = chat_content.val();
    if (content == '') {
        alert('空消息');
    }
    //清空聊天栏
    chat_content.val("");
    stompClient.send("/sendToUser", {'userId': userId},content);
}
function connect(){
    // 建立连接对象（还未发起连接）
    socket = new SockJS(host+"/endpointAric");

    // 获取 STOMP 子协议的客户端对象
    stompClient = Stomp.over(socket);

    // 向服务器发起websocket连接并发送CONNECT帧
    stompClient.connect(
        {
            userId: currentUser.userId // 携带客户端信息
        },
        function connectCallback(frame) {
            // 连接成功时（服务器响应 CONNECTED 帧）的回调方法
            subscribe();
            console.log("连接成功");
        },
        function errorCallBack(error) {
            // 连接失败时（服务器响应 ERROR 帧）的回调方法
            console.log("连接失败"+error);
            if (errorTimes < 10) {
                errorTimes++;
                setTimeout("connect();",8000);
            }
        }
    );
}
function chat(id,remarks) {
    var div=document.getElementById(id);
    if (div != null) {
        //说明该聊天框已经弹出过，主需要设置z-index就可以显示了
        setZIndex(div);
        return ;
    }
    $.ajax({
        url: '/framework_common/sysUser/'+id,
        type: 'get',
        async: false,
        data:{'pageNumber':1,pageSize:1},
        dataType: 'json',
        success:function (data) {
            var userInfo = data.data.datas[0];
            userInfo.remarks=remarks;
            initChatBox(userInfo);
        },
        error:function (data) {
            console.log('error');
        }
    });
    //initChatBox(users[id]);
}

/**
 * 加载历史消息
 */
function loadHistoryMessage(targetUserId) {
    $.ajax({
        url: '/tcp/message/selectHistoryMessage/',
        type: 'get',
        data:{'fromUserId':currentUser.userId,'targetUserId':targetUserId},
        async: true,
        dataType: 'json',
        success:function (data) {
            $.each(data.data, function (index, item) {
                if (item.fromUserId == currentUser.userId) {
                    setMessageInnerHTML(targetUserId, item, 1);
                } else {
                    setMessageInnerHTML(targetUserId, item, 0);
                }
            });
        },
        error:function (data) {
            console.log('error');
        }
    });
}
/**
 * 初始化聊天框
 */
function initChatBox(data){
    var div=document.getElementById(data.userId);
    if (div != null) {
        //说明该聊天框已经弹出过，主需要设置z-index就可以显示了
        setZIndex(div);
        return ;
    }
    users[data.userId]=data;
    var myTemplate = Handlebars.compile($("#chat-template").html());
    //将json对象用刚刚注册的Handlebars模版封装，得到最终的html，插入到基础table中。
    $('#chat').append(myTemplate(data));
    //加载历史消息
    loadHistoryMessage(data.userId);
    //重新获取聊天框
    div=document.getElementById(data.userId);
    setZIndex(div);
    //输入框绑定回车时间
    $('#'+data.userId+' input').bind('keyup',function(event) {
        if (event.keyCode == "13") {
            send(data.userId);
        }
    });
    div.onmousedown=function(ev) {
        setZIndex(div);
    }
    div.children[0].onmousedown=function(ev) {
        setZIndex(div);
        var oevent = ev || event;
        var distanceX = oevent.clientX - div.offsetLeft;
        var distanceY = oevent.clientY - div.offsetTop+80;
        document.onmousemove = function (ev) {
            var oevent = ev || event;
            div.style.left = oevent.clientX-distanceX  + 'px';
            div.style.top = oevent.clientY-distanceY  + 'px';
        };
        document.onmouseup = function () {
            document.onmousemove = null;
            document.onmouseup = null;
        };
    }
}

/**
 * 设置显示级别
 * @param div
 */
function setZIndex(div) {
    [].forEach.call(document.getElementsByClassName('container'),function(dv){
        dv.style.zIndex=1;
    });
    div.style.zIndex=2;
}

/**
 * 关闭聊天窗口
 * @param userId
 */
function closeChatBox(userId) {
    $('#' + userId).remove();
}