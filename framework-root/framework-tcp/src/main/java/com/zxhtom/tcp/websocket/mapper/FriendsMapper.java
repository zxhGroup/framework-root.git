package com.zxhtom.tcp.websocket.mapper;


import java.util.List;

import com.zxhtom.tcp.websocket.model.Friends;

import com.zxhtom.tcp.websocket.model.GroupFriends;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
public interface FriendsMapper extends Mapper<Friends>{

	/**
	 * 批量更新
	 * @param friendss
	 * @return
	 */
	Integer updateFriendsBatch(List<Friends> friendss);

	/**
	 * 批量新增
	 * @param friendss
	 * @return
	 */
	Integer insertFriendsBatch(List<Friends> friendss);

	/**
	 * 获取用户的好友列表
	 * @param userId
	 * @return
	 */
    List<GroupFriends> selectFriendsByCurrent(@Param("userId") Long userId);
}
