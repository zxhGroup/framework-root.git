package com.zxhtom.tcp.websocket.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.zxhtom.model.CustomUser;
import com.zxhtom.tcp.websocket.model.Friends;
import com.zxhtom.tcp.websocket.model.GroupFriends;
import com.zxhtom.tcp.websocket.service.FriendsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/tcp/friends")
@Api(tags = "tcp friends", description = "好友表信息表管理")
public class FriendsController {

	private Logger logger = LogManager.getLogger(FriendsController.class);
	@Autowired
	private FriendsService friendsService;

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有好友表信息列表 传0表示查询所有")
	public PagedResult<Friends> selectFriendssByPK(@ApiParam(value = "好友表主键", required = true) @PathVariable @NotNull Long userId,
                                                   @ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
                                                   @ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return friendsService.selectFriendssByPK(userId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteFriendsByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long userId) {
		return friendsService.deleteFriendsPK(userId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) Friends friends) {
		return friendsService.updateFriends(friends);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody Friends friends) {
		return friendsService.insertFriends(friends);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteFriendsByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> userIds) {
		return friendsService.deleteFriendsPKBatch(userIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<Friends> friendss) {
		return friendsService.updateFriendsBatch(friendss);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<Friends> friendss) {
		return friendsService.insertFriendsBatch(friendss);
	}

	@RequestMapping(value = "/selectFriends",method = RequestMethod.GET)
	@ApiOperation(value = "获取当前登录用户的好友信息")
	public List<GroupFriends> selectFriendsByCurrent() {
		return friendsService.selectFriendsByCurrent();
	}
}
