package com.zxhtom.tcp.websocket.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.Room;

public interface RoomService {

	/**
	 * 根据主键查询群聊信息
	 * @param roomId 群聊主键
	 * @return
	 */
	PagedResult<Room> selectRoomsByPK(Long roomId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除群聊信息
	 * @param roomId 群聊主键
	 * @return
	 */
	Integer deleteRoomPK(Long roomId);

	/**
	 * 单条更新数据
	 * @param Room
	 * @return
	 */
	Integer updateRoom(Room room);

	/**
	 * 单条新增数据
	 * @param room
	 * @return
	 */
	Integer insertRoom(Room room);
	
	/**
	 * 批量根据主键删除群聊信息
	 * @param roomIds 群聊主键
	 * @return
	 */
	Integer deleteRoomPKBatch(List<Long> roomIds);
	
	/**
	 * 批量更新数据
	 * @param rooms
	 * @return
	 */
	Integer updateRoomBatch(List<Room> rooms);

	/**
	 * 批量新增数据
	 * @param Rooms
	 * @return
	 */
	Integer insertRoomBatch(List<Room> rooms);
}
