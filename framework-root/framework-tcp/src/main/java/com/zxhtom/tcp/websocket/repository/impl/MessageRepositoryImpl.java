package com.zxhtom.tcp.websocket.repository.impl;

import java.util.*;

import com.zxhtom.tcp.websocket.mapper.MessageMapper;
import com.zxhtom.tcp.websocket.model.Message;
import com.zxhtom.tcp.websocket.repository.MessageRepository;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;

import tk.mybatis.mapper.entity.Example;

@Repository
public class MessageRepositoryImpl implements MessageRepository {

	@Autowired
	private MessageMapper messageMapper;
	@Override
	public PagedResult<Message> selectMessagesByPK(Long messageId, Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(Message.class,false,false);
		example.createCriteria().andEqualTo("messageId", messageId);
		return new PagedResult<>(messageMapper.selectByExample(example));
	}
	@Override
	public Integer deleteMessagesByPK(Long messageId) {
		Example example = new Example(Message.class,false,false);
		example.createCriteria().andEqualTo("messageId", messageId);
		return messageMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateMessage(Message message) {
		return messageMapper.updateByPrimaryKeySelective(message);
	}
	@Override
	public Integer insertMessage(Message message) {
		return messageMapper.insert(message);
	}
	
	@Override
	public Integer updateMessageBatch(List<Message> messages) {
		return messageMapper.updateMessageBatch(messages);
	}
	@Override
	public Integer insertMessageBatch(List<Message> messages) {
		return messageMapper.insertMessageBatch(messages);
	}

    @Override
    public List<Message> selectHistoryMessage(Long fromUserId, Long targetUserId) {
        PageHelper.startPage(1, 5);
        Date date = new Date();
		List<Message> messages = messageMapper.selectHistoryMessage(fromUserId, targetUserId,date);
		messages.addAll(messageMapper.selectHistoryMessage(targetUserId, fromUserId,date));
		Collections.sort(messages, new Comparator<Message>() {
			@Override
			public int compare(Message o1, Message o2) {
				return (int) (o2.getCreateTime().getTime()-o1.getCreateTime().getTime());
			}
		});
		return new PagedResult<>(messages).getDatas();
    }
}
