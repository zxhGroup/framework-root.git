package com.zxhtom.tcp.websocket.service.impl;

import java.util.List;

import com.zxhtom.tcp.websocket.model.Room;
import com.zxhtom.tcp.websocket.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.repository.RoomRepository;

@Service
public class RoomServiceImpl implements RoomService {

	@Autowired
	private RoomRepository roomRepository;

	@Override
	public PagedResult<Room> selectRoomsByPK(Long roomId, Integer pageNumber, Integer pageSize) {
		PagedResult<Room> result = new PagedResult<>();
		if(0==roomId){
			//查询所有
			roomId=null;
		}
		result = roomRepository.selectRoomsByPK(roomId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteRoomPK(Long roomId) {
		if(0==roomId){
			//删除所有
			roomId=null;
		}
		return roomRepository.deleteRoomsByPK(roomId);
	}

	@Override
	public Integer updateRoom(Room room) {
		return roomRepository.updateRoom(room);
	}

	@Override
	public Integer insertRoom(Room room) {
	room.setRoomId(IdUtil.getInstance().getIntId());
		return roomRepository.insertRoom(room);
	}
	
	@Override
	public Integer deleteRoomPKBatch(List<Long> roomIds){
		for(Long roomId : roomIds){
			roomRepository.deleteRoomsByPK(roomId);
		}
		return 1;
	}
	
	@Override
	public Integer updateRoomBatch(List<Room> rooms) {
		return roomRepository.updateRoomBatch(rooms);
	}

	@Override
	public Integer insertRoomBatch(List<Room> rooms) {
		for(Room room : rooms){
		room.setRoomId(IdUtil.getInstance().getIntId());
		}
		return roomRepository.insertRoomBatch(rooms);
	}

}
