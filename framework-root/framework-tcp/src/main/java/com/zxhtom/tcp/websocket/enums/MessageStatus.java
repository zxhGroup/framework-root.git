package com.zxhtom.tcp.websocket.enums;

import com.zxhtom.enums.EnumDesc;
import com.zxhtom.enums.IntValueEnum;

public enum  MessageStatus implements IntValueEnum {
    /**
     * 成功
     */
    @EnumDesc("撤回")
    SUCCESS(0, 0),
    /**
     * 失败
     */
    @EnumDesc("未撤回")
    FAILURE(1, 1);

    private MessageStatus(Integer code, Integer value) {
        this.value = value;
        this.code = code;
    }

    private Integer code;
    private Integer value;

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public Integer getCode() {
        return code;
    }
}
