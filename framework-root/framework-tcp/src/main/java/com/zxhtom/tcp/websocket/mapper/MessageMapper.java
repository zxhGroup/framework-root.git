package com.zxhtom.tcp.websocket.mapper;


import java.util.Date;
import java.util.List;

import com.zxhtom.tcp.websocket.model.Message;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
public interface MessageMapper extends Mapper<Message>{

	/**
	 * 批量更新
	 * @param messages
	 * @return
	 */
	Integer updateMessageBatch(List<Message> messages);

	/**
	 * 批量新增
	 * @param messages
	 * @return
	 */
	Integer insertMessageBatch(List<Message> messages);

	/**
	 * 获取历史数据
	 * @param fromUserId
	 * @param targetUserId
	 * @return
	 */
    List<Message> selectHistoryMessage(@Param("fromUserId") Long fromUserId, @Param("targetUserId") Long targetUserId, @Param("now") Date now);
}
