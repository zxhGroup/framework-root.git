package com.zxhtom.tcp.websocket.repository;

import com.zxhtom.tcp.websocket.model.ChatMessage;

import java.util.List;
/**
 * @author John
 */
public interface WebMessageRepository {
    void save(String chatRoomId, ChatMessage msg);

    List getList(String chatRoomId);
}
