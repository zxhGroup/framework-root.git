package com.zxhtom.tcp.websocket.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.Message;

public interface MessageRepository {

	/**
	 * 根据主键查询消息载体信息
	 * 
	 * @param messageId
	 *            消息载体主键
	 * @return
	 */
	PagedResult<Message> selectMessagesByPK(Long messageId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除消息载体信息
	 * @param messageId
	 * @return
	 */
	Integer deleteMessagesByPK(Long messageId);
	
	/**
	 * 单条更新消息载体信息
	 * @param messages
	 * @return
	 */
	Integer updateMessage(Message message);

	/**
	 * 单条新增信息
	 * @param messages
	 * @return
	 */
	Integer insertMessage(Message message);
	
	/**
	 * 批量更新消息载体信息
	 * @param messages
	 * @return
	 */
	Integer updateMessageBatch(List<Message> messages);

	/**
	 * 批量新增信息
	 * @param messages
	 * @return
	 */
	Integer insertMessageBatch(List<Message> messages);

	/**
	 * 获取历史数据
	 * @param fromUserId
	 * @param targetUserId
	 * @return
	 */
    List<Message> selectHistoryMessage(Long fromUserId, Long targetUserId);
}
