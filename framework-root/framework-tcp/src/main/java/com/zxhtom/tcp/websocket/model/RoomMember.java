package com.zxhtom.tcp.websocket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "房间群成员表")
@Table(name = "ROOM_MEMBER")
public class RoomMember {

	/**
	 * 房间ID
	 */
	@ApiModelProperty(value = "房间ID")
	@Id
	private Integer roomId;
	/**
	 * 群成员ID
	 */
	@ApiModelProperty(value = "群成员ID")
	private Integer memberId;
	/**
	 * 是否在线
	 */
	@ApiModelProperty(value = "是否在线")
	private Integer isOnline;
	
	/**
	 * 设置：房间ID
	 */
	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	/**
	 * 获取：房间ID
	 */
	public Integer getRoomId() {
		return roomId;
	}
	/**
	 * 设置：群成员ID
	 */
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：群成员ID
	 */
	public Integer getMemberId() {
		return memberId;
	}
	/**
	 * 设置：是否在线
	 */
	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}
	/**
	 * 获取：是否在线
	 */
	public Integer getIsOnline() {
		return isOnline;
	}

	@Override
	public String toString() {
		return "RoomMember [roomId=" + roomId+",memberId=" + memberId + ",isOnline=" + isOnline + "]";
	}

}
