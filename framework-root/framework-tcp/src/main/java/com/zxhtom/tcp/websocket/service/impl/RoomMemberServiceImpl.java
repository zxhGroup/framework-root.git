package com.zxhtom.tcp.websocket.service.impl;

import java.util.List;

import com.zxhtom.tcp.websocket.model.RoomMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.repository.RoomMemberRepository;
import com.zxhtom.tcp.websocket.service.RoomMemberService;

@Service
public class RoomMemberServiceImpl implements RoomMemberService {

	@Autowired
	private RoomMemberRepository roomMemberRepository;

	@Override
	public PagedResult<RoomMember> selectRoomMembersByPK(Long roomId, Integer pageNumber, Integer pageSize) {
		PagedResult<RoomMember> result = new PagedResult<>();
		if(0==roomId){
			//查询所有
			roomId=null;
		}
		result = roomMemberRepository.selectRoomMembersByPK(roomId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteRoomMemberPK(Long roomId) {
		if(0==roomId){
			//删除所有
			roomId=null;
		}
		return roomMemberRepository.deleteRoomMembersByPK(roomId);
	}

	@Override
	public Integer updateRoomMember(RoomMember roomMember) {
		return roomMemberRepository.updateRoomMember(roomMember);
	}

	@Override
	public Integer insertRoomMember(RoomMember roomMember) {
	roomMember.setRoomId(IdUtil.getInstance().getIntId());
		return roomMemberRepository.insertRoomMember(roomMember);
	}
	
	@Override
	public Integer deleteRoomMemberPKBatch(List<Long> roomIds){
		for(Long roomId : roomIds){
			roomMemberRepository.deleteRoomMembersByPK(roomId);
		}
		return 1;
	}
	
	@Override
	public Integer updateRoomMemberBatch(List<RoomMember> roomMembers) {
		return roomMemberRepository.updateRoomMemberBatch(roomMembers);
	}

	@Override
	public Integer insertRoomMemberBatch(List<RoomMember> roomMembers) {
		for(RoomMember roomMember : roomMembers){
		roomMember.setRoomId(IdUtil.getInstance().getIntId());
		}
		return roomMemberRepository.insertRoomMemberBatch(roomMembers);
	}

}
