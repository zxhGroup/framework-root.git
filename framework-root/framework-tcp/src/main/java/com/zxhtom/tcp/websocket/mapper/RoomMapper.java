package com.zxhtom.tcp.websocket.mapper;


import java.util.List;

import com.zxhtom.tcp.websocket.model.Room;

import tk.mybatis.mapper.common.Mapper;
public interface RoomMapper extends Mapper<Room>{

	/**
	 * 批量更新
	 * @param rooms
	 * @return
	 */
	Integer updateRoomBatch(List<Room> rooms);

	/**
	 * 批量新增
	 * @param rooms
	 * @return
	 */
	Integer insertRoomBatch(List<Room> rooms);
}
