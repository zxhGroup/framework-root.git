package com.zxhtom.tcp.websocket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "群聊表")
@Table(name = "ROOM")
public class Room {

	/**
	 * 房间ID
	 */
	@ApiModelProperty(value = "房间ID")
	@Id
	private Integer roomId;
	/**
	 * 房间名称
	 */
	@ApiModelProperty(value = "房间名称")
	private String roomName;
	/**
	 * 房间状态(0讨论组,1群聊)
	 */
	@ApiModelProperty(value = "房间状态(0讨论组,1群聊)")
	private Integer roomStatus;
	
	/**
	 * 设置：房间ID
	 */
	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	/**
	 * 获取：房间ID
	 */
	public Integer getRoomId() {
		return roomId;
	}
	/**
	 * 设置：房间名称
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	/**
	 * 获取：房间名称
	 */
	public String getRoomName() {
		return roomName;
	}
	/**
	 * 设置：房间状态(0讨论组,1群聊)
	 */
	public void setRoomStatus(Integer roomStatus) {
		this.roomStatus = roomStatus;
	}
	/**
	 * 获取：房间状态(0讨论组,1群聊)
	 */
	public Integer getRoomStatus() {
		return roomStatus;
	}

	@Override
	public String toString() {
		return "Room [roomId=" + roomId+",roomName=" + roomName + ",roomStatus=" + roomStatus + "]";
	}

}
