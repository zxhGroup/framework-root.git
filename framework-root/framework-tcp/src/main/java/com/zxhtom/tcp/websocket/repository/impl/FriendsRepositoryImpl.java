package com.zxhtom.tcp.websocket.repository.impl;

import java.util.List;

import com.zxhtom.tcp.websocket.mapper.FriendsMapper;
import com.zxhtom.tcp.websocket.model.Friends;
import com.zxhtom.tcp.websocket.model.GroupFriends;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.repository.FriendsRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class FriendsRepositoryImpl implements FriendsRepository{

	@Autowired
	private FriendsMapper friendsMapper;
	@Override
	public PagedResult<Friends> selectFriendssByPK(Long userId, Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(Friends.class,false,false);
		example.createCriteria().andEqualTo("userId", userId);
		return new PagedResult<>(friendsMapper.selectByExample(example));
	}
	@Override
	public Integer deleteFriendssByPK(Long userId) {
		Example example = new Example(Friends.class,false,false);
		example.createCriteria().andEqualTo("userId", userId);
		return friendsMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateFriends(Friends friends) {
		return friendsMapper.updateByPrimaryKeySelective(friends);
	}
	@Override
	public Integer insertFriends(Friends friends) {
		return friendsMapper.insert(friends);
	}
	
	@Override
	public Integer updateFriendsBatch(List<Friends> friendss) {
		return friendsMapper.updateFriendsBatch(friendss);
	}
	@Override
	public Integer insertFriendsBatch(List<Friends> friendss) {
		return friendsMapper.insertFriendsBatch(friendss);
	}

    @Override
    public List<GroupFriends> selectFriendsByCurrent(Long userId) {

		return friendsMapper.selectFriendsByCurrent(userId);
    }
}
