package com.zxhtom.tcp.websocket.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.Friends;
import com.zxhtom.tcp.websocket.model.GroupFriends;

public interface FriendsRepository {

	/**
	 * 根据主键查询好友表信息
	 * 
	 * @param userId
	 *            好友表主键
	 * @return
	 */
	PagedResult<Friends> selectFriendssByPK(Long userId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除好友表信息
	 * @param userId
	 * @return
	 */
	Integer deleteFriendssByPK(Long userId);
	
	/**
	 * 单条更新好友表信息
	 * @param friendss
	 * @return
	 */
	Integer updateFriends(Friends friends);

	/**
	 * 单条新增信息
	 * @param friendss
	 * @return
	 */
	Integer insertFriends(Friends friends);
	
	/**
	 * 批量更新好友表信息
	 * @param friendss
	 * @return
	 */
	Integer updateFriendsBatch(List<Friends> friendss);

	/**
	 * 批量新增信息
	 * @param friendss
	 * @return
	 */
	Integer insertFriendsBatch(List<Friends> friendss);

	/**
	 * 获取当前用户的好友列表
	 * @param userId
	 * @return
	 */
    List<GroupFriends> selectFriendsByCurrent(Long userId);
}
