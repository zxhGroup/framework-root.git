package com.zxhtom.tcp.websocket.controller.socket;

import com.zxhtom.CoreConstants;
import com.zxhtom.model.CustomUser;
import com.zxhtom.tcp.websocket.model.AricMessage;
import com.zxhtom.tcp.websocket.model.AricResponse;
import com.zxhtom.tcp.websocket.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * webSocket控制器
 */
@RestController
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    @Autowired
    private SimpUserRegistry userRegistry;
    @Autowired
    private MessageService messageService;
    /**
     * 广播模式
     * @param message
     * @return
     */
    @MessageMapping("/welcome") //当浏览器向服务端发送请求时,通过@MessageMapping映射/welcome这个地址,类似于@ResponseMapping
    @SendTo("/topic/getResponse")//当服务器有消息时,会对订阅了@SendTo中的路径的浏览器发送消息
    public AricResponse say(@Header("userId") String userId,AricMessage message) {
        try {
            //睡眠1秒
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new AricResponse(userId,null,"welcome," + message.getName() + "!");
    }

    /**
     * 多房间聊天室
     * @param chatRoomId
     * @param message
     * @return
     */
    @MessageMapping("/welcome/{chatRoomId}") //当浏览器向服务端发送请求时,通过@MessageMapping映射/welcome这个地址,类似于@ResponseMapping
    @SendTo("/topic/getResponse/{chatRoomId}")//当服务器有消息时,会对订阅了@SendTo中的路径的浏览器发送消息
    public AricResponse say(@Header("userId") String userId,@DestinationVariable("chatRoomId") String chatRoomId, AricMessage message) {

        try {
            //睡眠1秒
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new AricResponse(userId,null,"welcome," + message.getName()+chatRoomId + "!");
    }

    /*点对点通信*/
    @MessageMapping(value = "/sendToUser")
    public void templateTest1(@Payload  String message, @Header("userId") String userId,
                              @Headers Map<String, Object> headers) {
        int i = 1;
        for (SimpUser user : userRegistry.getUsers()) {
            System.out.println("用户" + i++ + "---" + user);
        }
        CustomUser userInfo = (CustomUser) ((Map)(headers.get("simpSessionAttributes"))).get(CoreConstants.USERINFO);
        String fromUserId = String.valueOf(userInfo.getUserId());
        //发送消息给指定用户
        messagingTemplate.convertAndSendToUser(userId, "/queue/message",new AricResponse(fromUserId,userId,message));
        if (!fromUserId.equals(userId)) {
            //给自己发送一条，消息同步
            messagingTemplate.convertAndSendToUser(fromUserId, "/queue/message",new AricResponse(fromUserId,userId,message));
        }
        //消息新增
        messageService.insertBackMessage(fromUserId,userId,message);
    }
}
