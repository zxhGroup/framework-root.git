package com.zxhtom.tcp.websocket.repository.impl;

import com.zxhtom.tcp.websocket.repository.ChatroomMemberRepository;
import org.springframework.stereotype.Repository;

/**
 * @author John
 */
@Repository
public class ChatroomMemberRepositoryImpl implements ChatroomMemberRepository {
    @Override
    public void remove(String chatRoomId, String username) {

    }

    @Override
    public Object getAll(String chatRoomId) {
        return null;
    }

    @Override
    public void save(String chatRoomId, String content) {

    }
}
