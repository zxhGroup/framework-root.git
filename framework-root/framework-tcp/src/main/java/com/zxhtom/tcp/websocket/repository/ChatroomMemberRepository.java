package com.zxhtom.tcp.websocket.repository;

/**
 * @author John
 */
public interface ChatroomMemberRepository {
    void remove(String chatRoomId, String username);

    Object getAll(String chatRoomId);

    void save(String chatRoomId, String content);
}