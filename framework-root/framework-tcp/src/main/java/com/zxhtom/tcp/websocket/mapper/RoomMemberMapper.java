package com.zxhtom.tcp.websocket.mapper;


import java.util.List;

import com.zxhtom.tcp.websocket.model.RoomMember;

import tk.mybatis.mapper.common.Mapper;
public interface RoomMemberMapper extends Mapper<RoomMember>{

	/**
	 * 批量更新
	 * @param roomMembers
	 * @return
	 */
	Integer updateRoomMemberBatch(List<RoomMember> roomMembers);

	/**
	 * 批量新增
	 * @param roomMembers
	 * @return
	 */
	Integer insertRoomMemberBatch(List<RoomMember> roomMembers);
}
