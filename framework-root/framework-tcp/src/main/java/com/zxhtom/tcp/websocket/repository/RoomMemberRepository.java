package com.zxhtom.tcp.websocket.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.RoomMember;

public interface RoomMemberRepository {

	/**
	 * 根据主键查询房间群成员信息
	 * 
	 * @param roomId
	 *            房间群成员主键
	 * @return
	 */
	PagedResult<RoomMember> selectRoomMembersByPK(Long roomId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除房间群成员信息
	 * @param roomId
	 * @return
	 */
	Integer deleteRoomMembersByPK(Long roomId);
	
	/**
	 * 单条更新房间群成员信息
	 * @param roomMembers
	 * @return
	 */
	Integer updateRoomMember(RoomMember roomMember);

	/**
	 * 单条新增信息
	 * @param roomMembers
	 * @return
	 */
	Integer insertRoomMember(RoomMember roomMember);
	
	/**
	 * 批量更新房间群成员信息
	 * @param roomMembers
	 * @return
	 */
	Integer updateRoomMemberBatch(List<RoomMember> roomMembers);

	/**
	 * 批量新增信息
	 * @param roomMembers
	 * @return
	 */
	Integer insertRoomMemberBatch(List<RoomMember> roomMembers);
}
