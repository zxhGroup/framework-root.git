package com.zxhtom.tcp.websocket.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.zxhtom.tcp.websocket.model.Message;
import com.zxhtom.tcp.websocket.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/tcp/message")
@Api(tags = "tcp message", description = "消息载体信息表管理")
public class MessageController {

	private Logger logger = LogManager.getLogger(MessageController.class);
	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/{messageId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有消息载体信息列表 传0表示查询所有")
	public PagedResult<Message> selectMessagesByPK(@ApiParam(value = "消息载体主键", required = true) @PathVariable @NotNull Long messageId,
                                                   @ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
                                                   @ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return messageService.selectMessagesByPK(messageId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteMessageByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long messageId) {
		return messageService.deleteMessagePK(messageId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) Message message) {
		return messageService.updateMessage(message);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody Message message) {
		return messageService.insertMessage(message);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteMessageByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> messageIds) {
		return messageService.deleteMessagePKBatch(messageIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<Message> messages) {
		return messageService.updateMessageBatch(messages);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<Message> messages) {
		return messageService.insertMessageBatch(messages);
	}

	@RequestMapping(value = "selectHistoryMessage",method = RequestMethod.GET)
	@ApiOperation(value = "获取聊天历史")
	public List<Message> selectHistoryMessage(@ApiParam(value = "发送者ID") @RequestParam Long fromUserId,@ApiParam(value = "接受者ID") @RequestParam Long targetUserId) {

		return messageService.selectHistoryMessage(fromUserId,targetUserId);
	}
}
