package com.zxhtom.tcp.websocket.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.zxhtom.tcp.websocket.enums.MessageStatus;
import com.zxhtom.tcp.websocket.model.Message;
import com.zxhtom.tcp.websocket.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.repository.MessageRepository;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageRepository messageRepository;

	@Override
	public PagedResult<Message> selectMessagesByPK(Long messageId, Integer pageNumber, Integer pageSize) {
		PagedResult<Message> result = new PagedResult<>();
		if(0==messageId){
			//查询所有
			messageId=null;
		}
		result = messageRepository.selectMessagesByPK(messageId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteMessagePK(Long messageId) {
		if(0==messageId){
			//删除所有
			messageId=null;
		}
		return messageRepository.deleteMessagesByPK(messageId);
	}

	@Override
	public Integer updateMessage(Message message) {
		return messageRepository.updateMessage(message);
	}

	@Override
	public Integer insertMessage(Message message) {
	message.setMessageId(IdUtil.getInstance().getId());
		return messageRepository.insertMessage(message);
	}
	
	@Override
	public Integer deleteMessagePKBatch(List<Long> messageIds){
		for(Long messageId : messageIds){
			messageRepository.deleteMessagesByPK(messageId);
		}
		return 1;
	}
	
	@Override
	public Integer updateMessageBatch(List<Message> messages) {
		return messageRepository.updateMessageBatch(messages);
	}

	@Override
	public Integer insertMessageBatch(List<Message> messages) {
		for(Message message : messages){
		message.setMessageId(IdUtil.getInstance().getId());
		}
		return messageRepository.insertMessageBatch(messages);
	}

    @Override
    public Integer insertBackMessage(String fromUserId, String userId, String message) {
		Message messageModel = new Message();
		messageModel.setContent(message);
		messageModel.setCreateTime(new Date());
		messageModel.setFromUserId(Integer.valueOf(fromUserId));
		messageModel.setTargetId(Integer.valueOf(userId));
		messageModel.setMessageId((IdUtil.getInstance().getId()));
		messageModel.setStatus(MessageStatus.FAILURE.getCode());
		messageModel.setType(0);
		return messageRepository.insertMessage(messageModel);
    }

    @Override
    public List<Message> selectHistoryMessage(Long fromUserId, Long targetUserId) {
		List<Message> messages = messageRepository.selectHistoryMessage(fromUserId, targetUserId);
		Collections.sort(messages, new Comparator<Message>() {
			@Override
			public int compare(Message o1, Message o2) {
				return (int) (o1.getCreateTime().getTime()-o2.getCreateTime().getTime());
			}
		});
		return messages;
    }

}
