package com.zxhtom.tcp.websocket.repository.impl;

import java.util.List;

import com.zxhtom.tcp.websocket.mapper.RoomMemberMapper;
import com.zxhtom.tcp.websocket.model.RoomMember;
import com.zxhtom.tcp.websocket.repository.RoomMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;

import tk.mybatis.mapper.entity.Example;

@Repository
public class RoomMemberRepositoryImpl implements RoomMemberRepository {

	@Autowired
	private RoomMemberMapper roomMemberMapper;
	@Override
	public PagedResult<RoomMember> selectRoomMembersByPK(Long roomId, Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(RoomMember.class,false,false);
		example.createCriteria().andEqualTo("roomId", roomId);
		return new PagedResult<>(roomMemberMapper.selectByExample(example));
	}
	@Override
	public Integer deleteRoomMembersByPK(Long roomId) {
		Example example = new Example(RoomMember.class,false,false);
		example.createCriteria().andEqualTo("roomId", roomId);
		return roomMemberMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateRoomMember(RoomMember roomMember) {
		return roomMemberMapper.updateByPrimaryKeySelective(roomMember);
	}
	@Override
	public Integer insertRoomMember(RoomMember roomMember) {
		return roomMemberMapper.insert(roomMember);
	}
	
	@Override
	public Integer updateRoomMemberBatch(List<RoomMember> roomMembers) {
		return roomMemberMapper.updateRoomMemberBatch(roomMembers);
	}
	@Override
	public Integer insertRoomMemberBatch(List<RoomMember> roomMembers) {
		return roomMemberMapper.insertRoomMemberBatch(roomMembers);
	}
}
