package com.zxhtom.tcp.websocket.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.Message;

public interface MessageService {

	/**
	 * 根据主键查询消息载体信息
	 * @param messageId 消息载体主键
	 * @return
	 */
	PagedResult<Message> selectMessagesByPK(Long messageId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除消息载体信息
	 * @param messageId 消息载体主键
	 * @return
	 */
	Integer deleteMessagePK(Long messageId);

	/**
	 * 单条更新数据
	 * @param Message
	 * @return
	 */
	Integer updateMessage(Message message);

	/**
	 * 单条新增数据
	 * @param message
	 * @return
	 */
	Integer insertMessage(Message message);
	
	/**
	 * 批量根据主键删除消息载体信息
	 * @param messageIds 消息载体主键
	 * @return
	 */
	Integer deleteMessagePKBatch(List<Long> messageIds);
	
	/**
	 * 批量更新数据
	 * @param messages
	 * @return
	 */
	Integer updateMessageBatch(List<Message> messages);

	/**
	 * 批量新增数据
	 * @param Messages
	 * @return
	 */
	Integer insertMessageBatch(List<Message> messages);

	/**
	 * insert
	 * @param fromUserId
	 * @param userId
	 * @param message
	 * @return
	 */
    Integer insertBackMessage(String fromUserId, String userId, String message);

	/**
	 * 获取两位聊天的历史记录
	 * @param fromUserId
	 * @param targetUserId
	 * @return
	 */
    List<Message> selectHistoryMessage(Long fromUserId, Long targetUserId);
}
