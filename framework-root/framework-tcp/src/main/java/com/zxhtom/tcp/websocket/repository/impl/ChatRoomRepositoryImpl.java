package com.zxhtom.tcp.websocket.repository.impl;

import com.zxhtom.tcp.websocket.repository.ChatRoomRepository;
import org.springframework.stereotype.Repository;

import javax.websocket.Session;

@Repository
public class ChatRoomRepositoryImpl implements ChatRoomRepository {
    @Override
    public String get(String chatRoomId, Session session) {
        return null;
    }

    @Override
    public void remove(String chatRoomId, Session session) {

    }

    @Override
    public Session[] getKeys(String chatRoomId) {
        return new Session[0];
    }

    @Override
    public void save(String chatRoomId, Session session, String content) {

    }
}
