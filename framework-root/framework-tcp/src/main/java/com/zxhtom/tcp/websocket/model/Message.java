package com.zxhtom.tcp.websocket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "消息载体表")
@Table(name = "MESSAGE")
public class Message {

	/**
	 * 消息流水号
	 */
	@ApiModelProperty(value = "消息流水号")
	@Id
	private Long messageId;
	/**
	 * 消息内容
	 */
	@ApiModelProperty(value = "消息内容")
	private String content;
	/**
	 * 是否撤回
	 */
	@ApiModelProperty(value = "是否撤回")
	private Integer status;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**
	 * 发信人
	 */
	@ApiModelProperty(value = "发信人")
	private Integer fromUserId;
	/**
	 * 接受者ID(可能是群聊ID可能是用户ID)
	 */
	@ApiModelProperty(value = "接受者ID(可能是群聊ID可能是用户ID)")
	private Integer targetId;
	/**
	 * 接受者类型(0群聊1用户)
	 */
	@ApiModelProperty(value = "接受者类型(0群聊1用户)")
	private Integer type;
	
	/**
	 * 设置：消息流水号
	 */
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	/**
	 * 获取：消息流水号
	 */
	public Long getMessageId() {
		return messageId;
	}
	/**
	 * 设置：消息内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：消息内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：是否撤回
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：是否撤回
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：发信人
	 */
	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}
	/**
	 * 获取：发信人
	 */
	public Integer getFromUserId() {
		return fromUserId;
	}
	/**
	 * 设置：接受者ID(可能是群聊ID可能是用户ID)
	 */
	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}
	/**
	 * 获取：接受者ID(可能是群聊ID可能是用户ID)
	 */
	public Integer getTargetId() {
		return targetId;
	}
	/**
	 * 设置：接受者类型(0群聊1用户)
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：接受者类型(0群聊1用户)
	 */
	public Integer getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Message [messageId=" + messageId+",content=" + content + ",status=" + status + ",createTime=" + createTime + ",fromUserId=" + fromUserId + ",targetId=" + targetId + ",type=" + type + "]";
	}

}
