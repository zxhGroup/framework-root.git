package com.zxhtom.tcp.websocket.controller.socket;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test")
public class TestController {

    @RequestMapping(value = "hello",method = RequestMethod.GET)
    public void test() {

    }
}
