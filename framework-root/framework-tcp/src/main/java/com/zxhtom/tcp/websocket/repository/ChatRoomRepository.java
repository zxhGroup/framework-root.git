package com.zxhtom.tcp.websocket.repository;

import javax.websocket.Session;

/**
 * @author John
 */
public interface ChatRoomRepository {
    String get(String chatRoomId, Session session);

    void remove(String chatRoomId, Session session);

    Session[] getKeys(String chatRoomId);

    void save(String chatRoomId, Session session, String content);
}
