package com.zxhtom.tcp.websocket.repository.impl;

import java.util.List;

import com.zxhtom.tcp.websocket.mapper.RoomMapper;
import com.zxhtom.tcp.websocket.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.repository.RoomRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class RoomRepositoryImpl implements RoomRepository{

	@Autowired
	private RoomMapper roomMapper;
	@Override
	public PagedResult<Room> selectRoomsByPK(Long roomId, Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(Room.class,false,false);
		example.createCriteria().andEqualTo("roomId", roomId);
		return new PagedResult<>(roomMapper.selectByExample(example));
	}
	@Override
	public Integer deleteRoomsByPK(Long roomId) {
		Example example = new Example(Room.class,false,false);
		example.createCriteria().andEqualTo("roomId", roomId);
		return roomMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateRoom(Room room) {
		return roomMapper.updateByPrimaryKeySelective(room);
	}
	@Override
	public Integer insertRoom(Room room) {
		return roomMapper.insert(room);
	}
	
	@Override
	public Integer updateRoomBatch(List<Room> rooms) {
		return roomMapper.updateRoomBatch(rooms);
	}
	@Override
	public Integer insertRoomBatch(List<Room> rooms) {
		return roomMapper.insertRoomBatch(rooms);
	}
}
