package com.zxhtom.tcp.websocket.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.zxhtom.tcp.websocket.model.Room;
import com.zxhtom.tcp.websocket.service.RoomService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/tcp/room")
@Api(tags = "tcp room", description = "群聊信息表管理")
public class RoomController {

	private Logger logger = LogManager.getLogger(RoomController.class);
	@Autowired
	private RoomService roomService;

	@RequestMapping(value = "/{roomId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有群聊信息列表 传0表示查询所有")
	public PagedResult<Room> selectRoomsByPK(@ApiParam(value = "群聊主键", required = true) @PathVariable @NotNull Long roomId,
											 @ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
											 @ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return roomService.selectRoomsByPK(roomId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteRoomByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long roomId) {
		return roomService.deleteRoomPK(roomId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) Room room) {
		return roomService.updateRoom(room);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody Room room) {
		return roomService.insertRoom(room);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteRoomByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> roomIds) {
		return roomService.deleteRoomPKBatch(roomIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<Room> rooms) {
		return roomService.updateRoomBatch(rooms);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<Room> rooms) {
		return roomService.insertRoomBatch(rooms);
	}
}
