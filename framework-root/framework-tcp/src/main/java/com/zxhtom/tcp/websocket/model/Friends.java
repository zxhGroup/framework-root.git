package com.zxhtom.tcp.websocket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "好友表表")
@Table(name = "FRIENDS")
public class Friends {

	/**
	 * 用户ID
	 */
	@ApiModelProperty(value = "用户ID")
	@Id
	private Integer userId;
	/**
	 * 好友ID
	 */
	@ApiModelProperty(value = "好友ID")
	private Integer friendId;
	/**
	 * 添加时间
	 */
	@ApiModelProperty(value = "添加时间")
	private Date createTime;
	/**
	 * 分组名称
	 */
	@ApiModelProperty(value = "分组名称")
	private String divideName;
	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remarks;
	
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * 设置：好友ID
	 */
	public void setFriendId(Integer friendId) {
		this.friendId = friendId;
	}
	/**
	 * 获取：好友ID
	 */
	public Integer getFriendId() {
		return friendId;
	}
	/**
	 * 设置：添加时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：添加时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：分组名称
	 */
	public void setDivideName(String divideName) {
		this.divideName = divideName;
	}
	/**
	 * 获取：分组名称
	 */
	public String getDivideName() {
		return divideName;
	}
	/**
	 * 设置：备注
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * 获取：备注
	 */
	public String getRemarks() {
		return remarks;
	}

	@Override
	public String toString() {
		return "Friends [userId=" + userId+",friendId=" + friendId + ",createTime=" + createTime + ",divideName=" + divideName + ",remarks=" + remarks + "]";
	}

}
