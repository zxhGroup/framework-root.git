package com.zxhtom.tcp.websocket.service.impl;

import java.util.List;

import com.zxhtom.CoreConstants;
import com.zxhtom.core.service.CustomService;
import com.zxhtom.model.CustomUser;
import com.zxhtom.tcp.websocket.model.Friends;
import com.zxhtom.tcp.websocket.model.GroupFriends;
import com.zxhtom.tcp.websocket.service.FriendsService;
import com.zxhtom.web.CurrentRequestService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.repository.FriendsRepository;

@Service
public class FriendsServiceImpl implements FriendsService {

	@Autowired
	private FriendsRepository friendsRepository;
	@Autowired
	private CustomService customService;
	@Override
	public PagedResult<Friends> selectFriendssByPK(Long userId, Integer pageNumber, Integer pageSize) {
		PagedResult<Friends> result = new PagedResult<>();
		if(0==userId){
			//查询所有
			userId=null;
		}
		result = friendsRepository.selectFriendssByPK(userId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteFriendsPK(Long userId) {
		if(0==userId){
			//删除所有
			userId=null;
		}
		return friendsRepository.deleteFriendssByPK(userId);
	}

	@Override
	public Integer updateFriends(Friends friends) {
		return friendsRepository.updateFriends(friends);
	}

	@Override
	public Integer insertFriends(Friends friends) {
	friends.setUserId(IdUtil.getInstance().getIntId());
		return friendsRepository.insertFriends(friends);
	}
	
	@Override
	public Integer deleteFriendsPKBatch(List<Long> userIds){
		for(Long userId : userIds){
			friendsRepository.deleteFriendssByPK(userId);
		}
		return 1;
	}
	
	@Override
	public Integer updateFriendsBatch(List<Friends> friendss) {
		return friendsRepository.updateFriendsBatch(friendss);
	}

	@Override
	public Integer insertFriendsBatch(List<Friends> friendss) {
		for(Friends friends : friendss){
		friends.setUserId(IdUtil.getInstance().getIntId());
		}
		return friendsRepository.insertFriendsBatch(friendss);
	}

    @Override
    public List<GroupFriends> selectFriendsByCurrent() {
		CustomUser userInfo = (CustomUser) SecurityUtils.getSubject().getPrincipal();
		return friendsRepository.selectFriendsByCurrent(userInfo.getUserId());
    }

}
