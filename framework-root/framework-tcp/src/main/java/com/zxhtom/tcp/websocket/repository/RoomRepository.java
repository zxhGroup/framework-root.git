package com.zxhtom.tcp.websocket.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.Room;

public interface RoomRepository {

	/**
	 * 根据主键查询群聊信息
	 * 
	 * @param roomId
	 *            群聊主键
	 * @return
	 */
	PagedResult<Room> selectRoomsByPK(Long roomId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除群聊信息
	 * @param roomId
	 * @return
	 */
	Integer deleteRoomsByPK(Long roomId);
	
	/**
	 * 单条更新群聊信息
	 * @param rooms
	 * @return
	 */
	Integer updateRoom(Room room);

	/**
	 * 单条新增信息
	 * @param rooms
	 * @return
	 */
	Integer insertRoom(Room room);
	
	/**
	 * 批量更新群聊信息
	 * @param rooms
	 * @return
	 */
	Integer updateRoomBatch(List<Room> rooms);

	/**
	 * 批量新增信息
	 * @param rooms
	 * @return
	 */
	Integer insertRoomBatch(List<Room> rooms);
}
