package com.zxhtom.tcp.websocket.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.RoomMember;

public interface RoomMemberService {

	/**
	 * 根据主键查询房间群成员信息
	 * @param roomId 房间群成员主键
	 * @return
	 */
	PagedResult<RoomMember> selectRoomMembersByPK(Long roomId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除房间群成员信息
	 * @param roomId 房间群成员主键
	 * @return
	 */
	Integer deleteRoomMemberPK(Long roomId);

	/**
	 * 单条更新数据
	 * @param RoomMember
	 * @return
	 */
	Integer updateRoomMember(RoomMember roomMember);

	/**
	 * 单条新增数据
	 * @param roomMember
	 * @return
	 */
	Integer insertRoomMember(RoomMember roomMember);
	
	/**
	 * 批量根据主键删除房间群成员信息
	 * @param roomIds 房间群成员主键
	 * @return
	 */
	Integer deleteRoomMemberPKBatch(List<Long> roomIds);
	
	/**
	 * 批量更新数据
	 * @param roomMembers
	 * @return
	 */
	Integer updateRoomMemberBatch(List<RoomMember> roomMembers);

	/**
	 * 批量新增数据
	 * @param RoomMembers
	 * @return
	 */
	Integer insertRoomMemberBatch(List<RoomMember> roomMembers);
}
