package com.zxhtom.tcp.websocket.model;

import java.util.Date;

/**
 * 服务端向浏览器发送的消息
 * @author John
 */
public class AricResponse {
    private String fromUserId;
    private String toUserId;
    private String content;
    private Date createTime;

    public AricResponse(String fromUserId ,String toUserId, String content) {
        this.fromUserId=fromUserId;
        this.toUserId=toUserId;
        this.content = content;
        this.createTime=new Date();
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
