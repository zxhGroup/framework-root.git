package com.zxhtom.tcp.websocket.controller.socket;

import com.alibaba.fastjson.JSONObject;
import com.zxhtom.tcp.websocket.enums.MessageType;
import com.zxhtom.tcp.websocket.model.ChatMessage;
import com.zxhtom.tcp.websocket.model.CommonMessageResponse;
import com.zxhtom.tcp.websocket.model.HistoryMessageResponse;
import com.zxhtom.tcp.websocket.model.SystemMessageResponse;
import com.zxhtom.tcp.websocket.repository.ChatRoomRepository;
import com.zxhtom.tcp.websocket.repository.ChatroomMemberRepository;
import com.zxhtom.tcp.websocket.repository.MessageRepository;
import com.zxhtom.tcp.websocket.repository.WebMessageRepository;
import com.zxhtom.utils.ContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ContextLoader;

import javax.annotation.PostConstruct;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 多聊天时版本
 */
@ServerEndpoint(value="/websocketServer/{chatRoomId}")
@RestController
public class RoomWebSocket {

    @Autowired
    private ChatRoomRepository chatRoomRepository ;
    @Autowired
    private WebMessageRepository messageRepository;
    @Autowired
    private ChatroomMemberRepository chatroomMemberRepository;

    @PostConstruct
    public void init(){
        System.out.println("init");
        //chatRoomRepository = ContextUtil.getApplicationContext().getBean(ChatRoomRepository.class);
        //messageRepository = ContextUtil.getApplicationContext().getBean(MessageRepository.class);
        //chatroomMemberRepository = ContextUtil.getApplicationContext().getBean(ChatroomMemberRepository.class);
    }
    public RoomWebSocket() {
    }

    @OnOpen
    public void onOpen(Session session) {
//        this.session = session;
    }

    @OnClose
    public void onClose(Session session, @PathParam("chatRoomId") String chatRoomId) {
        String username = chatRoomRepository.get(chatRoomId, session);
        chatroomMemberRepository.remove(chatRoomId, username);
        chatRoomRepository.remove(chatRoomId, session);

        for (Session sesion : chatRoomRepository.getKeys(chatRoomId)) {
            try {
                sendMsg(sesion, new SystemMessageResponse(MessageType.SYS_MSG, username, "exit", chatroomMemberRepository.getAll(chatRoomId)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnMessage
    public void onMessage(String message, Session session, @PathParam("chatRoomId") String chatRoomId) {
        JSONObject messageObject = JSONObject.parseObject(message);
        String type = messageObject.getString("messageType");
        String content = messageObject.getString("message");

        if (type.equals(MessageType.COM_MSG)) {
            //群发消息
            Date time = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            for (Session sesion : chatRoomRepository.getKeys(chatRoomId)) {
                try {
                    sendMsg(sesion, new CommonMessageResponse(MessageType.COM_MSG, sdf.format(time), chatRoomRepository.get(chatRoomId, session), content));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ChatMessage msg = new ChatMessage(sdf.format(time), chatRoomRepository.get(chatRoomId, session), content);
            messageRepository.save(chatRoomId, msg);
        } else if (type.equals(MessageType.SYS_MSG)) {
            chatRoomRepository.save(chatRoomId, session, content);
            chatroomMemberRepository.save(chatRoomId, content);

            for (Session sesion : chatRoomRepository.getKeys(chatRoomId)) {
                try {
                    sendMsg(sesion, new SystemMessageResponse(MessageType.SYS_MSG, content, "enter", chatroomMemberRepository.getAll(chatRoomId)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                List<ChatMessage> historyMessage = messageRepository.getList(chatRoomId);
                sendMsg(session, new HistoryMessageResponse(MessageType.HIS_MSG, historyMessage));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

    private void sendMsg(Session session, Object message) throws Exception {
        session.getBasicRemote().sendObject(message);
    }
}
