package com.zxhtom.tcp.websocket.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 分组结构好友
 */
public class GroupFriends {

    @ApiModelProperty(value = "分组名称")
    private String divideName;
    @ApiModelProperty(value = "好友列表")
    private List<Friends> friends;

    public String getDivideName() {
        return divideName;
    }

    public void setDivideName(String divideName) {
        this.divideName = divideName;
    }

    public List<Friends> getFriends() {
        return friends;
    }

    public void setFriends(List<Friends> friends) {
        this.friends = friends;
    }
}
