package com.zxhtom.tcp.websocket.model;

/**
 * 浏览器向服务端发送的消息
 */
public class AricMessage {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
