package com.zxhtom.tcp.websocket.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.tcp.websocket.model.Friends;
import com.zxhtom.tcp.websocket.model.GroupFriends;

public interface FriendsService {

	/**
	 * 根据主键查询好友表信息
	 * @param userId 好友表主键
	 * @return
	 */
	PagedResult<Friends> selectFriendssByPK(Long userId, Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除好友表信息
	 * @param userId 好友表主键
	 * @return
	 */
	Integer deleteFriendsPK(Long userId);

	/**
	 * 单条更新数据
	 * @param Friends
	 * @return
	 */
	Integer updateFriends(Friends friends);

	/**
	 * 单条新增数据
	 * @param friends
	 * @return
	 */
	Integer insertFriends(Friends friends);
	
	/**
	 * 批量根据主键删除好友表信息
	 * @param userIds 好友表主键
	 * @return
	 */
	Integer deleteFriendsPKBatch(List<Long> userIds);
	
	/**
	 * 批量更新数据
	 * @param friendss
	 * @return
	 */
	Integer updateFriendsBatch(List<Friends> friendss);

	/**
	 * 批量新增数据
	 * @param Friendss
	 * @return
	 */
	Integer insertFriendsBatch(List<Friends> friendss);

	/**
	 * 获取好友列表
	 * @return
	 */
    List<GroupFriends> selectFriendsByCurrent();
}
