var vm = new Vue({
    el:'#thread_monitor',
    data:function () {
        return {
            treechange:false,
            chartChange: false,
            stompClient:{},
            socket:{},
            monitorList:[],
            monitorName:'',
            interval_time:60000,
            data1:[],
            optionthread:[{
                name: "Total",
                type: 'line',
                key: 'total'
            },{
                name: "Waiting",
                type: 'line',
                key: 'waiting'
            },{
                name: "Runnable",
                type: 'line',
                key: 'runnable'
            },{
                name: "Sleeping",
                type: 'line',
                key: 'timedWaiting'
            }]
        }
    },
    methods:{
        selectedMonitor:function(opt){
            vm.monitorName=opt;
            vm.send();
        },
        getMonitor(){
            $.ajax({
                type: 'get',
                url: '/actuator/info/application',
                data:{'monitor':3},
                contentType: "application/json",
                success: (r)=>{
                    if(r.code === 0){
                        this.monitorList=r.data;
                        vm.treechange = true;
                    }else{
                        alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                                vm.loading = true
                            })},20);
                    }
                }
            });
        },
        send() {
            this.stompClient.send("/queue/thread", {'name': 'zxh'},vm.monitorName);
        },
        subscribe(){
            console.log("订阅开始");
            this.send();
            vm.stompClient.subscribe('/topic/thread', function (response) {
                console.log("订阅成功");
                vm.chartChange=true;
                vm.data1 = JSON.parse(response.body);
                setTimeout(function () {
                    vm.send();
                },vm.interval_time);
            });
        },
        connect(){
            // 建立连接对象（还未发起连接）
            this.socket = new SockJS(getHost()+"/endpointAric");

            // 获取 STOMP 子协议的客户端对象
            this.stompClient = Stomp.over(this.socket);

            // 向服务器发起websocket连接并发送CONNECT帧
            this.stompClient.connect(
                {
                    userId: getCurrentUser().userId, // 携带客户端信息
                    name: 'zz'
                },
                function connectCallback(frame) {
                    // 连接成功时（服务器响应 CONNECTED 帧）的回调方法
                    vm.subscribe();
                    console.log("连接成功");
                },
                function errorCallBack(error) {
                    // 连接失败时（服务器响应 ERROR 帧）的回调方法
                    console.log("连接失败"+error);
                    if (errorTimes < 10) {
                        errorTimes++;
                        setTimeout("connect();",8000);
                    }
                }
            );
        }
    },
    created: function () {
        this.getMonitor();
        this.connect();
    }
})