package com.zxhtom.actuator.controller;

import com.zxhtom.actuator.jvm.Jmap;
import com.zxhtom.actuator.jvm.Jstack;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/actuator/file")
@Api(description = "文件下载",tags = "actuator file")
public class FileController {
    private Logger logger = LogManager.getLogger(FileController.class);

    @RequestMapping(value = "/heap",method = RequestMethod.GET)
    @ApiOperation(value = "heap")
    public ResponseEntity<byte[]> heapDump() throws IOException {
 
        String dump = Jmap.dump();
        File file = new File(dump);
        logger.debug("DownLoad Dump:"+dump);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", file.getName());
        return new ResponseEntity<>(FileUtils.readFileToByteArray(file),headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/thread",method = RequestMethod.GET)
    @ApiOperation(value = "线程信息")
    public ResponseEntity<byte[]> threadDump() throws IOException {
        String dump = Jstack.dump();
        File file = new File(dump);
        logger.debug("DownLoad Dump:"+dump);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", file.getName());
        return new ResponseEntity<>(FileUtils.readFileToByteArray(file),headers, HttpStatus.CREATED);
    }

}
