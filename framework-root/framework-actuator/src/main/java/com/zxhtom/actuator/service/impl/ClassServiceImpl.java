package com.zxhtom.actuator.service.impl;

import com.zxhtom.actuator.model.ClassLoadEntity;
import com.zxhtom.actuator.repository.ClassLoadRepository;
import com.zxhtom.actuator.service.ClassService;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
@Service
public class ClassServiceImpl implements ClassService {
    @Autowired
    private ClassLoadRepository classLoadRepository;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
	public List<ClassLoadEntity> findAllByName(String name) {
        return 	classLoadRepository.findAllByName(name);
    }

    /**
     *  写入类加载信息
     * @param name
     * @param date
     * @param jstatClass
     */
    @Override
    public void save(ClassLoadEntity entity) {
        classLoadRepository.save(entity);
    }


    @Async
    @Override
    public void clearAll() {
        classLoadRepository.clearAll();
    }
}
