package com.zxhtom.actuator.service.schedule;

import com.zxhtom.actuator.service.CollectionSystemService;
import com.zxhtom.framework_task.quartz.AbstractJob;
import com.zxhtom.framework_task.quartz.Schedule;
import com.zxhtom.model.SysConfig;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

@DisallowConcurrentExecution
@Schedule(enable = true, taskName = "收集数据", cronExpression = "0 0/1 * * * ?", param = "", immediately = false)
public class CollectSchedule extends AbstractJob {
    @Autowired
    private SysConfig sysConfig;
    @Autowired
    private CollectionSystemService collectionSystemService;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        if (!sysConfig.getMonitor()) {
            return;
        }
        collectionSystemService.pullGc();
        collectionSystemService.pullClassload();
        collectionSystemService.pullThread();
    }
}
