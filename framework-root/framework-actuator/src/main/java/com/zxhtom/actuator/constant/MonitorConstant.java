package com.zxhtom.actuator.constant;

import java.util.HashMap;
import java.util.Map;

public class MonitorConstant {
    public static Map<Integer,String> tableMonitorMap = new HashMap<>();
    static {
        tableMonitorMap.put(1, "SYS_GC_TABLE");
        tableMonitorMap.put(2, "SYS_CLASS_TABLE");
        tableMonitorMap.put(3, "SYS_THREAD_TABLE");
    }
}
