package com.zxhtom.actuator.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Table;

@ApiModel(description = "class表")
@Table(name = "SYS_CLASS_TABLE")
public class ClassLoadEntity {
	/**
	 * 当前应用进程ID
	 */
	private Long id;
	/**
	 * 当前应用名称  配置文件 spring.application.name=xxxx
	 */
	private String name;
	/**
	 * 时间 格式 MM/dd HH:mm"
	 */
	private String date;
	/**
	 * 表示载入了类的数量
	 */
    private String loaded;
	/**
	 * 表示载入了类的合计
	 */
	private String bytes1;
	/**
	 * 表示卸载类的数量
	 */
	private String unloaded;
	/**
	 * 表示卸载类合计大小
	 */
    private String bytes2;
	/**
	 * 表示加载和卸载类总共的耗时
	 */
	private String time1;
	/**
	 * 表示编译任务执行的次数
	 */
	private String compiled;
	/**
	 * 表示编译失败的次数
	 */
    private String failed;
	/**
	 * 表示编译不可用的次数
	 */
	private String invalid;
	/**
	 * 表示编译的总耗时
	 */
    private String time2;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLoaded() {
		return loaded;
	}

	public void setLoaded(String loaded) {
		this.loaded = loaded;
	}

	public String getBytes1() {
		return bytes1;
	}

	public void setBytes1(String bytes1) {
		this.bytes1 = bytes1;
	}

	public String getUnloaded() {
		return unloaded;
	}

	public void setUnloaded(String unloaded) {
		this.unloaded = unloaded;
	}

	public String getBytes2() {
		return bytes2;
	}

	public void setBytes2(String bytes2) {
		this.bytes2 = bytes2;
	}

	public String getTime1() {
		return time1;
	}

	public void setTime1(String time1) {
		this.time1 = time1;
	}

	public String getCompiled() {
		return compiled;
	}

	public void setCompiled(String compiled) {
		this.compiled = compiled;
	}

	public String getFailed() {
		return failed;
	}

	public void setFailed(String failed) {
		this.failed = failed;
	}

	public String getInvalid() {
		return invalid;
	}

	public void setInvalid(String invalid) {
		this.invalid = invalid;
	}

	public String getTime2() {
		return time2;
	}

	public void setTime2(String time2) {
		this.time2 = time2;
	}
}
