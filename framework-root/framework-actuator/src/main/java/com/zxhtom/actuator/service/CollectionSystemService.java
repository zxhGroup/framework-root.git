package com.zxhtom.actuator.service;

/**
 * 收集系统信息
 * @author John
 */
public interface CollectionSystemService {
    /**
     * jvm gc和内存收信息收集
     */
    public void pullGc();


    /**
     * 线程加载信息收集
     */
    public void pullThread();

    /**
     * 类加载信息收集
     */
    public void pullClassload();


    /**
     * 每日的0:00:00时清空数据
     *
     */
    public void clearAll();
}
