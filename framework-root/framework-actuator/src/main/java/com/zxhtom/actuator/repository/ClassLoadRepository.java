package com.zxhtom.actuator.repository;

import com.zxhtom.actuator.model.ClassLoadEntity;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

public interface ClassLoadRepository {

    public List<ClassLoadEntity> findAllByName(String name);

    public void save(ClassLoadEntity entity);


    public void clearAll();
}
