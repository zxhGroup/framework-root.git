package com.zxhtom.actuator.repository;

import com.zxhtom.actuator.model.GcEntity;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.scheduling.annotation.Async;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

public interface GcRepository {
    public void save(GcEntity entity);

    public List<GcEntity> findAllByName(String name);

    public void clearAll();
}
