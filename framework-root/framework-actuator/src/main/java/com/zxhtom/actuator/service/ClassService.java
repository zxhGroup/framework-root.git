package com.zxhtom.actuator.service;

import com.zxhtom.actuator.model.ClassLoadEntity;

import java.util.List;


/**
 * @author John
 */
public interface ClassService {

	public List<ClassLoadEntity> findAllByName(String name);
    /**
     * 写入类加载信息
     * @param entity
     */
    public void save(ClassLoadEntity entity);


    public void clearAll();
}
