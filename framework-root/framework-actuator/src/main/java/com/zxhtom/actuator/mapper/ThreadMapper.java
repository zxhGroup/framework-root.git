package com.zxhtom.actuator.mapper;

import com.zxhtom.actuator.model.ThreadEntity;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author John
 */
public interface ThreadMapper extends Mapper<ThreadEntity> {
    Integer deleteAll();
}
