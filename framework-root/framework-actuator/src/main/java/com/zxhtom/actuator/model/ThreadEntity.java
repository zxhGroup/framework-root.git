package com.zxhtom.actuator.model;


import io.swagger.annotations.ApiModel;

import javax.persistence.Id;
import javax.persistence.Table;

@ApiModel(description = "线程表")
@Table(name = "SYS_THREAD_TABLE")
public class ThreadEntity {
	/**
	 * 当前应用进程ID
	 */
	@Id
    private Long id;
	/**
	 * 当前应用名称  配置文件 spring.application.name=xxxx
	 */
	private String name;
	/**
	 * 时间 格式 MM/dd HH:mm"
	 */
    private String date;
	/**
	 * 总线程数
	 */
	private int total;
	/**
	 * 运行中的线程
	 */
    private int runnable;
	/**
	 * 这个状态就是有限的(时间限制)的WAITING, 一般出现在调用wait(long), join(long)等情况下, 另外一个线程sleep后
	 * 休眠的线程数
	 */
	private int timedWaiting;
	/**
	 * 这个状态下是指线程拥有了某个锁之后, 调用了他的wait方法, 等待的线程数
	 */
    private int waiting;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRunnable() {
		return runnable;
	}

	public void setRunnable(int runnable) {
		this.runnable = runnable;
	}

	public int getTimedWaiting() {
		return timedWaiting;
	}

	public void setTimedWaiting(int timedWaiting) {
		this.timedWaiting = timedWaiting;
	}

	public int getWaiting() {
		return waiting;
	}

	public void setWaiting(int waiting) {
		this.waiting = waiting;
	}
}
