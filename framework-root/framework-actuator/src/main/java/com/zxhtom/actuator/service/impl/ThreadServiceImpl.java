package com.zxhtom.actuator.service.impl;

import com.zxhtom.actuator.mapper.ThreadMapper;
import com.zxhtom.actuator.model.ThreadEntity;
import com.zxhtom.actuator.repository.ThreadRepository;
import com.zxhtom.actuator.service.ThreadService;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ThreadServiceImpl implements ThreadService {
    @Autowired
    private ThreadRepository threadRepository;

    @Override
	public List<ThreadEntity> findAllByName(String name) {
        return threadRepository.findAllByName(name);
    }

    /**
     * 写入线程相关信息
     * @param entity
     */
    @Override
    public void save(ThreadEntity entity) {
        threadRepository.save(entity);
    }


    @Async
    @Override
    public void clearAll() {
        threadRepository.clearAll();
    }
}
