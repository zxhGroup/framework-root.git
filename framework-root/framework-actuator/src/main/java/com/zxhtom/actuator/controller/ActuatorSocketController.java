package com.zxhtom.actuator.controller;

import com.zxhtom.CoreConstants;
import com.zxhtom.actuator.model.ClassLoadEntity;
import com.zxhtom.actuator.model.GcEntity;
import com.zxhtom.actuator.model.ThreadEntity;
import com.zxhtom.actuator.service.ClassService;
import com.zxhtom.actuator.service.GcService;
import com.zxhtom.actuator.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * webSocket控制器
 * @author John
 */
@RestController
public class ActuatorSocketController {

    @Autowired
    private GcService gcService;
    @Autowired
    private ClassService classService;
    @Autowired
    private ThreadService threadService;
    /**
     * gc 内存信息
     * MessageMapping注解和我们之前使用的@RequestMapping类似  前端主动发送消息到后台时的地址
     * SendTo注解表示当服务器有消息需要推送的时候，会对订阅了@SendTo中路径的浏览器发送消息。
     * @return
     */
    @MessageMapping("/queue/gc")
    @SendTo("/topic/gc")
    public List<GcEntity> socketGc(String name){
        return gcService.findAllByName(name);
    }

    /**
     * 类加载相关信息
     * @param name
     * @return
     */
    @MessageMapping("/queue/cl")
    @SendTo("/topic/cl")
    public List<ClassLoadEntity> socketCl(String name){
        return classService.findAllByName(name);
    }

    /**
     * 线程相关信息
     * @param name
     * @return
     */
    @MessageMapping("/queue/thread")
    @SendTo("/topic/thread")
    public List<ThreadEntity> socketThread(String name){
        return threadService.findAllByName(name);
    }

}
