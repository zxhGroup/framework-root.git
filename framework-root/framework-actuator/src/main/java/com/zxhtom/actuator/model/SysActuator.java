package com.zxhtom.actuator.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Table;
import java.util.Date;

/**
 * @author John
 */
@ApiModel(description = "监控应用表")
@Table(name = "SYS_ACTUATOR")
public class SysActuator {
    /**
     * 流水号
     */
    private Integer id;
    /**
     * 应用名称
     */
    private String name;
    /**
     * 访问域名
     */
    private String url;
    /**
     * 日期
     */
    private Date date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
