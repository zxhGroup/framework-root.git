package com.zxhtom.actuator.controller;

import com.alibaba.fastjson.JSONObject;
import com.zxhtom.actuator.jvm.Jstack;
import com.zxhtom.actuator.jvm.Jstat;
import com.zxhtom.actuator.jvm.Server;
import com.zxhtom.actuator.model.*;
import com.zxhtom.actuator.service.SysActuatorService;
import com.zxhtom.actuator.util.ExecuteCmd;
import com.zxhtom.actuator.util.Res;
import com.zxhtom.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *  jvm 性能监控
 * @author zxh
 * @email 870775401@qq.com
 */
@RestController
@RequestMapping("/actuator/info")
@Api(tags = "actuator info", description = "监控系统信息")
public class SysztemController {
    @Autowired
    private SysActuatorService sysActuatorService;
    private Logger logger = LogManager.getLogger(SysztemController.class);
    @Value("${spring.application.name}")
    private String name="123";

    @RequestMapping(value = "/application",method = RequestMethod.GET)
    @ApiOperation(value = "监控服务列表")
    public List<String> application(@ApiParam(value = "服务监控区域(1-gc;2-cl;3-thrad)") @RequestParam Integer monitor){
        return sysActuatorService.selectApplicationByMonitor(monitor);
    }

    /**
     * 线程信息
     * @return
     */
    @RequestMapping(value = "/thread",method = RequestMethod.GET)
    @ApiOperation(value = "线程信息")
    public ThreadEntity thread(){
        String date = time();
        try {
            JstackEntity info = Jstack.jstack();
            ThreadEntity entity = new ThreadEntity();
            entity.setId(Long.valueOf(info.getId()));
            entity.setName(name);
            entity.setDate(date);
            entity.setTotal(info.getTotal());
            entity.setRunnable(info.getRUNNABLE());
            entity.setTimedWaiting(info.getTIMED_WAITING());
            entity.setWaiting(info.getWAITING());
            return entity;
        }catch (Exception e){
            e.printStackTrace();
            return new ThreadEntity();
        }
    }


    /**
     * 类加载信息
     * @ return
     */
    @RequestMapping(value = "/classload",method = RequestMethod.GET)
    @ApiOperation(value = "类加载信息")
    public ClassLoadEntity classload(){
        String date = time();
        try {
            List<KVEntity> jstatClass = Jstat.jstatClass();
            ClassLoadEntity entity = new ClassLoadEntity();
            entity.setId(Long.valueOf(getPid()));
            entity.setName(name);
            entity.setDate(date);
            entity.setLoaded(jstatClass.get(0).getValue());
            entity.setBytes1(jstatClass.get(1).getValue());
            entity.setUnloaded(jstatClass.get(2).getValue());
            entity.setBytes2(jstatClass.get(3).getValue());
            entity.setTime1(jstatClass.get(4).getValue());
            entity.setCompiled(jstatClass.get(5).getValue());
            entity.setFailed(jstatClass.get(6).getValue());
            entity.setInvalid(jstatClass.get(7).getValue());
            entity.setTime2(jstatClass.get(8).getValue());
            return entity;
        }catch (Exception e){
            e.printStackTrace();
            return new ClassLoadEntity();
        }
    }



    /**
     * gc信息  堆内存信息
     * @ return
     */
    @RequestMapping(value = "/gc",method = RequestMethod.GET)
    @ApiOperation(value = "堆内存信息")
    public GcEntity gc(){
        String date = time();
        try {
            List<KVEntity> kvEntities = Jstat.jstatGc();
            GcEntity entity = new GcEntity();
            entity.setId(Long.valueOf(getPid()));
            entity.setName(name);
            entity.setDate(date);
            entity.setS0c(kvEntities.get(0).getValue());
            entity.setS1c(kvEntities.get(1).getValue());
            entity.setS0u(kvEntities.get(2).getValue());
            entity.setS1u(kvEntities.get(3).getValue());
            entity.setEc(kvEntities.get(4).getValue());
            entity.setEu(kvEntities.get(5).getValue());
            entity.setOc(kvEntities.get(6).getValue());
            entity.setOu(kvEntities.get(7).getValue());
            entity.setMc(kvEntities.get(8).getValue());
            entity.setMu(kvEntities.get(9).getValue());
            entity.setCcsc(kvEntities.get(10).getValue());
            entity.setCcsu(kvEntities.get(11).getValue());
            entity.setYgc(kvEntities.get(12).getValue());
            entity.setYgct(kvEntities.get(13).getValue());
            entity.setFgc(kvEntities.get(14).getValue());
            entity.setFgct(kvEntities.get(15).getValue());
            entity.setGct(kvEntities.get(16).getValue());
            return entity;
        }catch (Exception e){
            e.printStackTrace();
            return new GcEntity();
        }
    }


    /**
     * 执行jvm linux命令
     * @return
     */
    @RequestMapping(value = "/runexec",method = RequestMethod.POST)
    @ApiOperation(value = "jvm linux命令")
    public String getRuntimeExec(@ApiParam(value = "参数信息") @RequestBody Map<String, String> params){
        try{
            ArrayList<String> list=new ArrayList<>();
            for(String s:params.keySet()){
                list.add(params.get(s));
            }
            String[] array = (String[])list.toArray(new String[list.size()]);
            logger.debug("runexec:{}", Arrays.toString(array));
            String result = ExecuteCmd.execute(array);
            return result;
        }catch (Exception e){
            logger.debug("RuntimeExec异常：{}",e);
            throw new BusinessException("RuntimeExec异常");
        }
    }

    /**
     * jvm 内存分配信息
     * @return
     */
    @RequestMapping(value = "/memoryInfo",method = RequestMethod.GET)
    @ApiOperation(value = "内存分配信息")
    public JSONObject memoryInfo() {
        //最大内存 M
        long maxMemory=Runtime.getRuntime().maxMemory()/1024/1024;
        //已分配内存
        long totalMemory=Runtime.getRuntime().totalMemory()/1024/1024;
        //已分配内存中的剩余空间
        long freeMemory= Runtime.getRuntime().freeMemory()/1024/1024;
        //最大可用内存
        long availableMemory=(Runtime.getRuntime().maxMemory()-Runtime.getRuntime().totalMemory()+Runtime.getRuntime().freeMemory())/1024/1024;
        //判断JDK版本
        String version=System.getProperty("java.version");
        //判断是32位还是64位
        String type=System.getProperty("sun.arch.data.model");

        JSONObject info=new JSONObject();
        info.put("maxMemory",maxMemory);
        info.put("totalMemory",totalMemory);
        info.put("freeMemory",freeMemory);
        info.put("availableMemory",availableMemory);
        info.put("java.version",version);
        info.put("sun.arch.data.model",type);
        return info;
    }
    
    
    
    
    /**
     * 系统物理内存 
     * @return
     */
    @RequestMapping(value = "/systemInfo",method = RequestMethod.GET)
    @ApiOperation(value = "系统物理内存")
    public Server systemInfo() {
		try {
			 Server server = new Server();
	         server.copyTo();
			return server;
		} catch (Exception e) {
			 logger.debug("systemInfo异常：{}",e);
	         return new Server();
		}
    }
    
    /**
     * 现在时间
     * @return
     */
    public static String time(){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd HH:mm:ss");
        return format.format(new Date());
    }

    /**
     * 获取当前应用进程id
     * @return
     */
    public static String  getPid(){
        String name = ManagementFactory.getRuntimeMXBean().getName();
        String pid = name.split("@")[0];
        return  pid;
    }


}
