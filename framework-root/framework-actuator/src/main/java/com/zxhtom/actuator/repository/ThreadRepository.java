package com.zxhtom.actuator.repository;

import com.zxhtom.actuator.model.ThreadEntity;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

public interface ThreadRepository {
    public List<ThreadEntity> findAllByName(String name);
    public void save(ThreadEntity entity);
    public void clearAll();
}
