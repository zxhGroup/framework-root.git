package com.zxhtom.actuator.service;

import com.zxhtom.actuator.model.GcEntity;

import java.util.List;
/**
 * @author John
 */
public interface GcService {

    /**
     * 写入gc信息
     * @param entity
     */
    public void save( GcEntity entity);

	public List<GcEntity> findAllByName(String name);
    
    public void clearAll();
}
