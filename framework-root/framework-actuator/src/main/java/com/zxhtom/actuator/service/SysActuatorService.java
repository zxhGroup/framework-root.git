package com.zxhtom.actuator.service;

import java.util.List;

public interface SysActuatorService {
    /**
     * 监控服务列表
     * @param monitor
     * @return
     */
    List<String> selectApplicationByMonitor(Integer monitor);
}

