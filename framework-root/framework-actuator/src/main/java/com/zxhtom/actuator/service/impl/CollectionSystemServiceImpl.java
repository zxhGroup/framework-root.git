package com.zxhtom.actuator.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zxhtom.actuator.mapper.SysActuatorMapper;
import com.zxhtom.actuator.model.ClassLoadEntity;
import com.zxhtom.actuator.model.GcEntity;
import com.zxhtom.actuator.model.SysActuator;
import com.zxhtom.actuator.model.ThreadEntity;
import com.zxhtom.actuator.service.ClassService;
import com.zxhtom.actuator.service.CollectionSystemService;
import com.zxhtom.actuator.service.GcService;
import com.zxhtom.actuator.service.ThreadService;
import com.zxhtom.actuator.util.ByteConvKbUtils;
import com.zxhtom.utils.ServiceInfoUtil;
import com.zxhtom.web.CurrentRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.zxhtom.utils.HttpClientUtil;

import java.util.Date;
import java.util.List;

/**
 * 收集系统信息
 * @author John
 */
@Service
public class CollectionSystemServiceImpl implements CollectionSystemService {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    @Autowired
    private CurrentRequestService currentRequestService;
    @Autowired
    private GcService gcService;
    @Autowired
    private ClassService classService;
    @Autowired
    private ThreadService threadService;
    @Autowired
    private SysActuatorMapper sysActuatorMapper;
    @Value("${server.port:8080}")
    private String port;
    /**
     * jvm gc和内存收信息收集
     */
    @Override
    public void pullGc(){
        List<SysActuator> list=sysActuatorMapper.selectAll();
        list.add(getLocalActuator());
        list.forEach(i->{
            String url=i.getUrl();
            JSONObject info=JSONObject.parseObject(HttpClientUtil.getInstance().httpGet(url+"/actuator/info/gc"));
            if(info.getIntValue("code")==0){
                GcEntity gc = info.getObject("data", GcEntity.class);
                gc.setS0c(ByteConvKbUtils.getMB(gc.getS0c()));
                gc.setS1c(ByteConvKbUtils.getMB(gc.getS1c()));
                gc.setS0u(ByteConvKbUtils.getMB(gc.getS0u()));
                gc.setS1u(ByteConvKbUtils.getMB(gc.getS1u()));

                gc.setEc(ByteConvKbUtils.getMB(gc.getEc()));
                gc.setEu(ByteConvKbUtils.getMB(gc.getEu()));
                gc.setOc(ByteConvKbUtils.getMB(gc.getOc()));
                gc.setOu(ByteConvKbUtils.getMB(gc.getOu()));
                gc.setMc(ByteConvKbUtils.getMB(gc.getMc()));
                gc.setMu(ByteConvKbUtils.getMB(gc.getMu()));
                gc.setCcsu(ByteConvKbUtils.getMB(gc.getCcsu()));
                gc.setCcsc(ByteConvKbUtils.getMB(gc.getCcsc()));
                gcService.save(gc);
            }
        });

    }

    /**
     * 添加本地服务
     * @return
     */
    private SysActuator getLocalActuator() {
        String localIp = currentRequestService.getLocalIp();
        SysActuator actuator = new SysActuator();
        actuator.setId(-1);
        actuator.setName("localhost");
        actuator.setUrl("http://"+localIp+":"+ port);
        actuator.setDate(new Date());
        return actuator;
    }


    /**
     * 线程加载信息收集
     */
    @Override
    public void pullThread(){
        List<SysActuator> list=sysActuatorMapper.selectAll();
        list.add(getLocalActuator());
        list.forEach(i->{
            String url=i.getUrl();
            JSONObject info=JSONObject.parseObject(HttpClientUtil.getInstance().httpGet(url+"/actuator/info/thread"));
            if(info.getIntValue("code")==0){
                ThreadEntity entity = info.getObject("data", ThreadEntity.class);
                threadService.save(entity);
            }
        });

    }

    /**
     * 类加载信息收集
     */
    @Override
    public void pullClassload(){
        List<SysActuator> list=sysActuatorMapper.selectAll();
        list.add(getLocalActuator());
        list.forEach(i->{
            String url=i.getUrl();
            JSONObject info=JSONObject.parseObject(HttpClientUtil.getInstance().httpGet(url+"/actuator/info/classload"));
            if(info.getIntValue("code")==0){
                ClassLoadEntity entity = info.getObject("data", ClassLoadEntity.class);
                entity.setBytes1(ByteConvKbUtils.getMB(entity.getBytes1()));
                entity.setBytes2(ByteConvKbUtils.getMB(entity.getBytes2()));
                classService.save(entity);
            }
        });
    }


    /**
     * 每日的0:00:00时清空数据
     *
     */
    @Override
    public void clearAll(){
        gcService.clearAll();
        threadService.clearAll();
        classService.clearAll();
    }
}
