package com.zxhtom.actuator.repository.impl;

import com.zxhtom.actuator.mapper.ClassLoadMapper;
import com.zxhtom.actuator.model.ClassLoadEntity;
import com.zxhtom.actuator.repository.ClassLoadRepository;
import com.zxhtom.actuator.service.ClassService;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Repository
public class ClassLoadRepositoryImpl implements ClassLoadRepository {
    @Autowired
    private ClassLoadMapper classLoadMapper;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
	public List<ClassLoadEntity> findAllByName(String name) {
        Example example = new Example(ClassLoadEntity.class, false, false);
        example.createCriteria().andEqualTo("name", name);
        return 	classLoadMapper.selectByExample(example);
    }

    /**
     *  写入类加载信息
     * @param name
     * @param date
     * @param jstatClass
     */
    @Override
    public void save(ClassLoadEntity entity) {
    	entity.setDate(JvmUtils.time());
    	entity.setId(IdUtil.getInstance().getId());
    	classLoadMapper.insert(entity);
    }


    @Async
    @Override
    public void clearAll() {
    	classLoadMapper.deleteAll();
    }
}
