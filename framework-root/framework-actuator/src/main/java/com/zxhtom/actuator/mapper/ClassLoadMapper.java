package com.zxhtom.actuator.mapper;

import com.zxhtom.actuator.model.ClassLoadEntity;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface ClassLoadMapper extends Mapper<ClassLoadEntity> {
    /**
     * 删除所有
     * @return
     */
    Integer deleteAll();
}
