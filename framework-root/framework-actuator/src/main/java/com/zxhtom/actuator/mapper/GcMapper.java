package com.zxhtom.actuator.mapper;

import com.zxhtom.actuator.model.GcEntity;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author John
 */
public interface GcMapper extends Mapper<GcEntity> {
    Integer deleteAll();
}
