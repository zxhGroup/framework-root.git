package com.zxhtom.actuator.service.impl;

import com.zxhtom.actuator.model.GcEntity;
import com.zxhtom.actuator.repository.GcRepository;
import com.zxhtom.actuator.service.GcService;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class GcServiceImpl implements GcService {
    @Autowired
    private GcRepository gcRepository;

    /**
     * 写入gc信息
     * @param entity
     */
    @Override
    public void save(GcEntity entity) {
    	gcRepository.save(entity);
    }

    @Override
	public List<GcEntity> findAllByName(String name) {
        return gcRepository.findAllByName(name);
    }
    
    @Async
    @Override
    public void clearAll() {
    	gcRepository.clearAll();
    }
}
