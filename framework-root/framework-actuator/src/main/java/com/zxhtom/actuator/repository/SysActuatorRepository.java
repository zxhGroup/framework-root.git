package com.zxhtom.actuator.repository;

import java.util.List;

public interface SysActuatorRepository {
    /**
     * 获取服务列表名
     * @param s
     * @return
     */
    List<String> selectApplicationByMonitor(String s);
}
