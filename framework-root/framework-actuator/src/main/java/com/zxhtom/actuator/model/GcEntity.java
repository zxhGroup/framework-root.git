package com.zxhtom.actuator.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.Table;

@ApiModel(description = "gc表")
@Table(name = "SYS_GC_TABLE")
public class GcEntity {
	/**
	 * 当前应用进程ID
	 */
	private Long id;
	/**
	 * 当前应用名称  配置文件 spring.application.name=xxxx
	 */
	private String name;
	/**
	 * 时间 格式 MM/dd HH:mm"
	 */
	private String date;

	/**
	 *  Survivor0空间的大小。单位KB。
	 */
    private String s0c;
	/**
	 * Survivor1空间的大小。单位KB。
	 */
	private String s1c;
	/**
	 * Survivor0已用空间的大小。单位KB
	 */
    private String s0u;
	/**
	 * Survivor1已用空间的大小。单位KB。
	 */
	private String s1u;
	/**
	 * Eden空间的大小。单位KB。
	 */
    private String ec;
	/**
	 * Eden已用空间的大小。单位KB。
	 */
	private String eu;
	/**
	 * 老年代空间的大小。单位KB。
	 */
    private String oc;
	/**
	 * 老年代已用空间的大小。单位KB。
	 */
	private String ou;
	/**
	 * 元空间的大小（Metaspace）
	 */
    private String mc;
	/**
	 * 元空间已使用大小（KB）
	 */
	private String mu;
	/**
	 * 压缩类空间大小（compressed class space）
	 */
    private String ccsc;
	/**
	 * 压缩类空间已使用大小（KB）
	 */
	private String ccsu;
	/**
	 * 新生代gc次数
	 */
    private String ygc;
	/**
	 * 新生代gc耗时（秒）
	 */
	private String ygct;
	/**
	 * Full gc次数
	 */
    private String fgc;
	/**
	 * Full gc耗时（秒）
	 */
	private String fgct;
	/**
	 * gc总耗时（秒）
	 */
	private String gct;

	public String getGct() {
		return gct;
	}

	public void setGct(String gct) {
		this.gct = gct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getS0c() {
		return s0c;
	}

	public void setS0c(String s0c) {
		this.s0c = s0c;
	}

	public String getS1c() {
		return s1c;
	}

	public void setS1c(String s1c) {
		this.s1c = s1c;
	}

	public String getS0u() {
		return s0u;
	}

	public void setS0u(String s0u) {
		this.s0u = s0u;
	}

	public String getS1u() {
		return s1u;
	}

	public void setS1u(String s1u) {
		this.s1u = s1u;
	}

	public String getEc() {
		return ec;
	}

	public void setEc(String ec) {
		this.ec = ec;
	}

	public String getEu() {
		return eu;
	}

	public void setEu(String eu) {
		this.eu = eu;
	}

	public String getOc() {
		return oc;
	}

	public void setOc(String oc) {
		this.oc = oc;
	}

	public String getOu() {
		return ou;
	}

	public void setOu(String ou) {
		this.ou = ou;
	}

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public String getMu() {
		return mu;
	}

	public void setMu(String mu) {
		this.mu = mu;
	}

	public String getCcsc() {
		return ccsc;
	}

	public void setCcsc(String ccsc) {
		this.ccsc = ccsc;
	}

	public String getCcsu() {
		return ccsu;
	}

	public void setCcsu(String ccsu) {
		this.ccsu = ccsu;
	}

	public String getYgc() {
		return ygc;
	}

	public void setYgc(String ygc) {
		this.ygc = ygc;
	}

	public String getYgct() {
		return ygct;
	}

	public void setYgct(String ygct) {
		this.ygct = ygct;
	}

	public String getFgc() {
		return fgc;
	}

	public void setFgc(String fgc) {
		this.fgc = fgc;
	}

	public String getFgct() {
		return fgct;
	}

	public void setFgct(String fgct) {
		this.fgct = fgct;
	}
}
