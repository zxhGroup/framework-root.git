package com.zxhtom.actuator;

import com.zxhtom.config.*;
import com.zxhtom.config.webservice.WebserviceConfig;
import com.zxhtom.config.websocket.WebSocketConfig;
import com.zxhtom.defcontroller.IndexController;
import com.zxhtom.framework_task.conf.TaskConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * 入口类, 扫描并注入其他配置类和服务
 * @author John
 */
@SpringBootApplication
@Import({QuickStartConfig.class, WebSocketConfig.class, IndexController.class,WebMvcConfig.class,CoreConfig.class,MybatisConfig.class, TaskConfig.class, WebserviceConfig.class,SpringfoxConfig.class})
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }


}
