package com.zxhtom.actuator.mapper;

import com.zxhtom.actuator.model.SysActuator;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 监控列表mapper
 * @author John
 */
public interface SysActuatorMapper extends Mapper<SysActuator> {
    /**
     * 获取监控服务列表
     * @param monitor
     * @return
     */
    List<String> selectApplicationByMonitor(@Param("monitor") String monitor);
}
