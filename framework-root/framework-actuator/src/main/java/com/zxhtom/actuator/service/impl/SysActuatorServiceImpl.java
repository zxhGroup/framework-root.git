package com.zxhtom.actuator.service.impl;

import com.zxhtom.actuator.constant.MonitorConstant;
import com.zxhtom.actuator.repository.SysActuatorRepository;
import com.zxhtom.actuator.service.SysActuatorService;
import com.zxhtom.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SysActuatorServiceImpl implements SysActuatorService {

    @Autowired
    private SysActuatorRepository sysActuatorRepository;
    @Override
    public List<String> selectApplicationByMonitor(Integer monitor) {
        if (!MonitorConstant.tableMonitorMap.containsKey(monitor)) {
            throw new BusinessException(monitor+"号，暂未开发");
        }
        return sysActuatorRepository.selectApplicationByMonitor(MonitorConstant.tableMonitorMap.get(monitor));
    }
}
