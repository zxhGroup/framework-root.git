package com.zxhtom.actuator.service;

import com.zxhtom.actuator.model.ThreadEntity;

import java.util.List;

/**
 * @author John
 */
public interface ThreadService {
	public List<ThreadEntity> findAllByName(String name);
    /**
     * 写入线程相关信息
     * @param entity
     */
    public void save(ThreadEntity entity);

    public void clearAll();
}
