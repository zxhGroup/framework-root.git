package com.zxhtom.actuator.repository.impl;

import com.zxhtom.actuator.mapper.ThreadMapper;
import com.zxhtom.actuator.model.ThreadEntity;
import com.zxhtom.actuator.repository.ThreadRepository;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;


@Repository
public class ThreadRepositoryImpl implements ThreadRepository {
    @Autowired
    private ThreadMapper threadMapper;

    @Override
	public List<ThreadEntity> findAllByName(String name) {
        Example example = new Example(ThreadEntity.class, false, false);
        example.createCriteria().andEqualTo("name", name);
        return threadMapper.selectByExample(example);
    }

    /**
     * 写入线程相关信息
     * @param entity
     */
    @Override
    public void save(ThreadEntity entity) {
    	entity.setDate(JvmUtils.time());
    	entity.setId(IdUtil.getInstance().getId());
    	threadMapper.insert(entity);
    }


    @Async
    @Override
    public void clearAll() {
    	threadMapper.deleteAll();
    }
}
