package com.zxhtom.actuator.repository.impl;

import com.zxhtom.actuator.mapper.SysActuatorMapper;
import com.zxhtom.actuator.repository.SysActuatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SysActuatorRepositoryImpl implements SysActuatorRepository {
    @Autowired
    private SysActuatorMapper sysActuatorMapper;
    @Override
    public List<String> selectApplicationByMonitor(String monitor) {
        return sysActuatorMapper.selectApplicationByMonitor(monitor);
    }
}
