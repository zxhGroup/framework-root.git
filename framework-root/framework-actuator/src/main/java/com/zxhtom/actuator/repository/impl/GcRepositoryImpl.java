package com.zxhtom.actuator.repository.impl;

import com.zxhtom.actuator.mapper.GcMapper;
import com.zxhtom.actuator.model.GcEntity;
import com.zxhtom.actuator.repository.GcRepository;
import com.zxhtom.actuator.util.JvmUtils;
import com.zxhtom.utils.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;

import java.util.List;


@Repository
public class GcRepositoryImpl implements GcRepository {
    @Autowired
    private GcMapper gcMapper;

    /**
     * 写入gc信息
     * @param entity
     */
    @Override
    public void save(GcEntity entity) {
    	entity.setDate(JvmUtils.time());
    	entity.setId(IdUtil.getInstance().getId());
    	gcMapper.insert(entity);
    }

    @Override
	public List<GcEntity> findAllByName(String name) {
        Example example = new Example(GcEntity.class, false, false);
        example.createCriteria().andEqualTo("name", name);
        return gcMapper.selectByExample(example);
    }
    
    @Async
    @Override
    public void clearAll() {
    	gcMapper.deleteAll();
    }
}
